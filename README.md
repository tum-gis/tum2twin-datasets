# tum2twin-datasets

This repository contains the 3D model datasets of the [TUM2TWIN team](https://tum2t.win).
The model datasets are maintained in this repository, since this GitLab instance does not impose restrictive bandwidth quotas on the use of [Git LFS](https://git-lfs.com/).
To clone it, just run:

```bash copy
git clone --depth 1 git@gitlab.lrz.de:tum-gis/tum2twin-datasets.git
```
