# Road Network Datasets

These models were derived from the OpenDRIVE dataset using the tool [r:trån](https://rtron.io).
Before conversion, the PROJ string of the header was adapted by removing `+units=m +vunits=m +no_defs` and the object with name `gml_tum_cut v2` is removed from the OpenDRIVE dataset.

Afterward, the conversion to CityGML 3.0 is ran with the following command:

```bash
java -jar rtron.jar opendrive-to-citygml 
    --plan-view-geometry-distance-tolerance 10.0
    --plan-view-geometry-angle-tolerance 10.0
    --reproject-model
    --crs-epsg 25832
    --add-offset 0.0 0.0 514.92
    ./opendrive
    ../citygml
```
