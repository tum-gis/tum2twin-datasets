<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-08-20 15:36:54 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4959462.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>691035.098 5336140.516 514.34</gml:lowerCorner>
         <gml:upperCorner>691059.124 5336179.71 529.684</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4959462">
<gml:name>DEBY_LOD2_4959462</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJ2v</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959462/BrückeRechtsThStr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959462_32897a5e-84f1-4519-908c-30bbf0deb8fb_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959462_32897a5e-84f1-4519-908c-30bbf0deb8fb_poly_0_">
0.0004820094941609121 0.0005915963045315003 0.9995178742972507 0.0005915963045315003 0.9995178742972507 0.9992193812454812 0.0004820094941609121 0.9992193812454812 0.0004820094941609121 0.0005915963045315003
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959462/BrückeRechtsThStr_T3lMS7GrdXAKvg4wWtiO.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959462_faa0f118-3931-4e11-a028-e1cebfbf3320_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959462_faa0f118-3931-4e11-a028-e1cebfbf3320_poly_0_">
0.015728976244232307 0.0005389646112464267 0.986310386623833 0.0005389646112464267 0.986310386623833 0.9994608889428247 0.015728976244232307 0.9994608889428247 0.015728976244232307 0.0005389646112464267
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959462/distorted_0_BrückeRechtsThStr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959462_03f118ca-6a4d-4658-a4c1-e92c72924572_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959462_03f118ca-6a4d-4658-a4c1-e92c72924572_poly_0_">
-6.661338147750938e-16 0.0 0.9999999999999989 0.0 0.9999999999999989 1.0 -6.661338147750938e-16 1.0 -6.661338147750938e-16 0.0
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959462/BrückeLinks.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959462_c5310590-2d79-4436-82d8-2cbf831c5db3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959462_c5310590-2d79-4436-82d8-2cbf831c5db3_poly_0_">
0.9996161895105684 0.9987860906020469 0.00038388416588697183 0.9987860906020469 0.00038388416588697183 0.0009447011610659776 0.9996161895105684 0.0009447011610659776 0.9996161895105684 0.9987860906020469
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>1000</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">15.344</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959462_d59093d9-7ac3-4daa-b0f1-e4b3ea536b1d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_d59093d9-7ac3-4daa-b0f1-e4b3ea536b1d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_d59093d9-7ac3-4daa-b0f1-e4b3ea536b1d_poly_0_">
<gml:posList srsDimension="3">
691038.444 5336143.137 529.684
691045.404 5336140.516 529.684
691045.595 5336140.994 529.684
691052.465 5336158.537 529.684
691059.124 5336175.681 529.684
691049.682 5336179.346 529.684
691048.747 5336179.71 529.684
691035.098 5336144.458 529.684
691038.444 5336143.137 529.684
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_1b6cb172-d0c9-4c0c-a18d-e1c250083095">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_1b6cb172-d0c9-4c0c-a18d-e1c250083095_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_1b6cb172-d0c9-4c0c-a18d-e1c250083095_poly_0_">
<gml:posList srsDimension="3">
691049.682 5336179.346 529.684
691049.682 5336179.346 514.34
691048.747 5336179.71 514.34
691048.747 5336179.71 529.684
691049.682 5336179.346 529.684
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_32897a5e-84f1-4519-908c-30bbf0deb8fb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_32897a5e-84f1-4519-908c-30bbf0deb8fb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_32897a5e-84f1-4519-908c-30bbf0deb8fb_poly_0_">
<gml:posList srsDimension="3">
691045.595 5336140.994 514.34
691052.465 5336158.537 514.34
691052.465 5336158.537 529.684
691045.595 5336140.994 529.684
691045.595 5336140.994 514.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_c7447f26-4d64-4365-8860-8429ec7c13ef">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_c7447f26-4d64-4365-8860-8429ec7c13ef_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_c7447f26-4d64-4365-8860-8429ec7c13ef_poly_0_">
<gml:posList srsDimension="3">
691059.124 5336175.681 529.684
691059.124 5336175.681 514.34
691049.682 5336179.346 514.34
691049.682 5336179.346 529.684
691059.124 5336175.681 529.684
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_e6eee0d7-c849-48bc-b571-35672eef2d03">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_e6eee0d7-c849-48bc-b571-35672eef2d03_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_e6eee0d7-c849-48bc-b571-35672eef2d03_poly_0_">
<gml:posList srsDimension="3">
691045.404 5336140.516 514.34
691045.404 5336140.516 529.684
691038.444 5336143.137 529.684
691038.444 5336143.137 514.34
691045.404 5336140.516 514.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_d3f65878-0147-422a-a659-8abe151624d2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_d3f65878-0147-422a-a659-8abe151624d2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_d3f65878-0147-422a-a659-8abe151624d2_poly_0_">
<gml:posList srsDimension="3">
691038.444 5336143.137 514.34
691038.444 5336143.137 529.684
691035.098 5336144.458 529.684
691035.098 5336144.458 514.34
691038.444 5336143.137 514.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_faa0f118-3931-4e11-a028-e1cebfbf3320">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_faa0f118-3931-4e11-a028-e1cebfbf3320_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_faa0f118-3931-4e11-a028-e1cebfbf3320_poly_0_">
<gml:posList srsDimension="3">
691045.404 5336140.516 514.34
691045.595 5336140.994 514.34
691045.595 5336140.994 529.684
691045.404 5336140.516 529.684
691045.404 5336140.516 514.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_03f118ca-6a4d-4658-a4c1-e92c72924572">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_03f118ca-6a4d-4658-a4c1-e92c72924572_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_03f118ca-6a4d-4658-a4c1-e92c72924572_poly_0_">
<gml:posList srsDimension="3">
691052.465 5336158.537 514.34
691059.124 5336175.681 514.34
691059.124 5336175.681 529.684
691052.465 5336158.537 529.684
691052.465 5336158.537 514.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959462_c5310590-2d79-4436-82d8-2cbf831c5db3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_c5310590-2d79-4436-82d8-2cbf831c5db3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_c5310590-2d79-4436-82d8-2cbf831c5db3_poly_0_">
<gml:posList srsDimension="3">
691035.098 5336144.458 529.684
691048.747 5336179.71 529.684
691048.747 5336179.71 514.34
691035.098 5336144.458 514.34
691035.098 5336144.458 529.684
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959462_e4d46f8f-b458-433b-b8ad-4eb9abc51628">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959462_e4d46f8f-b458-433b-b8ad-4eb9abc51628_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959462_e4d46f8f-b458-433b-b8ad-4eb9abc51628_poly_0_">
<gml:posList srsDimension="3">
691052.465 5336158.537 514.34
691045.595 5336140.994 514.34
691045.404 5336140.516 514.34
691038.444 5336143.137 514.34
691035.098 5336144.458 514.34
691048.747 5336179.71 514.34
691049.682 5336179.346 514.34
691059.124 5336175.681 514.34
691052.465 5336158.537 514.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Theresienstraße 90</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>