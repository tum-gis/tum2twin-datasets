<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-06-18 17:52:41 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Downloads\tum2twin-datasets-main\citygml\lod2-building-datasets\DEBY_LOD2_4959458.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>691000.964 5336127.139 514.36</gml:lowerCorner>
         <gml:upperCorner>691045.404 5336155.896 536.181</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4959458">
<gml:name>DEBY_LOD2_4959458</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJ0e</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959458/ZwischenBrücken.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959458_1403eb6f-130b-41d9-b04c-5828b2e8632b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959458_1403eb6f-130b-41d9-b04c-5828b2e8632b_poly_0_">
0.9948953736168695 0.9980363391160854 0.0019304759235005964 0.9984947538914679 0.0019304759235005964 0.001958334056460983 0.9948953736168695 0.0019583340564609825 0.9968662944249322 0.001958334056460984 0.9968662944249322 0.9980363391160854 0.9948953736168695 0.9980363391160854
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959458/ZwischenBrücken_EGuURIi4xZHtv9pDH0NC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959458_75d9f724-6781-4de5-815c-d2affffd935e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959458_75d9f724-6781-4de5-815c-d2affffd935e_poly_0_">
0.999485150225318 0.0008342221445981002 0.999485150225318 0.9972826199814571 0.0007773546504870055 0.9990015235485382 0.0007773546504870055 0.0008342221445981002 0.999485150225318 0.0008342221445981002
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">21.821</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly_0_">
<gml:posList srsDimension="3">
691002.619 5336146.88 536.181
691041.861 5336131.523 536.181
691006.1 5336155.896 531.743
691002.619 5336146.88 536.181
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly.EPm4wwZqs3WM8jM7Osmg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly_0_.AtYcFuGfGPGKDuOX766p">
<gml:posList srsDimension="3">
691006.123 5336155.887 531.743
691006.1 5336155.896 531.743
691041.861 5336131.523 536.181
691006.123 5336155.887 531.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly.rpQE0LIdr3AIWuw40SGQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly_0_.FyndbNbViBhm3Jd5v7lr">
<gml:posList srsDimension="3">
691017.704 5336151.336 531.751
691006.123 5336155.887 531.743
691041.861 5336131.523 536.181
691017.704 5336151.336 531.751
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly.uiTZMMJMKyCygDCzf2uD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly_0_.wgnyyRFL1bF0ZXWS91oE">
<gml:posList srsDimension="3">
691038.444 5336143.137 531.786
691017.704 5336151.336 531.751
691041.861 5336131.523 536.181
691038.444 5336143.137 531.786
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly.UVi1Uph2k4XIsDuS9nKr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_b8e2c5fc-728b-4388-bfdd-bc05b49eb39b_poly_0_.3GlJJmGgNhLlro4p97zc">
<gml:posList srsDimension="3">
691045.404 5336140.516 531.743
691038.444 5336143.137 531.786
691041.861 5336131.523 536.181
691045.404 5336140.516 531.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly_0_">
<gml:posList srsDimension="3">
691000.964 5336142.595 533.812
691012.511 5336138.042 533.795
691002.619 5336146.88 536.181
691000.964 5336142.595 533.812
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly.ZjQGWsYEcktTjJ7wNAwV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly_0_.k2y0h9cRR3ZHs0Agy9h3">
<gml:posList srsDimension="3">
691041.861 5336131.523 536.181
691002.619 5336146.88 536.181
691012.511 5336138.042 533.795
691041.861 5336131.523 536.181
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly.KWRj2RfHvqZLhMnbPAeS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly_0_.LvNDNsYaackaIfPKR8DI">
<gml:posList srsDimension="3">
691029.782 5336131.239 533.774
691041.861 5336131.523 536.181
691012.511 5336138.042 533.795
691029.782 5336131.239 533.774
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly.0zrUksbSRvUGxre5csj0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_154e259b-e252-4793-8f8d-bef92d6cf7be_poly_0_.YOO6EsY7G8dhKkTlFuMm">
<gml:posList srsDimension="3">
691040.133 5336127.139 533.75
691041.861 5336131.523 536.181
691029.782 5336131.239 533.774
691040.133 5336127.139 533.75
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_48516c67-2379-4bdd-afa9-945f85e0d530">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_48516c67-2379-4bdd-afa9-945f85e0d530_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_48516c67-2379-4bdd-afa9-945f85e0d530_poly_0_">
<gml:posList srsDimension="3">
691035.098 5336144.458 514.36
691035.098 5336144.458 531.781
691038.444 5336143.137 531.786
691038.444 5336143.137 514.36
691035.098 5336144.458 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_f1ee416e-8792-4052-af07-6eecbd5385fc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_f1ee416e-8792-4052-af07-6eecbd5385fc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_f1ee416e-8792-4052-af07-6eecbd5385fc_poly_0_">
<gml:posList srsDimension="3">
691038.444 5336143.137 514.36
691038.444 5336143.137 531.786
691045.404 5336140.516 531.743
691045.404 5336140.516 514.36
691038.444 5336143.137 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_1403eb6f-130b-41d9-b04c-5828b2e8632b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_1403eb6f-130b-41d9-b04c-5828b2e8632b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_1403eb6f-130b-41d9-b04c-5828b2e8632b_poly_0_">
<gml:posList srsDimension="3">
691006.123 5336155.887 531.743
691017.704 5336151.336 531.751
691017.704 5336151.336 514.36
691006.123 5336155.887 514.36
691006.1 5336155.896 514.36
691006.1 5336155.896 531.743
691006.123 5336155.887 531.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_4c3aeceb-7f60-4b12-b781-b1fe51be1da0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_4c3aeceb-7f60-4b12-b781-b1fe51be1da0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_4c3aeceb-7f60-4b12-b781-b1fe51be1da0_poly_0_">
<gml:posList srsDimension="3">
691012.511 5336138.042 514.36
691012.511 5336138.042 533.795
691000.964 5336142.595 533.812
691000.964 5336142.595 514.36
691012.511 5336138.042 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_75d9f724-6781-4de5-815c-d2affffd935e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_75d9f724-6781-4de5-815c-d2affffd935e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_75d9f724-6781-4de5-815c-d2affffd935e_poly_0_">
<gml:posList srsDimension="3">
691017.704 5336151.336 514.36
691017.704 5336151.336 531.751
691035.098 5336144.458 531.781
691035.098 5336144.458 514.36
691017.704 5336151.336 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_94ec6c1e-c182-4854-8099-4eb5d4fbdb72">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_94ec6c1e-c182-4854-8099-4eb5d4fbdb72_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_94ec6c1e-c182-4854-8099-4eb5d4fbdb72_poly_0_">
<gml:posList srsDimension="3">
691040.133 5336127.139 514.36
691040.133 5336127.139 533.75
691029.782 5336131.239 533.774
691029.782 5336131.239 514.36
691040.133 5336127.139 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_6f42cf0a-4b25-4ba8-ad75-1f8b1bdee218">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_6f42cf0a-4b25-4ba8-ad75-1f8b1bdee218_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_6f42cf0a-4b25-4ba8-ad75-1f8b1bdee218_poly_0_">
<gml:posList srsDimension="3">
691029.782 5336131.239 514.36
691029.782 5336131.239 533.774
691012.511 5336138.042 533.795
691012.511 5336138.042 514.36
691029.782 5336131.239 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly_0_">
<gml:posList srsDimension="3">
691040.133 5336127.139 514.36
691041.861 5336131.523 514.36
691040.133 5336127.139 533.75
691040.133 5336127.139 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly.vSLb8XbizUCgAYPmclN5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly_0_.xCWnUlBGpNn6ytc9b9EP">
<gml:posList srsDimension="3">
691041.861 5336131.523 536.181
691040.133 5336127.139 533.75
691041.861 5336131.523 514.36
691041.861 5336131.523 536.181
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly.WRG1a5VRYhC7ui1nnqYy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly_0_.BD4aKBQywHz9ZNJW0Vme">
<gml:posList srsDimension="3">
691045.404 5336140.516 514.36
691041.861 5336131.523 536.181
691041.861 5336131.523 514.36
691045.404 5336140.516 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly.PvsIT1iqWgJnvWEUMHx5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_16fe12ab-495e-4f26-8bb8-b764fe41df0b_poly_0_.DA39AjYSC0NOhXR0C2PJ">
<gml:posList srsDimension="3">
691045.404 5336140.516 531.743
691041.861 5336131.523 536.181
691045.404 5336140.516 514.36
691045.404 5336140.516 531.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8_poly_0_">
<gml:posList srsDimension="3">
691000.964 5336142.595 533.812
691002.619 5336146.88 536.181
691000.964 5336142.595 514.36
691000.964 5336142.595 533.812
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8_poly.pvMS1JCxLjohkxYI7O16">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8_poly_0_.Dw6bn6wOC55DKX3C0ro5">
<gml:posList srsDimension="3">
691006.1 5336155.896 514.36
691000.964 5336142.595 514.36
691002.619 5336146.88 536.181
691006.1 5336155.896 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8_poly.G1BLucNtwej4M9VwOXe3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_91d5302f-3765-4a74-9913-0746c3ca58d8_poly_0_.sKrh25VQHD0hpFrVmdUl">
<gml:posList srsDimension="3">
691006.1 5336155.896 531.743
691006.1 5336155.896 514.36
691002.619 5336146.88 536.181
691006.1 5336155.896 531.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959458_e62f7fac-733d-4fb0-be14-9f9cb8f2313d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959458_e62f7fac-733d-4fb0-be14-9f9cb8f2313d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959458_e62f7fac-733d-4fb0-be14-9f9cb8f2313d_poly_0_">
<gml:posList srsDimension="3">
691041.861 5336131.523 514.36
691040.133 5336127.139 514.36
691029.782 5336131.239 514.36
691000.964 5336142.595 514.36
691006.1 5336155.896 514.36
691006.123 5336155.887 514.36
691017.704 5336151.336 514.36
691038.444 5336143.137 514.36
691045.404 5336140.516 514.36
691041.861 5336131.523 514.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>