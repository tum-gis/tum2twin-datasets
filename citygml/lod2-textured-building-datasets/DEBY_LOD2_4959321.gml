<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-25 22:01:14 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Downloads\tum2twin-datasets-main\citygml\lod2-building-datasets\DEBY_LOD2_4959321.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>691042.684 5336073.722 514.25</gml:lowerCorner>
         <gml:upperCorner>691053.037 5336088.327 536.156</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4959321">
<gml:name>DEBY_LOD2_4959321</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJ0P</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959321/PhotoHinten.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959321_ab79f5b5-039c-4f3e-aeb7-d562cdbe0e2f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959321_ab79f5b5-039c-4f3e-aeb7-d562cdbe0e2f_poly_0_">
0.9991315133241307 0.00039161878442453823 0.9991315133241307 0.9991988122503798 0.0013165994091607902 0.9996083692748661 0.0013165994091607902 0.00039161878442453823 0.9991315133241307 0.00039161878442453823
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">21.906</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959321_839db357-78ce-4667-94de-79adcccf8468">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_839db357-78ce-4667-94de-79adcccf8468_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_839db357-78ce-4667-94de-79adcccf8468_poly_0_">
<gml:posList srsDimension="3">
691048.082 5336073.722 533.76
691050.559 5336079.974 536.156
691045.151 5336082.076 536.156
691042.684 5336075.844 533.768
691048.082 5336073.722 533.76
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6_poly_0_">
<gml:posList srsDimension="3">
691045.151 5336082.076 536.156
691050.559 5336079.974 536.156
691045.163 5336082.106 536.144
691045.151 5336082.076 536.156
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6_poly.Z7cKKxyGA14uxn5mAkBr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6_poly_0_.yro0nRKUJxd6h8CknejM">
<gml:posList srsDimension="3">
691047.633 5336088.327 533.76
691045.163 5336082.106 536.144
691050.559 5336079.974 536.156
691047.633 5336088.327 533.76
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6_poly.BgoAvV2oOJvoM3PXEZbG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_0ac2a9a1-5ca2-492f-b0cf-0b1d66a32ad6_poly_0_.tQ1ZZaBJDw0Nxj081uGI">
<gml:posList srsDimension="3">
691053.037 5336086.226 533.76
691047.633 5336088.327 533.76
691050.559 5336079.974 536.156
691053.037 5336086.226 533.76
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959321_ec43b45b-fa3e-4eff-9350-811940003f21">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_ec43b45b-fa3e-4eff-9350-811940003f21_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_ec43b45b-fa3e-4eff-9350-811940003f21_poly_0_">
<gml:posList srsDimension="3">
691045.163 5336082.106 514.25
691045.163 5336082.106 536.144
691047.633 5336088.327 533.76
691047.633 5336088.327 514.25
691045.163 5336082.106 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959321_152bfa6d-fc9a-49a6-8ce3-9121c975cc66">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_152bfa6d-fc9a-49a6-8ce3-9121c975cc66_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_152bfa6d-fc9a-49a6-8ce3-9121c975cc66_poly_0_">
<gml:posList srsDimension="3">
691047.633 5336088.327 514.25
691047.633 5336088.327 533.76
691053.037 5336086.226 533.76
691053.037 5336086.226 514.25
691047.633 5336088.327 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly_0_">
<gml:posList srsDimension="3">
691042.684 5336075.844 533.768
691045.151 5336082.076 536.156
691042.684 5336075.844 514.25
691042.684 5336075.844 533.768
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly.qg6UfA8FVZeDryN1BWuS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly_0_.94plH0Ks0J0x6Zh1HGmN">
<gml:posList srsDimension="3">
691045.151 5336082.076 514.25
691042.684 5336075.844 514.25
691045.151 5336082.076 536.156
691045.151 5336082.076 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly.VKypghr6SSJWjQU1blDl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly_0_.YUltyRDYzGN37lIfPXH9">
<gml:posList srsDimension="3">
691045.163 5336082.106 536.144
691045.151 5336082.076 514.25
691045.151 5336082.076 536.156
691045.163 5336082.106 536.144
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly.GwoenXlyCtOEcW1amSVC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_aa077451-f83f-4cec-8c0d-610cc627ed4b_poly_0_.OY3yXHJaHsAxMBIPBOfy">
<gml:posList srsDimension="3">
691045.163 5336082.106 514.25
691045.151 5336082.076 514.25
691045.163 5336082.106 536.144
691045.163 5336082.106 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly_0_">
<gml:posList srsDimension="3">
691048.082 5336073.722 514.25
691050.559 5336079.974 514.25
691048.082 5336073.722 533.76
691048.082 5336073.722 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly.beysvQLDAl78CulpnsQz">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly_0_.8OE401kHSufs4p0sfE6y">
<gml:posList srsDimension="3">
691050.559 5336079.974 536.156
691048.082 5336073.722 533.76
691050.559 5336079.974 514.25
691050.559 5336079.974 536.156
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly.y5VDgki1KDgxBwiZQxpk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly_0_.AcZwpjrsxv5vp3fH26QY">
<gml:posList srsDimension="3">
691053.037 5336086.226 514.25
691050.559 5336079.974 536.156
691050.559 5336079.974 514.25
691053.037 5336086.226 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly.GvF63COfiGJVEThpFQ5h">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_e14b897b-4a9d-45df-823f-18111496d914_poly_0_.HjLGv9PX0YQvhwW58LIk">
<gml:posList srsDimension="3">
691053.037 5336086.226 533.76
691050.559 5336079.974 536.156
691053.037 5336086.226 514.25
691053.037 5336086.226 533.76
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959321_ab79f5b5-039c-4f3e-aeb7-d562cdbe0e2f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_ab79f5b5-039c-4f3e-aeb7-d562cdbe0e2f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_ab79f5b5-039c-4f3e-aeb7-d562cdbe0e2f_poly_0_">
<gml:posList srsDimension="3">
691048.082 5336073.722 514.25
691048.082 5336073.722 533.76
691042.684 5336075.844 533.768
691042.684 5336075.844 514.25
691048.082 5336073.722 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959321_fa6ac6cb-8763-4d91-872b-82376bd89de1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959321_fa6ac6cb-8763-4d91-872b-82376bd89de1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959321_fa6ac6cb-8763-4d91-872b-82376bd89de1_poly_0_">
<gml:posList srsDimension="3">
691050.559 5336079.974 514.25
691048.082 5336073.722 514.25
691042.684 5336075.844 514.25
691045.151 5336082.076 514.25
691045.163 5336082.106 514.25
691047.633 5336088.327 514.25
691053.037 5336086.226 514.25
691050.559 5336079.974 514.25
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>