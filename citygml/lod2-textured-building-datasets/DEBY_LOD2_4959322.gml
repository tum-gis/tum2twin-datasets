<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-09 14:25:18 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Downloads\tum2twin-datasets-main\citygml\lod2-building-datasets\DEBY_LOD2_4959322.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>691040.133 5336116.984 514.62</gml:lowerCorner>
         <gml:upperCorner>691070.486 5336140.516 536.119</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4959322">
<gml:name>DEBY_LOD2_4959322</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJzM</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959322/distorted_0_RechtsVonPhoto.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly_0_">
1.0 0.0 1.0 1.0 0.0 0.0 1.0 0.0
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959322/distorted_1_RechtsVonPhoto.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly.zg5gSmbJahRXprGXK0Cv">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly_0_.LTogQ4NdM6Ngurqk4T91">
0.4516505133611535 0.9958306792518385 0.0 0.0 1.0 1.0 0.4516505133611535 0.9958306792518385
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959322/distorted_2_RechtsVonPhoto.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly.P6GJbQ007ri5cT2c8Ihd">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly_0_.B2o1N3re593VqRtOdSX9">
-2.220446049250313e-16 0.996569169041112 -2.220446049250313e-16 0.0 0.9999999999999999 1.0 -2.220446049250313e-16 0.996569169041112
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">21.499</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959322_83760d40-a45b-4e73-9b23-18575f5173e0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_83760d40-a45b-4e73-9b23-18575f5173e0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_83760d40-a45b-4e73-9b23-18575f5173e0_poly_0_">
<gml:posList srsDimension="3">
691040.133 5336127.139 534.078
691065.116 5336116.984 534.078
691041.849 5336131.493 536.119
691040.133 5336127.139 534.078
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_83760d40-a45b-4e73-9b23-18575f5173e0_poly.q9gWRMCKTbwv1Jepvytm">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_83760d40-a45b-4e73-9b23-18575f5173e0_poly_0_.M6fuYRWQSbxw1uCNh4zA">
<gml:posList srsDimension="3">
691066.828 5336121.339 536.119
691041.849 5336131.493 536.119
691065.116 5336116.984 534.078
691066.828 5336121.339 536.119
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly_0_">
<gml:posList srsDimension="3">
691041.849 5336131.493 536.119
691066.828 5336121.339 536.119
691045.404 5336140.516 531.889
691041.849 5336131.493 536.119
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly.pykxEDkYtsqNpzXrsbzl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly_0_.wUZ3zdWavy3kOFeG1oCO">
<gml:posList srsDimension="3">
691059.158 5336135.104 531.817
691045.404 5336140.516 531.889
691066.828 5336121.339 536.119
691059.158 5336135.104 531.817
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly.vY671XbkYQOlcO3TFo5n">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly_0_.b2EcwVyfbokLzOgJJMty">
<gml:posList srsDimension="3">
691070.436 5336130.519 531.817
691059.158 5336135.104 531.817
691066.828 5336121.339 536.119
691070.436 5336130.519 531.817
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly.c0bUcITrzesGt0axCvUt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_68156e47-9db9-43ef-bde3-6e2fdf01c7dd_poly_0_.KJjn4klb9PDkJwQYWsgL">
<gml:posList srsDimension="3">
691070.486 5336130.645 531.758
691059.158 5336135.104 531.817
691070.436 5336130.519 531.817
691070.486 5336130.645 531.758
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959322_64a1e525-6b84-4f98-9bf0-b987f272f09b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_64a1e525-6b84-4f98-9bf0-b987f272f09b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_64a1e525-6b84-4f98-9bf0-b987f272f09b_poly_0_">
<gml:posList srsDimension="3">
691065.116 5336116.984 514.62
691065.116 5336116.984 534.078
691040.133 5336127.139 534.078
691040.133 5336127.139 514.62
691065.116 5336116.984 514.62
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7_poly_0_">
<gml:posList srsDimension="3">
691040.133 5336127.139 534.078
691041.849 5336131.493 536.119
691040.133 5336127.139 514.62
691040.133 5336127.139 534.078
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7_poly.8QslW9AeyGLI10q6hquF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7_poly_0_.4Rg9AUOwAfPfckCK2aDU">
<gml:posList srsDimension="3">
691045.404 5336140.516 514.62
691040.133 5336127.139 514.62
691041.849 5336131.493 536.119
691045.404 5336140.516 514.62
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7_poly.xcdOaeprsK4r8X6qMwSK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_94b0dd77-389b-4938-858e-23b5b99aacf7_poly_0_.JzZwHJPoUZY5g2eP3kWZ">
<gml:posList srsDimension="3">
691045.404 5336140.516 531.889
691045.404 5336140.516 514.62
691041.849 5336131.493 536.119
691045.404 5336140.516 531.889
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly_0_">
<gml:posList srsDimension="3">
691065.116 5336116.984 514.62
691070.436 5336130.519 514.62
691065.116 5336116.984 534.078
691065.116 5336116.984 514.62
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly.9OIlh3eUYROAsZZWtprs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly_0_.4cyk6IsBImNgrpoKCoHM">
<gml:posList srsDimension="3">
691066.828 5336121.339 536.119
691065.116 5336116.984 534.078
691070.436 5336130.519 514.62
691066.828 5336121.339 536.119
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly.WAPStUpXaJGUxMopjerW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly_0_.5ITCK3PlBrPPvPddn3Sw">
<gml:posList srsDimension="3">
691070.436 5336130.519 531.817
691066.828 5336121.339 536.119
691070.436 5336130.519 514.62
691070.436 5336130.519 531.817
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly.cwHSzaUmdvPdK09kOaPw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly_0_.88G1TJKYeTaaglEVcdZ0">
<gml:posList srsDimension="3">
691070.486 5336130.645 514.62
691070.436 5336130.519 531.817
691070.436 5336130.519 514.62
691070.486 5336130.645 514.62
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly.j37jevE1hn4snJyx2A0B">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_0ae9171c-04bf-4503-82ca-f7227ce0b288_poly_0_.BjO9ThdvDydaZaUXWTLn">
<gml:posList srsDimension="3">
691070.486 5336130.645 531.758
691070.436 5336130.519 531.817
691070.486 5336130.645 514.62
691070.486 5336130.645 531.758
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly_0_">
<gml:posList srsDimension="3">
691045.404 5336140.516 514.62
691045.404 5336140.516 531.889
691070.486 5336130.645 514.62
691045.404 5336140.516 514.62
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly.zg5gSmbJahRXprGXK0Cv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly_0_.LTogQ4NdM6Ngurqk4T91">
<gml:posList srsDimension="3">
691059.158 5336135.104 531.817
691070.486 5336130.645 514.62
691045.404 5336140.516 531.889
691059.158 5336135.104 531.817
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly.P6GJbQ007ri5cT2c8Ihd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_9ceccd44-fdd9-48a2-8dfe-8637aa87a9ab_poly_0_.B2o1N3re593VqRtOdSX9">
<gml:posList srsDimension="3">
691070.486 5336130.645 531.758
691070.486 5336130.645 514.62
691059.158 5336135.104 531.817
691070.486 5336130.645 531.758
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959322_1e1c9e32-5760-4cff-a2a9-65870b890e43">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959322_1e1c9e32-5760-4cff-a2a9-65870b890e43_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959322_1e1c9e32-5760-4cff-a2a9-65870b890e43_poly_0_">
<gml:posList srsDimension="3">
691070.436 5336130.519 514.62
691065.116 5336116.984 514.62
691040.133 5336127.139 514.62
691045.404 5336140.516 514.62
691070.486 5336130.645 514.62
691070.436 5336130.519 514.62
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>