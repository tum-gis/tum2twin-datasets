<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-02 13:58:14 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Downloads\tum2twin-datasets-main\citygml\lod2-building-datasets\DEBY_LOD2_4906972.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690928.466 5335918.65 511.083</gml:lowerCorner>
         <gml:upperCorner>690964.542 5335948.062 535.16</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906972">
<gml:name>DEBY_LOD2_4906972</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMI9U</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906972/IMG_6235.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906972_4e3558a3-b00b-464f-bff4-01dbdf4b3a64_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906972_4e3558a3-b00b-464f-bff4-01dbdf4b3a64_poly_0_">
0.9977494282972801 0.0008964112320495754 0.9977494282972801 0.9990445351364668 0.0021874831502217695 0.9991045055837093 0.0021874831502217695 0.0008964112320495754 0.9977494282972801 0.0008964112320495754
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906972/IMG_6235_LrZNegFVAe22WuTC6Rcg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906972_c39b10f7-b174-41df-9d17-2b863c51e5d9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906972_c39b10f7-b174-41df-9d17-2b863c51e5d9_poly_0_">
0.9018926536850955 0.0008507303288592295 0.9018926536850955 0.9989602975965558 0.09928166595661025 0.9941249581857495 0.09928166595661025 0.0008507303288592295 0.9018926536850955 0.0008507303288592295
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906972/IMG_6235_nDWyZvSPOkxP1GsZN1f3.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906972_c4547f98-389d-4617-a5ef-405fbc939e8f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906972_c4547f98-389d-4617-a5ef-405fbc939e8f_poly_0_">
0.9992119497002022 0.0008395090483980484 0.9992119497002022 0.9992722427126024 0.0009068131737628438 0.9993319611606951 0.0009068131737628438 0.0008395090483980484 0.9992119497002022 0.0008395090483980484
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906972/IMG_6235_XLYBMmP7Qxz1i5u4RtHD.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906972_21770520-cce3-4bd8-b82a-20476ca497f8_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906972_21770520-cce3-4bd8-b82a-20476ca497f8_poly_0_">
0.8873103737031371 0.0008893619847556316 0.8873103737031371 0.9945327224387458 0.09524436088099009 0.9989502417342456 0.09524436088099009 0.0008893619847556316 0.8873103737031371 0.0008893619847556316
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906972/IMG_6235_H2on23Uyc5Akdc5z2To2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906972_3fb64e5b-dcd2-4962-914e-e68e2999fa0d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906972_3fb64e5b-dcd2-4962-914e-e68e2999fa0d_poly_0_">
0.9983132482682536 0.000818280041255574 0.9983132482682536 0.9991812646533762 0.0020009627320604073 0.9990612618906366 0.0020009627320604073 0.000818280041255574 0.9983132482682536 0.000818280041255574
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">20.16</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_">
<gml:posList srsDimension="3">
690962.675 5335929.32 535.16
690963.482 5335931.361 533.911
690963.249 5335931.452 533.91
690962.675 5335929.32 535.16
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.cfUJoZ7c67GTkUmnW5e3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.IcwJr6YTfDnvlS1Z6Qu8">
<gml:posList srsDimension="3">
690935.53 5335939.7 535.16
690962.675 5335929.32 535.16
690937.082 5335943.553 532.798
690935.53 5335939.7 535.16
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.TYNIReatqSWaBayZL5V8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.u18jUR6ZbmLggWJkDfLJ">
<gml:posList srsDimension="3">
690937.457 5335943.417 532.794
690937.082 5335943.553 532.798
690962.675 5335929.32 535.16
690937.457 5335943.417 532.794
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.bNtYBY3FcCk91QOcqKDz">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.nZ3pWQIao9myizqoOENF">
<gml:posList srsDimension="3">
690938.254 5335945.506 531.522
690937.457 5335943.417 532.794
690962.675 5335929.32 535.16
690938.254 5335945.506 531.522
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.xvv7WDsioXzDjNU5SIEB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.77yxer1IfE2QAdKUyRSd">
<gml:posList srsDimension="3">
690945.44 5335942.257 531.788
690938.254 5335945.506 531.522
690962.675 5335929.32 535.16
690945.44 5335942.257 531.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.yVoATWeuNzwSMFFsG91t">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.498CRMyoXrAiTZGwLCsW">
<gml:posList srsDimension="3">
690963.249 5335931.452 533.91
690945.44 5335942.257 531.788
690962.675 5335929.32 535.16
690963.249 5335931.452 533.91
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.19q62qfypxvqvc7jezjK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.bznjSVRd0kQ7GNuLzc3K">
<gml:posList srsDimension="3">
690964.351 5335934.247 532.201
690945.44 5335942.257 531.788
690963.249 5335931.452 533.91
690964.351 5335934.247 532.201
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.K6kYNCZEL938oHsDUYqV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.SxIw2VciYdvzsDzZCe6t">
<gml:posList srsDimension="3">
690964.542 5335934.725 531.908
690945.44 5335942.257 531.788
690964.351 5335934.247 532.201
690964.542 5335934.725 531.908
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly.zO5JmH42KfcfPNxMhVvT">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_ff4278ca-58db-4767-929e-30eeaecc0fbe_poly_0_.XGCIzDwzybC2ZqTVXr2f">
<gml:posList srsDimension="3">
690945.44 5335942.257 531.788
690945.613 5335942.692 531.521
690938.254 5335945.506 531.522
690945.44 5335942.257 531.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly_0_">
<gml:posList srsDimension="3">
690938.254 5335945.506 531.522
690945.613 5335942.692 531.521
690938.275 5335945.561 531.499
690938.254 5335945.506 531.522
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly.FVpBqNtKr9elnGQ3xOTE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly_0_.tlLPctUVc2QBpwwr0VeY">
<gml:posList srsDimension="3">
690939.232 5335947.97 530.488
690938.275 5335945.561 531.499
690945.613 5335942.692 531.521
690939.232 5335947.97 530.488
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly.AFq30iUAipxyFrvE7VSy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly_0_.8WoNZjhEFPUvIed6coZV">
<gml:posList srsDimension="3">
690939.268 5335948.062 530.449
690939.232 5335947.97 530.488
690945.613 5335942.692 531.521
690939.268 5335948.062 530.449
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly.j5EQOsu5o4d9fH0OrOp3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly_0_.JZr0LEYIegSq8CcOHrid">
<gml:posList srsDimension="3">
690946.344 5335945.255 530.486
690939.268 5335948.062 530.449
690945.613 5335942.692 531.521
690946.344 5335945.255 530.486
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly.MLetyjQKUKwu1CEHwKpQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_44b9ac86-8f4c-4d9f-9fe3-0bba182f0574_poly_0_.I3pa3itUFoccODo3e4CV">
<gml:posList srsDimension="3">
690946.589 5335945.155 530.489
690946.344 5335945.255 530.486
690945.613 5335942.692 531.521
690946.589 5335945.155 530.489
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_">
<gml:posList srsDimension="3">
690954.044 5335926.008 531.645
690960.404 5335923.574 531.644
690954.089 5335926.13 531.719
690954.044 5335926.008 531.645
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.eiWXCdTH0IiMihgG1Jqh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.G7ewuBgosXYjgqvmgOfT">
<gml:posList srsDimension="3">
690933.222 5335933.954 531.637
690939.573 5335931.529 531.639
690933.358 5335934.309 531.853
690933.222 5335933.954 531.637
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.NTbsZgGdhDy4FHMp0GYp">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.cLFvCh1bandfUdPBWnuc">
<gml:posList srsDimension="3">
690935.53 5335939.7 535.16
690933.358 5335934.309 531.853
690939.573 5335931.529 531.639
690935.53 5335939.7 535.16
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.UzC4jgAmH6GUaYm6gZTF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.uO34Ol6ZZQtQQ7Tm9l6A">
<gml:posList srsDimension="3">
690939.628 5335931.661 531.72
690935.53 5335939.7 535.16
690939.573 5335931.529 531.639
690939.628 5335931.661 531.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.S58bosbhtACzS4bBXED7">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.mxVxOD5m6XdntJMsKmz6">
<gml:posList srsDimension="3">
690962.675 5335929.32 535.16
690935.53 5335939.7 535.16
690939.628 5335931.661 531.72
690962.675 5335929.32 535.16
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.bNcYxgLuRkc4ft1cYuhY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.9BjrpEo18aKP43qIUWVd">
<gml:posList srsDimension="3">
690954.089 5335926.13 531.719
690962.675 5335929.32 535.16
690939.628 5335931.661 531.72
690954.089 5335926.13 531.719
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.C1zATuR2B2nknB9AYtRk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.e3O5SH7sOmvDk25hiRei">
<gml:posList srsDimension="3">
690960.404 5335923.574 531.644
690962.675 5335929.32 535.16
690954.089 5335926.13 531.719
690960.404 5335923.574 531.644
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly.8IVh84vPlpfVh93Fwe9w">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c7c21230-3b9a-4df7-a52c-1bb74afa39bc_poly_0_.j0Hl5gKq3yuqlzXmqH9S">
<gml:posList srsDimension="3">
690960.686 5335924.286 532.08
690962.675 5335929.32 535.16
690960.404 5335923.574 531.644
690960.686 5335924.286 532.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_d3f14ecc-c94c-4934-825e-6ac9cc5aaf54">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_d3f14ecc-c94c-4934-825e-6ac9cc5aaf54_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_d3f14ecc-c94c-4934-825e-6ac9cc5aaf54_poly_0_">
<gml:posList srsDimension="3">
690964.351 5335934.247 515.0
690964.351 5335934.247 532.201
690963.249 5335931.452 533.91
690963.249 5335931.452 515.0
690964.351 5335934.247 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_4e3558a3-b00b-464f-bff4-01dbdf4b3a64">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_4e3558a3-b00b-464f-bff4-01dbdf4b3a64_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_4e3558a3-b00b-464f-bff4-01dbdf4b3a64_poly_0_">
<gml:posList srsDimension="3">
690960.404 5335923.574 515.0
690960.404 5335923.574 531.644
690954.044 5335926.008 531.645
690954.044 5335926.008 515.0
690960.404 5335923.574 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_c39b10f7-b174-41df-9d17-2b863c51e5d9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c39b10f7-b174-41df-9d17-2b863c51e5d9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c39b10f7-b174-41df-9d17-2b863c51e5d9_poly_0_">
<gml:posList srsDimension="3">
690939.628 5335931.661 515.0
690939.628 5335931.661 531.72
690939.573 5335931.529 531.639
690939.573 5335931.529 515.0
690939.628 5335931.661 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_9ed99717-e44d-4ca1-b6d7-fbcd62f48431">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_9ed99717-e44d-4ca1-b6d7-fbcd62f48431_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_9ed99717-e44d-4ca1-b6d7-fbcd62f48431_poly_0_">
<gml:posList srsDimension="3">
690945.44 5335942.257 515.0
690945.44 5335942.257 531.788
690955.352 5335938.35 531.849
690955.352 5335938.35 515.0
690945.44 5335942.257 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_e418cd28-2d43-4e85-b7c7-d7e2ee51f113">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_e418cd28-2d43-4e85-b7c7-d7e2ee51f113_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_e418cd28-2d43-4e85-b7c7-d7e2ee51f113_poly_0_">
<gml:posList srsDimension="3">
690964.542 5335934.725 515.0
690964.542 5335934.725 531.908
690964.351 5335934.247 532.201
690964.351 5335934.247 515.0
690964.542 5335934.725 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_8c31a9a4-30a8-4088-89b8-9e9cbc2cc454">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_8c31a9a4-30a8-4088-89b8-9e9cbc2cc454_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_8c31a9a4-30a8-4088-89b8-9e9cbc2cc454_poly_0_">
<gml:posList srsDimension="3">
690946.344 5335945.255 515.0
690946.344 5335945.255 530.486
690946.589 5335945.155 530.489
690946.589 5335945.155 515.0
690946.344 5335945.255 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_c4547f98-389d-4617-a5ef-405fbc939e8f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c4547f98-389d-4617-a5ef-405fbc939e8f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c4547f98-389d-4617-a5ef-405fbc939e8f_poly_0_">
<gml:posList srsDimension="3">
690954.089 5335926.13 515.0
690954.089 5335926.13 531.719
690939.628 5335931.661 531.72
690939.628 5335931.661 515.0
690954.089 5335926.13 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_b35a7837-ea63-4ccb-abb3-de3d15028d0c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_b35a7837-ea63-4ccb-abb3-de3d15028d0c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_b35a7837-ea63-4ccb-abb3-de3d15028d0c_poly_0_">
<gml:posList srsDimension="3">
690937.082 5335943.553 515.0
690937.082 5335943.553 532.798
690937.457 5335943.417 532.794
690937.457 5335943.417 515.0
690937.082 5335943.553 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_be4294c8-e93d-4792-b732-3c1230919681">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_be4294c8-e93d-4792-b732-3c1230919681_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_be4294c8-e93d-4792-b732-3c1230919681_poly_0_">
<gml:posList srsDimension="3">
690933.222 5335933.954 515.0
690933.222 5335933.954 531.637
690933.358 5335934.309 531.853
690933.358 5335934.309 515.0
690933.222 5335933.954 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_21770520-cce3-4bd8-b82a-20476ca497f8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_21770520-cce3-4bd8-b82a-20476ca497f8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_21770520-cce3-4bd8-b82a-20476ca497f8_poly_0_">
<gml:posList srsDimension="3">
690954.044 5335926.008 515.0
690954.044 5335926.008 531.645
690954.089 5335926.13 531.719
690954.089 5335926.13 515.0
690954.044 5335926.008 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_3fb64e5b-dcd2-4962-914e-e68e2999fa0d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_3fb64e5b-dcd2-4962-914e-e68e2999fa0d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_3fb64e5b-dcd2-4962-914e-e68e2999fa0d_poly_0_">
<gml:posList srsDimension="3">
690939.573 5335931.529 515.0
690939.573 5335931.529 531.639
690933.222 5335933.954 531.637
690933.222 5335933.954 515.0
690939.573 5335931.529 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_b759e43e-56d8-4846-b067-5ea25f3fb097">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_b759e43e-56d8-4846-b067-5ea25f3fb097_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_b759e43e-56d8-4846-b067-5ea25f3fb097_poly_0_">
<gml:posList srsDimension="3">
690963.249 5335931.452 515.0
690963.249 5335931.452 533.91
690963.482 5335931.361 533.911
690963.482 5335931.361 515.0
690963.249 5335931.452 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_c14e7961-7bfb-415f-9779-b37c2b853857">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_c14e7961-7bfb-415f-9779-b37c2b853857_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_c14e7961-7bfb-415f-9779-b37c2b853857_poly_0_">
<gml:posList srsDimension="3">
690960.686 5335924.286 515.0
690960.686 5335924.286 532.08
690960.404 5335923.574 531.644
690960.404 5335923.574 515.0
690960.686 5335924.286 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_9f28eecd-ca04-4e29-b4ca-3e0222ecc273">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_9f28eecd-ca04-4e29-b4ca-3e0222ecc273_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_9f28eecd-ca04-4e29-b4ca-3e0222ecc273_poly_0_">
<gml:posList srsDimension="3">
690939.268 5335948.062 515.0
690939.268 5335948.062 530.449
690946.344 5335945.255 530.486
690946.344 5335945.255 515.0
690939.268 5335948.062 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_f2aaba17-3725-44b7-be67-4848aa343abc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_f2aaba17-3725-44b7-be67-4848aa343abc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_f2aaba17-3725-44b7-be67-4848aa343abc_poly_0_">
<gml:posList srsDimension="3">
690937.457 5335943.417 532.794
690938.254 5335945.506 531.522
690938.275 5335945.561 531.499
690938.275 5335945.561 515.0
690937.457 5335943.417 515.0
690937.457 5335943.417 532.794
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_00bd225f-d96b-44d1-b9d4-a9f2e6b73fd4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_00bd225f-d96b-44d1-b9d4-a9f2e6b73fd4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_00bd225f-d96b-44d1-b9d4-a9f2e6b73fd4_poly_0_">
<gml:posList srsDimension="3">
690955.352 5335938.35 515.0
690955.352 5335938.35 531.849
690964.542 5335934.725 531.908
690964.542 5335934.725 515.0
690955.352 5335938.35 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57_poly_0_">
<gml:posList srsDimension="3">
690933.358 5335934.309 531.853
690935.53 5335939.7 535.16
690933.358 5335934.309 515.0
690933.358 5335934.309 531.853
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57_poly.rvELwtPVk5JtgwTPuA1Z">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57_poly_0_.eR3JIHxgPwhOcl4qkxZn">
<gml:posList srsDimension="3">
690937.082 5335943.553 515.0
690933.358 5335934.309 515.0
690935.53 5335939.7 535.16
690937.082 5335943.553 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57_poly.0JJti0J6teT8FP7DgkEl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_d69edc9b-9dbf-47eb-b862-df5a00e30f57_poly_0_.kVjcWbWcQ7B7EPk7SQQI">
<gml:posList srsDimension="3">
690937.082 5335943.553 532.798
690937.082 5335943.553 515.0
690935.53 5335939.7 535.16
690937.082 5335943.553 532.798
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c_poly_0_">
<gml:posList srsDimension="3">
690960.686 5335924.286 515.0
690963.482 5335931.361 515.0
690960.686 5335924.286 532.08
690960.686 5335924.286 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c_poly.3ntPXg9ibaWRFkRGDtBO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c_poly_0_.4MvJVWYx5PxADWzAZWQp">
<gml:posList srsDimension="3">
690962.675 5335929.32 535.16
690960.686 5335924.286 532.08
690963.482 5335931.361 515.0
690962.675 5335929.32 535.16
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c_poly.A80c2tkbVotybpS0gkfz">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_13122314-173e-44e8-bd32-94413563df7c_poly_0_.wcUdqr3DfqSD5tpYKzJs">
<gml:posList srsDimension="3">
690963.482 5335931.361 533.911
690962.675 5335929.32 535.16
690963.482 5335931.361 515.0
690963.482 5335931.361 533.911
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly_0_">
<gml:posList srsDimension="3">
690945.44 5335942.257 515.0
690945.613 5335942.692 515.0
690945.44 5335942.257 531.788
690945.44 5335942.257 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly.126uj1AqcOSLB3J5qZaI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly_0_.HswC2sb7B6XC1Dv050Ke">
<gml:posList srsDimension="3">
690945.613 5335942.692 531.521
690945.44 5335942.257 531.788
690945.613 5335942.692 515.0
690945.613 5335942.692 531.521
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly.Fhwxr0yylYHhaVSfE3oc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly_0_.YjDFOdRpQPPl51EmylN2">
<gml:posList srsDimension="3">
690946.589 5335945.155 515.0
690945.613 5335942.692 531.521
690945.613 5335942.692 515.0
690946.589 5335945.155 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly.5UmAVubi6h0fRzuyllUX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_f7ba5a58-64c3-4ab4-bb63-558f2596381e_poly_0_.hrSIiVeiVhbMnDGHosbC">
<gml:posList srsDimension="3">
690946.589 5335945.155 530.489
690945.613 5335942.692 531.521
690946.589 5335945.155 515.0
690946.589 5335945.155 530.489
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly_0_">
<gml:posList srsDimension="3">
690938.275 5335945.561 531.499
690939.232 5335947.97 530.488
690938.275 5335945.561 515.0
690938.275 5335945.561 531.499
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly.jv1CGdn4N8G0jRgy0SqG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly_0_.xp1INYmv9wzRwJ1OttaG">
<gml:posList srsDimension="3">
690939.232 5335947.97 515.0
690938.275 5335945.561 515.0
690939.232 5335947.97 530.488
690939.232 5335947.97 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly.z0fktJKqkPfq69rSIxT5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly_0_.hRs0ikxcUxVfx9CxCc4w">
<gml:posList srsDimension="3">
690939.268 5335948.062 530.449
690939.232 5335947.97 515.0
690939.232 5335947.97 530.488
690939.268 5335948.062 530.449
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly.8XIjPHkqS2OrgNZq4Kac">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_dfede2ca-6389-4e33-aec0-686eba575c03_poly_0_.lUAYGLe2kGJ1yDADDbLM">
<gml:posList srsDimension="3">
690939.268 5335948.062 515.0
690939.232 5335947.97 515.0
690939.268 5335948.062 530.449
690939.268 5335948.062 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906972_44bc4449-bdbe-4aaa-ad48-fdd7df66a1d9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906972_44bc4449-bdbe-4aaa-ad48-fdd7df66a1d9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906972_44bc4449-bdbe-4aaa-ad48-fdd7df66a1d9_poly_0_">
<gml:posList srsDimension="3">
690964.351 5335934.247 515.0
690963.249 5335931.452 515.0
690963.482 5335931.361 515.0
690960.686 5335924.286 515.0
690960.404 5335923.574 515.0
690954.044 5335926.008 515.0
690954.089 5335926.13 515.0
690939.628 5335931.661 515.0
690939.573 5335931.529 515.0
690933.222 5335933.954 515.0
690933.358 5335934.309 515.0
690937.082 5335943.553 515.0
690937.457 5335943.417 515.0
690938.275 5335945.561 515.0
690939.232 5335947.97 515.0
690939.268 5335948.062 515.0
690946.344 5335945.255 515.0
690946.589 5335945.155 515.0
690945.613 5335942.692 515.0
690945.44 5335942.257 515.0
690964.542 5335934.725 515.0
690964.351 5335934.247 515.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>