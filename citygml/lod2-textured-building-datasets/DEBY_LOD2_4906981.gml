<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-08-20 15:04:27 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906981.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690985.324 5335907.262 514.93</gml:lowerCorner>
         <gml:upperCorner>691027.291 5335975.275 537.87</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906981">
<gml:name>DEBY_LOD2_4906981</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJ0o</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906981/DSC04694.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906981_ac380bd2-4ec6-4bb3-95d8-0aa44360323a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906981_ac380bd2-4ec6-4bb3-95d8-0aa44360323a_poly_0_">
0.9992951073707692 0.0007220565666636379 0.9992951073707692 0.9991939546342791 0.0007051595251536271 0.9991409853028166 0.0007051595251553466 0.0007220565666636379 0.9992951073707692 0.0007220565666636379
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906981/DSC1.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.4RVxPEb4B3Sd1gUTDrXb">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.msgtoyloxXojZcVPLqS2">
0.0005180713194443687 0.00039791198352012497 0.9948657522098645 0.00039791198352012497 0.9997840262089045 0.999413912254714 0.0005180713194443687 0.00039791198352012497
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906981/DSC04697.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.U6uY2tnEE4q9D5qvRJiI">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.sfNL7V7Qz5v4dMSzZfnI">
0.0005155688628667443 0.0005236894691987275 0.018816810911603477 0.0005236894691987119 0.9996041343504993 0.9994194760390561 0.0005155688628667443 0.0005236894691987275
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906981/C__Users_ge96wis_Documents_ProjezierteGML_DEBY_LOD2_4906981_C__Users_ge96wis_Documents_ProjezierteGML_DEBY_LOD2_4906981_6081ganzeFassade.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906981_0e2d5fa6-049d-4887-bcbb-ec1b76b21d0b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906981_0e2d5fa6-049d-4887-bcbb-ec1b76b21d0b_poly_0_">
0.9993053721328228 0.00800695691629968 0.9993053721328228 0.998223521753276 0.00042411436475703335 1.0022143727137216 0.00042411436475701947 0.011840188643206512 0.9993053721328228 0.00800695691629968
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906981/DSC1_uU4Afl7wG5moMSDu5qAO.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.UPaoogyaEgRM5cksmiIv">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.Q71Y2S0UKQKK2mDdvahx">
0.0003925509429271168 0.9944724374396307 0.0003925509429271168 0.0005209756025255426 0.9994879088229401 0.9997989667358341 0.0003925509429271168 0.9944724374396307
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3200</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">22.94</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906981_2e222a98-e3cc-4be8-9db2-9d88d67e07fb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_2e222a98-e3cc-4be8-9db2-9d88d67e07fb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_2e222a98-e3cc-4be8-9db2-9d88d67e07fb_poly_0_">
<gml:posList srsDimension="3">
691014.13 5335962.554 537.87
691026.962 5335968.321 533.919
691009.391 5335975.241 533.919
691014.13 5335962.554 537.87
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly_0_">
<gml:posList srsDimension="3">
690985.416 5335914.576 533.879
690997.65 5335920.687 537.87
690985.505 5335914.805 533.879
690985.416 5335914.576 533.879
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly.OYLDBGVlcdN0lTtQbhy5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly_0_.PAyObtFPgfpOVeobzXsq">
<gml:posList srsDimension="3">
691007.684 5335971.159 533.878
690985.505 5335914.805 533.879
690997.65 5335920.687 537.87
691007.684 5335971.159 533.878
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly.PntPUbvAA0U2yw7tHmIR">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly_0_.72p06n2ATGWgaT5sR77W">
<gml:posList srsDimension="3">
691014.13 5335962.554 537.87
691007.684 5335971.159 533.878
690997.65 5335920.687 537.87
691014.13 5335962.554 537.87
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly.2QSNFkSioffCHBUkWaP1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly_0_.a5I1KfKIk4Fs0nxZEVM0">
<gml:posList srsDimension="3">
691009.14 5335974.859 533.878
691007.684 5335971.159 533.878
691014.13 5335962.554 537.87
691009.14 5335974.859 533.878
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly.4YyJWWFZUPOIpZs94tLF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly_0_.xJBmgDkhKET80W136F4x">
<gml:posList srsDimension="3">
691009.304 5335975.275 533.878
691009.14 5335974.859 533.878
691014.13 5335962.554 537.87
691009.304 5335975.275 533.878
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly.lIHOr0fBXEk3wgJzEjmu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_53cfbefa-7bc5-4edf-8c59-aa0a74b3720c_poly_0_.Y1j3YYScKtTkHK8UjGhP">
<gml:posList srsDimension="3">
691009.391 5335975.241 533.919
691009.304 5335975.275 533.878
691014.13 5335962.554 537.87
691009.391 5335975.241 533.919
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906981_95cdb879-fe97-4f3c-abb7-751f4a5bfab8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_95cdb879-fe97-4f3c-abb7-751f4a5bfab8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_95cdb879-fe97-4f3c-abb7-751f4a5bfab8_poly_0_">
<gml:posList srsDimension="3">
690985.324 5335914.338 533.779
691003.292 5335907.262 533.78
690985.416 5335914.576 533.879
690985.324 5335914.338 533.779
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_95cdb879-fe97-4f3c-abb7-751f4a5bfab8_poly.KGHsCJHIM5uM1YERYII5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_95cdb879-fe97-4f3c-abb7-751f4a5bfab8_poly_0_.af4Oema7OGPRNKZm1LNv">
<gml:posList srsDimension="3">
690997.65 5335920.687 537.87
690985.416 5335914.576 533.879
691003.292 5335907.262 533.78
690997.65 5335920.687 537.87
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b_poly_0_">
<gml:posList srsDimension="3">
690997.65 5335920.687 537.87
691003.292 5335907.262 533.78
691014.13 5335962.554 537.87
690997.65 5335920.687 537.87
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b_poly.UJnzg4edBt7ClRPC3gUF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b_poly_0_.0n35olnnn2Ls97dO60He">
<gml:posList srsDimension="3">
691027.291 5335968.19 533.777
691014.13 5335962.554 537.87
691003.292 5335907.262 533.78
691027.291 5335968.19 533.777
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b_poly.JYMjQma5UJ1kuTZQT7Sy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_50e91b6e-0073-47e6-a9e2-674ee312369b_poly_0_.MBt5XPeldiWcHVfMOTvN">
<gml:posList srsDimension="3">
691026.962 5335968.321 533.919
691014.13 5335962.554 537.87
691027.291 5335968.19 533.777
691026.962 5335968.321 533.919
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_fd888d80-caed-4253-9cd1-a4156bb02944">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_fd888d80-caed-4253-9cd1-a4156bb02944_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_fd888d80-caed-4253-9cd1-a4156bb02944_poly_0_">
<gml:posList srsDimension="3">
690989.3 5335924.442 514.93
690989.3 5335924.442 533.881
691002.694 5335958.473 533.88
691002.694 5335958.473 514.93
690989.3 5335924.442 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_d87ea239-cf0e-4c3c-bb3a-eb67088e130b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d87ea239-cf0e-4c3c-bb3a-eb67088e130b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d87ea239-cf0e-4c3c-bb3a-eb67088e130b_poly_0_">
<gml:posList srsDimension="3">
691007.684 5335971.159 514.93
691007.684 5335971.159 533.878
691009.14 5335974.859 533.878
691009.14 5335974.859 514.93
691007.684 5335971.159 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_ef35c5e3-99ed-4419-b1d9-7227581edc50">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_ef35c5e3-99ed-4419-b1d9-7227581edc50_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_ef35c5e3-99ed-4419-b1d9-7227581edc50_poly_0_">
<gml:posList srsDimension="3">
690985.505 5335914.805 514.93
690985.505 5335914.805 533.879
690989.3 5335924.442 533.881
690989.3 5335924.442 514.93
690985.505 5335914.805 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_b13348ca-d6ad-49a2-962f-01c6e85bea15">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_b13348ca-d6ad-49a2-962f-01c6e85bea15_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_b13348ca-d6ad-49a2-962f-01c6e85bea15_poly_0_">
<gml:posList srsDimension="3">
691002.694 5335958.473 514.93
691002.694 5335958.473 533.88
691007.684 5335971.159 533.878
691007.684 5335971.159 514.93
691002.694 5335958.473 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_d9282039-bed3-4881-b5b2-5bd4c3b71533">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d9282039-bed3-4881-b5b2-5bd4c3b71533_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d9282039-bed3-4881-b5b2-5bd4c3b71533_poly_0_">
<gml:posList srsDimension="3">
691009.14 5335974.859 514.93
691009.14 5335974.859 533.878
691009.304 5335975.275 533.878
691009.304 5335975.275 514.93
691009.14 5335974.859 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_ac380bd2-4ec6-4bb3-95d8-0aa44360323a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_ac380bd2-4ec6-4bb3-95d8-0aa44360323a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_ac380bd2-4ec6-4bb3-95d8-0aa44360323a_poly_0_">
<gml:posList srsDimension="3">
691003.292 5335907.262 514.93
691003.292 5335907.262 533.78
690985.324 5335914.338 533.779
690985.324 5335914.338 514.93
691003.292 5335907.262 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly_0_">
<gml:posList srsDimension="3">
690985.324 5335914.338 533.779
690985.416 5335914.576 533.879
690985.324 5335914.338 514.93
690985.324 5335914.338 533.779
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly.fBZeseIxihMG0uoDroH1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly_0_.a6gVITLVis4ww0DOK1m0">
<gml:posList srsDimension="3">
690985.416 5335914.576 514.93
690985.324 5335914.338 514.93
690985.416 5335914.576 533.879
690985.416 5335914.576 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly.IBVVhhrXdZbGCI7u5aWo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly_0_.PIFebbfm4Q6jt9hEBHtp">
<gml:posList srsDimension="3">
690985.505 5335914.805 533.879
690985.416 5335914.576 514.93
690985.416 5335914.576 533.879
690985.505 5335914.805 533.879
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly.j0AToB142zeBvmvmOC9m">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_54204c75-af64-46f8-a55e-274cdfc5a816_poly_0_.BsYaGobXthmdyDSFwWtY">
<gml:posList srsDimension="3">
690985.505 5335914.805 514.93
690985.416 5335914.576 514.93
690985.505 5335914.805 533.879
690985.505 5335914.805 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_">
<gml:posList srsDimension="3">
691009.304 5335975.275 514.93
691009.304 5335975.275 533.878
691009.391 5335975.241 514.93
691009.304 5335975.275 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.4RVxPEb4B3Sd1gUTDrXb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.msgtoyloxXojZcVPLqS2">
<gml:posList srsDimension="3">
691026.962 5335968.321 514.93
691009.391 5335975.241 514.93
691009.304 5335975.275 533.878
691026.962 5335968.321 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.U6uY2tnEE4q9D5qvRJiI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.sfNL7V7Qz5v4dMSzZfnI">
<gml:posList srsDimension="3">
691027.291 5335968.19 514.93
691026.962 5335968.321 514.93
691009.304 5335975.275 533.878
691027.291 5335968.19 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.uirQ7mK7abKltEHDKV89">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.Dqg7SMLbvwzzOLJYevIj">
<gml:posList srsDimension="3">
691026.962 5335968.321 533.919
691027.291 5335968.19 533.777
691009.304 5335975.275 533.878
691026.962 5335968.321 533.919
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.WSdYUD5u3ya2YaQYZHJy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.ifSidPpPYHwABs58R1Io">
<gml:posList srsDimension="3">
691009.391 5335975.241 533.919
691026.962 5335968.321 533.919
691009.304 5335975.275 533.878
691009.391 5335975.241 533.919
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly.UPaoogyaEgRM5cksmiIv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_d234e9f5-0375-494c-87fa-b2d8d6329d67_poly_0_.Q71Y2S0UKQKK2mDdvahx">
<gml:posList srsDimension="3">
691027.291 5335968.19 533.777
691027.291 5335968.19 514.93
691009.304 5335975.275 533.878
691027.291 5335968.19 533.777
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906981_0e2d5fa6-049d-4887-bcbb-ec1b76b21d0b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_0e2d5fa6-049d-4887-bcbb-ec1b76b21d0b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_0e2d5fa6-049d-4887-bcbb-ec1b76b21d0b_poly_0_">
<gml:posList srsDimension="3">
691027.291 5335968.19 514.93
691027.291 5335968.19 533.777
691003.292 5335907.262 533.78
691003.292 5335907.262 514.93
691027.291 5335968.19 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906981_ba617113-8bba-4103-abb0-a026f563b495">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906981_ba617113-8bba-4103-abb0-a026f563b495_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906981_ba617113-8bba-4103-abb0-a026f563b495_poly_0_">
<gml:posList srsDimension="3">
691003.292 5335907.262 514.93
690985.324 5335914.338 514.93
690985.416 5335914.576 514.93
690985.505 5335914.805 514.93
691007.684 5335971.159 514.93
691009.14 5335974.859 514.93
691009.304 5335975.275 514.93
691009.391 5335975.241 514.93
691026.962 5335968.321 514.93
691027.291 5335968.19 514.93
691003.292 5335907.262 514.93
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>