<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-25 21:41:13 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_60042.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690876.7 5336096.008 510.36</gml:lowerCorner>
         <gml:upperCorner>691006.1 5336191.762 537.18</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_60042">
<gml:name>DEBY_LOD2_60042</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJMK</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a4347fd6-711b-4636-974d-0d435cfe92ce_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a4347fd6-711b-4636-974d-0d435cfe92ce_poly_0_">
0.9083521884384167 0.0007036458734834435 0.9083521884384167 0.999296106437037 0.07593235952542976 0.999296106437037 0.07593235952542976 0.0007036458734834435 0.9083521884384167 0.0007036458734834435
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_abc514c6-0691-4103-9f67-fd47781219d9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_abc514c6-0691-4103-9f67-fd47781219d9_poly_0_">
0.9663187174400321 0.0006454195107557818 0.9663187174400321 0.9993543721051951 0.03549095138939151 0.9993543721051951 0.03549095138939151 0.0006454195107557818 0.9663187174400321 0.0006454195107557818
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_901e62c1-3d71-4546-992d-9f343cd226c7_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_901e62c1-3d71-4546-992d-9f343cd226c7_poly_0_">
0.9639139038680078 0.0006830030814162773 0.9639139038680078 0.99931676355243 0.03756653320460046 0.99931676355243 0.03756653320460046 0.0006830030814162773 0.9639139038680078 0.0006830030814162773
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_c8478e5d-0a38-43de-a74a-58ed51d33f55_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_c8478e5d-0a38-43de-a74a-58ed51d33f55_poly_0_">
0.9753921672105434 0.0006457302918379836 0.9753921672105434 0.9993540611233347 0.03728553159443493 0.9993540611233347 0.03728553159443493 0.0006457302918379836 0.9753921672105434 0.0006457302918379836
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_m7LJFRDvZsr0sXYoMWyf.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d4d67b04-034c-40d0-bd8f-fe3eea857cd9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d4d67b04-034c-40d0-bd8f-fe3eea857cd9_poly_0_">
0.9095810783370553 0.000692979724587933 0.9095810783370553 0.9993067800400969 0.0702664478308801 0.9993067800400969 0.0702664478308801 0.000692979724587933 0.9095810783370553 0.000692979724587933
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_Dg0rLntru12BpBJOmRPd.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_badd4ae8-47a0-4bad-9ff2-13148c757b29_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_badd4ae8-47a0-4bad-9ff2-13148c757b29_poly_0_">
0.9969058806491384 0.0006396923375276462 0.9969058806491384 0.9993601029611144 0.004687974862344291 0.9993601029611144 0.004687974862344291 0.0006396923375276462 0.9969058806491384 0.0006396923375276462
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_aG6cUuknTUWDIOnVAVrO.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_8d50d6ed-f78c-4b19-8e14-8b9404b75007_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_8d50d6ed-f78c-4b19-8e14-8b9404b75007_poly_0_">
0.9095638767512417 0.0006994101675244796 0.9095638767512417 0.9993003451168094 0.07559918733035964 0.9993003451168094 0.07559918733035964 0.0006994101675244796 0.9095638767512417 0.0006994101675244796
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_6aWC8YwpnQGXnqJpquiX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_764bac12-c58a-4b19-95c4-e7766080dccd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_764bac12-c58a-4b19-95c4-e7766080dccd_poly_0_">
0.9942362722749047 0.0006375437195124642 0.9942362722749047 0.9993622529522621 0.0046976489555836665 0.9993622529522621 0.0046976489555836665 0.0006375437195124642 0.9942362722749047 0.0006375437195124642
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_iYlA1AuAi2Mk6w4iXQhM.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_78c7982e-797b-41e1-af6e-6d73c7a18ed6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_78c7982e-797b-41e1-af6e-6d73c7a18ed6_poly_0_">
0.9799251255495278 0.0006936256235801774 0.9799251255495278 0.9993061336929516 0.023723750327718562 0.9993061336929516 0.023723750327718562 0.0006936256235801774 0.9799251255495278 0.0006936256235801774
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_PwabPPpivwy3hI4GRCwt.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_4db14a6a-4994-4541-a2b1-6b13942aaee1_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_4db14a6a-4994-4541-a2b1-6b13942aaee1_poly_0_">
0.9526488659027876 0.0006884240732838787 0.9526488659027876 0.9993113388404448 0.03697071576775102 0.9993113388404448 0.03697071576775102 0.0006884240732838787 0.9526488659027876 0.0006884240732838787
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_O5szq4LOzqjV2Ho4gF8D.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_f0cc72ca-b1b3-461b-95b6-6fd9e433cd82_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_f0cc72ca-b1b3-461b-95b6-6fd9e433cd82_poly_0_">
0.9491926967748245 0.0007137289009543105 0.9491926967748245 0.9992860162581502 0.07541685912428697 0.9992860162581502 0.07541685912428697 0.0007137289009543105 0.9491926967748245 0.0007137289009543105
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_NmjDZxkhSomVbFNZWONs.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1bfb2600-03cb-4ca9-8d97-3ae008983449_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1bfb2600-03cb-4ca9-8d97-3ae008983449_poly_0_">
0.9519788792364849 0.0007038486459491716 0.9519788792364849 0.9992959035217576 0.07464072509013775 0.9992959035217576 0.07464072509013775 0.0007038486459491716 0.9519788792364849 0.0007038486459491716
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1d8ea246-b978-4b62-b0a9-255899818de3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1d8ea246-b978-4b62-b0a9-255899818de3_poly_0_">
0.9756750795146587 0.0006583589512453997 0.9756750795146587 0.9993414242234326 0.0370106220681663 0.9993414242234326 0.0370106220681663 0.0006583589512453997 0.9756750795146587 0.0006583589512453997
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_4YVIP33a1vIWAonXVzSn.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_b07f8fd7-d85d-46de-9275-28d7515fecbd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_b07f8fd7-d85d-46de-9275-28d7515fecbd_poly_0_">
0.9734485341849677 0.0006753604797570638 0.9734485341849677 0.9993244113487741 0.0223607906903851 0.9993244113487741 0.0223607906903851 0.0006753604797570638 0.9734485341849677 0.0006753604797570638
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_21dbf83a-67cf-4b03-80be-ec40113eb633_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_21dbf83a-67cf-4b03-80be-ec40113eb633_poly_0_">
0.9852720767075565 0.0006801929138105934 0.9852720767075565 0.99931957563691 0.02309721483274796 0.99931957563691 0.02309721483274796 0.0006801929138105934 0.9852720767075565 0.0006801929138105934
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_36iZUvvN6x7YPxZkSuDv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_fc09151a-af8c-4f0f-9848-2d818b9527b0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_fc09151a-af8c-4f0f-9848-2d818b9527b0_poly_0_">
0.9756762337064231 0.0006999803124502868 0.9756762337064231 0.9992997745726416 0.021840476255107433 0.9992997745726416 0.021840476255107433 0.0006999803124502868 0.9756762337064231 0.0006999803124502868
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_ze02vyjLMa9CO35JhCta.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a2df7afe-7d7b-4849-92ba-cc1190cf2788_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a2df7afe-7d7b-4849-92ba-cc1190cf2788_poly_0_">
0.9787963973853095 0.0006641620304531965 0.9787963973853095 0.9992118360822226 0.02534286407362174 0.9859113204899475 0.02534286407362174 0.0006641620304531965 0.9787963973853095 0.0006641620304531965
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_BCKDmmFqwAYDi2dZHgj0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a464434a-5692-452a-985c-0588ea813001_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a464434a-5692-452a-985c-0588ea813001_poly_0_">
0.9665532266879495 0.0006326618508358992 0.9665532266879495 0.999367137923644 0.035770352896463464 0.999367137923644 0.035770352896463464 0.0006326618508358992 0.9665532266879495 0.0006326618508358992
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_pMWfUPrttvCadvh0pB74.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_ec3ea692-af82-446b-9bf9-01e4595c5f5d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_ec3ea692-af82-446b-9bf9-01e4595c5f5d_poly_0_">
0.99877376698291 0.0007015097201820766 0.99877376698291 0.9992982440923394 0.0010660208319484 0.9992982440923394 0.0010660208319484 0.0007015097201820766 0.99877376698291 0.0007015097201820766
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_6Re3RBoOTvTq2DZwmEZB.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_8d20415c-a328-468e-bcd8-c7eb29b4b91f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_8d20415c-a328-468e-bcd8-c7eb29b4b91f_poly_0_">
0.9163145569618871 0.0007090423523270115 0.9163145569618871 0.9992907061433911 0.06923376084995247 0.9992907061433911 0.06923376084995247 0.0007090423523270115 0.9163145569618871 0.0007090423523270115
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_GtSU66jT3Qxq3751o6gv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_82d66082-4819-45d0-a1d0-74e723bc3c3c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_82d66082-4819-45d0-a1d0-74e723bc3c3c_poly_0_">
0.9197662165842839 0.0006557184502079844 0.9197662165842839 0.9993440664606648 0.06740039752453342 0.9993440664606648 0.06740039752453342 0.0006557184502079844 0.9197662165842839 0.0006557184502079844
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_Nr4VG76riFsy5HOYMbMc.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_6d522486-b184-45f6-ab34-806a4b751503_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_6d522486-b184-45f6-ab34-806a4b751503_poly_0_">
0.993620296086899 0.0006353680366362794 0.993620296086899 0.9993644300208568 0.0046418073521365955 0.9993644300208568 0.0046418073521365955 0.0006353680366362794 0.993620296086899 0.0006353680366362794
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_4lylktYUaJjiqDodVANQ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_0b9e5de2-b639-4ab8-9e6a-9a404ad3d79a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_0b9e5de2-b639-4ab8-9e6a-9a404ad3d79a_poly_0_">
0.9950323604508338 0.0006611979825775615 0.9950323604508338 0.9993385833175775 0.004846643381750226 0.9993385833175775 0.004846643381750226 0.0006611979825775615 0.9950323604508338 0.0006611979825775615
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_xTkskVfcGfCVazxd4Fpl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_6f9d34de-dd1c-4b4a-8185-2874e1622893_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_6f9d34de-dd1c-4b4a-8185-2874e1622893_poly_0_">
0.9601399685446381 0.0006256503234962215 0.9601399685446381 0.9993741538654536 0.033848679963426775 0.9993741538654536 0.033848679963426775 0.0006256503234962215 0.9601399685446381 0.0006256503234962215
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_s2w9PUDgVhEPbKtbUOlR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_2d20721f-7436-4609-ade8-0577b50d3435_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_2d20721f-7436-4609-ade8-0577b50d3435_poly_0_">
0.9949354473672471 0.0006494599339602259 0.9949354473672471 0.9993503290641533 0.004741501282040872 0.9993503290641533 0.004741501282040872 0.0006494599339602259 0.9949354473672471 0.0006494599339602259
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_c1Aga3LuYZlEUR0BsvyZ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_5636b5c3-87d1-4273-add6-6845d2b44b53_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_5636b5c3-87d1-4273-add6-6845d2b44b53_poly_0_">
0.9114251556315667 0.0007055371595848459 0.9114251556315667 0.9992942138172943 0.07675451885963014 0.9992942138172943 0.07675451885963014 0.0007055371595848459 0.9114251556315667 0.0007055371595848459
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_D248PL9dqcFwa2sU0whi.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_722fffd4-8a68-4f40-b4ab-7bce5db89823_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_722fffd4-8a68-4f40-b4ab-7bce5db89823_poly_0_">
0.9749036940353619 0.0006959982670826381 0.9749036940353619 0.9993037593996185 0.023976171842775784 0.9993037593996185 0.023976171842775784 0.0006959982670826381 0.9749036940353619 0.0006959982670826381
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_BVwzOjn8Ija9G6WDfFw2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_b668b56c-6c96-4638-b72e-7a744ab85b81_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_b668b56c-6c96-4638-b72e-7a744ab85b81_poly_0_">
0.9101011490497171 0.0006929042034117867 0.9101011490497171 0.9993068556136457 0.0748427056954597 0.9993068556136457 0.0748427056954597 0.0006929042034117867 0.9101011490497171 0.0006929042034117867
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_fiX9p3zpXlbdcMzjCVQ0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_135c641a-db79-4bbf-8501-f393a5020067_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_135c641a-db79-4bbf-8501-f393a5020067_poly_0_">
0.9944284378614583 0.0006381169623498007 0.9944284378614583 0.9993616793435308 0.0046697577775800525 0.9993616793435308 0.0046697577775800525 0.0006381169623498007 0.9944284378614583 0.0006381169623498007
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_tXVJZvAzu3WL1Hlx8zut.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_237e2a2a-7782-4d63-b9a1-180cff4701f2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_237e2a2a-7782-4d63-b9a1-180cff4701f2_poly_0_">
0.9745550222991994 0.0007130501837341232 0.9745550222991994 0.999286695459949 0.024582969578972325 0.999286695459949 0.024582969578972325 0.0007130501837341232 0.9745550222991994 0.0007130501837341232
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_TqlS35z8Po67k8eUw930.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_362f317d-ede2-4840-b36c-30685fff960f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_362f317d-ede2-4840-b36c-30685fff960f_poly_0_">
0.9536267172197483 0.0006348218902663636 0.9536267172197483 0.9993649765143294 0.03490592554860328 0.9993649765143294 0.03490592554860328 0.0006348218902663636 0.9536267172197483 0.0006348218902663636
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_f6X1G01ynrnEksQ3ED7k.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_600912e5-568c-4b9b-a109-9cdadaed2526_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_600912e5-568c-4b9b-a109-9cdadaed2526_poly_0_">
0.9967433500241665 0.0006878770754750874 0.9967433500241665 0.9993118862149623 0.005043920055193496 0.9993118862149623 0.005043920055193496 0.0006878770754750874 0.9967433500241665 0.0006878770754750874
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_zb2QRisX9UDOSW1gar0C.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_bb2d7263-b2e9-47ee-b4bf-42dddf9acef2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_bb2d7263-b2e9-47ee-b4bf-42dddf9acef2_poly_0_">
0.9640180333470738 0.0006875429695832282 0.9640180333470738 0.9993122205507998 0.03784983489519789 0.9993122205507998 0.03784983489519789 0.0006875429695832282 0.9640180333470738 0.0006875429695832282
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_T1HOrtbDluyCdqTYHFJw.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_0b28e24d-24ab-4e7d-82bc-e410f765cb1f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_0b28e24d-24ab-4e7d-82bc-e410f765cb1f_poly_0_">
0.9168860478231551 0.0006866611700669928 0.9168860478231551 0.9993131029566711 0.0752521233344341 0.9993131029566711 0.0752521233344341 0.0006866611700669928 0.9168860478231551 0.0006866611700669928
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_qdLAroUYmEGDtXmRpZFJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_5a466065-0b26-44d6-af31-605584ad1dee_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_5a466065-0b26-44d6-af31-605584ad1dee_poly_0_">
0.9953370072236307 0.0006858848194699784 0.9953370072236307 0.999313879840468 0.005051261928283424 0.999313879840468 0.005051261928283424 0.0006858848194699784 0.9953370072236307 0.0006858848194699784
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_UExXojh3MReTscDie9Ge.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_842a21e4-5df1-44ba-a1da-0c1f2750a7a7_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_842a21e4-5df1-44ba-a1da-0c1f2750a7a7_poly_0_">
0.9598187546327352 0.0006787741434094747 0.9598187546327352 0.9991436606638873 0.030033210702640645 0.9993288359024852 0.030033210702640645 0.0006787741434094747 0.9598187546327352 0.0006787741434094747
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_JBLarNF6NaTCPkGFVQJA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_828d55f9-31bb-4346-90f9-94641b423abb_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_828d55f9-31bb-4346-90f9-94641b423abb_poly_0_">
0.9964559505860624 0.0006565050668515536 0.9964559505860624 0.9993432793275316 0.004808308024935926 0.9993432793275316 0.004808308024935926 0.0006565050668515536 0.9964559505860624 0.0006565050668515536
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_daCKBBXRxQotryMS6hDF.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_404e9074-55fc-4481-a6f4-37d4ad2792f3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_404e9074-55fc-4481-a6f4-37d4ad2792f3_poly_0_">
0.9935107878708163 0.0006529908549703669 0.9935107878708163 0.999346795842032 0.004788353009088198 0.999346795842032 0.004788353009088198 0.0006529908549703669 0.9935107878708163 0.0006529908549703669
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_rz9wJnvoRSzikux4icOO.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_f2051be9-1acb-4b8b-9b3f-3270cdea8c7a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_f2051be9-1acb-4b8b-9b3f-3270cdea8c7a_poly_0_">
0.9688194265742283 0.0006363512955937021 0.9688194265742283 0.9993634461362372 0.034154028430400274 0.9993634461362372 0.034154028430400274 0.0006363512955937021 0.9688194265742283 0.0006363512955937021
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_t2rfOLj5Y94uPB4iOeOW.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_8cfab906-c11a-4082-96d4-abe78dbda8be_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_8cfab906-c11a-4082-96d4-abe78dbda8be_poly_0_">
0.9941639027914917 0.000685254152959745 0.9941639027914917 0.9993145109396778 0.0050529349150814085 0.9993145109396778 0.0050529349150814085 0.000685254152959745 0.9941639027914917 0.000685254152959745
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_6DfIqkHBE6EJt8ha44Lx.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_e70e7f60-20d9-48de-986b-7801de3656c4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_e70e7f60-20d9-48de-986b-7801de3656c4_poly_0_">
0.947744705871516 0.0007188834235147433 0.947744705871516 0.9992808580403956 0.07581932502415079 0.9992808580403956 0.07581932502415079 0.0007188834235147433 0.947744705871516 0.0007188834235147433
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_vLFymxJcaeTabLlZ4pTM.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_ef5aa5e7-9d48-4d1b-82c7-22564e1b5328_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_ef5aa5e7-9d48-4d1b-82c7-22564e1b5328_poly_0_">
0.9655571897531132 0.0006394911596637527 0.9655571897531132 0.9993603042677427 0.03409654131686324 0.9993603042677427 0.03409654131686324 0.0006394911596637527 0.9655571897531132 0.0006394911596637527
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_oVM9rqitVgOxuRD7RpaQ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_74c4660c-bccb-4527-95ec-ecb924fabbf6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_74c4660c-bccb-4527-95ec-ecb924fabbf6_poly_0_">
0.996649095713023 0.0006564716830306187 0.996649095713023 0.9993433127332849 0.00482732280205056 0.9993433127332849 0.00482732280205056 0.0006564716830306187 0.996649095713023 0.0006564716830306187
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_Syu4CcXQPcKmy7j4sRYP.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_078201c5-a342-4043-af1f-5d5e756c3c6e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_078201c5-a342-4043-af1f-5d5e756c3c6e_poly_0_">
0.9754531662564432 0.0006751849819957618 0.9754531662564432 0.9993245869651346 0.022839318789436902 0.9993245869651346 0.022839318789436902 0.0006751849819957618 0.9754531662564432 0.0006751849819957618
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_ugznNGVPRHcJxVkcSgbv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_955e4ee9-bae4-43fe-ac8b-3bf1bcb4f157_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_955e4ee9-bae4-43fe-ac8b-3bf1bcb4f157_poly_0_">
0.9828810267947574 0.0006971271496875628 0.9828810267947574 0.9993026297300595 0.024424616423203815 0.9993026297300595 0.024424616423203815 0.0006971271496875628 0.9828810267947574 0.0006971271496875628
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_rnmktriyq0VFISTmfwtg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_c937c608-cda3-4144-9ae3-c77725ba1d6c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_c937c608-cda3-4144-9ae3-c77725ba1d6c_poly_0_">
0.9949087440463518 0.0006349797812108296 0.9949087440463518 0.9993648185230681 0.0046679132904432485 0.9993648185230681 0.0046679132904432485 0.0006349797812108296 0.9949087440463518 0.0006349797812108296
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_XaHGiTxKn9Cve8bQuOGo.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_be4d5d00-cec3-4f2e-8285-5f0692a3e9d3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_be4d5d00-cec3-4f2e-8285-5f0692a3e9d3_poly_0_">
0.9701089171429089 0.0007131361997785628 0.9701089171429089 0.9992866093825178 0.024794726889865615 0.9992866093825178 0.024794726889865615 0.0007131361997785628 0.9701089171429089 0.0007131361997785628
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_pXiw501aNSMEP3YreXUQ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_68ce5193-a72e-4768-b3fb-93fcd016382b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_68ce5193-a72e-4768-b3fb-93fcd016382b_poly_0_">
0.9742939968900544 0.0006671654212281648 0.9742939968900544 0.9993326119124997 0.032716626519885494 0.9993326119124997 0.032716626519885494 0.0006671654212281648 0.9742939968900544 0.0006671654212281648
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_Mq2iFSajYRxyAeX1hFYo.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_db410556-7e76-48b5-a112-cd516501585b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_db410556-7e76-48b5-a112-cd516501585b_poly_0_">
0.9134892427090673 0.0006785978052173154 0.9134892427090673 0.9993211718300415 0.06917039019195137 0.9993211718300415 0.06917039019195137 0.0006785978052173154 0.9134892427090673 0.0006785978052173154
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_r6foqNTuNuDyivz7yUZs.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_46c4b230-751f-4901-9436-aad469967656_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_46c4b230-751f-4901-9436-aad469967656_poly_0_">
0.9149843906464081 0.0006731236102933586 0.9149843906464081 0.9993266497275741 0.0687498546186518 0.9993266497275741 0.0687498546186518 0.0006731236102933586 0.9149843906464081 0.0006731236102933586
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_4ra9DiJhgFqqAGjc24zQ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_3866203e-c93a-40ea-955d-5be859831a0c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_3866203e-c93a-40ea-955d-5be859831a0c_poly_0_">
0.910950364887718 0.0006929078764040255 0.910950364887718 0.9993068519381063 0.07049074894668195 0.9993068519381063 0.07049074894668195 0.0006929078764040255 0.910950364887718 0.0006929078764040255
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_LII1doZXYTwrK90qVSMv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_f4fd1a42-5073-49a4-9c8c-6fda5070a5d5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_f4fd1a42-5073-49a4-9c8c-6fda5070a5d5_poly_0_">
0.9233693118777442 0.0006949034991768483 0.9233693118777442 0.9993048549294778 0.06873446063554489 0.9993048549294778 0.06873446063554489 0.0006949034991768483 0.9233693118777442 0.0006949034991768483
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_V1cIJtAEZMeEnSq3cGkg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_8995afaa-cbe1-479e-a2e9-17fb461f00f2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_8995afaa-cbe1-479e-a2e9-17fb461f00f2_poly_0_">
0.9144363048646262 0.0006330800899400659 0.9144363048646262 0.99936671941966 0.06784122205202081 0.99936671941966 0.06784122205202081 0.0006330800899400659 0.9144363048646262 0.0006330800899400659
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_wtDI2h9sVqNyzWpI1IN9.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_862688db-7de6-4f6c-8d9f-b283acbda048_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_862688db-7de6-4f6c-8d9f-b283acbda048_poly_0_">
0.9960515883415786 0.0006824053819831177 0.9960515883415786 0.9993173616602288 0.005050909822014837 0.9993173616602288 0.005050909822014837 0.0006824053819831177 0.9960515883415786 0.0006824053819831177
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_Kju6GLShDAIpzFE84S1Y.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_3fb07a21-02d8-4db9-8f73-45ac3d998879_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_3fb07a21-02d8-4db9-8f73-45ac3d998879_poly_0_">
0.9262973815167488 0.000675965482720521 0.9262973815167488 0.9993238059367217 0.0669726582507355 0.9993238059367217 0.0669726582507355 0.000675965482720521 0.9262973815167488 0.000675965482720521
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/ZwischenBrücken1.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a60caa0d-ee7d-4fdf-9933-70c9de698c56_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a60caa0d-ee7d-4fdf-9933-70c9de698c56_poly_0_">
0.9989893068724498 0.001977374348961195 0.9989893068724498 0.9956309717538371 0.001242050922371618 0.9983004751421127 0.001242050922371618 0.001977374348961195 0.9989893068724498 0.001977374348961195
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_mvOZfJL51Qo1HSKJWEVA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_621b7a8e-1059-41ed-b447-b11ce9a704e8_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_621b7a8e-1059-41ed-b447-b11ce9a704e8_poly_0_">
0.9765344180971454 0.0006928515453764618 0.9765344180971454 0.9993069083081949 0.02394342744796063 0.9993069083081949 0.02394342744796063 0.0006928515453764618 0.9765344180971454 0.0006928515453764618
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_atBmpKKGDFzOAHtgRX8l.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d41551bb-cee8-48bb-aeb9-ffa4c617cd34_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d41551bb-cee8-48bb-aeb9-ffa4c617cd34_poly_0_">
0.9721046104291133 0.0007063914794650579 0.9721046104291133 0.9992933588938153 0.02464953419010385 0.9992933588938153 0.02464953419010385 0.0007063914794650579 0.9721046104291133 0.0007063914794650579
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_1WTdNStrz5YTHdPUHkZP.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_c1fe3f9e-6304-4a33-9155-8d8385f3cd36_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_c1fe3f9e-6304-4a33-9155-8d8385f3cd36_poly_0_">
0.9689358675402673 0.000705339270096531 0.9689358675402673 0.9992944118464925 0.024012182432443296 0.9992944118464925 0.024012182432443296 0.000705339270096531 0.9689358675402673 0.000705339270096531
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_feo7NEVWbaC7Sv8b1AJ1.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_dabcf386-6ba0-4d5e-b47c-ca9dccc306cf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_dabcf386-6ba0-4d5e-b47c-ca9dccc306cf_poly_0_">
0.9698541007602159 0.0006819783955877105 0.9698541007602159 0.9993177889381345 0.03692275601345685 0.9993177889381345 0.03692275601345685 0.0006819783955877105 0.9698541007602159 0.0006819783955877105
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_80TrPtdBrEJWa0KIBM0t.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a6460904-c877-4869-aad2-3635bc2d1399_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a6460904-c877-4869-aad2-3635bc2d1399_poly_0_">
0.9254199133330339 0.0006353389466097779 0.9254199133330339 0.9993644591293789 0.06579614373894493 0.9993644591293789 0.06579614373894493 0.0006353389466097779 0.9254199133330339 0.0006353389466097779
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_D8eCjx4rYLUgh7CtcTBm.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_111d24ff-ae11-4a45-bff1-35acf6c1464c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_111d24ff-ae11-4a45-bff1-35acf6c1464c_poly_0_">
0.9699497609423418 0.0006440440162755882 0.9699497609423418 0.999355748487144 0.03568737303199043 0.999355748487144 0.03568737303199043 0.0006440440162755882 0.9699497609423418 0.0006440440162755882
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_dV5rp1tg5CbeMDNwQItR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_aa97a0be-a451-4561-b9fd-7d595b9ae7a4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_aa97a0be-a451-4561-b9fd-7d595b9ae7a4_poly_0_">
0.9622778082034245 0.0006596673980489927 0.9622778082034245 0.9993401149137057 0.037101297008092615 0.9993401149137057 0.037101297008092615 0.0006596673980489927 0.9622778082034245 0.0006596673980489927
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_mUJyPugLal5PB68NdUkx.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_f05c4ad4-b6fd-4825-9fc0-136b39ba33ae_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_f05c4ad4-b6fd-4825-9fc0-136b39ba33ae_poly_0_">
0.966395931916125 0.0006336518274127379 0.966395931916125 0.9993661473198099 0.035819402137860834 0.9993661473198099 0.035819402137860834 0.0006336518274127379 0.966395931916125 0.0006336518274127379
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_b3l4i60w6UKT8DklcLeK.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_0a16743c-6365-4bbe-bd19-4e0b6d00589b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_0a16743c-6365-4bbe-bd19-4e0b6d00589b_poly_0_">
0.9167976410562417 0.0007071941678443462 0.9167976410562417 0.9992925556376504 0.06909784289887533 0.9992925556376504 0.06909784289887533 0.0007071941678443462 0.9167976410562417 0.0007071941678443462
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_JQmQCTCVP8p48q8XVKWO.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_4317508e-5431-4da3-855f-f8f7da3e94cf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_4317508e-5431-4da3-855f-f8f7da3e94cf_poly_0_">
0.9782837263929878 0.0006801637523417516 0.9782837263929878 0.999319604818229 0.02356503726855408 0.999319604818229 0.02356503726855408 0.0006801637523417516 0.9782837263929878 0.0006801637523417516
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_2iTJgHphJFLHv2UZPJfl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_794e415e-c77f-4a04-9619-560a390ecd9b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_794e415e-c77f-4a04-9619-560a390ecd9b_poly_0_">
0.9577457491524726 0.0006885159309400452 0.9577457491524726 0.9993112469194985 0.03850697950206694 0.9993112469194985 0.03850697950206694 0.0006885159309400452 0.9577457491524726 0.0006885159309400452
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_L5ZcZqVtQWZh1NAK8qHC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a864bb11-dc1a-442c-8d20-fea0e124472c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a864bb11-dc1a-442c-8d20-fea0e124472c_poly_0_">
0.9762826264745001 0.0006954171376390543 0.9762826264745001 0.9993043409336749 0.024024834253879135 0.9993043409336749 0.024024834253879135 0.0006954171376390543 0.9762826264745001 0.0006954171376390543
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_eGlU0sZi8aQ3bQkmNnJq.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_744966d5-a241-4e5d-af6a-83a7d2af2499_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_744966d5-a241-4e5d-af6a-83a7d2af2499_poly_0_">
0.9105241381638844 0.0006894995922287657 0.9105241381638844 0.9993102625799337 0.07000238589086472 0.9993102625799337 0.07000238589086472 0.0006894995922287657 0.9105241381638844 0.0006894995922287657
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_j6r0ILqZesrrSXjan33r.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_532ef2dd-1003-485c-a0cd-09db91f12c21_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_532ef2dd-1003-485c-a0cd-09db91f12c21_poly_0_">
0.9577379002971185 0.0006420199690442157 0.9577379002971185 0.9993577738368445 0.03366655243029726 0.9993577738368445 0.03366655243029726 0.0006420199690442157 0.9577379002971185 0.0006420199690442157
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_oH1gRwP5UkUvOCzHKdXE.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_17c60ba9-23d9-4712-a76f-6389d7ea6f0a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_17c60ba9-23d9-4712-a76f-6389d7ea6f0a_poly_0_">
0.9957241654852815 0.0006524720752558866 0.9957241654852815 0.999347314960619 0.004805974037005001 0.999347314960619 0.004805974037005001 0.0006524720752558866 0.9957241654852815 0.0006524720752558866
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_7m3mLb7MmpEzPUCqa6Ms.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_41c867bb-4735-41d0-bc18-bffdaca8eb18_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_41c867bb-4735-41d0-bc18-bffdaca8eb18_poly_0_">
0.9541889505581764 0.0006562401343340277 0.9541889505581764 0.9993435444340723 0.03624232904763858 0.9993435444340723 0.03624232904763858 0.0006562401343340277 0.9541889505581764 0.0006562401343340277
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_y4PItjRONCTSpeHSYOQl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d5412680-aac6-46c6-ba28-067a153a7f04_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d5412680-aac6-46c6-ba28-067a153a7f04_poly_0_">
0.9379047492870711 0.0006740504760370254 0.9379047492870711 0.9993257222370323 0.05592226728748528 0.9993257222370323 0.05592226728748528 0.0006740504760370254 0.9379047492870711 0.0006740504760370254
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_VR3FfZdYanqAMDzJK9WQ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_21560956-df3a-4266-9d10-a5475807ecfd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_21560956-df3a-4266-9d10-a5475807ecfd_poly_0_">
0.9509920165912131 0.00070734253056835 0.9509920165912131 0.9992924071699104 0.07491588932631998 0.9992924071699104 0.07491588932631998 0.00070734253056835 0.9509920165912131 0.00070734253056835
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_AsHPzPkC4NgCpZvEd6Yk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_017526be-821c-4bd1-b2bb-04f6a35e0f67_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_017526be-821c-4bd1-b2bb-04f6a35e0f67_poly_0_">
0.996922668629109 0.0006717419510713933 0.996922668629109 0.9987266309818096 0.003249947096460204 0.9993283707813364 0.003249947096460204 0.0006717419510713933 0.996922668629109 0.0006717419510713933
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_ghdR8ELYmxNsORzcgCLN.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_3fc2e68f-d658-4019-a118-24c56037bc1b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_3fc2e68f-d658-4019-a118-24c56037bc1b_poly_0_">
0.9943431719690601 0.0006424157494300036 0.9943431719690601 0.9993573778020979 0.004700778182364473 0.9993573778020979 0.004700778182364473 0.0006424157494300036 0.9943431719690601 0.0006424157494300036
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_WBIXsoDBNQUOobJbts94.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_58964adf-a38c-428a-96ca-fda2d469c5f0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_58964adf-a38c-428a-96ca-fda2d469c5f0_poly_0_">
0.9736125692586768 0.0007143016395468537 0.9736125692586768 0.9992854431102847 0.024579742654054826 0.9992854431102847 0.024579742654054826 0.0007143016395468537 0.9736125692586768 0.0007143016395468537
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_QrQmfagnqODA2fxgl4py.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_6c0ef761-95d2-4351-8170-0d1e2a95a41a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_6c0ef761-95d2-4351-8170-0d1e2a95a41a_poly_0_">
0.9957454654185831 0.0005865822356900015 0.9957454654185831 0.9994132456492268 0.0042999780926678 0.9994132456492268 0.0042999780926678 0.0005865822356900015 0.9957454654185831 0.0005865822356900015
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_x8vWo18XRdKmyHEfd4TH.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1df0a5c6-1926-41c3-8acc-eaf3a70cd1a4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1df0a5c6-1926-41c3-8acc-eaf3a70cd1a4_poly_0_">
0.9706869393666446 0.0006718739599222913 0.9706869393666446 0.9993279002189699 0.02249274776876664 0.9993279002189699 0.02249274776876664 0.0006718739599222913 0.9706869393666446 0.0006718739599222913
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_5fhv9hYZdKGafE7LPzhK.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_c578fba6-bcbf-47de-9b9b-47afcd32f6d1_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_c578fba6-bcbf-47de-9b9b-47afcd32f6d1_poly_0_">
0.9828822915831097 0.0006971144366846543 0.9828822915831097 0.9993026424519319 0.024424207816650778 0.9993026424519319 0.024424207816650778 0.0006971144366846543 0.9828822915831097 0.0006971144366846543
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_68hZ0aftTNFlf4HY17nG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a021cdef-a494-4f38-acbe-7dd29fc82fcd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a021cdef-a494-4f38-acbe-7dd29fc82fcd_poly_0_">
0.9934645941616136 0.0006600136256029666 0.9934645941616136 0.999339768457527 0.004805160499349981 0.999339768457527 0.004805160499349981 0.0006600136256029666 0.9934645941616136 0.0006600136256029666
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_W4EfmMg6Yz1Sjw7ygA3C.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_79e84c9e-e2c3-4269-a393-c369ee018ae6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_79e84c9e-e2c3-4269-a393-c369ee018ae6_poly_0_">
0.9450270043299653 0.0006919692861582821 0.9450270043299653 0.9993077911787749 0.07631506793952525 0.9993077911787749 0.07631506793952525 0.0006919692861582821 0.9450270043299653 0.0006919692861582821
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_7Qt54Zbs4kfDNFB1VAEK.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_7ffe7f7d-e201-4fed-87d5-3200599ad395_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_7ffe7f7d-e201-4fed-87d5-3200599ad395_poly_0_">
0.9716889958938282 0.0006525795061373487 0.9716889958938282 0.9993472074595847 0.03742014115745462 0.9993472074595847 0.03742014115745462 0.0006525795061373487 0.9716889958938282 0.0006525795061373487
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_yFUflHPp7vfOBFofYroC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d2da2303-a663-47a5-9536-ddab12f60f47_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d2da2303-a663-47a5-9536-ddab12f60f47_poly_0_">
0.949655207162742 0.0007120853153710213 0.949655207162742 0.9992876610163978 0.07528817629408557 0.9992876610163978 0.07528817629408557 0.0007120853153710213 0.949655207162742 0.0007120853153710213
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_KpeMIQF1r8gtPigIeGxF.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_ad7d2eb9-4849-4c46-ba0e-aaa3b4968e2f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_ad7d2eb9-4849-4c46-ba0e-aaa3b4968e2f_poly_0_">
0.9936940028477967 0.0006830027079974277 0.9936940028477967 0.9993167639261041 0.005013071083692644 0.9993167639261041 0.005013071083692644 0.0006830027079974277 0.9936940028477967 0.0006830027079974277
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_tIEhxDsR7xLQtHj8USGz.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_c74ea99f-2e7d-4b57-90bd-0f4c0c9f1ad0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_c74ea99f-2e7d-4b57-90bd-0f4c0c9f1ad0_poly_0_">
0.9529712732248186 0.0006641051877100468 0.9529712732248186 0.9993356741845436 0.03662107977484652 0.9993356741845436 0.03662107977484652 0.0006641051877100468 0.9529712732248186 0.0006641051877100468
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_oVB35kJsKMMYCIC93MFH.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_25fb3b46-7ad5-4ca3-97ce-0984b3a8e3fa_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_25fb3b46-7ad5-4ca3-97ce-0984b3a8e3fa_poly_0_">
0.9582744119092581 0.000675390242795353 0.9582744119092581 0.9992420459705503 0.03464518930506699 0.9993350137254828 0.03464518930506699 0.000675390242795353 0.9582744119092581 0.000675390242795353
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_M0KQG9dOSUpfZnolOABk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_b080822d-8a5b-4154-8aae-de9cf7ad5e0a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_b080822d-8a5b-4154-8aae-de9cf7ad5e0a_poly_0_">
0.9793361705057464 0.0005842635873702456 0.9793361705057464 0.999415565655831 0.03274667158953548 0.999415565655831 0.03274667158953548 0.0005842635873702456 0.9793361705057464 0.0005842635873702456
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_NnrqHi2RsI3ZmBMqjHNt.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_f9a114de-5439-416c-bf6c-af068b9a923a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_f9a114de-5439-416c-bf6c-af068b9a923a_poly_0_">
0.9180306056049972 0.0007046710933301968 0.9180306056049972 0.9952981738528817 0.07115257862830049 0.9991869214898115 0.07115257862830049 0.0007046710933301968 0.9180306056049972 0.0007046710933301968
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_mWFEVbIgre2x1fmAGg2i.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_2d02122a-abfa-4e76-9ec0-ff29ae7d8b46_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_2d02122a-abfa-4e76-9ec0-ff29ae7d8b46_poly_0_">
0.975074592180988 0.0006942518043766055 0.975074592180988 0.9993055070772845 0.023920916909446532 0.9993055070772845 0.023920916909446532 0.0006942518043766055 0.975074592180988 0.0006942518043766055
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_iaAT3tzyYAyxjrI9ovJC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d50c33cd-e804-4fa8-9ae5-14f79de62a1c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d50c33cd-e804-4fa8-9ae5-14f79de62a1c_poly_0_">
0.9113585041459888 0.0006914056730569788 0.9113585041459888 0.999308355182024 0.07037654362361856 0.999308355182024 0.07037654362361856 0.0006914056730569788 0.9113585041459888 0.0006914056730569788
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_xyUh2GS9XwkltFu0nHWt.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_0c44cdbd-5067-4dbd-a712-7ad023bf7a8f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_0c44cdbd-5067-4dbd-a712-7ad023bf7a8f_poly_0_">
0.9105915423626705 0.0006892510905092429 0.9105915423626705 0.9993105112530971 0.06998350203751791 0.9993105112530971 0.06998350203751791 0.0006892510905092429 0.9105915423626705 0.0006892510905092429
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_5tGex440WiQdA2HbPByV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_0672475a-de5f-4a63-882b-ee8246fcfb16_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_0672475a-de5f-4a63-882b-ee8246fcfb16_poly_0_">
0.9602217471310439 0.0006057041308328186 0.9602217471310439 0.9993941123470459 0.033643947142309116 0.9993941123470459 0.033643947142309116 0.0006057041308328186 0.9602217471310439 0.0006057041308328186
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_y9EtpHwc4pAWymDKAoTq.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_856da2a4-6214-47cd-8e9c-45810733b5f9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_856da2a4-6214-47cd-8e9c-45810733b5f9_poly_0_">
0.9540809876864387 0.0006738726940472203 0.9540809876864387 0.9938754447925479 0.052015234591408444 0.9992162283250634 0.052015234591408444 0.0006738726940472203 0.9540809876864387 0.0006738726940472203
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_yxHHCEN9jyj5FkaBZ7de.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d33fdb5b-8f12-4f04-a6a4-5421b5cfe80a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d33fdb5b-8f12-4f04-a6a4-5421b5cfe80a_poly_0_">
0.996143815932812 0.0006875164082916336 0.996143815932812 0.9993122471303673 0.005071695610194382 0.9993122471303673 0.005071695610194382 0.0006875164082916336 0.996143815932812 0.0006875164082916336
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_uBqTXVfNYinwJEIvB4VZ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_dce899c6-7e6d-4dd1-99ee-8b4b0ea32e43_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_dce899c6-7e6d-4dd1-99ee-8b4b0ea32e43_poly_0_">
0.9750865529845214 0.0006789970523546436 0.9750865529845214 0.9993207723116893 0.022958173253584846 0.9993207723116893 0.022958173253584846 0.0006789970523546436 0.9750865529845214 0.0006789970523546436
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_bmc82Vy1kzdcxYlm3bwJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_cd8dbee8-f871-462c-812a-ae1c0458836d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_cd8dbee8-f871-462c-812a-ae1c0458836d_poly_0_">
0.9329915695299515 0.0006461905194724558 0.9329915695299515 0.9993536005981949 0.06450162453185548 0.9993536005981949 0.06450162453185548 0.0006461905194724558 0.9329915695299515 0.0006461905194724558
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_agwX86pX679tQYfrL53m.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_43b22dff-c541-4c7b-b5f3-c65aff706f24_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_43b22dff-c541-4c7b-b5f3-c65aff706f24_poly_0_">
0.9219548337863444 0.0006344924047257121 0.9219548337863444 0.9993653062091299 0.11140283858368605 0.9993653062091299 0.11140283858368605 0.0006344924047257121 0.9219548337863444 0.0006344924047257121
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_12XCSPyciWdQSfqTwwcO.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_0e326372-05d3-4bba-81b9-17831c7a3ef6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_0e326372-05d3-4bba-81b9-17831c7a3ef6_poly_0_">
0.919726041676542 0.0006960263512725822 0.919726041676542 0.9993037312958662 0.06827243144133632 0.9993037312958662 0.06827243144133632 0.0006960263512725822 0.919726041676542 0.0006960263512725822
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_UuD7c7ppUKba1pqh0aHL.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_5de3993d-13b6-44c0-95b7-be9bb2af8bcd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_5de3993d-13b6-44c0-95b7-be9bb2af8bcd_poly_0_">
0.9513039571195847 0.000684092637482716 0.9513039571195847 0.9993156732510265 0.03660656039308208 0.9993156732510265 0.03660656039308208 0.000684092637482716 0.9513039571195847 0.000684092637482716
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_ddVB4zptzMP7Q0FyOKz0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_bfb3c0b6-8523-4429-8b6b-b701219683c2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_bfb3c0b6-8523-4429-8b6b-b701219683c2_poly_0_">
0.9524847832073515 0.000702059957687742 0.9524847832073515 0.9992976934683804 0.07449955557638788 0.9992976934683804 0.07449955557638788 0.000702059957687742 0.9524847832073515 0.000702059957687742
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_Cf0YJmJwW1hp1OBgKDSl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_00378845-412b-4112-b1ba-16e458d6d4af_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_00378845-412b-4112-b1ba-16e458d6d4af_poly_0_">
0.9938310518062972 0.0006412677328593288 0.9938310518062972 0.999358526556046 0.004738790627325962 0.999358526556046 0.004738790627325962 0.0006412677328593288 0.9938310518062972 0.0006412677328593288
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_A8ZaS16qojYUzOsMt0DF.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_046aee10-3cd1-4065-b503-c1bec6c2d40a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_046aee10-3cd1-4065-b503-c1bec6c2d40a_poly_0_">
0.9112532093408685 0.0006868133727276215 0.9112532093408685 0.9993129506494063 0.06979805755100088 0.9993129506494063 0.06979805755100088 0.0006868133727276215 0.9112532093408685 0.0006868133727276215
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_QS3q6IbFjveAu5Kq1MPR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_91a12944-c5ca-4925-a752-eaf36e7d2a10_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_91a12944-c5ca-4925-a752-eaf36e7d2a10_poly_0_">
0.9097787563639415 0.0006922497287818628 0.9097787563639415 0.999307510541903 0.07021111920552414 0.999307510541903 0.07021111920552414 0.0006922497287818628 0.9097787563639415 0.0006922497287818628
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6222.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_ef04023f-447e-47e9-90a3-d018b80ffbcf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_ef04023f-447e-47e9-90a3-d018b80ffbcf_poly_0_">
0.4354997845765055 0.08489466506151618 0.4653886631533632 0.7667230062233541 0.4609509214180761 0.7674802797821676 0.431101195962706 0.0851978880956965 0.4354997845765055 0.08489466506151618
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_XZ07WrMqXcNORAzBtBc9.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_16e87bad-8df3-4193-a0bd-e57237225ec4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_16e87bad-8df3-4193-a0bd-e57237225ec4_poly_0_">
0.9654193854241981 0.0006512425184475941 0.9654193854241981 0.9993485453195113 0.035771590890277594 0.9993485453195113 0.035771590890277594 0.0006512425184475941 0.9654193854241981 0.0006512425184475941
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_H9OTW5lhKRINS1ecY4Tr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_14cddc50-fc0d-4e48-8e54-ffe73b1286cd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_14cddc50-fc0d-4e48-8e54-ffe73b1286cd_poly_0_">
0.9691666073043166 0.0006490758565026941 0.9691666073043166 0.9993507133911622 0.03593172108317333 0.9993507133911622 0.03593172108317333 0.0006490758565026941 0.9691666073043166 0.0006490758565026941
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_9MOhUw7dYekEdQTy6NXG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_56728076-f979-4790-a070-06a5b745de7e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_56728076-f979-4790-a070-06a5b745de7e_poly_0_">
0.9937687020947408 0.0006341949644480065 0.9937687020947408 0.9993656038382217 0.004651335338934359 0.9993656038382217 0.004651335338934359 0.0006341949644480065 0.9937687020947408 0.0006341949644480065
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_ZtPiEdZFdD9xTcN9eT2U.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_89f81338-54c4-4881-8235-f1f58de1003b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_89f81338-54c4-4881-8235-f1f58de1003b_poly_0_">
0.9937339922217099 0.000641764808966776 0.9937339922217099 0.9993580291608258 0.004707182702281543 0.9993580291608258 0.004707182702281543 0.000641764808966776 0.9937339922217099 0.000641764808966776
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_IZuu9AWY9U6eLglPRhoY.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_c563949f-bc1a-4650-a6d7-5c04e9a57979_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_c563949f-bc1a-4650-a6d7-5c04e9a57979_poly_0_">
0.9615842706992055 0.0006981502204342254 0.9615842706992055 0.9993016059450183 0.03828931634373589 0.9993016059450183 0.03828931634373589 0.0006981502204342254 0.9615842706992055 0.0006981502204342254
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_7hCKB9J57H2K4TU2pXFD.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_99d03a20-d6e2-4fc7-847b-58fc44e3a290_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_99d03a20-d6e2-4fc7-847b-58fc44e3a290_poly_0_">
0.9645666886247515 0.0006839863563497628 0.9645666886247515 0.9993157796049162 0.03767952254895768 0.9993157796049162 0.03767952254895768 0.0006839863563497628 0.9645666886247515 0.0006839863563497628
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_yGpS3HZI4SwBVOJ8YfHh.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_4eec6248-174c-4a8f-873f-236e86143761_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_4eec6248-174c-4a8f-873f-236e86143761_poly_0_">
0.9697133955044137 0.0006455619659481844 0.9697133955044137 0.9993542295579824 0.03576113677129911 0.9993542295579824 0.03576113677129911 0.0006455619659481844 0.9697133955044137 0.0006455619659481844
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_B4JRW7blZOiZcZZmfoIn.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_9501eb07-9cd6-48b7-8d44-e9bda9e0bcbb_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_9501eb07-9cd6-48b7-8d44-e9bda9e0bcbb_poly_0_">
0.9227407870941597 0.0006499718936397044 0.9227407870941597 0.9993498167716023 0.06717141582919162 0.9993498167716023 0.06717141582919162 0.0006499718936397044 0.9227407870941597 0.0006499718936397044
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_Rb5RSMdsFlPTLYEePhBG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_470de155-ae06-43f0-9568-fe2457c109d4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_470de155-ae06-43f0-9568-fe2457c109d4_poly_0_">
0.37593338900622264 0.9990316622464462 0.0050854940490427936 0.9990316622459455 0.0050854940490427936 0.0006849550419550421 0.994250769412985 0.0006849550419951336 0.994250769412985 0.9990316622459856 0.37593338900622264 0.9990316622464462
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_vWT7GZq7pjxZlW0VvIHc.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_daade91c-d7f7-4544-8d07-e5a04fe5b82d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_daade91c-d7f7-4544-8d07-e5a04fe5b82d_poly_0_">
0.9580141186246465 0.000673041639928866 0.9580141186246465 0.9992013426528127 0.053599131925864185 0.9931256657037161 0.053599131925864185 0.000673041639928866 0.9580141186246465 0.000673041639928866
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_XfNCTqikaaFrNVvWBX7X.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_aebad0d5-386e-4b5c-a3e4-e3e5fbd4172b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_aebad0d5-386e-4b5c-a3e4-e3e5fbd4172b_poly_0_">
0.9185083472594879 0.0007054273299355667 0.9185083472594879 0.9992943237244881 0.06917402501415637 0.9992943237244881 0.06917402501415637 0.0007054273299355667 0.9185083472594879 0.0007054273299355667
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6223.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_829b1b94-c950-4ab9-8ab9-eb4f9868c9f6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_829b1b94-c950-4ab9-8ab9-eb4f9868c9f6_poly_0_">
0.23024045929302606 0.07612045102620629 0.33544716483400155 0.739689219431538 0.3307113832641257 0.7415751385142815 0.22547992737737108 0.07743180478038057 0.23024045929302606 0.07612045102620629
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_P1L6P5qo1dUZd5kJzyZR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_dedce3fe-2e18-49ec-9e18-d7b64b2e776b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_dedce3fe-2e18-49ec-9e18-d7b64b2e776b_poly_0_">
0.9128522421859104 0.0007223376048124265 0.9128522421859104 0.9992774013679591 0.07020584897805016 0.9992774013679591 0.07020584897805016 0.0007223376048124265 0.9128522421859104 0.0007223376048124265
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_H7fyaCVfpAtnaIDOlGUT.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_fae3af26-8a95-44fa-8b34-65358a584b07_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_fae3af26-8a95-44fa-8b34-65358a584b07_poly_0_">
0.9943982610241697 0.0006396382979581496 0.9943982610241697 0.9993601570352759 0.004680736526106344 0.9993601570352759 0.004680736526106344 0.0006396382979581496 0.9943982610241697 0.0006396382979581496
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_R50F8w1gmzr5BwvoJ0zx.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_9280f5af-aed8-4119-9386-31b05f573962_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_9280f5af-aed8-4119-9386-31b05f573962_poly_0_">
0.9695831904444105 0.000698645581196313 0.9695831904444105 0.9993011102380254 0.023802909703363184 0.9993011102380254 0.023802909703363184 0.000698645581196313 0.9695831904444105 0.000698645581196313
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_nRvlo0NG4iQ9wbhm3EvX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d426b6be-e67d-4fda-a324-4508b3f70511_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d426b6be-e67d-4fda-a324-4508b3f70511_poly_0_">
0.9981883356333364 0.0006823722303603332 0.9981883356333364 0.9993173948344913 0.0021049390800629286 0.9993173948344913 0.0021049390800629286 0.0006823722303603332 0.9981883356333364 0.0006823722303603332
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_O7rBKKm0ulEnuYYljtJT.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_2e2c37c9-6dd5-404c-8b05-fbd27eab4011_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_2e2c37c9-6dd5-404c-8b05-fbd27eab4011_poly_0_">
0.9231185611261645 0.0006526800357402279 0.9231185611261645 0.999347106864325 0.07184160977707421 0.999347106864325 0.07184160977707421 0.0006526800357402279 0.9231185611261645 0.0006526800357402279
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_pmTNretR0auwXOkMHBpJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_9985fbd6-ce6f-4317-a86a-ca7f9083b379_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_9985fbd6-ce6f-4317-a86a-ca7f9083b379_poly_0_">
0.9722092954104653 0.0006849955195948053 0.9722092954104653 0.9993147697503757 0.023439847984576545 0.9993147697503757 0.023439847984576545 0.0006849955195948053 0.9722092954104653 0.0006849955195948053
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_djX6OHP2keuPL9IqGJg4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_b27ec88f-e51b-4efd-a655-99a0ea734ad6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_b27ec88f-e51b-4efd-a655-99a0ea734ad6_poly_0_">
0.9179800099776649 0.0006797898110197629 0.9179800099776649 0.9993199790140173 0.07006716771284793 0.9993199790140173 0.07006716771284793 0.0006797898110197629 0.9179800099776649 0.0006797898110197629
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_pvxwaAtAnKCYeI0S6tE7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_e620c474-25bb-4638-a0c8-8214513cea95_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_e620c474-25bb-4638-a0c8-8214513cea95_poly_0_">
0.971427805612862 0.0006393646230852272 0.971427805612862 0.9993604308852904 0.03551697944763532 0.9993604308852904 0.03551697944763532 0.0006393646230852272 0.971427805612862 0.0006393646230852272
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_nSfjRDwXCpf1x2WwbWLz.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_3fd89040-a7a3-402b-984f-387c29dc63c8_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_3fd89040-a7a3-402b-984f-387c29dc63c8_poly_0_">
0.9374890335924171 0.0007178620930137663 0.9374890335924171 0.9992818801051858 0.07839495728207169 0.9992818801051858 0.07839495728207169 0.0007178620930137663 0.9374890335924171 0.0007178620930137663
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_EDa4gZayNh9DOWY8i5ft.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_abc49961-0dbd-402a-9128-38f974ccf90f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_abc49961-0dbd-402a-9128-38f974ccf90f_poly_0_">
0.993256922179139 0.0006888456435220951 0.993256922179139 0.9993109169796736 0.005070433502902816 0.9993109169796736 0.005070433502902816 0.0006888456435220951 0.993256922179139 0.0006888456435220951
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_nnSRMi9iSMuE3ll9CDum.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_fe8172a3-02ae-48b9-8388-c8d031218f9e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_fe8172a3-02ae-48b9-8388-c8d031218f9e_poly_0_">
0.9538235168335518 0.0006384627080563289 0.9538235168335518 0.9993613333769795 0.03514008724292239 0.9993613333769795 0.03514008724292239 0.0006384627080563289 0.9538235168335518 0.0006384627080563289
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_BSDhqTaNM9xVP7LSmevy.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_762258bd-caa9-4b28-9374-29ea347737e0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_762258bd-caa9-4b28-9374-29ea347737e0_poly_0_">
0.977085745146951 0.0006872377431189733 0.977085745146951 0.9993125259872363 0.023765123906997587 0.9993125259872363 0.023765123906997587 0.0006872377431189733 0.977085745146951 0.0006872377431189733
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_CPktXZ5ObDT1AXRb3mqr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_e134baeb-766c-4bf7-98b5-4e30475193ea_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_e134baeb-766c-4bf7-98b5-4e30475193ea_poly_0_">
0.923562927123744 0.0006511680503676314 0.923562927123744 0.9993486198361206 0.07171748582555493 0.9993486198361206 0.07171748582555493 0.0006511680503676314 0.923562927123744 0.0006511680503676314
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_DluB91KieQxWcUidyEoX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_e31358f2-ca74-4282-a9ab-fae7708647fa_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_e31358f2-ca74-4282-a9ab-fae7708647fa_poly_0_">
0.9943081454235951 0.0006276787523919403 0.9943081454235951 0.9993721241645165 0.004608016764549916 0.9993721241645165 0.004608016764549916 0.0006276787523919403 0.9943081454235951 0.0006276787523919403
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_sxHM7yQAFheHTndQVSxC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_177164fe-8188-4952-a4b2-54eab414983e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_177164fe-8188-4952-a4b2-54eab414983e_poly_0_">
0.9547622126315432 0.0006407496079717484 0.9547622126315432 0.9993590450132956 0.03535056944629389 0.9993590450132956 0.03535056944629389 0.0006407496079717484 0.9547622126315432 0.0006407496079717484
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_nhUeCCiJkVKnEHZvMVWs.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_f15b7312-f7a3-4bf2-8077-5db3d5c27ba6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_f15b7312-f7a3-4bf2-8077-5db3d5c27ba6_poly_0_">
0.9950352890389969 0.0006444131555006785 0.9950352890389969 0.9993553791099364 0.004705169510666352 0.9993553791099364 0.004705169510666352 0.0006444131555006785 0.9950352890389969 0.0006444131555006785
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_4mCmMlHZAiLHupGesCZl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1ce3ede4-aca3-4eac-bee8-a7f7d717f17b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1ce3ede4-aca3-4eac-bee8-a7f7d717f17b_poly_0_">
0.9955988852479614 0.005682944209381091 0.9955988852479614 0.9935867044396831 0.003995855067564946 0.9935867044396831 0.003995855067564946 0.005682944209381091 0.9955988852479614 0.005682944209381091
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_ezcDbjaO77uRdLtf7BHQ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_d538810c-4089-47d9-b778-e6c8a57efc41_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_d538810c-4089-47d9-b778-e6c8a57efc41_poly_0_">
0.9716924965329241 0.0006525577409124552 0.9716924965329241 0.9993472292390235 0.03741905335850504 0.9993472292390235 0.03741905335850504 0.0006525577409124552 0.9716924965329241 0.0006525577409124552
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_uueScWDiBQsZJ8s37DBG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1fc59354-6dd2-41f3-81ed-28dbcf1fce6d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1fc59354-6dd2-41f3-81ed-28dbcf1fce6d_poly_0_">
0.9940848590191074 0.000638896865154594 0.9940848590191074 0.9993608989423945 0.0046892275931966765 0.9993608989423945 0.0046892275931966765 0.000638896865154594 0.9940848590191074 0.000638896865154594
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_4_2ziRM1eDzE8a2nmT70qy.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_8ff3a765-247b-4cb8-a4fb-4f9de007636b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_8ff3a765-247b-4cb8-a4fb-4f9de007636b_poly_0_">
0.9394468758749674 0.0007111025634330773 0.9394468758749674 0.9992886444682162 0.07785636725841627 0.9992886444682162 0.07785636725841627 0.0007111025634330773 0.9394468758749674 0.0007111025634330773
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_fB1opt5DSWB4YgreydOp.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_a3c62c76-acf2-44ea-8782-b57b9fa50b04_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_a3c62c76-acf2-44ea-8782-b57b9fa50b04_poly_0_">
0.9520820867524833 0.0006753689000169849 0.9520820867524833 0.999057634536904 0.04534257566453448 0.9993338376719937 0.04534257566453448 0.0006753689000169849 0.9520820867524833 0.0006753689000169849
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_xbut8ML91uWjxneqt0dk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_ac52418c-0e53-4aa5-8cc2-28d9ba4fc11c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_ac52418c-0e53-4aa5-8cc2-28d9ba4fc11c_poly_0_">
0.9807751866105452 0.0006345875289794647 0.9807751866105452 0.999365211024473 0.029915732676833784 0.999365211024473 0.029915732676833784 0.0006345875289794647 0.9807751866105452 0.0006345875289794647
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_4B9hQtY2aYNDG1ZjcilB.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_2d2dab0b-e5fe-464c-ad3a-34679594bbad_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_2d2dab0b-e5fe-464c-ad3a-34679594bbad_poly_0_">
0.9212853527500116 0.000694874366316617 0.9212853527500116 0.9993048840825979 0.06839124064625324 0.9993048840825979 0.06839124064625324 0.000694874366316617 0.9212853527500116 0.000694874366316617
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_HIHNqfMEcRqCAVXDltUk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_5fbac2cd-2753-4750-9fa3-60fa5ea8db01_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_5fbac2cd-2753-4750-9fa3-60fa5ea8db01_poly_0_">
0.9158433250529008 0.0007108468046874592 0.9158433250529008 0.9992889004089454 0.06936627677763596 0.9992889004089454 0.06936627677763596 0.0007108468046874592 0.9158433250529008 0.0007108468046874592
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_TAbP9Flnij8NamNoREds.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_2a61ec4b-e275-43f6-9b38-e32ca2235dc5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_2a61ec4b-e275-43f6-9b38-e32ca2235dc5_poly_0_">
0.9477632002068272 0.0007188175024730322 0.9477632002068272 0.9992809240088631 0.0758141884309893 0.9992809240088631 0.0758141884309893 0.0007188175024730322 0.9477632002068272 0.0007188175024730322
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_MXhMuPnq9h5kQP1JEZRF.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_cce571c3-e217-4db7-8975-35e19cc82064_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_cce571c3-e217-4db7-8975-35e19cc82064_poly_0_">
0.9529521049836411 0.0006881095336442243 0.9529521049836411 0.9993116535967393 0.03697684534495593 0.9993116535967393 0.03697684534495593 0.0006881095336442243 0.9529521049836411 0.0006881095336442243
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_ufeO93ZyJhgSEhd6ucbL.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_65bfe999-e854-490e-9b7f-146c38745338_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_65bfe999-e854-490e-9b7f-146c38745338_poly_0_">
0.9792818317420995 0.0006974389737527013 0.9792818317420995 0.9993023176883938 0.023829933134180692 0.9993023176883938 0.023829933134180692 0.0006974389737527013 0.9792818317420995 0.0006974389737527013
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_ErcP4pXpZ6yhhuz3GGlH.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_de908942-088d-4445-b66a-e082a263af58_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_de908942-088d-4445-b66a-e082a263af58_poly_0_">
0.9731370561796666 0.000642029068762694 0.9731370561796666 0.9993577647312797 0.03581040623222975 0.9993577647312797 0.03581040623222975 0.000642029068762694 0.9731370561796666 0.000642029068762694
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_DNyj8bBKAeltMtamMnB2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_b82c92d3-74e5-4844-ba08-972957850dc3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_b82c92d3-74e5-4844-ba08-972957850dc3_poly_0_">
0.9743847197366335 0.0006818843392879631 0.9743847197366335 0.9993178830586235 0.02909364217066468 0.9993178830586235 0.02909364217066468 0.0006818843392879631 0.9743847197366335 0.0006818843392879631
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_Yjpezr8TbzIIn3XeTmSw.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_40586976-8885-4ae9-a331-f6609c18ca6a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_40586976-8885-4ae9-a331-f6609c18ca6a_poly_0_">
0.9754238984954782 0.0006754892222767409 0.9754238984954782 0.9993242825192324 0.022848808471991333 0.9993242825192324 0.022848808471991333 0.0006754892222767409 0.9754238984954782 0.0006754892222767409
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_3_WavkCyY7H5YIQELKn2cR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_3dd0d38d-f178-45cd-9cbb-1d81884c209e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_3dd0d38d-f178-45cd-9cbb-1d81884c209e_poly_0_">
0.9965954168241176 0.0006459504124937984 0.9965954168241176 0.9993538408604125 0.004765740468034352 0.9993538408604125 0.004765740468034352 0.0006459504124937984 0.9965954168241176 0.0006459504124937984
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_vQwWSTRLRsOOvSOhhcdV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_5d3098b7-6e9f-43bc-bff7-52b6216b4500_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_5d3098b7-6e9f-43bc-bff7-52b6216b4500_poly_0_">
0.9829796023068127 0.0006961364012526559 0.9829796023068127 0.9993036211692224 0.024392769139268466 0.9993036211692224 0.024392769139268466 0.0006961364012526559 0.9829796023068127 0.0006961364012526559
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_jg6fSfAdbXjz7GS0Kz8s.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_2f5aaf43-6593-41b6-8fba-03da5e0eea8a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_2f5aaf43-6593-41b6-8fba-03da5e0eea8a_poly_0_">
0.9946529474468804 0.0006473881257675495 0.9946529474468804 0.9993524022167369 0.004740567236055426 0.9993524022167369 0.004740567236055426 0.0006473881257675495 0.9946529474468804 0.0006473881257675495
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6_zBEqmzLNPmSZ08GHHvar.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_37e12e2e-88bd-4505-939c-4005d8cf53cf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_37e12e2e-88bd-4505-939c-4005d8cf53cf_poly_0_">
0.9635720233047778 0.0006852220993360355 0.9635720233047778 0.999314543015283 0.03767269160962172 0.999314543015283 0.03767269160962172 0.0006852220993360355 0.9635720233047778 0.0006852220993360355
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_1_KzCBFLBKw5cmlQ8vu2vl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_3bd69527-2e1d-476c-bc3c-6a4e69bc324f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_3bd69527-2e1d-476c-bc3c-6a4e69bc324f_poly_0_">
0.9211635707283321 0.0006953360085412404 0.9211635707283321 0.9993044221192321 0.06842561676241488 0.9993044221192321 0.06842561676241488 0.0006953360085412404 0.9211635707283321 0.0006953360085412404
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_37ixaXaqmQI1KQkmf2PT.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_">
0.9965909233445363 0.0006484617049812288 0.9965909233445363 0.9990846523714384 0.005529109472373506 0.0006484617049812288 0.9965909233445363 0.0006484617049812288
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_2tgjYWx0UzWp65PuRGiS.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.nfE6o2KynGG4kgAB8utr">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.aUO7hQBVvRNqroQfznLN">
0.0050971634321062 0.000677350639774014 0.09548326076164138 0.0006773506397740142 1.0001328943016734 0.9990843573775101 0.0050971634321062 0.000677350639774014
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_5_B5bKfurmAbC6tRukXTkA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.G3D2d00yKqGHMERNQvKt">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.nLnaknN3rPJ6ojOU8Jg6">
0.0908031941912597 0.9488582552936304 0.0004940048710952993 0.0009139146502184039 0.9946879265111761 0.9992771211059156 0.0908031941912597 0.9488582552936304
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6219.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.K2jgbhcMK6VG5ZSf6UEU">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.b76jxa3uDeMdUY2C5xb9">
0.25863876027082505 0.7834387254136707 0.2546338117872417 0.7593693508208679 0.3143104969833146 0.7828336915461779 0.25863876027082505 0.7834387254136707
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
<app:target uri="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.6JEcQK9xQ9gzfnUQwIA8">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.ucymvFOl9HzUQ3qPkqQT">
0.2657894912536644 0.8177902541800504 0.25897481619716917 0.8198310791778232 0.1413197882326196 0.1213938968586222 0.2657894912536644 0.8177902541800504
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_FLzHYLlnXmU2U3PFo8I6.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.pEYQ1Dn4pTBFNCbxQBQK">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.i2pZ1Y8qQXIAdtge4Is4">
0.9584207336480688 0.999245040385793 0.03951041255990617 0.999245040385793 0.9584207336480688 0.0010676303985521999 0.9584207336480688 0.999245040385793
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_Wv1O7xad8kMl5mK9Zbk9.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.1uZd5Nh3Mm6rSAajpraG">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.6N7N8GyIi3A4nPsoWekl">
0.03947015041435975 0.0007797523387733028 0.953682393438811 0.0007797523387733028 0.03947015041435975 0.9988973088338735 0.03947015041435975 0.0007797523387733028
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_7ZUuensEspc04Qa1urTb.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.XrRyTWVfFQIX27G0r2t0">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.cWA7WRXyqCdUOBoJTzNv">
0.0036280494695652976 0.9990259695428697 0.9963584874252902 0.0012439348426320847 0.9963584874252902 0.9990259695428697 0.0036280494695652976 0.9990259695428697
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_aOTQX0Q0ofcWiPzkBNyC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.W5BQ78iUvpg4gCkx0hhs">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.iHGE93S9sURsVI45lA8I">
0.003225064294149149 0.0008861709339816005 0.996899917791005 0.0008861709339816005 0.003225064294149149 0.9987019478040268 0.003225064294149149 0.0008861709339816005
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_GnB3cSu4eVbpXewrX8Jx.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_">
0.9963220933719938 0.9993924070791177 0.005571627697115389 0.9993924070791177 0.9963220933719938 0.0008578725360013085 0.9963220933719938 0.9993924070791177
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_2_uWpb3bVkSdMvvMOTPOAA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly.qMKmdRlmxPi5DfEf12T6">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_.Q1Oub1OYjLiGNvgdEQGx">
0.0052671246095599145 0.0006075929208822888 0.9960175902844384 0.0006075929208822888 0.0052671246095599145 0.9991421274639987 0.0052671246095599145 0.0006075929208822888
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/distorted_0_IMG_6220.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly.tJXCxiBko4G9vXPYiV0t">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_.MmXWl2qBvxQ3EzBYQbjS">
0.0 0.9999999999999999 1.0 0.0 1.0 0.9999999999999999 0.0 0.9999999999999999
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/distorted_1_IMG_6220.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly.xpXgpqEsKRyjf1uzVLCb">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_.mTMKJgIaHgZVwUiVR5IN">
2.842170943040401e-14 0.0 1.0000000000000284 0.0 2.842170943040401e-14 0.9999999999999999 2.842170943040401e-14 0.0
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>1000</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">26.82</bldg:measuredHeight>
<bldg:storeysAboveGround>5</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_078c5169-6d15-44f6-9130-2c4d9268efb1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_078c5169-6d15-44f6-9130-2c4d9268efb1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_078c5169-6d15-44f6-9130-2c4d9268efb1_poly_0_">
<gml:posList srsDimension="3">
690918.274 5336167.493 529.6
690920.206 5336172.4 529.6
690913.43 5336175.0 529.6
690911.572 5336170.074 529.6
690918.274 5336167.493 529.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_f3b7b775-1e43-4cf1-9059-10cce688beb8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f3b7b775-1e43-4cf1-9059-10cce688beb8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f3b7b775-1e43-4cf1-9059-10cce688beb8_poly_0_">
<gml:posList srsDimension="3">
690904.105 5336149.143 535.78
690909.053 5336161.694 535.78
690905.061 5336163.266 535.34
690900.113 5336150.715 535.34
690904.105 5336149.143 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_9c96ac35-41a3-4d18-ab6b-05feda815b6f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9c96ac35-41a3-4d18-ab6b-05feda815b6f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9c96ac35-41a3-4d18-ab6b-05feda815b6f_poly_0_">
<gml:posList srsDimension="3">
690957.21 5336160.573 532.82
691001.362 5336143.625 532.82
691002.54 5336146.678 532.82
690954.258 5336165.213 532.82
690953.08 5336162.159 532.82
690957.21 5336160.573 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_97bb0c80-a1fd-479d-9284-b245bfd5be11">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_97bb0c80-a1fd-479d-9284-b245bfd5be11_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_97bb0c80-a1fd-479d-9284-b245bfd5be11_poly_0_">
<gml:posList srsDimension="3">
690892.274 5336096.008 533.61
690892.309 5336096.139 533.61
690892.533 5336096.048 533.61
690904.751 5336127.101 533.61
690907.671 5336125.954 533.61
690908.539 5336128.154 533.61
690906.106 5336129.116 533.61
690905.575 5336129.325 533.61
690905.686 5336129.607 533.61
690918.902 5336163.014 533.61
690916.831 5336163.833 533.61
690915.058 5336159.329 533.61
690913.044 5336160.122 533.61
690908.096 5336147.571 533.61
690910.11 5336146.776 533.61
690903.656 5336130.408 533.61
690901.642 5336131.202 533.61
690888.379 5336097.547 533.61
690892.274 5336096.008 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_c452c4b8-1669-460b-bff1-7ff33be02a1c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c452c4b8-1669-460b-bff1-7ff33be02a1c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c452c4b8-1669-460b-bff1-7ff33be02a1c_poly_0_">
<gml:posList srsDimension="3">
690876.923 5336102.195 533.29
690876.877 5336102.093 533.29
690880.402 5336100.7 533.29
690905.061 5336163.266 533.29
690915.058 5336159.329 533.29
690916.831 5336163.833 533.29
690916.3 5336164.043 533.29
690916.837 5336165.405 533.29
690917.371 5336165.202 533.29
690949.517 5336152.928 533.29
690953.08 5336162.159 533.29
690954.258 5336165.213 533.29
690956.778 5336171.745 533.29
690957.714 5336174.168 533.29
690955.064 5336175.175 533.29
690955.3 5336175.785 533.29
690954.944 5336175.921 533.29
690954.708 5336175.311 533.29
690951.813 5336176.409 533.29
690952.059 5336177.029 533.29
690951.684 5336177.155 533.29
690951.448 5336176.545 533.29
690948.552 5336177.663 533.29
690948.779 5336178.262 533.29
690948.413 5336178.398 533.29
690948.187 5336177.799 533.29
690945.282 5336178.907 533.29
690945.508 5336179.516 533.29
690945.133 5336179.652 533.29
690944.896 5336179.052 533.29
690942.011 5336180.16 533.29
690942.238 5336180.76 533.29
690941.872 5336180.905 533.29
690941.635 5336180.306 533.29
690938.729 5336181.423 533.29
690938.956 5336182.013 533.29
690938.6 5336182.159 533.29
690938.363 5336181.569 533.29
690935.468 5336182.657 533.29
690935.694 5336183.257 533.29
690935.328 5336183.403 533.29
690935.081 5336182.802 533.29
690932.195 5336183.921 533.29
690932.422 5336184.51 533.29
690932.056 5336184.656 533.29
690931.819 5336184.066 533.29
690930.737 5336184.478 533.29
690928.933 5336185.165 533.29
690929.16 5336185.764 533.29
690928.784 5336185.9 533.29
690928.547 5336185.31 533.29
690925.661 5336186.408 533.29
690925.888 5336187.008 533.29
690925.512 5336187.153 533.29
690925.285 5336186.554 533.29
690922.389 5336187.662 533.29
690922.616 5336188.261 533.29
690922.25 5336188.407 533.29
690922.013 5336187.807 533.29
690919.111 5336188.845 533.29
690919.345 5336189.515 533.29
690918.979 5336189.66 533.29
690918.808 5336189.173 533.29
690911.852 5336191.762 533.29
690906.48 5336177.667 533.29
690913.437 5336175.019 533.29
690913.43 5336175.0 533.29
690920.206 5336172.4 533.29
690918.274 5336167.493 533.29
690911.572 5336170.074 533.29
690911.425 5336169.686 533.29
690904.341 5336172.489 533.29
690904.177 5336172.072 533.29
690903.994 5336172.145 533.29
690903.838 5336171.789 533.29
690904.03 5336171.716 533.29
690902.893 5336168.82 533.29
690902.71 5336168.893 533.29
690902.584 5336168.518 533.29
690902.757 5336168.454 533.29
690901.619 5336165.568 533.29
690901.436 5336165.641 533.29
690901.3 5336165.265 533.29
690901.473 5336165.202 533.29
690901.294 5336164.749 533.29
690900.335 5336162.306 533.29
690900.152 5336162.378 533.29
690900.016 5336162.013 533.29
690900.189 5336161.95 533.29
690899.051 5336159.054 533.29
690898.878 5336159.127 533.29
690898.732 5336158.761 533.29
690898.915 5336158.688 533.29
690897.767 5336155.792 533.29
690897.584 5336155.865 533.29
690897.449 5336155.499 533.29
690897.631 5336155.427 533.29
690896.493 5336152.541 533.29
690896.31 5336152.613 533.29
690896.165 5336152.248 533.29
690896.348 5336152.175 533.29
690895.21 5336149.278 533.29
690895.027 5336149.351 533.29
690894.881 5336148.985 533.29
690895.064 5336148.913 533.29
690893.926 5336146.036 533.29
690893.753 5336146.099 533.29
690893.607 5336145.733 533.29
690893.78 5336145.66 533.29
690892.643 5336142.753 533.29
690892.46 5336142.826 533.29
690892.324 5336142.47 533.29
690892.497 5336142.397 533.29
690891.359 5336139.51 533.29
690891.186 5336139.573 533.29
690891.03 5336139.217 533.29
690891.223 5336139.144 533.29
690890.075 5336136.247 533.29
690889.892 5336136.32 533.29
690889.746 5336135.964 533.29
690889.939 5336135.891 533.29
690888.791 5336132.984 533.29
690888.619 5336133.047 533.29
690888.473 5336132.691 533.29
690889.083 5336132.445 533.29
690888.992 5336132.211 533.29
690888.382 5336132.458 533.29
690888.236 5336132.102 533.29
690888.419 5336132.029 533.29
690887.28 5336129.142 533.29
690887.108 5336129.205 533.29
690886.942 5336128.848 533.29
690887.135 5336128.776 533.29
690885.997 5336125.879 533.29
690885.824 5336125.952 533.29
690885.667 5336125.606 533.29
690885.86 5336125.533 533.29
690884.723 5336122.626 533.29
690884.54 5336122.699 533.29
690884.394 5336122.333 533.29
690884.577 5336122.26 533.29
690883.439 5336119.373 533.29
690883.246 5336119.446 533.29
690883.081 5336119.069 533.29
690883.284 5336118.987 533.29
690882.155 5336116.121 533.29
690881.962 5336116.193 533.29
690881.826 5336115.828 533.29
690882.019 5336115.755 533.29
690880.872 5336112.858 533.29
690880.689 5336112.931 533.29
690880.533 5336112.554 533.29
690880.726 5336112.482 533.29
690879.588 5336109.585 533.29
690879.405 5336109.658 533.29
690879.249 5336109.291 533.29
690879.442 5336109.219 533.29
690878.313 5336106.342 533.29
690878.13 5336106.415 533.29
690877.975 5336106.039 533.29
690878.158 5336105.966 533.29
690877.029 5336103.079 533.29
690876.836 5336103.152 533.29
690876.7 5336102.786 533.29
690876.883 5336102.713 533.29
690876.71 5336102.276 533.29
690876.923 5336102.195 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_f731a532-0d42-4680-8040-9f8477cb1c11">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f731a532-0d42-4680-8040-9f8477cb1c11_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f731a532-0d42-4680-8040-9f8477cb1c11_poly_0_">
<gml:posList srsDimension="3">
690893.594 5336122.478 536.63
690897.583 5336120.902 536.63
690901.642 5336131.202 536.63
690903.656 5336130.408 536.63
690910.11 5336146.776 536.63
690904.105 5336149.143 536.63
690900.113 5336150.715 536.63
690889.606 5336124.054 536.63
690893.594 5336122.478 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_310d7682-58b2-4201-abaf-afee5c66a4dc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_310d7682-58b2-4201-abaf-afee5c66a4dc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_310d7682-58b2-4201-abaf-afee5c66a4dc_poly_0_">
<gml:posList srsDimension="3">
690888.379 5336097.547 535.29
690897.583 5336120.902 535.29
690893.594 5336122.478 535.83
690884.391 5336099.124 535.83
690888.379 5336097.547 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_">
<gml:posList srsDimension="3">
690954.258 5336165.213 536.41
691002.54 5336146.678 536.41
690956.778 5336171.745 533.29
690954.258 5336165.213 536.41
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.FKX5A0L8AdjIO3PmikYv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.EUbKjsCenQC0qMc1Ze19">
<gml:posList srsDimension="3">
690957.714 5336174.168 532.132
690956.778 5336171.745 533.29
691002.54 5336146.678 536.41
690957.714 5336174.168 532.132
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.RTrcqQMaZLcaK9wDwzyL">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.hUO9ekh25lOkCWTFnjF4">
<gml:posList srsDimension="3">
690957.979 5336174.068 532.132
690957.714 5336174.168 532.132
691002.54 5336146.678 536.41
690957.979 5336174.068 532.132
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.BdUeJ2QssXWIwD1FKzZj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.nrDB8Iznf09RlhxmaGoj">
<gml:posList srsDimension="3">
690958.834 5336173.941 532.048
690957.979 5336174.068 532.132
691002.54 5336146.678 536.41
690958.834 5336173.941 532.048
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.rq6YHlEMnOaDjgtzKGG8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.TdIIzGaKs4AAKBlZeFMY">
<gml:posList srsDimension="3">
690958.205 5336174.677 531.842
690957.979 5336174.068 532.132
690958.834 5336173.941 532.048
690958.205 5336174.677 531.842
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.WVXflTI11zUvKvHG089G">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.DZdc6eAzU5y0sdxkMVgH">
<gml:posList srsDimension="3">
690958.498 5336174.358 531.928
690958.205 5336174.677 531.842
690958.834 5336173.941 532.048
690958.498 5336174.358 531.928
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.6xglb9Yg0J9EXPNXZfof">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.d6Sd8Xr5lFuEZI6ItJ5p">
<gml:posList srsDimension="3">
690959.109 5336173.821 532.054
690958.834 5336173.941 532.048
691002.54 5336146.678 536.41
690959.109 5336173.821 532.054
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.wKTZmsjfW8NU1H3TQcFH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.7m6WL8Wedc6aN6dlXQ8S">
<gml:posList srsDimension="3">
690959.218 5336174.096 531.922
690959.109 5336173.821 532.054
691002.54 5336146.678 536.41
690959.218 5336174.096 531.922
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.Woskhazf9e2nNkN2HMp1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.hjZpW2EJIbhrkrUjhCyM">
<gml:posList srsDimension="3">
690963.275 5336172.222 532.053
690959.218 5336174.096 531.922
691002.54 5336146.678 536.41
690963.275 5336172.222 532.053
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.Eyle0pg80vpLilEyp42q">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.hsBTMJQHQVuj4CUoJXNe">
<gml:posList srsDimension="3">
690963.519 5336172.132 532.052
690963.275 5336172.222 532.053
691002.54 5336146.678 536.41
690963.519 5336172.132 532.052
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.lfRBe6naDEhBMRtsNRF6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.sCUX3pf4ApJlXNd3xvgZ">
<gml:posList srsDimension="3">
690970.5 5336169.452 532.052
690963.519 5336172.132 532.052
691002.54 5336146.678 536.41
690970.5 5336169.452 532.052
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.iwc3xnPFAGvVBMqdOIY3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.yw5aqk8G6sKcGAPIn7gD">
<gml:posList srsDimension="3">
690973.711 5336168.196 532.061
690970.5 5336169.452 532.052
691002.54 5336146.678 536.41
690973.711 5336168.196 532.061
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.USgayCiZQYKEg43Br1IH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.El0QzTidRSSGjtQssRcr">
<gml:posList srsDimension="3">
690973.802 5336168.429 531.949
690973.711 5336168.196 532.061
691002.54 5336146.678 536.41
690973.802 5336168.429 531.949
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.u1BVefm3TYaSJ0HA1gWD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.PBuZxuekkVm8nq7XSqrS">
<gml:posList srsDimension="3">
690973.945 5336168.375 531.949
690973.802 5336168.429 531.949
691002.54 5336146.678 536.41
690973.945 5336168.375 531.949
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.lbyVakluUOU4WTM1HR3E">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.sOBKoPrwrwmKDr0oO0QF">
<gml:posList srsDimension="3">
691006.1 5336155.896 532.007
690973.945 5336168.375 531.949
691002.54 5336146.678 536.41
691006.1 5336155.896 532.007
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.1K7JT6WnbotIjfx7Iqmg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.rABxbIu8YseSHhZK5xEC">
<gml:posList srsDimension="3">
690963.275 5336172.222 532.053
690963.355 5336172.476 531.935
690959.218 5336174.096 531.922
690963.275 5336172.222 532.053
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.1JQmBifWCaVHrZCotrI1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.Il5xkWkJKh28Mth5t0EY">
<gml:posList srsDimension="3">
690958.834 5336173.941 532.048
690958.925 5336174.185 531.932
690958.498 5336174.358 531.928
690958.834 5336173.941 532.048
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly.vo0buhtXzdSRfseq5VzB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4791b77-6e80-4101-a723-9bc89f03d776_poly_0_.l8SmsZegDamCiryIpaLr">
<gml:posList srsDimension="3">
690958.498 5336174.358 531.928
690958.571 5336174.531 531.844
690958.205 5336174.677 531.842
690958.498 5336174.358 531.928
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0_poly_0_">
<gml:posList srsDimension="3">
690904.105 5336149.143 535.78
690905.053 5336148.769 535.675
690909.053 5336161.694 535.78
690904.105 5336149.143 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0_poly.VTQTkuDql898XEy99AWH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0_poly_0_.610qkNyLkr2eSQIHuXYQ">
<gml:posList srsDimension="3">
690908.096 5336147.571 535.34
690909.053 5336161.694 535.78
690905.053 5336148.769 535.675
690908.096 5336147.571 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0_poly.w6RRL6Rg7NdkxLPukbeb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_73db96c6-af23-44a1-984c-de375af2b5e0_poly_0_.g8PG1X7KTvTfcqqQPe7G">
<gml:posList srsDimension="3">
690913.044 5336160.122 535.34
690909.053 5336161.694 535.78
690908.096 5336147.571 535.34
690913.044 5336160.122 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly_0_">
<gml:posList srsDimension="3">
690907.018 5336123.897 535.794
690914.45 5336120.954 535.79
690907.811 5336125.9 536.662
690907.018 5336123.897 535.794
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly.gdGhrIqH7jdM4GmVuQk1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly_0_.5X3hgEcawJ1p5VcASMwz">
<gml:posList srsDimension="3">
690908.142 5336127.148 537.18
690907.811 5336125.9 536.662
690914.45 5336120.954 535.79
690908.142 5336127.148 537.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly.xLpnxmt6KvIVmRCMuKnF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly_0_.RJ69OtHTlB1B79oNPeSa">
<gml:posList srsDimension="3">
690915.726 5336124.156 537.18
690908.142 5336127.148 537.18
690914.45 5336120.954 535.79
690915.726 5336124.156 537.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly.FKSvHX3ZXSoYISZhNu6C">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9fddc4b5-3a4e-4913-afee-cce2ec404ec8_poly_0_.WZRhx6QkuhFcrIwGuhLT">
<gml:posList srsDimension="3">
690907.811 5336125.9 536.662
690908.142 5336127.148 537.18
690907.671 5336125.954 536.662
690907.811 5336125.9 536.662
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_">
<gml:posList srsDimension="3">
690997.72 5336134.381 533.645
691000.812 5336142.389 536.055
691000.69 5336142.434 536.054
690997.72 5336134.381 533.645
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.cM7BM895mpREEpvaLWya">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.D4qe52pzqCcFUps59eLR">
<gml:posList srsDimension="3">
690954.339 5336151.016 533.633
690997.414 5336134.289 533.59
690954.43 5336151.249 533.703
690954.339 5336151.016 533.633
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.t7xIJCyWYYliIifVqn6r">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.s1VqOEMJ3zjXtTGPb68W">
<gml:posList srsDimension="3">
690957.21 5336160.573 536.43
690954.43 5336151.249 533.703
690997.414 5336134.289 533.59
690957.21 5336160.573 536.43
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.QiAebvYussv8w1mGRB2B">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.bM1VMO0ppn5iETO3ziaA">
<gml:posList srsDimension="3">
690997.487 5336134.472 533.646
690957.21 5336160.573 536.43
690997.414 5336134.289 533.59
690997.487 5336134.472 533.646
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.lw1Ejvhp4IKI4ST8DDuO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.DIbdgYeLfMmw4HD3Yx8L">
<gml:posList srsDimension="3">
691000.781 5336142.668 536.124
690957.21 5336160.573 536.43
690997.487 5336134.472 533.646
691000.781 5336142.668 536.124
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.GCBZSNluQt6nRe9VjtRQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.36IZuHm9Y0RUcmCW5I9B">
<gml:posList srsDimension="3">
691001.362 5336143.625 536.434
690957.21 5336160.573 536.43
691000.781 5336142.668 536.124
691001.362 5336143.625 536.434
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.FdeCmIOTZeuUtxlpL6zc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.hpC8Fs9I50qO44VpDdj6">
<gml:posList srsDimension="3">
691000.69 5336142.434 536.054
691000.781 5336142.668 536.124
690997.487 5336134.472 533.646
691000.69 5336142.434 536.054
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.qv9yQlaf7CkPFyzlJ59l">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.3x5dCCgBwO0t6Twt1BLS">
<gml:posList srsDimension="3">
690997.72 5336134.381 533.645
691000.69 5336142.434 536.054
690997.487 5336134.472 533.646
690997.72 5336134.381 533.645
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.PUrx4Rgefz11Buk4NhjA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.bwJe84BSAFNRGbGRl7CW">
<gml:posList srsDimension="3">
691000.964 5336142.595 536.124
691001.362 5336143.625 536.434
691000.781 5336142.668 536.124
691000.964 5336142.595 536.124
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.XQTfF2XyU2fhWrFq3gHc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.T1uKonsd0d5eIKpcNMRw">
<gml:posList srsDimension="3">
690949.517 5336152.928 533.644
690953.651 5336151.351 533.647
690953.08 5336162.159 536.43
690949.517 5336152.928 533.644
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.Sbeow76wrnfkm4RZg3IE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.ylVwCVf2eueke11pxvLc">
<gml:posList srsDimension="3">
690957.21 5336160.573 536.43
690953.08 5336162.159 536.43
690953.651 5336151.351 533.647
690957.21 5336160.573 536.43
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.458mtEhMjaMfe18PD7qI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.mXIeupG4j7lGOfB3DSIu">
<gml:posList srsDimension="3">
690953.869 5336151.267 533.651
690957.21 5336160.573 536.43
690953.651 5336151.351 533.647
690953.869 5336151.267 533.651
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.U0onNsOVqbbH8CwPyrc1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.VsYIsREEuDXaItSIAwDF">
<gml:posList srsDimension="3">
690953.932 5336151.44 533.703
690957.21 5336160.573 536.43
690953.869 5336151.267 533.651
690953.932 5336151.44 533.703
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly.4tjMm16KcGhfSNYM99Jo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2cfe6de7-5ffd-4731-83a9-aaf8df848fb4_poly_0_.eKpTLqgfF8GKqRA8B3ta">
<gml:posList srsDimension="3">
690954.43 5336151.249 533.703
690957.21 5336160.573 536.43
690953.932 5336151.44 533.703
690954.43 5336151.249 533.703
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_fca12de3-378f-4d64-bc63-6540bf40d4ce">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_fca12de3-378f-4d64-bc63-6540bf40d4ce_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_fca12de3-378f-4d64-bc63-6540bf40d4ce_poly_0_">
<gml:posList srsDimension="3">
690880.402 5336100.7 535.29
690884.391 5336099.124 535.83
690889.606 5336124.054 535.29
690880.402 5336100.7 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_fca12de3-378f-4d64-bc63-6540bf40d4ce_poly.CBMlWLapTmwMIipCDJJ2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_fca12de3-378f-4d64-bc63-6540bf40d4ce_poly_0_.7p2GhuPOKTTyH2h9DZ8u">
<gml:posList srsDimension="3">
690893.594 5336122.478 535.83
690889.606 5336124.054 535.29
690884.391 5336099.124 535.83
690893.594 5336122.478 535.83
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly_0_">
<gml:posList srsDimension="3">
690908.142 5336127.148 537.18
690915.726 5336124.156 537.18
690908.539 5336128.154 536.743
690908.142 5336127.148 537.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly.O7AqpX3yr5UXbDzptS27">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly_0_.2OS47i3UNbFVv2ed4N4w">
<gml:posList srsDimension="3">
690908.686 5336128.096 536.743
690908.539 5336128.154 536.743
690915.726 5336124.156 537.18
690908.686 5336128.096 536.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly.4l7svT5BUsZ77mIDOKt7">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly_0_.xF4aFMkxriifw5cGFGqz">
<gml:posList srsDimension="3">
690909.488 5336130.129 535.862
690908.686 5336128.096 536.743
690915.726 5336124.156 537.18
690909.488 5336130.129 535.862
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly.kx72OXPlkatRfDG0Z8D5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4c212bc4-40bc-47d8-8614-25196eb4463a_poly_0_.TwqSni5z5lhEM0aMXRat">
<gml:posList srsDimension="3">
690916.939 5336127.197 535.86
690909.488 5336130.129 535.862
690915.726 5336124.156 537.18
690916.939 5336127.197 535.86
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a4347fd6-711b-4636-974d-0d435cfe92ce">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a4347fd6-711b-4636-974d-0d435cfe92ce_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a4347fd6-711b-4636-974d-0d435cfe92ce_poly_0_">
<gml:posList srsDimension="3">
690893.753 5336146.099 510.36
690893.753 5336146.099 533.29
690893.926 5336146.036 533.29
690893.926 5336146.036 510.36
690893.753 5336146.099 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_abc514c6-0691-4103-9f67-fd47781219d9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_abc514c6-0691-4103-9f67-fd47781219d9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_abc514c6-0691-4103-9f67-fd47781219d9_poly_0_">
<gml:posList srsDimension="3">
690902.584 5336168.518 510.36
690902.584 5336168.518 533.29
690902.71 5336168.893 533.29
690902.71 5336168.893 510.36
690902.584 5336168.518 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_901e62c1-3d71-4546-992d-9f343cd226c7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_901e62c1-3d71-4546-992d-9f343cd226c7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_901e62c1-3d71-4546-992d-9f343cd226c7_poly_0_">
<gml:posList srsDimension="3">
690922.25 5336188.407 510.36
690922.25 5336188.407 533.29
690922.616 5336188.261 533.29
690922.616 5336188.261 510.36
690922.25 5336188.407 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c8478e5d-0a38-43de-a74a-58ed51d33f55">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c8478e5d-0a38-43de-a74a-58ed51d33f55_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c8478e5d-0a38-43de-a74a-58ed51d33f55_poly_0_">
<gml:posList srsDimension="3">
690885.667 5336125.606 510.36
690885.667 5336125.606 533.29
690885.824 5336125.952 533.29
690885.824 5336125.952 510.36
690885.667 5336125.606 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_6f37d04b-6a9d-4386-a46b-3273e571f494">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_6f37d04b-6a9d-4386-a46b-3273e571f494_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_6f37d04b-6a9d-4386-a46b-3273e571f494_poly_0_">
<gml:posList srsDimension="3">
690963.275 5336172.222 510.36
690963.275 5336172.222 532.053
690963.519 5336172.132 532.052
690963.519 5336172.132 510.36
690963.275 5336172.222 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d4d67b04-034c-40d0-bd8f-fe3eea857cd9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d4d67b04-034c-40d0-bd8f-fe3eea857cd9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d4d67b04-034c-40d0-bd8f-fe3eea857cd9_poly_0_">
<gml:posList srsDimension="3">
690895.027 5336149.351 510.36
690895.027 5336149.351 533.29
690895.21 5336149.278 533.29
690895.21 5336149.278 510.36
690895.027 5336149.351 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1d110cf2-7429-45fd-825b-922e6a31366c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1d110cf2-7429-45fd-825b-922e6a31366c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1d110cf2-7429-45fd-825b-922e6a31366c_poly_0_">
<gml:posList srsDimension="3">
690970.5 5336169.452 510.36
690970.5 5336169.452 532.052
690973.711 5336168.196 532.061
690973.711 5336168.196 510.36
690970.5 5336169.452 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_badd4ae8-47a0-4bad-9ff2-13148c757b29">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_badd4ae8-47a0-4bad-9ff2-13148c757b29_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_badd4ae8-47a0-4bad-9ff2-13148c757b29_poly_0_">
<gml:posList srsDimension="3">
690880.872 5336112.858 510.36
690880.872 5336112.858 533.29
690882.019 5336115.755 533.29
690882.019 5336115.755 510.36
690880.872 5336112.858 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8d50d6ed-f78c-4b19-8e14-8b9404b75007">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8d50d6ed-f78c-4b19-8e14-8b9404b75007_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8d50d6ed-f78c-4b19-8e14-8b9404b75007_poly_0_">
<gml:posList srsDimension="3">
690891.186 5336139.573 510.36
690891.186 5336139.573 533.29
690891.359 5336139.51 533.29
690891.359 5336139.51 510.36
690891.186 5336139.573 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_764bac12-c58a-4b19-95c4-e7766080dccd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_764bac12-c58a-4b19-95c4-e7766080dccd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_764bac12-c58a-4b19-95c4-e7766080dccd_poly_0_">
<gml:posList srsDimension="3">
690878.313 5336106.342 510.36
690878.313 5336106.342 533.29
690879.442 5336109.219 533.29
690879.442 5336109.219 510.36
690878.313 5336106.342 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_78c7982e-797b-41e1-af6e-6d73c7a18ed6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_78c7982e-797b-41e1-af6e-6d73c7a18ed6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_78c7982e-797b-41e1-af6e-6d73c7a18ed6_poly_0_">
<gml:posList srsDimension="3">
690945.508 5336179.516 510.36
690945.508 5336179.516 533.29
690945.282 5336178.907 533.29
690945.282 5336178.907 510.36
690945.508 5336179.516 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_4db14a6a-4994-4541-a2b1-6b13942aaee1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4db14a6a-4994-4541-a2b1-6b13942aaee1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4db14a6a-4994-4541-a2b1-6b13942aaee1_poly_0_">
<gml:posList srsDimension="3">
690945.133 5336179.652 510.36
690945.133 5336179.652 533.29
690945.508 5336179.516 533.29
690945.508 5336179.516 510.36
690945.133 5336179.652 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f0cc72ca-b1b3-461b-95b6-6fd9e433cd82">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f0cc72ca-b1b3-461b-95b6-6fd9e433cd82_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f0cc72ca-b1b3-461b-95b6-6fd9e433cd82_poly_0_">
<gml:posList srsDimension="3">
690903.994 5336172.145 510.36
690903.994 5336172.145 533.29
690904.177 5336172.072 533.29
690904.177 5336172.072 510.36
690903.994 5336172.145 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_66146f35-8648-4f94-92b5-7dd08edeb31d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_66146f35-8648-4f94-92b5-7dd08edeb31d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_66146f35-8648-4f94-92b5-7dd08edeb31d_poly_0_">
<gml:posList srsDimension="3">
690973.711 5336168.196 510.36
690973.711 5336168.196 532.061
690973.802 5336168.429 531.949
690973.802 5336168.429 510.36
690973.711 5336168.196 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1bfb2600-03cb-4ca9-8d97-3ae008983449">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1bfb2600-03cb-4ca9-8d97-3ae008983449_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1bfb2600-03cb-4ca9-8d97-3ae008983449_poly_0_">
<gml:posList srsDimension="3">
690878.13 5336106.415 510.36
690878.13 5336106.415 533.29
690878.313 5336106.342 533.29
690878.313 5336106.342 510.36
690878.13 5336106.415 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d641b1e8-2338-4340-a3e0-edcd06575557">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d641b1e8-2338-4340-a3e0-edcd06575557_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d641b1e8-2338-4340-a3e0-edcd06575557_poly_0_">
<gml:posList srsDimension="3">
691000.781 5336142.668 510.36
691000.781 5336142.668 536.124
691000.69 5336142.434 536.054
691000.69 5336142.434 510.36
691000.781 5336142.668 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1d8ea246-b978-4b62-b0a9-255899818de3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1d8ea246-b978-4b62-b0a9-255899818de3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1d8ea246-b978-4b62-b0a9-255899818de3_poly_0_">
<gml:posList srsDimension="3">
690948.413 5336178.398 510.36
690948.413 5336178.398 533.29
690948.779 5336178.262 533.29
690948.779 5336178.262 510.36
690948.413 5336178.398 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b07f8fd7-d85d-46de-9275-28d7515fecbd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b07f8fd7-d85d-46de-9275-28d7515fecbd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b07f8fd7-d85d-46de-9275-28d7515fecbd_poly_0_">
<gml:posList srsDimension="3">
690952.059 5336177.029 510.36
690952.059 5336177.029 533.29
690951.813 5336176.409 533.29
690951.813 5336176.409 510.36
690952.059 5336177.029 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_21dbf83a-67cf-4b03-80be-ec40113eb633">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_21dbf83a-67cf-4b03-80be-ec40113eb633_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_21dbf83a-67cf-4b03-80be-ec40113eb633_poly_0_">
<gml:posList srsDimension="3">
690888.382 5336132.458 510.36
690888.382 5336132.458 533.29
690888.992 5336132.211 533.29
690888.992 5336132.211 510.36
690888.382 5336132.458 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_32a12acf-56a9-4423-888e-638923865de8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_32a12acf-56a9-4423-888e-638923865de8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_32a12acf-56a9-4423-888e-638923865de8_poly_0_">
<gml:posList srsDimension="3">
690910.11 5336146.776 533.61
690910.11 5336146.776 536.63
690903.656 5336130.408 536.63
690903.656 5336130.408 533.61
690910.11 5336146.776 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_bcc0d079-cbc3-44ca-bd95-51fee27ad842">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_bcc0d079-cbc3-44ca-bd95-51fee27ad842_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_bcc0d079-cbc3-44ca-bd95-51fee27ad842_poly_0_">
<gml:posList srsDimension="3">
690954.339 5336151.016 510.36
690954.339 5336151.016 533.633
690954.43 5336151.249 533.703
690954.43 5336151.249 510.36
690954.339 5336151.016 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_fc09151a-af8c-4f0f-9848-2d818b9527b0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_fc09151a-af8c-4f0f-9848-2d818b9527b0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_fc09151a-af8c-4f0f-9848-2d818b9527b0_poly_0_">
<gml:posList srsDimension="3">
690919.345 5336189.515 510.36
690919.345 5336189.515 533.29
690919.111 5336188.845 533.29
690919.111 5336188.845 510.36
690919.345 5336189.515 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a179f965-eba8-44c5-bd46-e68362cdac44">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a179f965-eba8-44c5-bd46-e68362cdac44_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a179f965-eba8-44c5-bd46-e68362cdac44_poly_0_">
<gml:posList srsDimension="3">
690957.714 5336174.168 532.132
690957.714 5336174.168 533.29
690956.778 5336171.745 533.29
690957.714 5336174.168 532.132
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5e559a52-62bc-444c-9477-7aa8d7823b08">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5e559a52-62bc-444c-9477-7aa8d7823b08_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5e559a52-62bc-444c-9477-7aa8d7823b08_poly_0_">
<gml:posList srsDimension="3">
690907.018 5336123.897 510.36
690907.018 5336123.897 535.794
690907.811 5336125.9 536.662
690907.811 5336125.9 510.36
690907.018 5336123.897 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a2df7afe-7d7b-4849-92ba-cc1190cf2788">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a2df7afe-7d7b-4849-92ba-cc1190cf2788_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a2df7afe-7d7b-4849-92ba-cc1190cf2788_poly_0_">
<gml:posList srsDimension="3">
690957.979 5336174.068 510.36
690957.979 5336174.068 532.132
690958.205 5336174.677 531.842
690958.205 5336174.677 510.36
690957.979 5336174.068 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a464434a-5692-452a-985c-0588ea813001">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a464434a-5692-452a-985c-0588ea813001_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a464434a-5692-452a-985c-0588ea813001_poly_0_">
<gml:posList srsDimension="3">
690888.473 5336132.691 510.36
690888.473 5336132.691 533.29
690888.619 5336133.047 533.29
690888.619 5336133.047 510.36
690888.473 5336132.691 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d5f2d665-e6a6-4f8b-b7e3-c555ee156ae6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d5f2d665-e6a6-4f8b-b7e3-c555ee156ae6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d5f2d665-e6a6-4f8b-b7e3-c555ee156ae6_poly_0_">
<gml:posList srsDimension="3">
690954.43 5336151.249 510.36
690954.43 5336151.249 533.703
690953.932 5336151.44 533.703
690953.932 5336151.44 510.36
690954.43 5336151.249 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ec3ea692-af82-446b-9bf9-01e4595c5f5d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ec3ea692-af82-446b-9bf9-01e4595c5f5d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ec3ea692-af82-446b-9bf9-01e4595c5f5d_poly_0_">
<gml:posList srsDimension="3">
690906.48 5336177.667 510.36
690906.48 5336177.667 533.29
690911.852 5336191.762 533.29
690911.852 5336191.762 510.36
690906.48 5336177.667 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8d20415c-a328-468e-bcd8-c7eb29b4b91f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8d20415c-a328-468e-bcd8-c7eb29b4b91f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8d20415c-a328-468e-bcd8-c7eb29b4b91f_poly_0_">
<gml:posList srsDimension="3">
690889.939 5336135.891 510.36
690889.939 5336135.891 533.29
690889.746 5336135.964 533.29
690889.746 5336135.964 510.36
690889.939 5336135.891 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_dd6d876a-fdd6-4716-b013-245193fe1575">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_dd6d876a-fdd6-4716-b013-245193fe1575_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_dd6d876a-fdd6-4716-b013-245193fe1575_poly_0_">
<gml:posList srsDimension="3">
690903.656 5336130.408 533.61
690903.656 5336130.408 536.63
690901.642 5336131.202 536.63
690901.642 5336131.202 533.61
690903.656 5336130.408 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2c31775b-c361-427e-beef-e5044d4ad8e7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2c31775b-c361-427e-beef-e5044d4ad8e7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2c31775b-c361-427e-beef-e5044d4ad8e7_poly_0_">
<gml:posList srsDimension="3">
690913.044 5336160.122 533.61
690913.044 5336160.122 535.34
690908.096 5336147.571 535.34
690908.096 5336147.571 533.61
690913.044 5336160.122 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_25bb8a99-6b91-42b2-b7d4-486d43f210ff">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_25bb8a99-6b91-42b2-b7d4-486d43f210ff_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_25bb8a99-6b91-42b2-b7d4-486d43f210ff_poly_0_">
<gml:posList srsDimension="3">
690997.487 5336134.472 510.36
690997.487 5336134.472 533.646
690997.414 5336134.289 533.59
690997.414 5336134.289 510.36
690997.487 5336134.472 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2ace3359-42cb-405c-8a4a-354dc99593ce">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2ace3359-42cb-405c-8a4a-354dc99593ce_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2ace3359-42cb-405c-8a4a-354dc99593ce_poly_0_">
<gml:posList srsDimension="3">
690954.258 5336165.213 532.82
690954.258 5336165.213 533.29
690953.08 5336162.159 533.29
690953.08 5336162.159 532.82
690954.258 5336165.213 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a2b053f6-734c-421c-97c9-0b033ddde28d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a2b053f6-734c-421c-97c9-0b033ddde28d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a2b053f6-734c-421c-97c9-0b033ddde28d_poly_0_">
<gml:posList srsDimension="3">
690892.533 5336096.048 510.36
690892.533 5336096.048 533.61
690892.309 5336096.139 533.61
690892.309 5336096.139 510.36
690892.533 5336096.048 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_805991d2-3a30-4ee4-a048-6f1f4665a255">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_805991d2-3a30-4ee4-a048-6f1f4665a255_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_805991d2-3a30-4ee4-a048-6f1f4665a255_poly_0_">
<gml:posList srsDimension="3">
690904.341 5336172.489 510.36
690904.341 5336172.489 533.29
690911.425 5336169.686 533.29
690911.425 5336169.686 510.36
690904.341 5336172.489 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_82d66082-4819-45d0-a1d0-74e723bc3c3c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_82d66082-4819-45d0-a1d0-74e723bc3c3c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_82d66082-4819-45d0-a1d0-74e723bc3c3c_poly_0_">
<gml:posList srsDimension="3">
690901.436 5336165.641 510.36
690901.436 5336165.641 533.29
690901.619 5336165.568 533.29
690901.619 5336165.568 510.36
690901.436 5336165.641 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_6d522486-b184-45f6-ab34-806a4b751503">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_6d522486-b184-45f6-ab34-806a4b751503_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_6d522486-b184-45f6-ab34-806a4b751503_poly_0_">
<gml:posList srsDimension="3">
690897.767 5336155.792 510.36
690897.767 5336155.792 533.29
690898.915 5336158.688 533.29
690898.915 5336158.688 510.36
690897.767 5336155.792 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0b9e5de2-b639-4ab8-9e6a-9a404ad3d79a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0b9e5de2-b639-4ab8-9e6a-9a404ad3d79a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0b9e5de2-b639-4ab8-9e6a-9a404ad3d79a_poly_0_">
<gml:posList srsDimension="3">
690945.282 5336178.907 510.36
690945.282 5336178.907 533.29
690948.187 5336177.799 533.29
690948.187 5336177.799 510.36
690945.282 5336178.907 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_6f9d34de-dd1c-4b4a-8185-2874e1622893">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_6f9d34de-dd1c-4b4a-8185-2874e1622893_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_6f9d34de-dd1c-4b4a-8185-2874e1622893_poly_0_">
<gml:posList srsDimension="3">
690901.3 5336165.265 510.36
690901.3 5336165.265 533.29
690901.436 5336165.641 533.29
690901.436 5336165.641 510.36
690901.3 5336165.265 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2d20721f-7436-4609-ade8-0577b50d3435">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2d20721f-7436-4609-ade8-0577b50d3435_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2d20721f-7436-4609-ade8-0577b50d3435_poly_0_">
<gml:posList srsDimension="3">
690892.643 5336142.753 510.36
690892.643 5336142.753 533.29
690893.78 5336145.66 533.29
690893.78 5336145.66 510.36
690892.643 5336142.753 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5636b5c3-87d1-4273-add6-6845d2b44b53">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5636b5c3-87d1-4273-add6-6845d2b44b53_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5636b5c3-87d1-4273-add6-6845d2b44b53_poly_0_">
<gml:posList srsDimension="3">
690888.619 5336133.047 510.36
690888.619 5336133.047 533.29
690888.791 5336132.984 533.29
690888.791 5336132.984 510.36
690888.619 5336133.047 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_722fffd4-8a68-4f40-b4ab-7bce5db89823">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_722fffd4-8a68-4f40-b4ab-7bce5db89823_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_722fffd4-8a68-4f40-b4ab-7bce5db89823_poly_0_">
<gml:posList srsDimension="3">
690925.888 5336187.008 510.36
690925.888 5336187.008 533.29
690925.661 5336186.408 533.29
690925.661 5336186.408 510.36
690925.888 5336187.008 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b668b56c-6c96-4638-b72e-7a744ab85b81">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b668b56c-6c96-4638-b72e-7a744ab85b81_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b668b56c-6c96-4638-b72e-7a744ab85b81_poly_0_">
<gml:posList srsDimension="3">
690902.757 5336168.454 510.36
690902.757 5336168.454 533.29
690902.584 5336168.518 533.29
690902.584 5336168.518 510.36
690902.757 5336168.454 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_cfa5aa68-f32c-495f-b667-50d6d9912c12">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_cfa5aa68-f32c-495f-b667-50d6d9912c12_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_cfa5aa68-f32c-495f-b667-50d6d9912c12_poly_0_">
<gml:posList srsDimension="3">
690953.932 5336151.44 510.36
690953.932 5336151.44 533.703
690953.869 5336151.267 533.651
690953.869 5336151.267 510.36
690953.932 5336151.44 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_135c641a-db79-4bbf-8501-f393a5020067">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_135c641a-db79-4bbf-8501-f393a5020067_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_135c641a-db79-4bbf-8501-f393a5020067_poly_0_">
<gml:posList srsDimension="3">
690879.588 5336109.585 510.36
690879.588 5336109.585 533.29
690880.726 5336112.482 533.29
690880.726 5336112.482 510.36
690879.588 5336109.585 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_237e2a2a-7782-4d63-b9a1-180cff4701f2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_237e2a2a-7782-4d63-b9a1-180cff4701f2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_237e2a2a-7782-4d63-b9a1-180cff4701f2_poly_0_">
<gml:posList srsDimension="3">
690929.16 5336185.764 510.36
690929.16 5336185.764 533.29
690928.933 5336185.165 533.29
690928.933 5336185.165 510.36
690929.16 5336185.764 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_362f317d-ede2-4840-b36c-30685fff960f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_362f317d-ede2-4840-b36c-30685fff960f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_362f317d-ede2-4840-b36c-30685fff960f_poly_0_">
<gml:posList srsDimension="3">
690876.7 5336102.786 510.36
690876.7 5336102.786 533.29
690876.836 5336103.152 533.29
690876.836 5336103.152 510.36
690876.7 5336102.786 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_600912e5-568c-4b9b-a109-9cdadaed2526">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_600912e5-568c-4b9b-a109-9cdadaed2526_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_600912e5-568c-4b9b-a109-9cdadaed2526_poly_0_">
<gml:posList srsDimension="3">
690938.729 5336181.423 510.36
690938.729 5336181.423 533.29
690941.635 5336180.306 533.29
690941.635 5336180.306 510.36
690938.729 5336181.423 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_bb2d7263-b2e9-47ee-b4bf-42dddf9acef2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_bb2d7263-b2e9-47ee-b4bf-42dddf9acef2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_bb2d7263-b2e9-47ee-b4bf-42dddf9acef2_poly_0_">
<gml:posList srsDimension="3">
690941.872 5336180.905 510.36
690941.872 5336180.905 533.29
690942.238 5336180.76 533.29
690942.238 5336180.76 510.36
690941.872 5336180.905 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0b28e24d-24ab-4e7d-82bc-e410f765cb1f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0b28e24d-24ab-4e7d-82bc-e410f765cb1f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0b28e24d-24ab-4e7d-82bc-e410f765cb1f_poly_0_">
<gml:posList srsDimension="3">
690887.108 5336129.205 510.36
690887.108 5336129.205 533.29
690887.28 5336129.142 533.29
690887.28 5336129.142 510.36
690887.108 5336129.205 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5a466065-0b26-44d6-af31-605584ad1dee">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5a466065-0b26-44d6-af31-605584ad1dee_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5a466065-0b26-44d6-af31-605584ad1dee_poly_0_">
<gml:posList srsDimension="3">
690932.195 5336183.921 510.36
690932.195 5336183.921 533.29
690935.081 5336182.802 533.29
690935.081 5336182.802 510.36
690932.195 5336183.921 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_842a21e4-5df1-44ba-a1da-0c1f2750a7a7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_842a21e4-5df1-44ba-a1da-0c1f2750a7a7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_842a21e4-5df1-44ba-a1da-0c1f2750a7a7_poly_0_">
<gml:posList srsDimension="3">
690958.498 5336174.358 510.36
690958.498 5336174.358 531.928
690958.925 5336174.185 531.932
690958.925 5336174.185 510.36
690958.498 5336174.358 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_828d55f9-31bb-4346-90f9-94641b423abb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_828d55f9-31bb-4346-90f9-94641b423abb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_828d55f9-31bb-4346-90f9-94641b423abb_poly_0_">
<gml:posList srsDimension="3">
690890.075 5336136.247 510.36
690890.075 5336136.247 533.29
690891.223 5336139.144 533.29
690891.223 5336139.144 510.36
690890.075 5336136.247 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_404e9074-55fc-4481-a6f4-37d4ad2792f3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_404e9074-55fc-4481-a6f4-37d4ad2792f3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_404e9074-55fc-4481-a6f4-37d4ad2792f3_poly_0_">
<gml:posList srsDimension="3">
690891.359 5336139.51 510.36
690891.359 5336139.51 533.29
690892.497 5336142.397 533.29
690892.497 5336142.397 510.36
690891.359 5336139.51 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f2051be9-1acb-4b8b-9b3f-3270cdea8c7a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f2051be9-1acb-4b8b-9b3f-3270cdea8c7a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f2051be9-1acb-4b8b-9b3f-3270cdea8c7a_poly_0_">
<gml:posList srsDimension="3">
690877.975 5336106.039 510.36
690877.975 5336106.039 533.29
690878.13 5336106.415 533.29
690878.13 5336106.415 510.36
690877.975 5336106.039 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8cfab906-c11a-4082-96d4-abe78dbda8be">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8cfab906-c11a-4082-96d4-abe78dbda8be_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8cfab906-c11a-4082-96d4-abe78dbda8be_poly_0_">
<gml:posList srsDimension="3">
690925.661 5336186.408 510.36
690925.661 5336186.408 533.29
690928.547 5336185.31 533.29
690928.547 5336185.31 510.36
690925.661 5336186.408 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_4df3e675-071b-4920-8cf5-d5e305a2502c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4df3e675-071b-4920-8cf5-d5e305a2502c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4df3e675-071b-4920-8cf5-d5e305a2502c_poly_0_">
<gml:posList srsDimension="3">
690997.414 5336134.289 510.36
690997.414 5336134.289 533.59
690954.339 5336151.016 533.633
690954.339 5336151.016 510.36
690997.414 5336134.289 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_e70e7f60-20d9-48de-986b-7801de3656c4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_e70e7f60-20d9-48de-986b-7801de3656c4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_e70e7f60-20d9-48de-986b-7801de3656c4_poly_0_">
<gml:posList srsDimension="3">
690889.892 5336136.32 510.36
690889.892 5336136.32 533.29
690890.075 5336136.247 533.29
690890.075 5336136.247 510.36
690889.892 5336136.32 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ef5aa5e7-9d48-4d1b-82c7-22564e1b5328">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ef5aa5e7-9d48-4d1b-82c7-22564e1b5328_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ef5aa5e7-9d48-4d1b-82c7-22564e1b5328_poly_0_">
<gml:posList srsDimension="3">
690880.533 5336112.554 510.36
690880.533 5336112.554 533.29
690880.689 5336112.931 533.29
690880.689 5336112.931 510.36
690880.533 5336112.554 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_74c4660c-bccb-4527-95ec-ecb924fabbf6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_74c4660c-bccb-4527-95ec-ecb924fabbf6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_74c4660c-bccb-4527-95ec-ecb924fabbf6_poly_0_">
<gml:posList srsDimension="3">
690948.552 5336177.663 510.36
690948.552 5336177.663 533.29
690951.448 5336176.545 533.29
690951.448 5336176.545 510.36
690948.552 5336177.663 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_078201c5-a342-4043-af1f-5d5e756c3c6e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_078201c5-a342-4043-af1f-5d5e756c3c6e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_078201c5-a342-4043-af1f-5d5e756c3c6e_poly_0_">
<gml:posList srsDimension="3">
690951.448 5336176.545 510.36
690951.448 5336176.545 533.29
690951.684 5336177.155 533.29
690951.684 5336177.155 510.36
690951.448 5336176.545 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_955e4ee9-bae4-43fe-ac8b-3bf1bcb4f157">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_955e4ee9-bae4-43fe-ac8b-3bf1bcb4f157_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_955e4ee9-bae4-43fe-ac8b-3bf1bcb4f157_poly_0_">
<gml:posList srsDimension="3">
690928.547 5336185.31 510.36
690928.547 5336185.31 533.29
690928.784 5336185.9 533.29
690928.784 5336185.9 510.36
690928.547 5336185.31 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c937c608-cda3-4144-9ae3-c77725ba1d6c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c937c608-cda3-4144-9ae3-c77725ba1d6c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c937c608-cda3-4144-9ae3-c77725ba1d6c_poly_0_">
<gml:posList srsDimension="3">
690877.029 5336103.079 510.36
690877.029 5336103.079 533.29
690878.158 5336105.966 533.29
690878.158 5336105.966 510.36
690877.029 5336103.079 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_be4d5d00-cec3-4f2e-8285-5f0692a3e9d3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_be4d5d00-cec3-4f2e-8285-5f0692a3e9d3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_be4d5d00-cec3-4f2e-8285-5f0692a3e9d3_poly_0_">
<gml:posList srsDimension="3">
690938.956 5336182.013 510.36
690938.956 5336182.013 533.29
690938.729 5336181.423 533.29
690938.729 5336181.423 510.36
690938.956 5336182.013 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_68ce5193-a72e-4768-b3fb-93fcd016382b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_68ce5193-a72e-4768-b3fb-93fcd016382b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_68ce5193-a72e-4768-b3fb-93fcd016382b_poly_0_">
<gml:posList srsDimension="3">
690904.177 5336172.072 510.36
690904.177 5336172.072 533.29
690904.341 5336172.489 533.29
690904.341 5336172.489 510.36
690904.177 5336172.072 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_db410556-7e76-48b5-a112-cd516501585b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_db410556-7e76-48b5-a112-cd516501585b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_db410556-7e76-48b5-a112-cd516501585b_poly_0_">
<gml:posList srsDimension="3">
690897.584 5336155.865 510.36
690897.584 5336155.865 533.29
690897.767 5336155.792 533.29
690897.767 5336155.792 510.36
690897.584 5336155.865 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_46c4b230-751f-4901-9436-aad469967656">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_46c4b230-751f-4901-9436-aad469967656_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_46c4b230-751f-4901-9436-aad469967656_poly_0_">
<gml:posList srsDimension="3">
690876.883 5336102.713 510.36
690876.883 5336102.713 533.29
690876.7 5336102.786 533.29
690876.7 5336102.786 510.36
690876.883 5336102.713 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_3866203e-c93a-40ea-955d-5be859831a0c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_3866203e-c93a-40ea-955d-5be859831a0c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_3866203e-c93a-40ea-955d-5be859831a0c_poly_0_">
<gml:posList srsDimension="3">
690895.064 5336148.913 510.36
690895.064 5336148.913 533.29
690894.881 5336148.985 533.29
690894.881 5336148.985 510.36
690895.064 5336148.913 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ac07a559-d936-4fed-a8ca-9811872dea57">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ac07a559-d936-4fed-a8ca-9811872dea57_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ac07a559-d936-4fed-a8ca-9811872dea57_poly_0_">
<gml:posList srsDimension="3">
690892.309 5336096.139 510.36
690892.309 5336096.139 533.61
690892.274 5336096.008 533.61
690892.274 5336096.008 510.36
690892.309 5336096.139 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f4fd1a42-5073-49a4-9c8c-6fda5070a5d5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f4fd1a42-5073-49a4-9c8c-6fda5070a5d5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f4fd1a42-5073-49a4-9c8c-6fda5070a5d5_poly_0_">
<gml:posList srsDimension="3">
690904.03 5336171.716 510.36
690904.03 5336171.716 533.29
690903.838 5336171.789 533.29
690903.838 5336171.789 510.36
690904.03 5336171.716 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b2854399-b61e-4f75-a9c2-c9e3714b2928">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b2854399-b61e-4f75-a9c2-c9e3714b2928_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b2854399-b61e-4f75-a9c2-c9e3714b2928_poly_0_">
<gml:posList srsDimension="3">
690949.517 5336152.928 533.29
690949.517 5336152.928 533.644
690953.08 5336162.159 536.43
690953.08 5336162.159 533.29
690949.517 5336152.928 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8995afaa-cbe1-479e-a2e9-17fb461f00f2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8995afaa-cbe1-479e-a2e9-17fb461f00f2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8995afaa-cbe1-479e-a2e9-17fb461f00f2_poly_0_">
<gml:posList srsDimension="3">
690898.878 5336159.127 510.36
690898.878 5336159.127 533.29
690899.051 5336159.054 533.29
690899.051 5336159.054 510.36
690898.878 5336159.127 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_862688db-7de6-4f6c-8d9f-b283acbda048">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_862688db-7de6-4f6c-8d9f-b283acbda048_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_862688db-7de6-4f6c-8d9f-b283acbda048_poly_0_">
<gml:posList srsDimension="3">
690919.111 5336188.845 510.36
690919.111 5336188.845 533.29
690922.013 5336187.807 533.29
690922.013 5336187.807 510.36
690919.111 5336188.845 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_3fb07a21-02d8-4db9-8f73-45ac3d998879">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_3fb07a21-02d8-4db9-8f73-45ac3d998879_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_3fb07a21-02d8-4db9-8f73-45ac3d998879_poly_0_">
<gml:posList srsDimension="3">
690887.135 5336128.776 510.36
690887.135 5336128.776 533.29
690886.942 5336128.848 533.29
690886.942 5336128.848 510.36
690887.135 5336128.776 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a60caa0d-ee7d-4fdf-9933-70c9de698c56">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a60caa0d-ee7d-4fdf-9933-70c9de698c56_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a60caa0d-ee7d-4fdf-9933-70c9de698c56_poly_0_">
<gml:posList srsDimension="3">
690973.945 5336168.375 510.36
690973.945 5336168.375 531.949
691006.1 5336155.896 532.007
691006.1 5336155.896 510.36
690973.945 5336168.375 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_621b7a8e-1059-41ed-b447-b11ce9a704e8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_621b7a8e-1059-41ed-b447-b11ce9a704e8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_621b7a8e-1059-41ed-b447-b11ce9a704e8_poly_0_">
<gml:posList srsDimension="3">
690925.285 5336186.554 510.36
690925.285 5336186.554 533.29
690925.512 5336187.153 533.29
690925.512 5336187.153 510.36
690925.285 5336186.554 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d41551bb-cee8-48bb-aeb9-ffa4c617cd34">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d41551bb-cee8-48bb-aeb9-ffa4c617cd34_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d41551bb-cee8-48bb-aeb9-ffa4c617cd34_poly_0_">
<gml:posList srsDimension="3">
690932.422 5336184.51 510.36
690932.422 5336184.51 533.29
690932.195 5336183.921 533.29
690932.195 5336183.921 510.36
690932.422 5336184.51 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c1fe3f9e-6304-4a33-9155-8d8385f3cd36">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c1fe3f9e-6304-4a33-9155-8d8385f3cd36_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c1fe3f9e-6304-4a33-9155-8d8385f3cd36_poly_0_">
<gml:posList srsDimension="3">
690922.013 5336187.807 510.36
690922.013 5336187.807 533.29
690922.25 5336188.407 533.29
690922.25 5336188.407 510.36
690922.013 5336187.807 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_dabcf386-6ba0-4d5e-b47c-ca9dccc306cf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_dabcf386-6ba0-4d5e-b47c-ca9dccc306cf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_dabcf386-6ba0-4d5e-b47c-ca9dccc306cf_poly_0_">
<gml:posList srsDimension="3">
690925.512 5336187.153 510.36
690925.512 5336187.153 533.29
690925.888 5336187.008 533.29
690925.888 5336187.008 510.36
690925.512 5336187.153 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a6460904-c877-4869-aad2-3635bc2d1399">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a6460904-c877-4869-aad2-3635bc2d1399_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a6460904-c877-4869-aad2-3635bc2d1399_poly_0_">
<gml:posList srsDimension="3">
690884.54 5336122.699 510.36
690884.54 5336122.699 533.29
690884.723 5336122.626 533.29
690884.723 5336122.626 510.36
690884.54 5336122.699 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_111d24ff-ae11-4a45-bff1-35acf6c1464c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_111d24ff-ae11-4a45-bff1-35acf6c1464c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_111d24ff-ae11-4a45-bff1-35acf6c1464c_poly_0_">
<gml:posList srsDimension="3">
690884.394 5336122.333 510.36
690884.394 5336122.333 533.29
690884.54 5336122.699 533.29
690884.54 5336122.699 510.36
690884.394 5336122.333 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_aa97a0be-a451-4561-b9fd-7d595b9ae7a4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_aa97a0be-a451-4561-b9fd-7d595b9ae7a4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_aa97a0be-a451-4561-b9fd-7d595b9ae7a4_poly_0_">
<gml:posList srsDimension="3">
690889.746 5336135.964 510.36
690889.746 5336135.964 533.29
690889.892 5336136.32 533.29
690889.892 5336136.32 510.36
690889.746 5336135.964 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8f5cf1a1-d0f6-4b47-844b-1fb6c881ecf3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8f5cf1a1-d0f6-4b47-844b-1fb6c881ecf3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8f5cf1a1-d0f6-4b47-844b-1fb6c881ecf3_poly_0_">
<gml:posList srsDimension="3">
690920.206 5336172.4 529.6
690920.206 5336172.4 533.29
690913.43 5336175.0 533.29
690913.43 5336175.0 529.6
690920.206 5336172.4 529.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1eea132f-f29c-4a78-ba40-2e1eff907fae">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1eea132f-f29c-4a78-ba40-2e1eff907fae_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1eea132f-f29c-4a78-ba40-2e1eff907fae_poly_0_">
<gml:posList srsDimension="3">
690916.831 5336163.833 533.29
690916.831 5336163.833 533.61
690918.902 5336163.014 533.61
690918.902 5336163.014 510.36
690916.3 5336164.043 510.36
690916.3 5336164.043 533.29
690916.831 5336163.833 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_bab8e94d-c443-47ec-a7a7-ba6e435a251c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_bab8e94d-c443-47ec-a7a7-ba6e435a251c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_bab8e94d-c443-47ec-a7a7-ba6e435a251c_poly_0_">
<gml:posList srsDimension="3">
690973.802 5336168.429 510.36
690973.802 5336168.429 531.949
690973.945 5336168.375 531.949
690973.945 5336168.375 510.36
690973.802 5336168.429 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f05c4ad4-b6fd-4825-9fc0-136b39ba33ae">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f05c4ad4-b6fd-4825-9fc0-136b39ba33ae_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f05c4ad4-b6fd-4825-9fc0-136b39ba33ae_poly_0_">
<gml:posList srsDimension="3">
690888.236 5336132.102 510.36
690888.236 5336132.102 533.29
690888.382 5336132.458 533.29
690888.382 5336132.458 510.36
690888.236 5336132.102 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0a16743c-6365-4bbe-bd19-4e0b6d00589b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0a16743c-6365-4bbe-bd19-4e0b6d00589b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0a16743c-6365-4bbe-bd19-4e0b6d00589b_poly_0_">
<gml:posList srsDimension="3">
690876.836 5336103.152 510.36
690876.836 5336103.152 533.29
690877.029 5336103.079 533.29
690877.029 5336103.079 510.36
690876.836 5336103.152 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_4317508e-5431-4da3-855f-f8f7da3e94cf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4317508e-5431-4da3-855f-f8f7da3e94cf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4317508e-5431-4da3-855f-f8f7da3e94cf_poly_0_">
<gml:posList srsDimension="3">
690948.187 5336177.799 510.36
690948.187 5336177.799 533.29
690948.413 5336178.398 533.29
690948.413 5336178.398 510.36
690948.187 5336177.799 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_952ec3b5-af16-4a76-8c26-e0257a1d4b0f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_952ec3b5-af16-4a76-8c26-e0257a1d4b0f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_952ec3b5-af16-4a76-8c26-e0257a1d4b0f_poly_0_">
<gml:posList srsDimension="3">
690997.72 5336134.381 510.36
690997.72 5336134.381 533.645
690997.487 5336134.472 533.646
690997.487 5336134.472 510.36
690997.72 5336134.381 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_794e415e-c77f-4a04-9619-560a390ecd9b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_794e415e-c77f-4a04-9619-560a390ecd9b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_794e415e-c77f-4a04-9619-560a390ecd9b_poly_0_">
<gml:posList srsDimension="3">
690938.6 5336182.159 510.36
690938.6 5336182.159 533.29
690938.956 5336182.013 533.29
690938.956 5336182.013 510.36
690938.6 5336182.159 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a864bb11-dc1a-442c-8d20-fea0e124472c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a864bb11-dc1a-442c-8d20-fea0e124472c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a864bb11-dc1a-442c-8d20-fea0e124472c_poly_0_">
<gml:posList srsDimension="3">
690922.616 5336188.261 510.36
690922.616 5336188.261 533.29
690922.389 5336187.662 533.29
690922.389 5336187.662 510.36
690922.616 5336188.261 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_90b1e511-64cb-4e0b-bab2-dd2e4445d8cf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_90b1e511-64cb-4e0b-bab2-dd2e4445d8cf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_90b1e511-64cb-4e0b-bab2-dd2e4445d8cf_poly_0_">
<gml:posList srsDimension="3">
690954.258 5336165.213 533.29
690954.258 5336165.213 536.41
690956.778 5336171.745 533.29
690954.258 5336165.213 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_744966d5-a241-4e5d-af6a-83a7d2af2499">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_744966d5-a241-4e5d-af6a-83a7d2af2499_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_744966d5-a241-4e5d-af6a-83a7d2af2499_poly_0_">
<gml:posList srsDimension="3">
690898.915 5336158.688 510.36
690898.915 5336158.688 533.29
690898.732 5336158.761 533.29
690898.732 5336158.761 510.36
690898.915 5336158.688 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b53f0414-3c8f-43d6-884a-8107cb0f72cf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b53f0414-3c8f-43d6-884a-8107cb0f72cf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b53f0414-3c8f-43d6-884a-8107cb0f72cf_poly_0_">
<gml:posList srsDimension="3">
690916.837 5336165.405 510.36
690916.837 5336165.405 533.29
690916.3 5336164.043 533.29
690916.3 5336164.043 510.36
690916.837 5336165.405 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_532ef2dd-1003-485c-a0cd-09db91f12c21">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_532ef2dd-1003-485c-a0cd-09db91f12c21_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_532ef2dd-1003-485c-a0cd-09db91f12c21_poly_0_">
<gml:posList srsDimension="3">
690883.081 5336119.069 510.36
690883.081 5336119.069 533.29
690883.246 5336119.446 533.29
690883.246 5336119.446 510.36
690883.081 5336119.069 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ce2a042b-0a7b-48af-9de1-f320e5711ed9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ce2a042b-0a7b-48af-9de1-f320e5711ed9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ce2a042b-0a7b-48af-9de1-f320e5711ed9_poly_0_">
<gml:posList srsDimension="3">
690909.488 5336130.129 510.36
690909.488 5336130.129 535.862
690916.939 5336127.197 535.86
690916.939 5336127.197 510.36
690909.488 5336130.129 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_17c60ba9-23d9-4712-a76f-6389d7ea6f0a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_17c60ba9-23d9-4712-a76f-6389d7ea6f0a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_17c60ba9-23d9-4712-a76f-6389d7ea6f0a_poly_0_">
<gml:posList srsDimension="3">
690951.813 5336176.409 510.36
690951.813 5336176.409 533.29
690954.708 5336175.311 533.29
690954.708 5336175.311 510.36
690951.813 5336176.409 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_6cc073cf-52db-44f2-a574-5b0ca277c532">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_6cc073cf-52db-44f2-a574-5b0ca277c532_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_6cc073cf-52db-44f2-a574-5b0ca277c532_poly_0_">
<gml:posList srsDimension="3">
690963.519 5336172.132 510.36
690963.519 5336172.132 532.052
690970.5 5336169.452 532.052
690970.5 5336169.452 510.36
690963.519 5336172.132 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_41c867bb-4735-41d0-bc18-bffdaca8eb18">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_41c867bb-4735-41d0-bc18-bffdaca8eb18_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_41c867bb-4735-41d0-bc18-bffdaca8eb18_poly_0_">
<gml:posList srsDimension="3">
690891.03 5336139.217 510.36
690891.03 5336139.217 533.29
690891.186 5336139.573 533.29
690891.186 5336139.573 510.36
690891.03 5336139.217 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_37ce2a1d-ca9e-4fcf-905b-fec8f32e39f7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_37ce2a1d-ca9e-4fcf-905b-fec8f32e39f7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_37ce2a1d-ca9e-4fcf-905b-fec8f32e39f7_poly_0_">
<gml:posList srsDimension="3">
690913.437 5336175.019 510.36
690913.437 5336175.019 533.29
690906.48 5336177.667 533.29
690906.48 5336177.667 510.36
690913.437 5336175.019 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d5412680-aac6-46c6-ba28-067a153a7f04">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d5412680-aac6-46c6-ba28-067a153a7f04_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d5412680-aac6-46c6-ba28-067a153a7f04_poly_0_">
<gml:posList srsDimension="3">
690888.992 5336132.211 510.36
690888.992 5336132.211 533.29
690889.083 5336132.445 533.29
690889.083 5336132.445 510.36
690888.992 5336132.211 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_21560956-df3a-4266-9d10-a5475807ecfd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_21560956-df3a-4266-9d10-a5475807ecfd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_21560956-df3a-4266-9d10-a5475807ecfd_poly_0_">
<gml:posList srsDimension="3">
690892.46 5336142.826 510.36
690892.46 5336142.826 533.29
690892.643 5336142.753 533.29
690892.643 5336142.753 510.36
690892.46 5336142.826 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_017526be-821c-4bd1-b2bb-04f6a35e0f67">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_017526be-821c-4bd1-b2bb-04f6a35e0f67_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_017526be-821c-4bd1-b2bb-04f6a35e0f67_poly_0_">
<gml:posList srsDimension="3">
690959.218 5336174.096 510.36
690959.218 5336174.096 531.922
690963.355 5336172.476 531.935
690963.355 5336172.476 510.36
690959.218 5336174.096 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_3fc2e68f-d658-4019-a118-24c56037bc1b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_3fc2e68f-d658-4019-a118-24c56037bc1b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_3fc2e68f-d658-4019-a118-24c56037bc1b_poly_0_">
<gml:posList srsDimension="3">
690895.21 5336149.278 510.36
690895.21 5336149.278 533.29
690896.348 5336152.175 533.29
690896.348 5336152.175 510.36
690895.21 5336149.278 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_58964adf-a38c-428a-96ca-fda2d469c5f0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_58964adf-a38c-428a-96ca-fda2d469c5f0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_58964adf-a38c-428a-96ca-fda2d469c5f0_poly_0_">
<gml:posList srsDimension="3">
690935.694 5336183.257 510.36
690935.694 5336183.257 533.29
690935.468 5336182.657 533.29
690935.468 5336182.657 510.36
690935.694 5336183.257 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_6c0ef761-95d2-4351-8170-0d1e2a95a41a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_6c0ef761-95d2-4351-8170-0d1e2a95a41a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_6c0ef761-95d2-4351-8170-0d1e2a95a41a_poly_0_">
<gml:posList srsDimension="3">
690899.051 5336159.054 510.36
690899.051 5336159.054 533.29
690900.189 5336161.95 533.29
690900.189 5336161.95 510.36
690899.051 5336159.054 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c45c5911-40d7-4788-b96e-ec798431139c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c45c5911-40d7-4788-b96e-ec798431139c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c45c5911-40d7-4788-b96e-ec798431139c_poly_0_">
<gml:posList srsDimension="3">
690963.355 5336172.476 510.36
690963.355 5336172.476 531.935
690963.275 5336172.222 532.053
690963.275 5336172.222 510.36
690963.355 5336172.476 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_dbfe2c57-865c-46cb-be4e-9b3f01492ad0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_dbfe2c57-865c-46cb-be4e-9b3f01492ad0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_dbfe2c57-865c-46cb-be4e-9b3f01492ad0_poly_0_">
<gml:posList srsDimension="3">
690914.45 5336120.954 510.36
690914.45 5336120.954 535.79
690907.018 5336123.897 535.794
690907.018 5336123.897 510.36
690914.45 5336120.954 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1df0a5c6-1926-41c3-8acc-eaf3a70cd1a4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1df0a5c6-1926-41c3-8acc-eaf3a70cd1a4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1df0a5c6-1926-41c3-8acc-eaf3a70cd1a4_poly_0_">
<gml:posList srsDimension="3">
690889.083 5336132.445 510.36
690889.083 5336132.445 533.29
690888.473 5336132.691 533.29
690888.473 5336132.691 510.36
690889.083 5336132.445 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c578fba6-bcbf-47de-9b9b-47afcd32f6d1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c578fba6-bcbf-47de-9b9b-47afcd32f6d1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c578fba6-bcbf-47de-9b9b-47afcd32f6d1_poly_0_">
<gml:posList srsDimension="3">
690931.819 5336184.066 510.36
690931.819 5336184.066 533.29
690932.056 5336184.656 533.29
690932.056 5336184.656 510.36
690931.819 5336184.066 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a021cdef-a494-4f38-acbe-7dd29fc82fcd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a021cdef-a494-4f38-acbe-7dd29fc82fcd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a021cdef-a494-4f38-acbe-7dd29fc82fcd_poly_0_">
<gml:posList srsDimension="3">
690888.791 5336132.984 510.36
690888.791 5336132.984 533.29
690889.939 5336135.891 533.29
690889.939 5336135.891 510.36
690888.791 5336132.984 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_79e84c9e-e2c3-4269-a393-c369ee018ae6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_79e84c9e-e2c3-4269-a393-c369ee018ae6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_79e84c9e-e2c3-4269-a393-c369ee018ae6_poly_0_">
<gml:posList srsDimension="3">
690893.78 5336145.66 510.36
690893.78 5336145.66 533.29
690893.607 5336145.733 533.29
690893.607 5336145.733 510.36
690893.78 5336145.66 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_7ffe7f7d-e201-4fed-87d5-3200599ad395">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_7ffe7f7d-e201-4fed-87d5-3200599ad395_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_7ffe7f7d-e201-4fed-87d5-3200599ad395_poly_0_">
<gml:posList srsDimension="3">
690892.324 5336142.47 510.36
690892.324 5336142.47 533.29
690892.46 5336142.826 533.29
690892.46 5336142.826 510.36
690892.324 5336142.47 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d2da2303-a663-47a5-9536-ddab12f60f47">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d2da2303-a663-47a5-9536-ddab12f60f47_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d2da2303-a663-47a5-9536-ddab12f60f47_poly_0_">
<gml:posList srsDimension="3">
690884.577 5336122.26 510.36
690884.577 5336122.26 533.29
690884.394 5336122.333 533.29
690884.394 5336122.333 510.36
690884.577 5336122.26 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ad7d2eb9-4849-4c46-ba0e-aaa3b4968e2f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ad7d2eb9-4849-4c46-ba0e-aaa3b4968e2f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ad7d2eb9-4849-4c46-ba0e-aaa3b4968e2f_poly_0_">
<gml:posList srsDimension="3">
690922.389 5336187.662 510.36
690922.389 5336187.662 533.29
690925.285 5336186.554 533.29
690925.285 5336186.554 510.36
690922.389 5336187.662 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c74ea99f-2e7d-4b57-90bd-0f4c0c9f1ad0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c74ea99f-2e7d-4b57-90bd-0f4c0c9f1ad0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c74ea99f-2e7d-4b57-90bd-0f4c0c9f1ad0_poly_0_">
<gml:posList srsDimension="3">
690903.838 5336171.789 510.36
690903.838 5336171.789 533.29
690903.994 5336172.145 533.29
690903.994 5336172.145 510.36
690903.838 5336171.789 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_25fb3b46-7ad5-4ca3-97ce-0984b3a8e3fa">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_25fb3b46-7ad5-4ca3-97ce-0984b3a8e3fa_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_25fb3b46-7ad5-4ca3-97ce-0984b3a8e3fa_poly_0_">
<gml:posList srsDimension="3">
690958.205 5336174.677 510.36
690958.205 5336174.677 531.842
690958.571 5336174.531 531.844
690958.571 5336174.531 510.36
690958.205 5336174.677 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b080822d-8a5b-4154-8aae-de9cf7ad5e0a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b080822d-8a5b-4154-8aae-de9cf7ad5e0a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b080822d-8a5b-4154-8aae-de9cf7ad5e0a_poly_0_">
<gml:posList srsDimension="3">
690898.732 5336158.761 510.36
690898.732 5336158.761 533.29
690898.878 5336159.127 533.29
690898.878 5336159.127 510.36
690898.732 5336158.761 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f9a114de-5439-416c-bf6c-af068b9a923a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f9a114de-5439-416c-bf6c-af068b9a923a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f9a114de-5439-416c-bf6c-af068b9a923a_poly_0_">
<gml:posList srsDimension="3">
690958.571 5336174.531 510.36
690958.571 5336174.531 531.844
690958.498 5336174.358 531.928
690958.498 5336174.358 510.36
690958.571 5336174.531 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2d02122a-abfa-4e76-9ec0-ff29ae7d8b46">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2d02122a-abfa-4e76-9ec0-ff29ae7d8b46_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2d02122a-abfa-4e76-9ec0-ff29ae7d8b46_poly_0_">
<gml:posList srsDimension="3">
690942.238 5336180.76 510.36
690942.238 5336180.76 533.29
690942.011 5336180.16 533.29
690942.011 5336180.16 510.36
690942.238 5336180.76 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d50c33cd-e804-4fa8-9ae5-14f79de62a1c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d50c33cd-e804-4fa8-9ae5-14f79de62a1c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d50c33cd-e804-4fa8-9ae5-14f79de62a1c_poly_0_">
<gml:posList srsDimension="3">
690896.31 5336152.613 510.36
690896.31 5336152.613 533.29
690896.493 5336152.541 533.29
690896.493 5336152.541 510.36
690896.31 5336152.613 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0c44cdbd-5067-4dbd-a712-7ad023bf7a8f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0c44cdbd-5067-4dbd-a712-7ad023bf7a8f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0c44cdbd-5067-4dbd-a712-7ad023bf7a8f_poly_0_">
<gml:posList srsDimension="3">
690902.71 5336168.893 510.36
690902.71 5336168.893 533.29
690902.893 5336168.82 533.29
690902.893 5336168.82 510.36
690902.71 5336168.893 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0672475a-de5f-4a63-882b-ee8246fcfb16">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0672475a-de5f-4a63-882b-ee8246fcfb16_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0672475a-de5f-4a63-882b-ee8246fcfb16_poly_0_">
<gml:posList srsDimension="3">
690900.016 5336162.013 510.36
690900.016 5336162.013 533.29
690900.152 5336162.378 533.29
690900.152 5336162.378 510.36
690900.016 5336162.013 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_e5bda8de-1161-4f5b-8424-2fd40953482c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_e5bda8de-1161-4f5b-8424-2fd40953482c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_e5bda8de-1161-4f5b-8424-2fd40953482c_poly_0_">
<gml:posList srsDimension="3">
691000.812 5336142.389 510.36
691000.812 5336142.389 536.055
690997.72 5336134.381 533.645
690997.72 5336134.381 510.36
691000.812 5336142.389 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_9a27f6cf-0620-4d15-892a-d4cf381ada46">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9a27f6cf-0620-4d15-892a-d4cf381ada46_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9a27f6cf-0620-4d15-892a-d4cf381ada46_poly_0_">
<gml:posList srsDimension="3">
691000.69 5336142.434 510.36
691000.69 5336142.434 536.054
691000.812 5336142.389 536.055
691000.812 5336142.389 510.36
691000.69 5336142.434 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_50ec8db1-20b2-4f06-ac8c-50a82cac4b1e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_50ec8db1-20b2-4f06-ac8c-50a82cac4b1e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_50ec8db1-20b2-4f06-ac8c-50a82cac4b1e_poly_0_">
<gml:posList srsDimension="3">
690876.923 5336102.195 510.36
690876.923 5336102.195 533.29
690876.71 5336102.276 533.29
690876.71 5336102.276 510.36
690876.923 5336102.195 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_856da2a4-6214-47cd-8e9c-45810733b5f9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_856da2a4-6214-47cd-8e9c-45810733b5f9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_856da2a4-6214-47cd-8e9c-45810733b5f9_poly_0_">
<gml:posList srsDimension="3">
690958.925 5336174.185 510.36
690958.925 5336174.185 531.932
690958.834 5336173.941 532.048
690958.834 5336173.941 510.36
690958.925 5336174.185 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_26465291-c4b7-45f1-9c5f-d8b055833a71">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_26465291-c4b7-45f1-9c5f-d8b055833a71_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_26465291-c4b7-45f1-9c5f-d8b055833a71_poly_0_">
<gml:posList srsDimension="3">
691000.964 5336142.595 510.36
691000.964 5336142.595 536.124
691000.781 5336142.668 536.124
691000.781 5336142.668 510.36
691000.964 5336142.595 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d33fdb5b-8f12-4f04-a6a4-5421b5cfe80a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d33fdb5b-8f12-4f04-a6a4-5421b5cfe80a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d33fdb5b-8f12-4f04-a6a4-5421b5cfe80a_poly_0_">
<gml:posList srsDimension="3">
690935.468 5336182.657 510.36
690935.468 5336182.657 533.29
690938.363 5336181.569 533.29
690938.363 5336181.569 510.36
690935.468 5336182.657 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_dce899c6-7e6d-4dd1-99ee-8b4b0ea32e43">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_dce899c6-7e6d-4dd1-99ee-8b4b0ea32e43_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_dce899c6-7e6d-4dd1-99ee-8b4b0ea32e43_poly_0_">
<gml:posList srsDimension="3">
690954.708 5336175.311 510.36
690954.708 5336175.311 533.29
690954.944 5336175.921 533.29
690954.944 5336175.921 510.36
690954.708 5336175.311 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_cd8dbee8-f871-462c-812a-ae1c0458836d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_cd8dbee8-f871-462c-812a-ae1c0458836d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_cd8dbee8-f871-462c-812a-ae1c0458836d_poly_0_">
<gml:posList srsDimension="3">
690883.246 5336119.446 510.36
690883.246 5336119.446 533.29
690883.439 5336119.373 533.29
690883.439 5336119.373 510.36
690883.246 5336119.446 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_42a4c094-5679-4631-8adf-b75f0b607460">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_42a4c094-5679-4631-8adf-b75f0b607460_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_42a4c094-5679-4631-8adf-b75f0b607460_poly_0_">
<gml:posList srsDimension="3">
690911.572 5336170.074 529.6
690911.572 5336170.074 533.29
690918.274 5336167.493 533.29
690918.274 5336167.493 529.6
690911.572 5336170.074 529.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_43b22dff-c541-4c7b-b5f3-c65aff706f24">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_43b22dff-c541-4c7b-b5f3-c65aff706f24_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_43b22dff-c541-4c7b-b5f3-c65aff706f24_poly_0_">
<gml:posList srsDimension="3">
690876.877 5336102.093 510.36
690876.877 5336102.093 533.29
690876.923 5336102.195 533.29
690876.923 5336102.195 510.36
690876.877 5336102.093 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0e326372-05d3-4bba-81b9-17831c7a3ef6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0e326372-05d3-4bba-81b9-17831c7a3ef6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0e326372-05d3-4bba-81b9-17831c7a3ef6_poly_0_">
<gml:posList srsDimension="3">
690882.019 5336115.755 510.36
690882.019 5336115.755 533.29
690881.826 5336115.828 533.29
690881.826 5336115.828 510.36
690882.019 5336115.755 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5de3993d-13b6-44c0-95b7-be9bb2af8bcd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5de3993d-13b6-44c0-95b7-be9bb2af8bcd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5de3993d-13b6-44c0-95b7-be9bb2af8bcd_poly_0_">
<gml:posList srsDimension="3">
690928.784 5336185.9 510.36
690928.784 5336185.9 533.29
690929.16 5336185.764 533.29
690929.16 5336185.764 510.36
690928.784 5336185.9 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_bfb3c0b6-8523-4429-8b6b-b701219683c2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_bfb3c0b6-8523-4429-8b6b-b701219683c2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_bfb3c0b6-8523-4429-8b6b-b701219683c2_poly_0_">
<gml:posList srsDimension="3">
690878.158 5336105.966 510.36
690878.158 5336105.966 533.29
690877.975 5336106.039 533.29
690877.975 5336106.039 510.36
690878.158 5336105.966 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_00378845-412b-4112-b1ba-16e458d6d4af">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_00378845-412b-4112-b1ba-16e458d6d4af_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_00378845-412b-4112-b1ba-16e458d6d4af_poly_0_">
<gml:posList srsDimension="3">
690882.155 5336116.121 510.36
690882.155 5336116.121 533.29
690883.284 5336118.987 533.29
690883.284 5336118.987 510.36
690882.155 5336116.121 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_046aee10-3cd1-4065-b503-c1bec6c2d40a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_046aee10-3cd1-4065-b503-c1bec6c2d40a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_046aee10-3cd1-4065-b503-c1bec6c2d40a_poly_0_">
<gml:posList srsDimension="3">
690896.348 5336152.175 510.36
690896.348 5336152.175 533.29
690896.165 5336152.248 533.29
690896.165 5336152.248 510.36
690896.348 5336152.175 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5b65c270-5478-4548-8387-8f5e5961a805">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5b65c270-5478-4548-8387-8f5e5961a805_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5b65c270-5478-4548-8387-8f5e5961a805_poly_0_">
<gml:posList srsDimension="3">
690904.751 5336127.101 510.36
690904.751 5336127.101 533.61
690892.533 5336096.048 533.61
690892.533 5336096.048 510.36
690904.751 5336127.101 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_91a12944-c5ca-4925-a752-eaf36e7d2a10">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_91a12944-c5ca-4925-a752-eaf36e7d2a10_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_91a12944-c5ca-4925-a752-eaf36e7d2a10_poly_0_">
<gml:posList srsDimension="3">
690880.689 5336112.931 510.36
690880.689 5336112.931 533.29
690880.872 5336112.858 533.29
690880.872 5336112.858 510.36
690880.689 5336112.931 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8b7a1b4b-bd5b-4b57-986e-9b7363475ea7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8b7a1b4b-bd5b-4b57-986e-9b7363475ea7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8b7a1b4b-bd5b-4b57-986e-9b7363475ea7_poly_0_">
<gml:posList srsDimension="3">
690915.058 5336159.329 533.29
690915.058 5336159.329 533.61
690916.831 5336163.833 533.61
690916.831 5336163.833 533.29
690915.058 5336159.329 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ef04023f-447e-47e9-90a3-d018b80ffbcf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ef04023f-447e-47e9-90a3-d018b80ffbcf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ef04023f-447e-47e9-90a3-d018b80ffbcf_poly_0_">
<gml:posList srsDimension="3">
690888.419 5336132.029 510.36
690888.419 5336132.029 533.29
690888.236 5336132.102 533.29
690888.236 5336132.102 510.36
690888.419 5336132.029 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_16e87bad-8df3-4193-a0bd-e57237225ec4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_16e87bad-8df3-4193-a0bd-e57237225ec4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_16e87bad-8df3-4193-a0bd-e57237225ec4_poly_0_">
<gml:posList srsDimension="3">
690951.684 5336177.155 510.36
690951.684 5336177.155 533.29
690952.059 5336177.029 533.29
690952.059 5336177.029 510.36
690951.684 5336177.155 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_14cddc50-fc0d-4e48-8e54-ffe73b1286cd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_14cddc50-fc0d-4e48-8e54-ffe73b1286cd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_14cddc50-fc0d-4e48-8e54-ffe73b1286cd_poly_0_">
<gml:posList srsDimension="3">
690893.607 5336145.733 510.36
690893.607 5336145.733 533.29
690893.753 5336146.099 533.29
690893.753 5336146.099 510.36
690893.607 5336145.733 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_56728076-f979-4790-a070-06a5b745de7e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56728076-f979-4790-a070-06a5b745de7e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56728076-f979-4790-a070-06a5b745de7e_poly_0_">
<gml:posList srsDimension="3">
690887.28 5336129.142 510.36
690887.28 5336129.142 533.29
690888.419 5336132.029 533.29
690888.419 5336132.029 510.36
690887.28 5336129.142 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_89f81338-54c4-4881-8235-f1f58de1003b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_89f81338-54c4-4881-8235-f1f58de1003b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_89f81338-54c4-4881-8235-f1f58de1003b_poly_0_">
<gml:posList srsDimension="3">
690883.439 5336119.373 510.36
690883.439 5336119.373 533.29
690884.577 5336122.26 533.29
690884.577 5336122.26 510.36
690883.439 5336119.373 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_c563949f-bc1a-4650-a6d7-5c04e9a57979">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_c563949f-bc1a-4650-a6d7-5c04e9a57979_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_c563949f-bc1a-4650-a6d7-5c04e9a57979_poly_0_">
<gml:posList srsDimension="3">
690935.328 5336183.403 510.36
690935.328 5336183.403 533.29
690935.694 5336183.257 533.29
690935.694 5336183.257 510.36
690935.328 5336183.403 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_99d03a20-d6e2-4fc7-847b-58fc44e3a290">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_99d03a20-d6e2-4fc7-847b-58fc44e3a290_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_99d03a20-d6e2-4fc7-847b-58fc44e3a290_poly_0_">
<gml:posList srsDimension="3">
690918.979 5336189.66 510.36
690918.979 5336189.66 533.29
690919.345 5336189.515 533.29
690919.345 5336189.515 510.36
690918.979 5336189.66 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_4eec6248-174c-4a8f-873f-236e86143761">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_4eec6248-174c-4a8f-873f-236e86143761_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_4eec6248-174c-4a8f-873f-236e86143761_poly_0_">
<gml:posList srsDimension="3">
690894.881 5336148.985 510.36
690894.881 5336148.985 533.29
690895.027 5336149.351 533.29
690895.027 5336149.351 510.36
690894.881 5336148.985 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_9501eb07-9cd6-48b7-8d44-e9bda9e0bcbb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9501eb07-9cd6-48b7-8d44-e9bda9e0bcbb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9501eb07-9cd6-48b7-8d44-e9bda9e0bcbb_poly_0_">
<gml:posList srsDimension="3">
690900.152 5336162.378 510.36
690900.152 5336162.378 533.29
690900.335 5336162.306 533.29
690900.335 5336162.306 510.36
690900.152 5336162.378 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_470de155-ae06-43f0-9568-fe2457c109d4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_470de155-ae06-43f0-9568-fe2457c109d4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_470de155-ae06-43f0-9568-fe2457c109d4_poly_0_">
<gml:posList srsDimension="3">
690930.737 5336184.478 533.29
690931.819 5336184.066 533.29
690931.819 5336184.066 510.36
690928.933 5336185.165 510.36
690928.933 5336185.165 533.29
690930.737 5336184.478 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_daade91c-d7f7-4544-8d07-e5a04fe5b82d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_daade91c-d7f7-4544-8d07-e5a04fe5b82d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_daade91c-d7f7-4544-8d07-e5a04fe5b82d_poly_0_">
<gml:posList srsDimension="3">
690959.109 5336173.821 510.36
690959.109 5336173.821 532.054
690959.218 5336174.096 531.922
690959.218 5336174.096 510.36
690959.109 5336173.821 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_aebad0d5-386e-4b5c-a3e4-e3e5fbd4172b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_aebad0d5-386e-4b5c-a3e4-e3e5fbd4172b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_aebad0d5-386e-4b5c-a3e4-e3e5fbd4172b_poly_0_">
<gml:posList srsDimension="3">
690881.962 5336116.193 510.36
690881.962 5336116.193 533.29
690882.155 5336116.121 533.29
690882.155 5336116.121 510.36
690881.962 5336116.193 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_829b1b94-c950-4ab9-8ab9-eb4f9868c9f6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_829b1b94-c950-4ab9-8ab9-eb4f9868c9f6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_829b1b94-c950-4ab9-8ab9-eb4f9868c9f6_poly_0_">
<gml:posList srsDimension="3">
690883.284 5336118.987 510.36
690883.284 5336118.987 533.29
690883.081 5336119.069 533.29
690883.081 5336119.069 510.36
690883.284 5336118.987 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_dedce3fe-2e18-49ec-9e18-d7b64b2e776b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_dedce3fe-2e18-49ec-9e18-d7b64b2e776b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_dedce3fe-2e18-49ec-9e18-d7b64b2e776b_poly_0_">
<gml:posList srsDimension="3">
690885.86 5336125.533 510.36
690885.86 5336125.533 533.29
690885.667 5336125.606 533.29
690885.667 5336125.606 510.36
690885.86 5336125.533 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_fae3af26-8a95-44fa-8b34-65358a584b07">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_fae3af26-8a95-44fa-8b34-65358a584b07_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_fae3af26-8a95-44fa-8b34-65358a584b07_poly_0_">
<gml:posList srsDimension="3">
690885.997 5336125.879 510.36
690885.997 5336125.879 533.29
690887.135 5336128.776 533.29
690887.135 5336128.776 510.36
690885.997 5336125.879 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_9280f5af-aed8-4119-9386-31b05f573962">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9280f5af-aed8-4119-9386-31b05f573962_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9280f5af-aed8-4119-9386-31b05f573962_poly_0_">
<gml:posList srsDimension="3">
690944.896 5336179.052 510.36
690944.896 5336179.052 533.29
690945.133 5336179.652 533.29
690945.133 5336179.652 510.36
690944.896 5336179.052 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d426b6be-e67d-4fda-a324-4508b3f70511">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d426b6be-e67d-4fda-a324-4508b3f70511_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d426b6be-e67d-4fda-a324-4508b3f70511_poly_0_">
<gml:posList srsDimension="3">
690911.852 5336191.762 510.36
690911.852 5336191.762 533.29
690918.808 5336189.173 533.29
690918.808 5336189.173 510.36
690911.852 5336191.762 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2e2c37c9-6dd5-404c-8b05-fbd27eab4011">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e2c37c9-6dd5-404c-8b05-fbd27eab4011_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e2c37c9-6dd5-404c-8b05-fbd27eab4011_poly_0_">
<gml:posList srsDimension="3">
690901.473 5336165.202 510.36
690901.473 5336165.202 533.29
690901.3 5336165.265 533.29
690901.3 5336165.265 510.36
690901.473 5336165.202 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_9985fbd6-ce6f-4317-a86a-ca7f9083b379">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9985fbd6-ce6f-4317-a86a-ca7f9083b379_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9985fbd6-ce6f-4317-a86a-ca7f9083b379_poly_0_">
<gml:posList srsDimension="3">
690941.635 5336180.306 510.36
690941.635 5336180.306 533.29
690941.872 5336180.905 533.29
690941.872 5336180.905 510.36
690941.635 5336180.306 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b27ec88f-e51b-4efd-a655-99a0ea734ad6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b27ec88f-e51b-4efd-a655-99a0ea734ad6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b27ec88f-e51b-4efd-a655-99a0ea734ad6_poly_0_">
<gml:posList srsDimension="3">
690897.631 5336155.427 510.36
690897.631 5336155.427 533.29
690897.449 5336155.499 533.29
690897.449 5336155.499 510.36
690897.631 5336155.427 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_e620c474-25bb-4638-a0c8-8214513cea95">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_e620c474-25bb-4638-a0c8-8214513cea95_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_e620c474-25bb-4638-a0c8-8214513cea95_poly_0_">
<gml:posList srsDimension="3">
690886.942 5336128.848 510.36
690886.942 5336128.848 533.29
690887.108 5336129.205 533.29
690887.108 5336129.205 510.36
690886.942 5336128.848 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_3fd89040-a7a3-402b-984f-387c29dc63c8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_3fd89040-a7a3-402b-984f-387c29dc63c8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_3fd89040-a7a3-402b-984f-387c29dc63c8_poly_0_">
<gml:posList srsDimension="3">
690892.497 5336142.397 510.36
690892.497 5336142.397 533.29
690892.324 5336142.47 533.29
690892.324 5336142.47 510.36
690892.497 5336142.397 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_abc49961-0dbd-402a-9128-38f974ccf90f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_abc49961-0dbd-402a-9128-38f974ccf90f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_abc49961-0dbd-402a-9128-38f974ccf90f_poly_0_">
<gml:posList srsDimension="3">
690942.011 5336180.16 510.36
690942.011 5336180.16 533.29
690944.896 5336179.052 533.29
690944.896 5336179.052 510.36
690942.011 5336180.16 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_fe8172a3-02ae-48b9-8388-c8d031218f9e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_fe8172a3-02ae-48b9-8388-c8d031218f9e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_fe8172a3-02ae-48b9-8388-c8d031218f9e_poly_0_">
<gml:posList srsDimension="3">
690897.449 5336155.499 510.36
690897.449 5336155.499 533.29
690897.584 5336155.865 533.29
690897.584 5336155.865 510.36
690897.449 5336155.499 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1bc7e06e-3ab5-45ae-aaa5-540652e15c8f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1bc7e06e-3ab5-45ae-aaa5-540652e15c8f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1bc7e06e-3ab5-45ae-aaa5-540652e15c8f_poly_0_">
<gml:posList srsDimension="3">
691002.54 5336146.678 532.82
691002.54 5336146.678 536.41
690954.258 5336165.213 536.41
690954.258 5336165.213 533.29
690954.258 5336165.213 532.82
691002.54 5336146.678 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_762258bd-caa9-4b28-9374-29ea347737e0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_762258bd-caa9-4b28-9374-29ea347737e0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_762258bd-caa9-4b28-9374-29ea347737e0_poly_0_">
<gml:posList srsDimension="3">
690948.779 5336178.262 510.36
690948.779 5336178.262 533.29
690948.552 5336177.663 533.29
690948.552 5336177.663 510.36
690948.779 5336178.262 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_e134baeb-766c-4bf7-98b5-4e30475193ea">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_e134baeb-766c-4bf7-98b5-4e30475193ea_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_e134baeb-766c-4bf7-98b5-4e30475193ea_poly_0_">
<gml:posList srsDimension="3">
690900.189 5336161.95 510.36
690900.189 5336161.95 533.29
690900.016 5336162.013 533.29
690900.016 5336162.013 510.36
690900.189 5336161.95 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_e31358f2-ca74-4282-a9ab-fae7708647fa">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_e31358f2-ca74-4282-a9ab-fae7708647fa_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_e31358f2-ca74-4282-a9ab-fae7708647fa_poly_0_">
<gml:posList srsDimension="3">
690901.619 5336165.568 510.36
690901.619 5336165.568 533.29
690902.757 5336168.454 533.29
690902.757 5336168.454 510.36
690901.619 5336165.568 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_177164fe-8188-4952-a4b2-54eab414983e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_177164fe-8188-4952-a4b2-54eab414983e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_177164fe-8188-4952-a4b2-54eab414983e_poly_0_">
<gml:posList srsDimension="3">
690881.826 5336115.828 510.36
690881.826 5336115.828 533.29
690881.962 5336116.193 533.29
690881.962 5336116.193 510.36
690881.826 5336115.828 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2688ef82-c9cb-4a3a-b1de-e64bddd18a55">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2688ef82-c9cb-4a3a-b1de-e64bddd18a55_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2688ef82-c9cb-4a3a-b1de-e64bddd18a55_poly_0_">
<gml:posList srsDimension="3">
690908.686 5336128.096 510.36
690908.686 5336128.096 536.743
690909.488 5336130.129 535.862
690909.488 5336130.129 510.36
690908.686 5336128.096 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f15b7312-f7a3-4bf2-8077-5db3d5c27ba6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f15b7312-f7a3-4bf2-8077-5db3d5c27ba6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f15b7312-f7a3-4bf2-8077-5db3d5c27ba6_poly_0_">
<gml:posList srsDimension="3">
690884.723 5336122.626 510.36
690884.723 5336122.626 533.29
690885.86 5336125.533 533.29
690885.86 5336125.533 510.36
690884.723 5336122.626 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1ce3ede4-aca3-4eac-bee8-a7f7d717f17b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1ce3ede4-aca3-4eac-bee8-a7f7d717f17b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1ce3ede4-aca3-4eac-bee8-a7f7d717f17b_poly_0_">
<gml:posList srsDimension="3">
690918.274 5336167.493 529.6
690918.274 5336167.493 533.29
690920.206 5336172.4 533.29
690920.206 5336172.4 529.6
690918.274 5336167.493 529.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_d538810c-4089-47d9-b778-e6c8a57efc41">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_d538810c-4089-47d9-b778-e6c8a57efc41_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_d538810c-4089-47d9-b778-e6c8a57efc41_poly_0_">
<gml:posList srsDimension="3">
690954.944 5336175.921 510.36
690954.944 5336175.921 533.29
690955.3 5336175.785 533.29
690955.3 5336175.785 510.36
690954.944 5336175.921 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1fc59354-6dd2-41f3-81ed-28dbcf1fce6d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1fc59354-6dd2-41f3-81ed-28dbcf1fce6d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1fc59354-6dd2-41f3-81ed-28dbcf1fce6d_poly_0_">
<gml:posList srsDimension="3">
690896.493 5336152.541 510.36
690896.493 5336152.541 533.29
690897.631 5336155.427 533.29
690897.631 5336155.427 510.36
690896.493 5336152.541 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_8ff3a765-247b-4cb8-a4fb-4f9de007636b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_8ff3a765-247b-4cb8-a4fb-4f9de007636b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_8ff3a765-247b-4cb8-a4fb-4f9de007636b_poly_0_">
<gml:posList srsDimension="3">
690885.824 5336125.952 510.36
690885.824 5336125.952 533.29
690885.997 5336125.879 533.29
690885.997 5336125.879 510.36
690885.824 5336125.952 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a3c62c76-acf2-44ea-8782-b57b9fa50b04">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a3c62c76-acf2-44ea-8782-b57b9fa50b04_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a3c62c76-acf2-44ea-8782-b57b9fa50b04_poly_0_">
<gml:posList srsDimension="3">
690958.834 5336173.941 510.36
690958.834 5336173.941 532.048
690959.109 5336173.821 532.054
690959.109 5336173.821 510.36
690958.834 5336173.941 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_ac52418c-0e53-4aa5-8cc2-28d9ba4fc11c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_ac52418c-0e53-4aa5-8cc2-28d9ba4fc11c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_ac52418c-0e53-4aa5-8cc2-28d9ba4fc11c_poly_0_">
<gml:posList srsDimension="3">
690876.71 5336102.276 510.36
690876.71 5336102.276 533.29
690876.883 5336102.713 533.29
690876.883 5336102.713 510.36
690876.71 5336102.276 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2d2dab0b-e5fe-464c-ad3a-34679594bbad">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2d2dab0b-e5fe-464c-ad3a-34679594bbad_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2d2dab0b-e5fe-464c-ad3a-34679594bbad_poly_0_">
<gml:posList srsDimension="3">
690879.442 5336109.219 510.36
690879.442 5336109.219 533.29
690879.249 5336109.291 533.29
690879.249 5336109.291 510.36
690879.442 5336109.219 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5fbac2cd-2753-4750-9fa3-60fa5ea8db01">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5fbac2cd-2753-4750-9fa3-60fa5ea8db01_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5fbac2cd-2753-4750-9fa3-60fa5ea8db01_poly_0_">
<gml:posList srsDimension="3">
690891.223 5336139.144 510.36
690891.223 5336139.144 533.29
690891.03 5336139.217 533.29
690891.03 5336139.217 510.36
690891.223 5336139.144 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2a61ec4b-e275-43f6-9b38-e32ca2235dc5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2a61ec4b-e275-43f6-9b38-e32ca2235dc5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2a61ec4b-e275-43f6-9b38-e32ca2235dc5_poly_0_">
<gml:posList srsDimension="3">
690879.405 5336109.658 510.36
690879.405 5336109.658 533.29
690879.588 5336109.585 533.29
690879.588 5336109.585 510.36
690879.405 5336109.658 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_cce571c3-e217-4db7-8975-35e19cc82064">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_cce571c3-e217-4db7-8975-35e19cc82064_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_cce571c3-e217-4db7-8975-35e19cc82064_poly_0_">
<gml:posList srsDimension="3">
690879.249 5336109.291 510.36
690879.249 5336109.291 533.29
690879.405 5336109.658 533.29
690879.405 5336109.658 510.36
690879.249 5336109.291 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_65bfe999-e854-490e-9b7f-146c38745338">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_65bfe999-e854-490e-9b7f-146c38745338_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_65bfe999-e854-490e-9b7f-146c38745338_poly_0_">
<gml:posList srsDimension="3">
690935.081 5336182.802 510.36
690935.081 5336182.802 533.29
690935.328 5336183.403 533.29
690935.328 5336183.403 510.36
690935.081 5336182.802 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_de908942-088d-4445-b66a-e082a263af58">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_de908942-088d-4445-b66a-e082a263af58_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_de908942-088d-4445-b66a-e082a263af58_poly_0_">
<gml:posList srsDimension="3">
690896.165 5336152.248 510.36
690896.165 5336152.248 533.29
690896.31 5336152.613 533.29
690896.31 5336152.613 510.36
690896.165 5336152.248 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b82c92d3-74e5-4844-ba08-972957850dc3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b82c92d3-74e5-4844-ba08-972957850dc3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b82c92d3-74e5-4844-ba08-972957850dc3_poly_0_">
<gml:posList srsDimension="3">
690918.808 5336189.173 510.36
690918.808 5336189.173 533.29
690918.979 5336189.66 533.29
690918.979 5336189.66 510.36
690918.808 5336189.173 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_40586976-8885-4ae9-a331-f6609c18ca6a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_40586976-8885-4ae9-a331-f6609c18ca6a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_40586976-8885-4ae9-a331-f6609c18ca6a_poly_0_">
<gml:posList srsDimension="3">
690955.3 5336175.785 510.36
690955.3 5336175.785 533.29
690955.064 5336175.175 533.29
690955.064 5336175.175 510.36
690955.3 5336175.785 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_3dd0d38d-f178-45cd-9cbb-1d81884c209e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_3dd0d38d-f178-45cd-9cbb-1d81884c209e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_3dd0d38d-f178-45cd-9cbb-1d81884c209e_poly_0_">
<gml:posList srsDimension="3">
690893.926 5336146.036 510.36
690893.926 5336146.036 533.29
690895.064 5336148.913 533.29
690895.064 5336148.913 510.36
690893.926 5336146.036 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_5d3098b7-6e9f-43bc-bff7-52b6216b4500">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_5d3098b7-6e9f-43bc-bff7-52b6216b4500_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_5d3098b7-6e9f-43bc-bff7-52b6216b4500_poly_0_">
<gml:posList srsDimension="3">
690938.363 5336181.569 510.36
690938.363 5336181.569 533.29
690938.6 5336182.159 533.29
690938.6 5336182.159 510.36
690938.363 5336181.569 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2f5aaf43-6593-41b6-8fba-03da5e0eea8a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2f5aaf43-6593-41b6-8fba-03da5e0eea8a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2f5aaf43-6593-41b6-8fba-03da5e0eea8a_poly_0_">
<gml:posList srsDimension="3">
690902.893 5336168.82 510.36
690902.893 5336168.82 533.29
690904.03 5336171.716 533.29
690904.03 5336171.716 510.36
690902.893 5336168.82 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_37e12e2e-88bd-4505-939c-4005d8cf53cf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_37e12e2e-88bd-4505-939c-4005d8cf53cf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_37e12e2e-88bd-4505-939c-4005d8cf53cf_poly_0_">
<gml:posList srsDimension="3">
690932.056 5336184.656 510.36
690932.056 5336184.656 533.29
690932.422 5336184.51 533.29
690932.422 5336184.51 510.36
690932.056 5336184.656 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_3bd69527-2e1d-476c-bc3c-6a4e69bc324f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_3bd69527-2e1d-476c-bc3c-6a4e69bc324f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_3bd69527-2e1d-476c-bc3c-6a4e69bc324f_poly_0_">
<gml:posList srsDimension="3">
690880.726 5336112.482 510.36
690880.726 5336112.482 533.29
690880.533 5336112.554 533.29
690880.533 5336112.554 510.36
690880.726 5336112.482 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly_0_">
<gml:posList srsDimension="3">
690889.606 5336124.054 536.63
690900.113 5336150.715 536.63
690889.606 5336124.054 535.29
690889.606 5336124.054 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly.ZLLpH3dFEpjDQJom2GlI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly_0_.7rIKuvh5GHe5JlVafI3P">
<gml:posList srsDimension="3">
690880.402 5336100.7 535.29
690889.606 5336124.054 535.29
690880.402 5336100.7 533.29
690880.402 5336100.7 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly.apWmyUeNybVGGQeSXL4N">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly_0_.4JGiCbO8JYqTxIiEQ88b">
<gml:posList srsDimension="3">
690905.061 5336163.266 533.29
690880.402 5336100.7 533.29
690889.606 5336124.054 535.29
690905.061 5336163.266 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly.r4OWEqW0ud96Sg20dMnU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly_0_.85TqD5eU7XBmNRO8sNiI">
<gml:posList srsDimension="3">
690900.113 5336150.715 535.34
690905.061 5336163.266 533.29
690889.606 5336124.054 535.29
690900.113 5336150.715 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly.hdx12nwe6s0RaZmbtF6D">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly_0_.b4v0qDpsrZ3lw4fQmiDF">
<gml:posList srsDimension="3">
690900.113 5336150.715 536.63
690900.113 5336150.715 535.34
690889.606 5336124.054 535.29
690900.113 5336150.715 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly.Ck4C2YuocQdKNItTjHOB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a34a681d-365f-4170-9aa7-13eac03774c8_poly_0_.zy45AcO16yuSG4RLa8mT">
<gml:posList srsDimension="3">
690905.061 5336163.266 535.34
690905.061 5336163.266 533.29
690900.113 5336150.715 535.34
690905.061 5336163.266 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_">
<gml:posList srsDimension="3">
690955.064 5336175.175 510.36
690955.064 5336175.175 533.29
690957.714 5336174.168 510.36
690955.064 5336175.175 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.nfE6o2KynGG4kgAB8utr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.aUO7hQBVvRNqroQfznLN">
<gml:posList srsDimension="3">
690957.979 5336174.068 510.36
690957.714 5336174.168 510.36
690955.064 5336175.175 533.29
690957.979 5336174.068 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.G3D2d00yKqGHMERNQvKt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.nLnaknN3rPJ6ojOU8Jg6">
<gml:posList srsDimension="3">
690957.714 5336174.168 532.132
690957.979 5336174.068 510.36
690955.064 5336175.175 533.29
690957.714 5336174.168 532.132
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.K2jgbhcMK6VG5ZSf6UEU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.b76jxa3uDeMdUY2C5xb9">
<gml:posList srsDimension="3">
690957.714 5336174.168 533.29
690957.714 5336174.168 532.132
690955.064 5336175.175 533.29
690957.714 5336174.168 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly.6JEcQK9xQ9gzfnUQwIA8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1936c890-38b4-4826-9340-4dd1447db96c_poly_0_.ucymvFOl9HzUQ3qPkqQT">
<gml:posList srsDimension="3">
690957.714 5336174.168 532.132
690957.979 5336174.068 532.132
690957.979 5336174.068 510.36
690957.714 5336174.168 532.132
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly_0_">
<gml:posList srsDimension="3">
690888.379 5336097.547 533.61
690901.642 5336131.202 533.61
690888.379 5336097.547 535.29
690888.379 5336097.547 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly.lWFRAWoyNtYkpUWZoEnx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly_0_.yffW5GuSOffsj6iI8HqY">
<gml:posList srsDimension="3">
690897.583 5336120.902 535.29
690888.379 5336097.547 535.29
690901.642 5336131.202 533.61
690897.583 5336120.902 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly.4WKFzMlfxXRM5IpCAqdq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly_0_.1DCsVvOUOvtNAjYCVOTH">
<gml:posList srsDimension="3">
690897.583 5336120.902 536.63
690897.583 5336120.902 535.29
690901.642 5336131.202 533.61
690897.583 5336120.902 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly.yMduCP68IUGqx4lfiXin">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_a85a655e-511c-4c8f-896c-281abafe94bc_poly_0_.0zkavQj56rkQz4gKcjMi">
<gml:posList srsDimension="3">
690901.642 5336131.202 536.63
690897.583 5336120.902 536.63
690901.642 5336131.202 533.61
690901.642 5336131.202 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly_0_">
<gml:posList srsDimension="3">
690915.058 5336159.329 533.29
690905.061 5336163.266 533.29
690913.044 5336160.122 533.61
690915.058 5336159.329 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly.T1E05WTN1QGC9mfrfO9z">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly_0_.PBSLrXe8AfKDTUmSc4us">
<gml:posList srsDimension="3">
690905.061 5336163.266 535.34
690913.044 5336160.122 533.61
690905.061 5336163.266 533.29
690905.061 5336163.266 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly.aKbXCgkxkWE4npTLHAzG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly_0_.7ol5D7MVcET8FT6IPHyB">
<gml:posList srsDimension="3">
690913.044 5336160.122 535.34
690913.044 5336160.122 533.61
690905.061 5336163.266 535.34
690913.044 5336160.122 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly.OaVeFHbB6JI660jqoRT1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly_0_.MtMNre2ZUkpI1OPJpi72">
<gml:posList srsDimension="3">
690909.053 5336161.694 535.78
690913.044 5336160.122 535.34
690905.061 5336163.266 535.34
690909.053 5336161.694 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly.TGiUQaihvxWtoLwo631Y">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f5fc8b55-382b-46c1-ab2b-b3f4f38d3f6b_poly_0_.XorVT0pbjLN9SYfAP3jn">
<gml:posList srsDimension="3">
690913.044 5336160.122 533.61
690915.058 5336159.329 533.61
690915.058 5336159.329 533.29
690913.044 5336160.122 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_">
<gml:posList srsDimension="3">
690900.113 5336150.715 535.34
690900.113 5336150.715 536.63
690904.105 5336149.143 535.78
690900.113 5336150.715 535.34
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly.KJvdsqT0iYMaRvKu6SZU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_.SnVS0v2O1y7t5rqtbcYB">
<gml:posList srsDimension="3">
690908.096 5336147.571 533.61
690908.096 5336147.571 535.34
690910.11 5336146.776 533.61
690908.096 5336147.571 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly.KKWcWOfx3sgoGF1XU3rU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_.mQjktLBWQRM56vwsB3T1">
<gml:posList srsDimension="3">
690910.11 5336146.776 536.63
690910.11 5336146.776 533.61
690908.096 5336147.571 535.34
690910.11 5336146.776 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly.vnLDIsDy2mXS1u8jHOzv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_.U1M5YOBe2tOW64oOydSM">
<gml:posList srsDimension="3">
690905.053 5336148.769 535.675
690910.11 5336146.776 536.63
690908.096 5336147.571 535.34
690905.053 5336148.769 535.675
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly.LcNkliaiQrK0kRJiOp86">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_.zPs5J9jpg0bIBJDMAXHB">
<gml:posList srsDimension="3">
690904.105 5336149.143 535.78
690910.11 5336146.776 536.63
690905.053 5336148.769 535.675
690904.105 5336149.143 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly.9MazogdvJ0VtNHj90FSn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_.Hc15HlpX0NO0I5IXoeVO">
<gml:posList srsDimension="3">
690900.113 5336150.715 536.63
690910.11 5336146.776 536.63
690904.105 5336149.143 535.78
690900.113 5336150.715 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly.jk6kb4HW2N7IFV1Yy47c">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_63630a7d-06cb-4b07-92cb-d4ff32991ded_poly_0_.JWOmEPbkEHwndDQ5tcQ3">
<gml:posList srsDimension="3">
690910.11 5336146.776 536.63
690904.105 5336149.143 536.63
690900.113 5336150.715 536.63
690910.11 5336146.776 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_">
<gml:posList srsDimension="3">
691000.964 5336142.595 510.36
691002.54 5336146.678 510.36
691000.964 5336142.595 536.124
691000.964 5336142.595 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly.1PTL2lc7AxEFRbarB6ak">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_.JCRZp7yh3DSFjnu86a0w">
<gml:posList srsDimension="3">
691001.362 5336143.625 532.82
691000.964 5336142.595 536.124
691002.54 5336146.678 510.36
691001.362 5336143.625 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly.0uyX0uSR0lGN40H2aimU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_.q0vfsXzVa7AmOIeMYrRw">
<gml:posList srsDimension="3">
691002.54 5336146.678 532.82
691001.362 5336143.625 532.82
691002.54 5336146.678 510.36
691002.54 5336146.678 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly.IKb8MDUQdryXZbS8Nasr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_.RSOFnc2YzkeU3hBgw1ox">
<gml:posList srsDimension="3">
691006.1 5336155.896 510.36
691002.54 5336146.678 532.82
691002.54 5336146.678 510.36
691006.1 5336155.896 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly.EpYghcvlvSFDPIe75fyj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_.XN2PtvGUrPTAxHZKnY9v">
<gml:posList srsDimension="3">
691002.54 5336146.678 536.41
691002.54 5336146.678 532.82
691006.1 5336155.896 510.36
691002.54 5336146.678 536.41
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly.XdttamYNrTQHtqOWvSdQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_.NGpdaGWFlCbwZksxi9G1">
<gml:posList srsDimension="3">
691006.1 5336155.896 532.007
691002.54 5336146.678 536.41
691006.1 5336155.896 510.36
691006.1 5336155.896 532.007
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly.e0SSIboSuSkh9al9d0DN">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_f7323ea2-c3c5-4ca9-8549-0a4b41b8375f_poly_0_.WXh125cVHpHUrOomJ03Q">
<gml:posList srsDimension="3">
691001.362 5336143.625 532.82
691001.362 5336143.625 536.434
691000.964 5336142.595 536.124
691001.362 5336143.625 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly_0_">
<gml:posList srsDimension="3">
690905.575 5336129.325 510.36
690905.575 5336129.325 533.61
690906.106 5336129.116 510.36
690905.575 5336129.325 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly.wQMpTDo9PB7r1UtyQXPC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly_0_.nZP45a0zi9cJBEIVlgnr">
<gml:posList srsDimension="3">
690908.686 5336128.096 510.36
690906.106 5336129.116 510.36
690905.575 5336129.325 533.61
690908.686 5336128.096 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly.tbH2CJRXAG7GIlvEsfGf">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly_0_.G6oVAB1Gyc8IWYdTGz7J">
<gml:posList srsDimension="3">
690908.539 5336128.154 533.61
690908.686 5336128.096 510.36
690905.575 5336129.325 533.61
690908.539 5336128.154 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly.YuVNrJSMd66fAOpnBbBG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly_0_.g0vI7rRxbCez4EQutNqQ">
<gml:posList srsDimension="3">
690908.686 5336128.096 536.743
690908.686 5336128.096 510.36
690908.539 5336128.154 533.61
690908.686 5336128.096 536.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly.7YgH7wwa3zy6W4yQuOTX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly_0_.vVEjk0IxvOMWYOEb6Hfn">
<gml:posList srsDimension="3">
690908.539 5336128.154 533.61
690906.106 5336129.116 533.61
690905.575 5336129.325 533.61
690908.539 5336128.154 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly.l6Cp14cDgrI7pLuhfkdZ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_2e12d189-7905-4dc8-87aa-9835e69ab707_poly_0_.ZGBs1jxCnVAMLIL1I8eo">
<gml:posList srsDimension="3">
690908.539 5336128.154 536.743
690908.686 5336128.096 536.743
690908.539 5336128.154 533.61
690908.539 5336128.154 536.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_">
<gml:posList srsDimension="3">
690913.43 5336175.0 533.29
690913.437 5336175.019 533.29
690913.43 5336175.0 529.6
690913.43 5336175.0 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.WqpsqknsNKMgG6cMf5t6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.vpomUWTGqA6xkmPZCsQ6">
<gml:posList srsDimension="3">
690913.43 5336175.0 510.36
690913.43 5336175.0 529.6
690913.437 5336175.019 533.29
690913.43 5336175.0 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.wVevryn01ceJs9xaqavp">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.36sOz0nkascS18YA1RIF">
<gml:posList srsDimension="3">
690913.437 5336175.019 510.36
690913.43 5336175.0 510.36
690913.437 5336175.019 533.29
690913.437 5336175.019 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.pEYQ1Dn4pTBFNCbxQBQK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.i2pZ1Y8qQXIAdtge4Is4">
<gml:posList srsDimension="3">
690911.425 5336169.686 533.29
690911.572 5336170.074 533.29
690911.425 5336169.686 510.36
690911.425 5336169.686 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.1uZd5Nh3Mm6rSAajpraG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.6N7N8GyIi3A4nPsoWekl">
<gml:posList srsDimension="3">
690911.572 5336170.074 510.36
690911.425 5336169.686 510.36
690911.572 5336170.074 533.29
690911.572 5336170.074 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.XrRyTWVfFQIX27G0r2t0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.cWA7WRXyqCdUOBoJTzNv">
<gml:posList srsDimension="3">
690913.43 5336175.0 529.6
690911.572 5336170.074 510.36
690911.572 5336170.074 529.6
690913.43 5336175.0 529.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly.W5BQ78iUvpg4gCkx0hhs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52a66d7d-a81e-45ee-a97f-eaa08c74e507_poly_0_.iHGE93S9sURsVI45lA8I">
<gml:posList srsDimension="3">
690913.43 5336175.0 510.36
690911.572 5336170.074 510.36
690913.43 5336175.0 529.6
690913.43 5336175.0 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43_poly_0_">
<gml:posList srsDimension="3">
690914.45 5336120.954 510.36
690916.939 5336127.197 510.36
690914.45 5336120.954 535.79
690914.45 5336120.954 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43_poly.bvfFwKMOB2wLGrGYnSpj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43_poly_0_.1n150qQwY0AiFALreHu7">
<gml:posList srsDimension="3">
690915.726 5336124.156 537.18
690914.45 5336120.954 535.79
690916.939 5336127.197 510.36
690915.726 5336124.156 537.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43_poly.Ha44z0mtBJ50TUcRukYy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_1fb32531-4a76-4823-aab6-ed36f21e6b43_poly_0_.c3YVriwEl8mTFiNXUecb">
<gml:posList srsDimension="3">
690916.939 5336127.197 535.86
690915.726 5336124.156 537.18
690916.939 5336127.197 510.36
690916.939 5336127.197 535.86
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_">
<gml:posList srsDimension="3">
690900.335 5336162.306 533.29
690901.294 5336164.749 533.29
690900.335 5336162.306 510.36
690900.335 5336162.306 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly.qMKmdRlmxPi5DfEf12T6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_.Q1Oub1OYjLiGNvgdEQGx">
<gml:posList srsDimension="3">
690901.294 5336164.749 510.36
690900.335 5336162.306 510.36
690901.294 5336164.749 533.29
690901.294 5336164.749 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly.tJXCxiBko4G9vXPYiV0t">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_.MmXWl2qBvxQ3EzBYQbjS">
<gml:posList srsDimension="3">
690901.473 5336165.202 533.29
690901.294 5336164.749 510.36
690901.294 5336164.749 533.29
690901.473 5336165.202 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly.xpXgpqEsKRyjf1uzVLCb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_34c79e93-c3a9-45d4-9032-ae7810737128_poly_0_.mTMKJgIaHgZVwUiVR5IN">
<gml:posList srsDimension="3">
690901.473 5336165.202 510.36
690901.294 5336164.749 510.36
690901.473 5336165.202 533.29
690901.473 5336165.202 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly_0_">
<gml:posList srsDimension="3">
690905.575 5336129.325 510.36
690905.686 5336129.607 510.36
690905.575 5336129.325 533.61
690905.575 5336129.325 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly.DJWGSYFvC9z6xBFTtKMX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly_0_.rojtUyLhCYPtrWfYKzvW">
<gml:posList srsDimension="3">
690905.686 5336129.607 533.61
690905.575 5336129.325 533.61
690905.686 5336129.607 510.36
690905.686 5336129.607 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly.GSNC8LjWuO5rFBGOrKES">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly_0_.J3JFow0pdTp9iTwDm2lj">
<gml:posList srsDimension="3">
690918.902 5336163.014 510.36
690905.686 5336129.607 533.61
690905.686 5336129.607 510.36
690918.902 5336163.014 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly.ShgnB26KZIbJbhmLq6hi">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_0f9a9ee2-255c-4cf3-8451-c03884163c4c_poly_0_.IeqD8vHv31Q9cDzB1OSg">
<gml:posList srsDimension="3">
690918.902 5336163.014 533.61
690905.686 5336129.607 533.61
690918.902 5336163.014 510.36
690918.902 5336163.014 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly_0_">
<gml:posList srsDimension="3">
690907.811 5336125.9 510.36
690907.811 5336125.9 536.662
690907.671 5336125.954 510.36
690907.811 5336125.9 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly.m2rjCsWazpZ8PbmnEv2j">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly_0_.J1zdqEG3VTWDhwteCZqj">
<gml:posList srsDimension="3">
690907.671 5336125.954 533.61
690907.671 5336125.954 510.36
690907.811 5336125.9 536.662
690907.671 5336125.954 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly.xWk5gX03vxXFrJT4ecQD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly_0_.xLxIH3MgCFuLrV6CPrXs">
<gml:posList srsDimension="3">
690905.237 5336126.91 510.36
690907.671 5336125.954 510.36
690907.671 5336125.954 533.61
690905.237 5336126.91 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly.gDvZSPv86GfrT5dkA46f">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly_0_.D0Ttq30XCqt57IrWF72w">
<gml:posList srsDimension="3">
690904.751 5336127.101 510.36
690905.237 5336126.91 510.36
690907.671 5336125.954 533.61
690904.751 5336127.101 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly.uDRL2ojpEa9HlN2k4oJP">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly_0_.4nDY63pZilgehBQgAUwr">
<gml:posList srsDimension="3">
690907.671 5336125.954 536.662
690907.671 5336125.954 533.61
690907.811 5336125.9 536.662
690907.671 5336125.954 536.662
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly.R9vtAkuOO93lgI9RVwth">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_9257447a-220a-4c4a-919d-55d60d2ed7ab_poly_0_.KCMlECLiGgDwDuMJbAAR">
<gml:posList srsDimension="3">
690907.671 5336125.954 533.61
690904.751 5336127.101 533.61
690904.751 5336127.101 510.36
690907.671 5336125.954 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_">
<gml:posList srsDimension="3">
690892.274 5336096.008 510.36
690892.274 5336096.008 533.61
690880.402 5336100.7 510.36
690892.274 5336096.008 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.m4yzWyz4NwgYHaDNOs9c">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.RZWsV8ij1hdnR11X4PVU">
<gml:posList srsDimension="3">
690876.877 5336102.093 510.36
690880.402 5336100.7 510.36
690892.274 5336096.008 533.61
690876.877 5336102.093 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.sw393fT43sH5WChaNAYu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.qYHGfY28z4d1iv5xd914">
<gml:posList srsDimension="3">
690880.402 5336100.7 533.29
690876.877 5336102.093 510.36
690892.274 5336096.008 533.61
690880.402 5336100.7 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.v1BVAS7JmGIUghfwlBv8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.cBEizUYvSxhXMr8NdzAz">
<gml:posList srsDimension="3">
690888.379 5336097.547 533.61
690880.402 5336100.7 533.29
690892.274 5336096.008 533.61
690888.379 5336097.547 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.9i0MifGCCXqNtdvJzT81">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.efNAyk2GsycNfqNoOPKd">
<gml:posList srsDimension="3">
690880.402 5336100.7 535.29
690880.402 5336100.7 533.29
690888.379 5336097.547 533.61
690880.402 5336100.7 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.5KoM46YgSN9eDvPCHq0w">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.07cxWqM7sacoVCsmbV4I">
<gml:posList srsDimension="3">
690888.379 5336097.547 535.29
690880.402 5336100.7 535.29
690888.379 5336097.547 533.61
690888.379 5336097.547 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.U3OGGJ6aEpTWPbqgclDV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.JsSIPfgzDNA1oKPmjmj5">
<gml:posList srsDimension="3">
690884.391 5336099.124 535.83
690880.402 5336100.7 535.29
690888.379 5336097.547 535.29
690884.391 5336099.124 535.83
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly.iZyLEu6YxcaxPKiCj060">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_09873be2-8023-47c8-8eea-e7ef00aa49c0_poly_0_.Pk5tUpvCB6TEqDcNe5Fd">
<gml:posList srsDimension="3">
690880.402 5336100.7 533.29
690876.877 5336102.093 533.29
690876.877 5336102.093 510.36
690880.402 5336100.7 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly_0_">
<gml:posList srsDimension="3">
690953.08 5336162.159 532.82
690953.08 5336162.159 533.29
690957.21 5336160.573 532.82
690953.08 5336162.159 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly.Roua41xoqebVXgQ5uJRP">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly_0_.DqcI7hYwotklqf8SSkuG">
<gml:posList srsDimension="3">
691001.362 5336143.625 532.82
690957.21 5336160.573 532.82
690953.08 5336162.159 533.29
691001.362 5336143.625 532.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly.zVmk4hdkjne8LgzJ6spc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly_0_.hoAFE7DiVTmt3CQP0HWw">
<gml:posList srsDimension="3">
691001.362 5336143.625 536.434
691001.362 5336143.625 532.82
690953.08 5336162.159 533.29
691001.362 5336143.625 536.434
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly.ERLR7Gu8IIOvpNjLRIje">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly_0_.tPnL9k9vHhTiwAmI5yZY">
<gml:posList srsDimension="3">
690957.21 5336160.573 536.43
691001.362 5336143.625 536.434
690953.08 5336162.159 533.29
690957.21 5336160.573 536.43
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly.Fu1648012Og4BP9pyW7H">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_b9a5f97f-ccdd-447f-a82f-d1ea89db2a17_poly_0_.87enfnGCJa4di3IIrDp8">
<gml:posList srsDimension="3">
690953.08 5336162.159 536.43
690957.21 5336160.573 536.43
690953.08 5336162.159 533.29
690953.08 5336162.159 536.43
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_">
<gml:posList srsDimension="3">
690953.869 5336151.267 510.36
690953.869 5336151.267 533.651
690953.651 5336151.351 510.36
690953.869 5336151.267 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.D5nFMAiRpf0XpDS3VDFA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.ZMfRLDmdqMDWKSSsbcu3">
<gml:posList srsDimension="3">
690917.371 5336165.202 510.36
690953.651 5336151.351 510.36
690953.869 5336151.267 533.651
690917.371 5336165.202 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.2GS0KtAPNYtpXvjKGlxr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.y3IjHpuj0bG3lVfM44VO">
<gml:posList srsDimension="3">
690916.837 5336165.405 510.36
690917.371 5336165.202 510.36
690953.869 5336151.267 533.651
690916.837 5336165.405 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.tuzgOfGotC0va7u7umkt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.gYvjnHK2IqlXadEEwFnU">
<gml:posList srsDimension="3">
690949.517 5336152.928 533.29
690916.837 5336165.405 510.36
690953.869 5336151.267 533.651
690949.517 5336152.928 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.zefqpX7ixbFtK8tvlvAJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.SgUtfVUdHRu2QuaLz9wJ">
<gml:posList srsDimension="3">
690953.651 5336151.351 533.647
690949.517 5336152.928 533.29
690953.869 5336151.267 533.651
690953.651 5336151.351 533.647
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.Y67cisCSuCkA2jXyQ4gg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.REI3o3ep3lHCg5ixyxzn">
<gml:posList srsDimension="3">
690949.517 5336152.928 533.644
690949.517 5336152.928 533.29
690953.651 5336151.351 533.647
690949.517 5336152.928 533.644
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.tTD5aTK9xLnVbgkXYc0U">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.FihjW6kRGsDi5XVHUfN3">
<gml:posList srsDimension="3">
690916.837 5336165.405 510.36
690949.517 5336152.928 533.29
690916.837 5336165.405 533.29
690916.837 5336165.405 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly.6nglSNLynuDRlBH5nZ83">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_56b313a4-f0ec-44d2-be2b-978c30dacb97_poly_0_.MSwd19AB7ALmZxKAaHbD">
<gml:posList srsDimension="3">
690916.837 5336165.405 533.29
690917.371 5336165.202 533.29
690949.517 5336152.928 533.29
690916.837 5336165.405 533.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465_poly_0_">
<gml:posList srsDimension="3">
690907.671 5336125.954 536.662
690908.142 5336127.148 537.18
690907.671 5336125.954 533.61
690907.671 5336125.954 536.662
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465_poly.m1p49X7GKQ0NVRjQcG7r">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465_poly_0_.lXs66TG1vEJPAF2WIwbu">
<gml:posList srsDimension="3">
690908.539 5336128.154 533.61
690907.671 5336125.954 533.61
690908.142 5336127.148 537.18
690908.539 5336128.154 533.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465_poly.zr3hzJrBJki9tW7tIClM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_13a5f33f-458f-4f19-807d-1d57714fa465_poly_0_.YbC6LiofOPSgUtLowYBS">
<gml:posList srsDimension="3">
690908.539 5336128.154 536.743
690908.539 5336128.154 533.61
690908.142 5336127.148 537.18
690908.539 5336128.154 536.743
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly_0_">
<gml:posList srsDimension="3">
690897.583 5336120.902 535.29
690897.583 5336120.902 536.63
690893.594 5336122.478 535.83
690897.583 5336120.902 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly.TeOfQw5GSsAhaaNj0HKb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly_0_.ASFZcRFvQwShex7DhI7c">
<gml:posList srsDimension="3">
690889.606 5336124.054 535.29
690893.594 5336122.478 535.83
690889.606 5336124.054 536.63
690889.606 5336124.054 535.29
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly.YU3Q6B4WNCRSddVeOSj9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly_0_.CHsYSDLITz4jXdDQGBQ2">
<gml:posList srsDimension="3">
690897.583 5336120.902 536.63
690889.606 5336124.054 536.63
690893.594 5336122.478 535.83
690897.583 5336120.902 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly.yzkLbvg3UgWv6qHQQ1wC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_971901e3-a8b8-48b0-bc08-f1b86c56c2c0_poly_0_.WxnRS7c0yArNdKQqVoBO">
<gml:posList srsDimension="3">
690893.594 5336122.478 536.63
690889.606 5336124.054 536.63
690897.583 5336120.902 536.63
690893.594 5336122.478 536.63
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_60042_52d47af2-d970-4d47-90cc-3273293e288a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_60042_52d47af2-d970-4d47-90cc-3273293e288a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_60042_52d47af2-d970-4d47-90cc-3273293e288a_poly_0_">
<gml:posList srsDimension="3">
691002.54 5336146.678 510.36
691000.964 5336142.595 510.36
691000.781 5336142.668 510.36
691000.69 5336142.434 510.36
691000.812 5336142.389 510.36
690997.72 5336134.381 510.36
690997.487 5336134.472 510.36
690997.414 5336134.289 510.36
690954.339 5336151.016 510.36
690954.43 5336151.249 510.36
690953.932 5336151.44 510.36
690953.869 5336151.267 510.36
690953.651 5336151.351 510.36
690917.371 5336165.202 510.36
690916.837 5336165.405 510.36
690916.3 5336164.043 510.36
690918.902 5336163.014 510.36
690905.686 5336129.607 510.36
690905.575 5336129.325 510.36
690906.106 5336129.116 510.36
690908.686 5336128.096 510.36
690909.488 5336130.129 510.36
690916.939 5336127.197 510.36
690914.45 5336120.954 510.36
690907.018 5336123.897 510.36
690907.811 5336125.9 510.36
690907.671 5336125.954 510.36
690905.237 5336126.91 510.36
690904.751 5336127.101 510.36
690892.533 5336096.048 510.36
690892.309 5336096.139 510.36
690892.274 5336096.008 510.36
690880.402 5336100.7 510.36
690876.877 5336102.093 510.36
690876.923 5336102.195 510.36
690876.71 5336102.276 510.36
690876.883 5336102.713 510.36
690876.7 5336102.786 510.36
690876.836 5336103.152 510.36
690877.029 5336103.079 510.36
690878.158 5336105.966 510.36
690877.975 5336106.039 510.36
690878.13 5336106.415 510.36
690878.313 5336106.342 510.36
690879.442 5336109.219 510.36
690879.249 5336109.291 510.36
690879.405 5336109.658 510.36
690879.588 5336109.585 510.36
690880.726 5336112.482 510.36
690880.533 5336112.554 510.36
690880.689 5336112.931 510.36
690880.872 5336112.858 510.36
690882.019 5336115.755 510.36
690881.826 5336115.828 510.36
690881.962 5336116.193 510.36
690882.155 5336116.121 510.36
690883.284 5336118.987 510.36
690883.081 5336119.069 510.36
690883.246 5336119.446 510.36
690883.439 5336119.373 510.36
690884.577 5336122.26 510.36
690884.394 5336122.333 510.36
690884.54 5336122.699 510.36
690884.723 5336122.626 510.36
690885.86 5336125.533 510.36
690885.667 5336125.606 510.36
690885.824 5336125.952 510.36
690885.997 5336125.879 510.36
690887.135 5336128.776 510.36
690886.942 5336128.848 510.36
690887.108 5336129.205 510.36
690887.28 5336129.142 510.36
690888.419 5336132.029 510.36
690888.236 5336132.102 510.36
690888.382 5336132.458 510.36
690888.992 5336132.211 510.36
690889.083 5336132.445 510.36
690888.473 5336132.691 510.36
690888.619 5336133.047 510.36
690888.791 5336132.984 510.36
690889.939 5336135.891 510.36
690889.746 5336135.964 510.36
690889.892 5336136.32 510.36
690890.075 5336136.247 510.36
690891.223 5336139.144 510.36
690891.03 5336139.217 510.36
690891.186 5336139.573 510.36
690891.359 5336139.51 510.36
690892.497 5336142.397 510.36
690892.324 5336142.47 510.36
690892.46 5336142.826 510.36
690892.643 5336142.753 510.36
690893.78 5336145.66 510.36
690893.607 5336145.733 510.36
690893.753 5336146.099 510.36
690893.926 5336146.036 510.36
690895.064 5336148.913 510.36
690894.881 5336148.985 510.36
690895.027 5336149.351 510.36
690895.21 5336149.278 510.36
690896.348 5336152.175 510.36
690896.165 5336152.248 510.36
690896.31 5336152.613 510.36
690896.493 5336152.541 510.36
690897.631 5336155.427 510.36
690897.449 5336155.499 510.36
690897.584 5336155.865 510.36
690897.767 5336155.792 510.36
690898.915 5336158.688 510.36
690898.732 5336158.761 510.36
690898.878 5336159.127 510.36
690899.051 5336159.054 510.36
690900.189 5336161.95 510.36
690900.016 5336162.013 510.36
690900.152 5336162.378 510.36
690900.335 5336162.306 510.36
690901.294 5336164.749 510.36
690901.473 5336165.202 510.36
690901.3 5336165.265 510.36
690901.436 5336165.641 510.36
690901.619 5336165.568 510.36
690902.757 5336168.454 510.36
690902.584 5336168.518 510.36
690902.71 5336168.893 510.36
690902.893 5336168.82 510.36
690904.03 5336171.716 510.36
690903.838 5336171.789 510.36
690903.994 5336172.145 510.36
690904.177 5336172.072 510.36
690904.341 5336172.489 510.36
690911.425 5336169.686 510.36
690911.572 5336170.074 510.36
690913.43 5336175.0 510.36
690913.437 5336175.019 510.36
690906.48 5336177.667 510.36
690911.852 5336191.762 510.36
690918.808 5336189.173 510.36
690918.979 5336189.66 510.36
690919.345 5336189.515 510.36
690919.111 5336188.845 510.36
690922.013 5336187.807 510.36
690922.25 5336188.407 510.36
690922.616 5336188.261 510.36
690922.389 5336187.662 510.36
690925.285 5336186.554 510.36
690925.512 5336187.153 510.36
690925.888 5336187.008 510.36
690925.661 5336186.408 510.36
690928.547 5336185.31 510.36
690928.784 5336185.9 510.36
690929.16 5336185.764 510.36
690928.933 5336185.165 510.36
690931.819 5336184.066 510.36
690932.056 5336184.656 510.36
690932.422 5336184.51 510.36
690932.195 5336183.921 510.36
690935.081 5336182.802 510.36
690935.328 5336183.403 510.36
690935.694 5336183.257 510.36
690935.468 5336182.657 510.36
690938.363 5336181.569 510.36
690938.6 5336182.159 510.36
690938.956 5336182.013 510.36
690938.729 5336181.423 510.36
690941.635 5336180.306 510.36
690941.872 5336180.905 510.36
690942.238 5336180.76 510.36
690942.011 5336180.16 510.36
690944.896 5336179.052 510.36
690945.133 5336179.652 510.36
690945.508 5336179.516 510.36
690945.282 5336178.907 510.36
690948.187 5336177.799 510.36
690948.413 5336178.398 510.36
690948.779 5336178.262 510.36
690948.552 5336177.663 510.36
690951.448 5336176.545 510.36
690951.684 5336177.155 510.36
690952.059 5336177.029 510.36
690951.813 5336176.409 510.36
690954.708 5336175.311 510.36
690954.944 5336175.921 510.36
690955.3 5336175.785 510.36
690955.064 5336175.175 510.36
690957.714 5336174.168 510.36
690957.979 5336174.068 510.36
690958.205 5336174.677 510.36
690958.571 5336174.531 510.36
690958.498 5336174.358 510.36
690958.925 5336174.185 510.36
690958.834 5336173.941 510.36
690959.109 5336173.821 510.36
690959.218 5336174.096 510.36
690963.355 5336172.476 510.36
690963.275 5336172.222 510.36
690963.519 5336172.132 510.36
690970.5 5336169.452 510.36
690973.711 5336168.196 510.36
690973.802 5336168.429 510.36
690973.945 5336168.375 510.36
691006.1 5336155.896 510.36
691002.54 5336146.678 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_60042/IMG_6223.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#_NO_PARENT_ID_PG.MdvHiV4X1ahiuqjsyLEJ">
<app:TexCoordList>
<app:textureCoordinates ring="#_NO_PARENT_ID_LR.2PkoIhdAEyEssPEW8yV8">
-0.0008508344441988644 0.0 0.0 0.0 0.0 0.0011344459255984859 -0.0008508344441988644 0.0011344459255984859 -0.0008508344441988644 0.0
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="_NO_PARENT_ID_PG.MdvHiV4X1ahiuqjsyLEJ">
<gml:exterior>
<gml:LinearRing gml:id="_NO_PARENT_ID_LR.2PkoIhdAEyEssPEW8yV8">
<gml:posList srsDimension="3">
690876.725 5336096.008 510.36
690876.7 5336096.008 510.36
690876.7 5336096.033 510.36
690876.725 5336096.033 510.36
690876.725 5336096.008 510.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>