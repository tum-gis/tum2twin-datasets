<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-06-28 11:27:16 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Downloads\tum2twin-datasets-main\citygml\lod2-building-datasets\DEBY_LOD2_4959324.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>691036.12 5336070.103 511.2</gml:lowerCorner>
         <gml:upperCorner>691048.082 5336077.865 521.36</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4959324">
<gml:name>DEBY_LOD2_4959324</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJ0n</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959324/PhotoHinten.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959324_01457879-b7e4-4955-981c-e7cf8ce7b876_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959324_01457879-b7e4-4955-981c-e7cf8ce7b876_poly_0_">
0.9994791945430629 0.0005793784833558754 0.9994791945430629 0.9994180652199551 0.0005209411684297116 0.9994180652199551 0.0005209411684297116 0.0005793784833558754 0.9994791945430629 0.0005793784833558754
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>1000</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">10.16</bldg:measuredHeight>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959324_1ce133b0-b7d4-404f-a908-ffa54380e4c0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_1ce133b0-b7d4-404f-a908-ffa54380e4c0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_1ce133b0-b7d4-404f-a908-ffa54380e4c0_poly_0_">
<gml:posList srsDimension="3">
691046.641 5336070.103 521.36
691048.082 5336073.722 521.36
691037.55 5336077.865 521.36
691036.12 5336074.236 521.36
691046.641 5336070.103 521.36
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959324_3b67d8e3-4622-4201-8adf-145d9fc8deeb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_3b67d8e3-4622-4201-8adf-145d9fc8deeb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_3b67d8e3-4622-4201-8adf-145d9fc8deeb_poly_0_">
<gml:posList srsDimension="3">
691048.082 5336073.722 511.2
691048.082 5336073.722 521.36
691046.641 5336070.103 521.36
691046.641 5336070.103 511.2
691048.082 5336073.722 511.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959324_01457879-b7e4-4955-981c-e7cf8ce7b876">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_01457879-b7e4-4955-981c-e7cf8ce7b876_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_01457879-b7e4-4955-981c-e7cf8ce7b876_poly_0_">
<gml:posList srsDimension="3">
691046.641 5336070.103 511.2
691046.641 5336070.103 521.36
691036.12 5336074.236 521.36
691036.12 5336074.236 511.2
691046.641 5336070.103 511.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959324_76551dcd-f98e-43bb-a636-f183e5df36b8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_76551dcd-f98e-43bb-a636-f183e5df36b8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_76551dcd-f98e-43bb-a636-f183e5df36b8_poly_0_">
<gml:posList srsDimension="3">
691037.55 5336077.865 511.2
691037.55 5336077.865 521.36
691042.684 5336075.844 521.36
691042.684 5336075.844 511.2
691037.55 5336077.865 511.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959324_cd188fbc-322c-4f57-a312-7be99b752fe4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_cd188fbc-322c-4f57-a312-7be99b752fe4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_cd188fbc-322c-4f57-a312-7be99b752fe4_poly_0_">
<gml:posList srsDimension="3">
691042.684 5336075.844 511.2
691042.684 5336075.844 521.36
691048.082 5336073.722 521.36
691048.082 5336073.722 511.2
691042.684 5336075.844 511.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959324_67f06d31-b2aa-4bfd-a095-44db57c7408b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_67f06d31-b2aa-4bfd-a095-44db57c7408b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_67f06d31-b2aa-4bfd-a095-44db57c7408b_poly_0_">
<gml:posList srsDimension="3">
691036.12 5336074.236 511.2
691036.12 5336074.236 521.36
691037.55 5336077.865 521.36
691037.55 5336077.865 511.2
691036.12 5336074.236 511.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959324_dae3548b-e941-470c-9366-dfcc9a120c31">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959324_dae3548b-e941-470c-9366-dfcc9a120c31_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959324_dae3548b-e941-470c-9366-dfcc9a120c31_poly_0_">
<gml:posList srsDimension="3">
691036.12 5336074.236 511.2
691037.55 5336077.865 511.2
691048.082 5336073.722 511.2
691046.641 5336070.103 511.2
691036.12 5336074.236 511.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>