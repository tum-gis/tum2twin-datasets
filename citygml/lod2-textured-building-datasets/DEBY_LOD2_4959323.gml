<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-08-08 16:51:48 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4959323.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>691046.45 5336062.49 514.59</gml:lowerCorner>
         <gml:upperCorner>691088.732 5336131.183 537.822</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4959323">
<gml:name>DEBY_LOD2_4959323</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJ0C</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/PhotoHinten.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_">
0.9994563481546458 0.0005463186802427035 0.9994563481546458 0.999285804270536 0.0007107242863123864 0.0005463186802427035 0.9994563481546458 0.0005463186802427035
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/PhotoEckHR_6XkKBOkzTkBU4G8jOTWu.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly.WQjsUUCf7LyrXV7YB5u5">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_.4UEdPgNryE5kTWKLn4ET">
0.0009686530522814988 0.0009291386690586768 0.0019652290344489296 0.0009291386690583859 0.9992572497096026 0.9992776077734489 0.0009686530522814988 0.0009291386690586768
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/PhotoHinten_BpqyUELsP2HXtE7gyuDJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly.PZLK9qeij0aiGrumtU6l">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_.oRoclMzmtLiOTLTTS6XO">
0.0005430745188957642 0.9992611908019337 0.0005430745188957642 0.0007131863913062195 0.9992898537596424 0.9990535600893673 0.0005430745188957642 0.9992611908019337
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/Photoganz23.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly.PUelmli1AxtzBPFRFwc5">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly_0_.hYvOTv1afexza65BI11h">
0.9994810971570544 0.0012725638167240697 0.000370439932811481 0.9987557706160675 0.000370439932811481 0.0012725638167240697 0.9994810971570544 0.0012725638167240697
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/Photoganz23_Yq95SXfR2wBj0NzVBWTG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly.tDiCeqeAMQomZiKjOEXD">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly_0_.EmVMIsJO3zcHMo5j0Ruw">
0.9996293174873353 0.9982461252158604 0.0005189052514911197 0.999282953083944 0.9996293174873353 0.0014398128404017982 0.9996293174873353 0.9982461252158604
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/PhotoEckVR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_">
0.999226681456766 0.0007785974300072398 0.999226681456766 0.9992008662923917 0.0010111771800979108 0.0007785974300072398 0.999226681456766 0.0007785974300072398
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/PhotoEckVR_OUaAACFr6DgtHg4XEfJ7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly.TerDSrl4rbFSQIMwEDjh">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_.i0zKU3A46Exz2NYBYgwH">
0.0010081314002856612 0.0009704479085838047 0.0021171460159183915 0.000970447908584599 0.9992264506483096 0.9991030024290343 0.0010081314002856612 0.0009704479085838047
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959323/PhotoEckVR_Nm5DfxnD7vM52pe1hsJg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly.KrlVmmCfMnH4S4c5ODCA">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_.8yrldNzHtk3L2af78fd8">
0.0007721858955673788 0.9991688509680886 0.0007721858955673788 0.001016319736850335 0.9989906294902715 0.9997917878504877 0.0007721858955673788 0.9991688509680886
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3200</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">23.232</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959323_32bdadaa-833a-4760-9642-d63fca511d9f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_32bdadaa-833a-4760-9642-d63fca511d9f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_32bdadaa-833a-4760-9642-d63fca511d9f_poly_0_">
<gml:posList srsDimension="3">
691076.152 5336118.616 537.822
691088.712 5336124.078 533.827
691070.695 5336131.183 533.83
691076.152 5336118.616 537.822
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120_poly_0_">
<gml:posList srsDimension="3">
691059.004 5336075.059 537.822
691064.451 5336062.523 533.838
691076.152 5336118.616 537.822
691059.004 5336075.059 537.822
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120_poly.BV5HQ4TQiKTyYpzrNRzB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120_poly_0_.yVkBzdii3H9Noc44TI6F">
<gml:posList srsDimension="3">
691088.732 5336124.07 533.818
691076.152 5336118.616 537.822
691064.451 5336062.523 533.838
691088.732 5336124.07 533.818
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120_poly.6kPZHovD24IvzEbLmQpx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_29cf46ce-2544-48f0-bc69-680ff87c9120_poly_0_.eo6etdE25hLZTLT2Uw1S">
<gml:posList srsDimension="3">
691088.712 5336124.078 533.827
691076.152 5336118.616 537.822
691088.732 5336124.07 533.818
691088.712 5336124.078 533.827
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959323_ccdea706-4778-47d2-801e-34bc4dbb664a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_ccdea706-4778-47d2-801e-34bc4dbb664a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_ccdea706-4778-47d2-801e-34bc4dbb664a_poly_0_">
<gml:posList srsDimension="3">
691046.468 5336069.608 533.835
691064.437 5336062.49 533.823
691059.004 5336075.059 537.822
691046.468 5336069.608 533.835
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_ccdea706-4778-47d2-801e-34bc4dbb664a_poly.O7AIbMDXY78PyE8NWZGF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_ccdea706-4778-47d2-801e-34bc4dbb664a_poly_0_.Y03bvRWE1bDcfCQ294Lx">
<gml:posList srsDimension="3">
691064.451 5336062.523 533.838
691059.004 5336075.059 537.822
691064.437 5336062.49 533.823
691064.451 5336062.523 533.838
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_">
<gml:posList srsDimension="3">
691046.45 5336069.615 533.827
691046.468 5336069.608 533.835
691046.641 5336070.103 533.827
691046.45 5336069.615 533.827
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly.5xrBoeUFVcpgaxGGPoeB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_.kq7Qx6ruWhL9RWkZzeCV">
<gml:posList srsDimension="3">
691059.004 5336075.059 537.822
691046.641 5336070.103 533.827
691046.468 5336069.608 533.835
691059.004 5336075.059 537.822
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly.c9XEUV1QKwYdiufJWJRi">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_.6GffucU8dP1uFTimNB4x">
<gml:posList srsDimension="3">
691048.082 5336073.722 533.832
691046.641 5336070.103 533.827
691059.004 5336075.059 537.822
691048.082 5336073.722 533.832
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly.OclEH6tey2hOSOLeGZjt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_.J3Qp0UyRGJtxHTuLBUGB">
<gml:posList srsDimension="3">
691053.037 5336086.226 533.845
691048.082 5336073.722 533.832
691059.004 5336075.059 537.822
691053.037 5336086.226 533.845
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly.CZNFqPlBfq12SfA5wlGd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_.t84HSzqQGIN7esPT32tk">
<gml:posList srsDimension="3">
691070.486 5336130.645 533.831
691053.037 5336086.226 533.845
691059.004 5336075.059 537.822
691070.486 5336130.645 533.831
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly.YQPRfrTI21qqwBm6fVPU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_.Nqk3iHSb581tSyjP5KHg">
<gml:posList srsDimension="3">
691076.152 5336118.616 537.822
691070.486 5336130.645 533.831
691059.004 5336075.059 537.822
691076.152 5336118.616 537.822
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly.IJwFFGi9pd0n2kqHXAIh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_d7493ceb-68eb-4cdc-8a72-4ea4cb4103ac_poly_0_.EtRO56erAypvIplpdbox">
<gml:posList srsDimension="3">
691070.695 5336131.183 533.83
691070.486 5336130.645 533.831
691076.152 5336118.616 537.822
691070.695 5336131.183 533.83
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_30ccf6c9-48c2-4451-9cce-a44f2a9ba572">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_30ccf6c9-48c2-4451-9cce-a44f2a9ba572_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_30ccf6c9-48c2-4451-9cce-a44f2a9ba572_poly_0_">
<gml:posList srsDimension="3">
691065.116 5336116.984 514.59
691065.116 5336116.984 533.834
691070.486 5336130.645 533.831
691070.486 5336130.645 514.59
691065.116 5336116.984 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_e62eac03-e7b7-4f9b-a460-d9785fda6cea">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_e62eac03-e7b7-4f9b-a460-d9785fda6cea_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_e62eac03-e7b7-4f9b-a460-d9785fda6cea_poly_0_">
<gml:posList srsDimension="3">
691046.641 5336070.103 514.59
691046.641 5336070.103 533.827
691048.082 5336073.722 533.832
691048.082 5336073.722 514.59
691046.641 5336070.103 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_8f6787dc-56d7-4487-8a3c-937d1a4e7fd1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_8f6787dc-56d7-4487-8a3c-937d1a4e7fd1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_8f6787dc-56d7-4487-8a3c-937d1a4e7fd1_poly_0_">
<gml:posList srsDimension="3">
691048.082 5336073.722 533.832
691053.037 5336086.226 533.845
691053.037 5336086.226 514.59
691048.082 5336073.722 514.59
691048.082 5336073.722 533.832
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_9932d520-d3d1-42ae-b671-05dfe1af5c3e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_9932d520-d3d1-42ae-b671-05dfe1af5c3e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_9932d520-d3d1-42ae-b671-05dfe1af5c3e_poly_0_">
<gml:posList srsDimension="3">
691053.037 5336086.226 514.59
691053.037 5336086.226 533.845
691065.116 5336116.984 533.834
691065.116 5336116.984 514.59
691053.037 5336086.226 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_7a05894f-7a83-46aa-8cde-080a22da11af">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_7a05894f-7a83-46aa-8cde-080a22da11af_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_7a05894f-7a83-46aa-8cde-080a22da11af_poly_0_">
<gml:posList srsDimension="3">
691070.486 5336130.645 514.59
691070.486 5336130.645 533.831
691070.695 5336131.183 533.83
691070.695 5336131.183 514.59
691070.486 5336130.645 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_f50c74e0-04b3-4477-bef2-60e5b0fa79e0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_f50c74e0-04b3-4477-bef2-60e5b0fa79e0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_f50c74e0-04b3-4477-bef2-60e5b0fa79e0_poly_0_">
<gml:posList srsDimension="3">
691046.45 5336069.615 514.59
691046.45 5336069.615 533.827
691046.641 5336070.103 533.827
691046.641 5336070.103 514.59
691046.45 5336069.615 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_">
<gml:posList srsDimension="3">
691064.437 5336062.49 514.59
691064.437 5336062.49 533.823
691046.468 5336069.608 514.59
691064.437 5336062.49 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly.WQjsUUCf7LyrXV7YB5u5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_.4UEdPgNryE5kTWKLn4ET">
<gml:posList srsDimension="3">
691046.45 5336069.615 514.59
691046.468 5336069.608 514.59
691064.437 5336062.49 533.823
691046.45 5336069.615 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly.tiRU4Bn6OUxqxok1zNB0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_.KuL0PUif9oJaC26YyYsP">
<gml:posList srsDimension="3">
691046.468 5336069.608 533.835
691046.45 5336069.615 533.827
691064.437 5336062.49 533.823
691046.468 5336069.608 533.835
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly.PZLK9qeij0aiGrumtU6l">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_652151f9-f41e-404f-97d3-19b720adc35f_poly_0_.oRoclMzmtLiOTLTTS6XO">
<gml:posList srsDimension="3">
691046.45 5336069.615 533.827
691046.45 5336069.615 514.59
691064.437 5336062.49 533.823
691046.45 5336069.615 533.827
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly_0_">
<gml:posList srsDimension="3">
691064.437 5336062.49 514.59
691064.451 5336062.523 514.59
691064.437 5336062.49 533.823
691064.437 5336062.49 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly.JdxuxhYDE966Kws2urSa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly_0_.B32QWMbbK5ULHxkTYmJL">
<gml:posList srsDimension="3">
691064.451 5336062.523 533.838
691064.437 5336062.49 533.823
691064.451 5336062.523 514.59
691064.451 5336062.523 533.838
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly.PUelmli1AxtzBPFRFwc5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly_0_.hYvOTv1afexza65BI11h">
<gml:posList srsDimension="3">
691088.732 5336124.07 514.59
691064.451 5336062.523 533.838
691064.451 5336062.523 514.59
691088.732 5336124.07 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly.tDiCeqeAMQomZiKjOEXD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_5150f87d-bb94-4ffe-bdac-c5d180e863d5_poly_0_.EmVMIsJO3zcHMo5j0Ruw">
<gml:posList srsDimension="3">
691088.732 5336124.07 533.818
691064.451 5336062.523 533.838
691088.732 5336124.07 514.59
691088.732 5336124.07 533.818
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_">
<gml:posList srsDimension="3">
691070.695 5336131.183 514.59
691070.695 5336131.183 533.83
691088.712 5336124.078 514.59
691070.695 5336131.183 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly.TerDSrl4rbFSQIMwEDjh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_.i0zKU3A46Exz2NYBYgwH">
<gml:posList srsDimension="3">
691088.732 5336124.07 514.59
691088.712 5336124.078 514.59
691070.695 5336131.183 533.83
691088.732 5336124.07 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly.KrlVmmCfMnH4S4c5ODCA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_.8yrldNzHtk3L2af78fd8">
<gml:posList srsDimension="3">
691088.732 5336124.07 533.818
691088.732 5336124.07 514.59
691070.695 5336131.183 533.83
691088.732 5336124.07 533.818
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly.ChSNbFikhvZBobsawCzf">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_6ed27e15-5c94-46f7-ad07-b40e67955997_poly_0_.EliGA0xCQeciH8OMlXPV">
<gml:posList srsDimension="3">
691088.712 5336124.078 533.827
691088.732 5336124.07 533.818
691070.695 5336131.183 533.83
691088.712 5336124.078 533.827
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959323_c724d139-9a83-4c34-bd3e-cb2544f27d67">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959323_c724d139-9a83-4c34-bd3e-cb2544f27d67_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959323_c724d139-9a83-4c34-bd3e-cb2544f27d67_poly_0_">
<gml:posList srsDimension="3">
691064.451 5336062.523 514.59
691064.437 5336062.49 514.59
691046.468 5336069.608 514.59
691046.45 5336069.615 514.59
691046.641 5336070.103 514.59
691048.082 5336073.722 514.59
691053.037 5336086.226 514.59
691070.486 5336130.645 514.59
691070.695 5336131.183 514.59
691088.712 5336124.078 514.59
691088.732 5336124.07 514.59
691064.451 5336062.523 514.59
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>