<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-08-20 15:26:38 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906981.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690953.026 5335962.625 509.79</gml:lowerCorner>
         <gml:upperCorner>691054.069 5336105.592 547.33</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/image0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_c5c69a3e-71dc-4998-b005-4ff910d3bac8_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_c5c69a3e-71dc-4998-b005-4ff910d3bac8_poly_0_">
0.9996949839949019 0.0004461890292198329 0.9996949839949019 0.9995537113951315 0.0004787074450283144 0.9995537113951317 0.0004787074450280926 0.0004461890292198329 0.9996949839949019 0.0004461890292198329
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04704#2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_b8dae246-a9db-4b76-bb02-296cf532f0da_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_b8dae246-a9db-4b76-bb02-296cf532f0da_poly_0_">
0.0006908598748607275 0.0007335121505559803 0.9993089013577325 0.0007335121505559803 0.9993089013577325 0.9990295637550071 0.0006908598748607275 0.9990295637550071 0.0006908598748607275 0.0007335121505559803
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/image0_V1wQOioknU1WRDg4pYqH.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_2bf95c01-66b3-4b03-b50b-218f093715cd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_2bf95c01-66b3-4b03-b50b-218f093715cd_poly_0_">
0.9982855820573835 0.00043510598387333803 0.998285582057385 0.9995647993266161 0.003379496897847111 0.9995647993266163 0.0033794968978453355 0.00043510598387333824 0.9982855820573835 0.00043510598387333803
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/C__Users_ge96wis_Documents_ProjezierteGML_DEBY_LOD2_4959457_DSC04699.JPG</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_4786632e-7831-415e-b46c-5b381361df5e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_4786632e-7831-415e-b46c-5b381361df5e_poly_0_">
0.998417021664828 0.0012287611193526615 0.998417021664828 0.9984248243431515 0.0015877527882715015 0.9984248243431515 0.0015877527882715015 0.0012287611193526615 0.998417021664828 0.0012287611193526615
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/C__Users_ge96wis_Documents_ProjezierteGML_DEBY_LOD2_4959457_DSC04699_YRL3HE0CbldLM9P1WZMs.JPG</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly.uCHH81LPzfU98IVG25aG">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_.fkcfr60rk6X5KbyUWZ8V">
0.001541870900131137 0.0004798513730395397 0.09057577824202845 0.0004798513730395397 0.9997934342504413 0.9994220982496066 0.001541870900131137 0.0004798513730395397
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/C__Users_ge96wis_Documents_ProjezierteGML_DEBY_LOD2_4959457_DSC04699_u7l471qk7InJMLSbPUNJ.JPG</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly.qp6ZjK8R5EyEcv0ioQ0i">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_.es0Rgg3egPrGvHpxHz9W">
0.08939006997454957 0.9995672441093888 0.00031726755661587447 0.0005952179463922688 0.9990052577444715 0.9995672441093888 0.08939006997454957 0.9995672441093888
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/C__Users_ge96wis_Documents_ProjezierteGML_DEBY_LOD2_4959457_DSC04699_V1xCyeUQQnv361LOc2K5.JPG</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_">
0.9989970906646874 0.0004222091722695886 0.9989970906646874 0.999408722371447 0.001658019230721841 0.0004222091722695886 0.9989970906646874 0.0004222091722695886
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04703_jNZoDdjguHrlihXLo2WJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.cQE7IuQJ8nIPqcH7JVsD">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.i0UZldZhMAhzbiv0NOlB">
0.0003521870048834863 0.9994890799979218 0.0003521870048834863 0.0003891199696988572 0.9996683050290558 0.9995295490500941 0.0003521870048834863 0.9995295490500941 0.0003521870048834863 0.9994890799979218
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04703_fAyo3Vn32CgG84tcjHVr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.9bBaI8fm6EsaKrkNpJOV">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.pnJDzAR2tb29Gaxq4szH">
0.00033173610556191946 0.00030449284768321503 0.9997509257504034 0.00030449284768321503 0.9997509257504034 0.9995930509334625 0.00033173610556191946 0.00030449284768321503
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04702_XEHz2UbHapZGYi9W1fMf.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly.BLjh7DpyyBYB2iUUETGa">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly_0_.Z4jJgPG4rgpKPOxde1f9">
0.9983726052783322 0.0004023074840003269 0.9983726052783322 0.9994957736838301 0.9983726052783322 0.9995362424729666 0.0016664271839630374 0.0004023074840003269 0.9983726052783322 0.0004023074840003269
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04702_rBt5jM8PeYjU0N6YX2Tm.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly.0aFL494z3uJnl8OQKJ28">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly_0_.qJZkJpoEF45L89pILsLZ">
0.9997377338480589 0.999313305635394 0.000286490753384383 0.9993133056354111 0.8504035948485424 0.000505002863187512 0.9997377338480589 0.999272850035656 0.9997377338480589 0.999313305635394
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04702_I5lTdwF0EKTZhEGgPCgo.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly_0_">
0.00024836718799647883 0.00038245296920369124 0.9996632015656044 0.00038245296920369124 0.00024836718799647883 0.9995309896055338 0.00024836718799647883 0.00038245296920369124
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04700#2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_">
0.9993102239603702 0.000844211343297694 0.9993102239603702 0.9988164362688973 0.0009189158887816795 0.000844211343297694 0.9993102239603702 0.000844211343297694
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04700_Q1GnHMijthPcPp65qaLg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.9Agpc7P2tygnBZMTpzwu">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.Tk9iFpO7wovFgOhYZYGu">
0.00028740228157264625 0.000355509947500048 0.013142655827353522 0.00035550994750004625 0.9997633539683501 0.9996093075480238 0.00028740228157264625 0.000355509947500048
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4959457/DSC04700_V6cfBYDmZqBqLUNZqZsI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.l3ue9b2w7YCmhk6MHFnF">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.x7DWwdcv8O4t6CYSfj43">
0.00014264475707537191 0.00032509279788159036 0.9997194244825023 0.9994427998966355 0.012999180838238585 0.9994428000173605 0.00014264475707537191 0.00032509279788159036
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
<app:target uri="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.l3ue9b2w7YCmhk6MHFnF.E7vI4T14VAjyLBUcdUOe">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.x7DWwdcv8O4t6CYSfj43.i1eA04t1x6NgsQPIlYWU">
0.8131123315587181 1.6592340778161334 0.6499067050014457 1.7383088074577395 1.5689175881454378 1.292957016410742 0.8131123315587181 1.6592340778161334
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_88dac68a-5ed3-4d97-b8d1-eb2241df39cc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_88dac68a-5ed3-4d97-b8d1-eb2241df39cc_poly.2U8IGfEKNRE6iZk3Y7Fe">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_88dac68a-5ed3-4d97-b8d1-eb2241df39cc_poly_0_.K71bkAZuz67ZHrI8PQZ0">
<gml:posList srsDimension="3">
691034.1 5336093.588 540.5
691034.1 5336093.587 540.5
691042.933 5336090.154 540.5
691034.1 5336093.588 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_88dac68a-5ed3-4d97-b8d1-eb2241df39cc_poly.h6yNWIN4z7vuLOBW0X6T">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_88dac68a-5ed3-4d97-b8d1-eb2241df39cc_poly_0_.yeU4JIORwzlqDuWvPkvT">
<gml:posList srsDimension="3">
691039.571 5336081.644 540.5
691042.933 5336090.154 540.5
691034.1 5336093.587 540.5
691032.515 5336089.559 540.5
691026.161 5336092.029 540.5
691027.744 5336096.057 540.5
691019.798 5336099.147 540.5
691016.45 5336090.631 540.5
691019.82 5336089.321 540.5
691039.571 5336081.644 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_1c0858ec-3277-47e0-a674-b9d8c51a4d5c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_1c0858ec-3277-47e0-a674-b9d8c51a4d5c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_1c0858ec-3277-47e0-a674-b9d8c51a4d5c_poly_0_">
<gml:posList srsDimension="3">
691045.178 5336082.144 536.15
691047.633 5336088.327 533.788
691042.933 5336090.154 533.788
691040.489 5336083.967 536.15
691045.178 5336082.144 536.15
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_a4330d99-0bad-4389-a575-17d4b74fc6c8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_a4330d99-0bad-4389-a575-17d4b74fc6c8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_a4330d99-0bad-4389-a575-17d4b74fc6c8_poly_0_">
<gml:posList srsDimension="3">
690993.459 5335971.542 545.4
690988.22 5335983.562 546.82
690976.197 5335978.319 545.4
690993.459 5335971.542 545.4
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_73cf7b67-2091-4a3b-9a3e-c892bd2fd186">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_73cf7b67-2091-4a3b-9a3e-c892bd2fd186_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_73cf7b67-2091-4a3b-9a3e-c892bd2fd186_poly_0_">
<gml:posList srsDimension="3">
690969.736 5335971.41 540.61
690990.303 5335963.334 540.61
690992.122 5335962.625 540.61
690994.638 5335968.968 540.61
690998.599 5335978.991 540.61
691009.161 5336005.716 540.61
691009.528 5336005.573 540.61
691025.984 5336047.24 540.61
691025.549 5336047.411 540.61
691037.55 5336077.865 540.61
691038.008 5336077.685 540.61
691038.027 5336077.734 540.61
691039.571 5336081.644 540.61
691019.82 5336089.321 540.61
691018.282 5336085.409 540.61
691035.569 5336078.689 540.61
690993.459 5335971.542 540.61
690976.197 5335978.319 540.61
690972.833 5335979.64 540.61
690969.619 5335971.456 540.61
690969.736 5335971.41 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_58b00512-879e-426c-be70-e1de4350b74e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_58b00512-879e-426c-be70-e1de4350b74e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_58b00512-879e-426c-be70-e1de4350b74e_poly_0_">
<gml:posList srsDimension="3">
690988.22 5335983.562 546.82
690993.459 5335971.542 545.4
691023.532 5336073.418 546.82
690988.22 5335983.562 546.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_58b00512-879e-426c-be70-e1de4350b74e_poly.ApQknURVVLu3gbpcDbRQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_58b00512-879e-426c-be70-e1de4350b74e_poly_0_.SAvUYLJHA6LNknG43m00">
<gml:posList srsDimension="3">
691035.569 5336078.689 545.4
691023.532 5336073.418 546.82
690993.459 5335971.542 545.4
691035.569 5336078.689 545.4
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_45c7a653-4770-4aea-8ba1-1fd890ff8149">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_45c7a653-4770-4aea-8ba1-1fd890ff8149_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_45c7a653-4770-4aea-8ba1-1fd890ff8149_poly_0_">
<gml:posList srsDimension="3">
691014.528 5336003.619 534.479
691037.683 5335994.568 534.479
691054.069 5336036.165 534.479
691025.984 5336047.24 534.479
691021.365 5336035.538 534.479
691039.915 5336028.221 534.479
691032.309 5336008.867 534.479
691013.725 5336016.195 534.479
691009.528 5336005.573 534.479
691014.528 5336003.619 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_b1cbbe5e-9a92-465e-9ac6-654e0eff533f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b1cbbe5e-9a92-465e-9ac6-654e0eff533f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b1cbbe5e-9a92-465e-9ac6-654e0eff533f_poly_0_">
<gml:posList srsDimension="3">
690985.287 5336020.695 533.82
690986.05 5336022.639 533.82
690976.77 5336026.268 533.82
690976.006 5336024.323 533.82
690985.287 5336020.695 533.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_d04c2fc6-04ed-4dd5-a6cc-1210b8bf05c2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d04c2fc6-04ed-4dd5-a6cc-1210b8bf05c2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d04c2fc6-04ed-4dd5-a6cc-1210b8bf05c2_poly_0_">
<gml:posList srsDimension="3">
691013.26 5336091.872 532.02
691015.024 5336096.358 532.02
691005.733 5336099.97 532.02
691003.97 5336095.483 532.02
691013.26 5336091.872 532.02
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_9639348f-a117-4ac6-aca9-79e57eb01aae">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_9639348f-a117-4ac6-aca9-79e57eb01aae_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_9639348f-a117-4ac6-aca9-79e57eb01aae_poly_0_">
<gml:posList srsDimension="3">
690988.22 5335983.562 546.82
691023.532 5336073.418 546.82
691018.282 5336085.409 545.4
690976.197 5335978.319 545.4
690988.22 5335983.562 546.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_f6528330-49f9-49f8-bd69-79fc6d3775f2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_f6528330-49f9-49f8-bd69-79fc6d3775f2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_f6528330-49f9-49f8-bd69-79fc6d3775f2_poly_0_">
<gml:posList srsDimension="3">
691042.684 5336075.844 527.279
691042.711 5336075.913 527.279
691038.027 5336077.734 527.279
691038.008 5336077.685 527.279
691042.684 5336075.844 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_07e1575b-dad4-4cc4-b4de-443f1bb2173f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07e1575b-dad4-4cc4-b4de-443f1bb2173f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07e1575b-dad4-4cc4-b4de-443f1bb2173f_poly_0_">
<gml:posList srsDimension="3">
690976.197 5335978.319 540.53
691019.82 5336089.321 540.53
691016.45 5336090.631 540.53
691014.985 5336086.903 540.53
691015.17 5336086.831 540.53
691004.591 5336059.805 540.53
691004.373 5336059.891 540.53
690985.134 5336010.945 540.53
690985.323 5336010.872 540.53
690974.65 5335983.902 540.53
690974.527 5335983.95 540.53
690972.833 5335979.64 540.53
690976.197 5335978.319 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_19bd3aa3-949f-4967-9a89-68fc0a35c222">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_19bd3aa3-949f-4967-9a89-68fc0a35c222_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_19bd3aa3-949f-4967-9a89-68fc0a35c222_poly_0_">
<gml:posList srsDimension="3">
690967.999 5335976.692 532.15
690969.648 5335980.889 532.15
690960.371 5335984.532 532.15
690958.722 5335980.334 532.15
690967.999 5335976.692 532.15
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_e4cce685-5f73-4e4e-80a8-34c8240a5ef9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e4cce685-5f73-4e4e-80a8-34c8240a5ef9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e4cce685-5f73-4e4e-80a8-34c8240a5ef9_poly_0_">
<gml:posList srsDimension="3">
691032.515 5336089.559 547.33
691034.1 5336093.588 547.33
691027.744 5336096.057 547.33
691026.161 5336092.029 547.33
691032.515 5336089.559 547.33
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_0d9e3443-9759-42c0-a3f7-905c971d3129">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_0d9e3443-9759-42c0-a3f7-905c971d3129_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_0d9e3443-9759-42c0-a3f7-905c971d3129_poly_0_">
<gml:posList srsDimension="3">
690996.423 5336049.027 533.42
690997.34 5336051.356 533.42
690988.067 5336055.013 533.42
690987.151 5336052.683 533.42
690996.423 5336049.027 533.42
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly.HzFySIiTOmSLhTNhHUkC.zWF4e3Du3nVXTKUGMzHv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly_0_.HNpvSZlakZMEd8GAP0fy.H0bbmm9t2NmnADxiBI5K">
<gml:posList srsDimension="3">
691045.163 5336082.106 536.135
691045.178 5336082.144 536.15
691042.711 5336075.913 533.77
691045.163 5336082.106 536.135
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly_0_">
<gml:posList srsDimension="3">
691038.027 5336077.734 533.77
691042.711 5336075.913 533.77
691040.489 5336083.967 536.15
691038.027 5336077.734 533.77
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly.HzFySIiTOmSLhTNhHUkC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly_0_.HNpvSZlakZMEd8GAP0fy">
<gml:posList srsDimension="3">
691040.489 5336083.967 536.15
691042.711 5336075.913 533.77
691045.178 5336082.144 536.15
691040.489 5336083.967 536.15
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly.HzFySIiTOmSLhTNhHUkC.kA4HVqgdepk2eT87WX4Z">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b4523440-fb27-4aad-81a8-46a9349ecda1_poly_0_.HNpvSZlakZMEd8GAP0fy.l71rvA5twlo1ywcKB7pa">
<gml:posList srsDimension="3">
691045.163 5336082.106 536.135
691045.178 5336082.144 536.15
691042.711 5336075.913 533.77
691045.163 5336082.106 536.135
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_97e9ea08-8880-4bf7-88bf-47a9bace2f90">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_97e9ea08-8880-4bf7-88bf-47a9bace2f90_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_97e9ea08-8880-4bf7-88bf-47a9bace2f90_poly_0_">
<gml:posList srsDimension="3">
691023.532 5336073.418 546.82
691035.569 5336078.689 545.4
691018.282 5336085.409 545.4
691023.532 5336073.418 546.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_dc047c43-f074-4306-a470-7d2bbf666212">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_dc047c43-f074-4306-a470-7d2bbf666212_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_dc047c43-f074-4306-a470-7d2bbf666212_poly_0_">
<gml:posList srsDimension="3">
690957.971 5335990.458 530.92
690953.026 5335977.974 530.92
690969.619 5335971.456 530.92
690974.527 5335983.95 530.92
690962.066 5335988.848 530.92
690957.971 5335990.458 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
<gml:interior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_dc047c43-f074-4306-a470-7d2bbf666212_poly_0_.OLXd72ILwyF6jHpUMKGs">
<gml:posList srsDimension="3">
690960.371 5335984.532 530.92
690969.648 5335980.889 530.92
690967.999 5335976.692 530.92
690958.722 5335980.334 530.92
690960.371 5335984.532 530.92
</gml:posList>
</gml:LinearRing>
</gml:interior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_f5d787b1-1fee-441a-898d-0d1bab1fc83f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_f5d787b1-1fee-441a-898d-0d1bab1fc83f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_f5d787b1-1fee-441a-898d-0d1bab1fc83f_poly_0_">
<gml:posList srsDimension="3">
690985.134 5336010.945 530.92
691004.373 5336059.891 530.92
690987.828 5336066.368 530.92
690968.595 5336017.425 530.92
690985.134 5336010.945 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
<gml:interior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_f5d787b1-1fee-441a-898d-0d1bab1fc83f_poly_0_.30QRKW5BZyp1creeMC03">
<gml:posList srsDimension="3">
690976.77 5336026.268 530.92
690986.05 5336022.639 530.92
690985.287 5336020.695 530.92
690976.006 5336024.323 530.92
690976.77 5336026.268 530.92
</gml:posList>
</gml:LinearRing>
</gml:interior>
<gml:interior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_f5d787b1-1fee-441a-898d-0d1bab1fc83f_poly_0_.vGlofU88U4U8ACnhLiIK">
<gml:posList srsDimension="3">
690988.067 5336055.013 530.92
690997.34 5336051.356 530.92
690996.423 5336049.027 530.92
690987.151 5336052.683 530.92
690988.067 5336055.013 530.92
</gml:posList>
</gml:LinearRing>
</gml:interior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_5a201d96-94bb-4fd1-b827-ed4b04c86a96">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5a201d96-94bb-4fd1-b827-ed4b04c86a96_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5a201d96-94bb-4fd1-b827-ed4b04c86a96_poly_0_">
<gml:posList srsDimension="3">
691018.718 5336014.226 527.279
691032.309 5336008.867 527.279
691039.915 5336028.221 527.279
691026.358 5336033.569 527.279
691021.365 5336035.538 527.279
691013.725 5336016.195 527.279
691018.718 5336014.226 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
<gml:interior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5a201d96-94bb-4fd1-b827-ed4b04c86a96_poly_0_.Ir11ZOmCY649EZTgdIsS">
<gml:posList srsDimension="3">
691018.943 5336019.52 527.279
691023.314 5336030.639 527.279
691034.403 5336026.258 527.279
691030.043 5336015.168 527.279
691018.943 5336019.52 527.279
</gml:posList>
</gml:LinearRing>
</gml:interior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4959457_6c105139-c8d5-4df9-b425-91b4c4815423">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6c105139-c8d5-4df9-b425-91b4c4815423_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6c105139-c8d5-4df9-b425-91b4c4815423_poly_0_">
<gml:posList srsDimension="3">
691012.608 5336087.832 530.85
691014.985 5336086.903 530.85
691019.798 5336099.147 530.85
691017.446 5336100.061 530.85
691003.217 5336105.592 530.85
690998.46 5336093.385 530.85
691012.608 5336087.832 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
<gml:interior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6c105139-c8d5-4df9-b425-91b4c4815423_poly_0_.CbVgvajgEUdkcmYjochw">
<gml:posList srsDimension="3">
691005.733 5336099.97 530.85
691015.024 5336096.358 530.85
691013.26 5336091.872 530.85
691003.97 5336095.483 530.85
691005.733 5336099.97 530.85
</gml:posList>
</gml:LinearRing>
</gml:interior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_0545338e-0318-4de3-a657-0ae79a1ccd6f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_0545338e-0318-4de3-a657-0ae79a1ccd6f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_0545338e-0318-4de3-a657-0ae79a1ccd6f_poly_0_">
<gml:posList srsDimension="3">
691035.569 5336078.689 540.61
691035.569 5336078.689 545.4
690993.459 5335971.542 545.4
690993.459 5335971.542 540.61
691035.569 5336078.689 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_9e796a00-4ff1-475a-9e79-574a17c3e1f2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_9e796a00-4ff1-475a-9e79-574a17c3e1f2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_9e796a00-4ff1-475a-9e79-574a17c3e1f2_poly_0_">
<gml:posList srsDimension="3">
691018.282 5336085.409 540.61
691018.282 5336085.409 545.4
691035.569 5336078.689 545.4
691035.569 5336078.689 540.61
691018.282 5336085.409 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_fd635945-d683-4794-b8af-b1677e32614f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_fd635945-d683-4794-b8af-b1677e32614f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_fd635945-d683-4794-b8af-b1677e32614f_poly_0_">
<gml:posList srsDimension="3">
691012.608 5336087.832 509.79
691012.608 5336087.832 530.85
690998.46 5336093.385 530.85
690998.46 5336093.385 509.79
691012.608 5336087.832 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_ed54e689-9b5a-4f95-af75-fc682106653a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_ed54e689-9b5a-4f95-af75-fc682106653a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_ed54e689-9b5a-4f95-af75-fc682106653a_poly_0_">
<gml:posList srsDimension="3">
690953.026 5335977.974 509.79
690953.026 5335977.974 530.92
690957.971 5335990.458 530.92
690957.971 5335990.458 509.79
690953.026 5335977.974 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_b199d45e-ea8e-48fc-bf54-097bcd17e145">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b199d45e-ea8e-48fc-bf54-097bcd17e145_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b199d45e-ea8e-48fc-bf54-097bcd17e145_poly_0_">
<gml:posList srsDimension="3">
691013.26 5336091.872 530.85
691013.26 5336091.872 532.02
691003.97 5336095.483 532.02
691003.97 5336095.483 530.85
691013.26 5336091.872 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_552bb88c-e86a-4fcc-979c-65b1ab61e5d9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_552bb88c-e86a-4fcc-979c-65b1ab61e5d9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_552bb88c-e86a-4fcc-979c-65b1ab61e5d9_poly_0_">
<gml:posList srsDimension="3">
690998.46 5336093.385 530.85
691003.217 5336105.592 530.85
691003.217 5336105.592 509.79
690998.46 5336093.385 509.79
690998.46 5336093.385 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_c5c69a3e-71dc-4998-b005-4ff910d3bac8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_c5c69a3e-71dc-4998-b005-4ff910d3bac8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_c5c69a3e-71dc-4998-b005-4ff910d3bac8_poly_0_">
<gml:posList srsDimension="3">
691009.161 5336005.716 509.79
691009.161 5336005.716 540.61
690998.599 5335978.991 540.61
690998.599 5335978.991 509.79
691009.161 5336005.716 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_b8dae246-a9db-4b76-bb02-296cf532f0da">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b8dae246-a9db-4b76-bb02-296cf532f0da_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b8dae246-a9db-4b76-bb02-296cf532f0da_poly_0_">
<gml:posList srsDimension="3">
691025.549 5336047.411 509.79
691037.55 5336077.865 509.79
691037.55 5336077.865 540.61
691025.549 5336047.411 540.61
691025.549 5336047.411 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_2881490d-b915-470a-8393-3d3a3e1a4897">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_2881490d-b915-470a-8393-3d3a3e1a4897_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_2881490d-b915-470a-8393-3d3a3e1a4897_poly_0_">
<gml:posList srsDimension="3">
690996.423 5336049.027 530.92
690996.423 5336049.027 533.42
690987.151 5336052.683 533.42
690987.151 5336052.683 530.92
690996.423 5336049.027 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_cd320a7e-23dc-4f4b-b3bf-13e80a0f1e44">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_cd320a7e-23dc-4f4b-b3bf-13e80a0f1e44_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_cd320a7e-23dc-4f4b-b3bf-13e80a0f1e44_poly_0_">
<gml:posList srsDimension="3">
691003.97 5336095.483 530.85
691003.97 5336095.483 532.02
691005.733 5336099.97 532.02
691005.733 5336099.97 530.85
691003.97 5336095.483 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_27f08ef8-cc99-4ca6-aa54-8d00e6575e04">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_27f08ef8-cc99-4ca6-aa54-8d00e6575e04_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_27f08ef8-cc99-4ca6-aa54-8d00e6575e04_poly_0_">
<gml:posList srsDimension="3">
690987.151 5336052.683 530.92
690987.151 5336052.683 533.42
690988.067 5336055.013 533.42
690988.067 5336055.013 530.92
690987.151 5336052.683 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_93313f8b-4a17-4854-bf21-0808c38fdb6a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_93313f8b-4a17-4854-bf21-0808c38fdb6a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_93313f8b-4a17-4854-bf21-0808c38fdb6a_poly_0_">
<gml:posList srsDimension="3">
690992.122 5335962.625 509.79
690992.122 5335962.625 540.61
690990.303 5335963.334 540.61
690990.303 5335963.334 509.79
690992.122 5335962.625 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_6c83b50c-2235-431c-ba99-b4799756204c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6c83b50c-2235-431c-ba99-b4799756204c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6c83b50c-2235-431c-ba99-b4799756204c_poly_0_">
<gml:posList srsDimension="3">
690993.459 5335971.542 540.61
690993.459 5335971.542 545.4
690976.197 5335978.319 545.4
690976.197 5335978.319 540.61
690993.459 5335971.542 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_ba99a5d8-0aca-4454-ba9b-adb342b6be29">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_ba99a5d8-0aca-4454-ba9b-adb342b6be29_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_ba99a5d8-0aca-4454-ba9b-adb342b6be29_poly_0_">
<gml:posList srsDimension="3">
691015.024 5336096.358 530.85
691015.024 5336096.358 532.02
691013.26 5336091.872 532.02
691013.26 5336091.872 530.85
691015.024 5336096.358 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_7e90948b-ed74-4965-986d-c9f1e01d3d05">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7e90948b-ed74-4965-986d-c9f1e01d3d05_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7e90948b-ed74-4965-986d-c9f1e01d3d05_poly_0_">
<gml:posList srsDimension="3">
690985.287 5336020.695 530.92
690985.287 5336020.695 533.82
690976.006 5336024.323 533.82
690976.006 5336024.323 530.92
690985.287 5336020.695 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_0797a53f-9a49-4eca-8e0d-d6bb1229fabc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_0797a53f-9a49-4eca-8e0d-d6bb1229fabc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_0797a53f-9a49-4eca-8e0d-d6bb1229fabc_poly_0_">
<gml:posList srsDimension="3">
691032.309 5336008.867 527.279
691032.309 5336008.867 534.479
691039.915 5336028.221 534.479
691039.915 5336028.221 534.478
691039.915 5336028.221 527.279
691032.309 5336008.867 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_9442e008-1bc8-459c-8ccc-802088b7c36d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_9442e008-1bc8-459c-8ccc-802088b7c36d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_9442e008-1bc8-459c-8ccc-802088b7c36d_poly_0_">
<gml:posList srsDimension="3">
691032.515 5336089.559 540.5
691032.515 5336089.559 547.33
691026.161 5336092.029 547.33
691026.161 5336092.029 540.5
691032.515 5336089.559 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_82a88bf3-a972-4f7d-b7d8-cf88d3b0e7f8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_82a88bf3-a972-4f7d-b7d8-cf88d3b0e7f8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_82a88bf3-a972-4f7d-b7d8-cf88d3b0e7f8_poly_0_">
<gml:posList srsDimension="3">
690972.833 5335979.64 540.53
690972.833 5335979.64 540.61
690976.197 5335978.319 540.61
690976.197 5335978.319 540.53
690972.833 5335979.64 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_e4b3c3b2-b527-4731-a3ca-192a40ab3524">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e4b3c3b2-b527-4731-a3ca-192a40ab3524_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e4b3c3b2-b527-4731-a3ca-192a40ab3524_poly_0_">
<gml:posList srsDimension="3">
690988.067 5336055.013 530.92
690988.067 5336055.013 533.42
690997.34 5336051.356 533.42
690997.34 5336051.356 530.92
690988.067 5336055.013 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_7ecc3dc8-de8c-4436-9e8a-59211b219416">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7ecc3dc8-de8c-4436-9e8a-59211b219416_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7ecc3dc8-de8c-4436-9e8a-59211b219416_poly_0_">
<gml:posList srsDimension="3">
690976.006 5336024.323 530.92
690976.006 5336024.323 533.82
690976.77 5336026.268 533.82
690976.77 5336026.268 530.92
690976.006 5336024.323 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_12bfe0c0-30d6-47b3-8819-542a58876da1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_12bfe0c0-30d6-47b3-8819-542a58876da1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_12bfe0c0-30d6-47b3-8819-542a58876da1_poly_0_">
<gml:posList srsDimension="3">
690974.65 5335983.902 540.53
690985.323 5336010.872 540.53
690985.323 5336010.872 509.79
690974.65 5335983.902 509.79
690974.65 5335983.902 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_fb5495cc-20dd-4489-8e15-c8e8ec8d025e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_fb5495cc-20dd-4489-8e15-c8e8ec8d025e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_fb5495cc-20dd-4489-8e15-c8e8ec8d025e_poly_0_">
<gml:posList srsDimension="3">
690985.134 5336010.945 540.53
691004.373 5336059.891 540.53
691004.373 5336059.891 530.92
690985.134 5336010.945 530.92
690985.134 5336010.945 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_19a7db05-ce7d-402b-acc6-cdb0e0926e64">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_19a7db05-ce7d-402b-acc6-cdb0e0926e64_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_19a7db05-ce7d-402b-acc6-cdb0e0926e64_poly_0_">
<gml:posList srsDimension="3">
691004.591 5336059.805 509.79
691004.591 5336059.805 540.53
691015.17 5336086.831 540.53
691015.17 5336086.831 509.79
691004.591 5336059.805 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_28af3c7c-fc05-46d1-b987-03bd3d05f95b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_28af3c7c-fc05-46d1-b987-03bd3d05f95b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_28af3c7c-fc05-46d1-b987-03bd3d05f95b_poly_0_">
<gml:posList srsDimension="3">
690967.999 5335976.692 530.92
690967.999 5335976.692 532.15
690958.722 5335980.334 532.15
690958.722 5335980.334 530.92
690967.999 5335976.692 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_e66a6a75-673f-421d-8e7f-ca23aaca9f0f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e66a6a75-673f-421d-8e7f-ca23aaca9f0f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e66a6a75-673f-421d-8e7f-ca23aaca9f0f_poly_0_">
<gml:posList srsDimension="3">
691023.314 5336030.639 509.79
691023.314 5336030.639 527.279
691018.943 5336019.52 527.279
691018.943 5336019.52 509.79
691023.314 5336030.639 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_4b6474c0-4746-4fc5-8f2b-f8c9fccc3b01">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4b6474c0-4746-4fc5-8f2b-f8c9fccc3b01_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4b6474c0-4746-4fc5-8f2b-f8c9fccc3b01_poly_0_">
<gml:posList srsDimension="3">
691030.043 5336015.168 509.79
691030.043 5336015.168 527.279
691034.403 5336026.258 527.279
691034.403 5336026.258 509.79
691030.043 5336015.168 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_baf7d7e9-9ff4-4746-9a5a-911950b150d2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_baf7d7e9-9ff4-4746-9a5a-911950b150d2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_baf7d7e9-9ff4-4746-9a5a-911950b150d2_poly_0_">
<gml:posList srsDimension="3">
690976.77 5336026.268 530.92
690976.77 5336026.268 533.82
690986.05 5336022.639 533.82
690986.05 5336022.639 530.92
690976.77 5336026.268 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_1e8be220-9c18-416b-b333-6afc25cd958d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_1e8be220-9c18-416b-b333-6afc25cd958d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_1e8be220-9c18-416b-b333-6afc25cd958d_poly_0_">
<gml:posList srsDimension="3">
690994.638 5335968.968 509.79
690994.638 5335968.968 540.61
690992.122 5335962.625 540.61
690992.122 5335962.625 509.79
690994.638 5335968.968 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_2bf95c01-66b3-4b03-b50b-218f093715cd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_2bf95c01-66b3-4b03-b50b-218f093715cd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_2bf95c01-66b3-4b03-b50b-218f093715cd_poly_0_">
<gml:posList srsDimension="3">
690998.599 5335978.991 509.79
690998.599 5335978.991 540.61
690997.142 5335975.311 540.61
690997.142 5335975.311 509.79
690998.599 5335978.991 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_815306fe-eaf2-4aba-8079-76db53460a75">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_815306fe-eaf2-4aba-8079-76db53460a75_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_815306fe-eaf2-4aba-8079-76db53460a75_poly_0_">
<gml:posList srsDimension="3">
690960.371 5335984.532 530.92
690960.371 5335984.532 532.15
690969.648 5335980.889 532.15
690969.648 5335980.889 530.92
690960.371 5335984.532 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_0056045d-d186-4d8a-ab63-e8ed0fa2e5ea">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_0056045d-d186-4d8a-ab63-e8ed0fa2e5ea_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_0056045d-d186-4d8a-ab63-e8ed0fa2e5ea_poly_0_">
<gml:posList srsDimension="3">
690969.648 5335980.889 530.92
690969.648 5335980.889 532.15
690967.999 5335976.692 532.15
690967.999 5335976.692 530.92
690969.648 5335980.889 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_4786632e-7831-415e-b46c-5b381361df5e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4786632e-7831-415e-b46c-5b381361df5e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4786632e-7831-415e-b46c-5b381361df5e_poly_0_">
<gml:posList srsDimension="3">
691042.711 5336075.913 527.279
691042.711 5336075.913 533.77
691038.027 5336077.734 533.77
691038.027 5336077.734 527.279
691042.711 5336075.913 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly_0_">
<gml:posList srsDimension="3">
691013.725 5336016.195 527.279
691021.365 5336035.538 527.279
691013.725 5336016.195 534.479
691013.725 5336016.195 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly.JCTv9eSLO7nUpeICpdG8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly_0_.0UkGwj0I7vutW8Oa0BAs">
<gml:posList srsDimension="3">
691021.365 5336035.538 527.279
691021.365 5336035.538 534.479
691013.725 5336016.195 534.479
691021.365 5336035.538 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly.nrLLqZvEPLnD5Vnwm9Hc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly_0_.jbA13ZTjB5xZY5zVTddH">
<gml:posList srsDimension="3">
691025.984 5336047.24 540.61
691009.528 5336005.573 540.61
691013.725 5336016.195 534.479
691025.984 5336047.24 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly.ySyejuzubVKYmbmNnjDb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly_0_.DI9bsr5GjIQa8mIisbDS">
<gml:posList srsDimension="3">
691021.365 5336035.538 534.479
691025.984 5336047.24 540.61
691013.725 5336016.195 534.479
691021.365 5336035.538 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly.R86PUh9fyDFbPZzEzhvB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly_0_.VnQBKbzGwi6vhOU8m0d5">
<gml:posList srsDimension="3">
691025.984 5336047.24 534.479
691025.984 5336047.24 540.61
691021.365 5336035.538 534.479
691025.984 5336047.24 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly.7n2YCkZu17wydSing6H9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da0d28a4-75e2-4eb8-a175-0c64b669841f_poly_0_.JeWBzQ8zF2w4k8jxMDrx">
<gml:posList srsDimension="3">
691009.528 5336005.573 534.479
691013.725 5336016.195 534.479
691009.528 5336005.573 540.61
691009.528 5336005.573 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93_poly_0_">
<gml:posList srsDimension="3">
691013.725 5336016.195 527.279
691013.725 5336016.195 534.479
691018.718 5336014.226 527.279
691013.725 5336016.195 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93_poly.lI9Z0iQKd9L02Vir7o7V">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93_poly_0_.5UuPBIlWM28vK0P77iAH">
<gml:posList srsDimension="3">
691032.309 5336008.867 534.479
691032.309 5336008.867 527.279
691013.725 5336016.195 534.479
691032.309 5336008.867 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93_poly.m6mnhEN0zF3mqMPSQRt9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_89f90b9f-613b-4603-bea2-4ef4352e8e93_poly_0_.MtXA6lU5xEW9ezlNlt8Q">
<gml:posList srsDimension="3">
691032.309 5336008.867 527.279
691018.718 5336014.226 527.279
691013.725 5336016.195 534.479
691032.309 5336008.867 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_e3d1b31a-6a77-41cc-9421-5b4b6c196399">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e3d1b31a-6a77-41cc-9421-5b4b6c196399_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e3d1b31a-6a77-41cc-9421-5b4b6c196399_poly_0_">
<gml:posList srsDimension="3">
690997.34 5336051.356 530.92
690997.34 5336051.356 533.42
690996.423 5336049.027 533.42
690996.423 5336049.027 530.92
690997.34 5336051.356 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_eea9d3b4-b286-4751-b88d-b4e2581c9a5c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_eea9d3b4-b286-4751-b88d-b4e2581c9a5c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_eea9d3b4-b286-4751-b88d-b4e2581c9a5c_poly_0_">
<gml:posList srsDimension="3">
691026.161 5336092.029 540.5
691026.161 5336092.029 547.33
691027.744 5336096.057 547.33
691027.744 5336096.057 540.5
691026.161 5336092.029 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_210976b4-04ab-40c7-a292-28ec46de99e1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_210976b4-04ab-40c7-a292-28ec46de99e1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_210976b4-04ab-40c7-a292-28ec46de99e1_poly_0_">
<gml:posList srsDimension="3">
690958.722 5335980.334 530.92
690958.722 5335980.334 532.15
690960.371 5335984.532 532.15
690960.371 5335984.532 530.92
690958.722 5335980.334 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_">
<gml:posList srsDimension="3">
691038.008 5336077.685 527.279
691038.027 5336077.734 527.279
691038.008 5336077.685 540.61
691038.008 5336077.685 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly.aIxPeNMoW6T8IQr4HkVX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_.1KxfI1UAQgFXLwPEfxtt">
<gml:posList srsDimension="3">
691039.571 5336081.644 540.5
691038.027 5336077.734 540.61
691040.489 5336083.967 536.15
691039.571 5336081.644 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly.EbMwxxkGCFnCgwNF6LXL">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_.S1YrVmgPKo3EeJOgOsIB">
<gml:posList srsDimension="3">
691042.933 5336090.154 540.5
691039.571 5336081.644 540.5
691040.489 5336083.967 536.15
691042.933 5336090.154 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly.fGT26qlcbWR7ISwmKvLr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_.rf0CgmnDak4fSWBv6tMO">
<gml:posList srsDimension="3">
691040.489 5336083.967 536.15
691038.027 5336077.734 540.61
691038.027 5336077.734 533.77
691040.489 5336083.967 536.15
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly.3Zm6IiPHVI0cD9I6m6P4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_.JLFkL5MFK6BY3LSmKQ33">
<gml:posList srsDimension="3">
691039.571 5336081.644 540.5
691039.571 5336081.644 540.61
691038.027 5336077.734 540.61
691039.571 5336081.644 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly.4SJ0OkyVKwdiuOPeX85H">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_.yT4A2Vkdum9YL4mDYcg1">
<gml:posList srsDimension="3">
691038.027 5336077.734 540.61
691038.008 5336077.685 540.61
691038.027 5336077.734 527.279
691038.027 5336077.734 533.77
691038.027 5336077.734 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly.XiyC7B5B8rFHGHZ9pVTY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_d1a6e864-2f43-41b9-bbbe-3898481b49b5_poly_0_.Qujm85KLu5fpnd9glS0d">
<gml:posList srsDimension="3">
691042.933 5336090.154 533.788
691042.933 5336090.154 540.5
691040.489 5336083.967 536.15
691042.933 5336090.154 533.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly.qA0o9seHNxPy3D4XxvGq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_.xha1XGdRyKDqWxFBfDCe">
<gml:posList srsDimension="3">
690974.65 5335983.902 540.53
690974.65 5335983.902 509.79
690974.527 5335983.95 530.92
690974.65 5335983.902 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly.i4v9vtyTgsb5hd2XRmLH.ueTtRFN4hhicGKPgblBX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_.lTlTkwc1BFTtl9Z5wR3H.helbNItRqGIq39HfVsuK">
<gml:posList srsDimension="3">
690962.066 5335988.848 530.92
690974.527 5335983.95 530.92
690957.971 5335990.458 530.92
690962.066 5335988.848 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly.kZsxVrq2BPONOhwAlyZY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_.0sUtKNp6CHNcN5b1jJBh">
<gml:posList srsDimension="3">
690974.65 5335983.902 509.79
690974.527 5335983.95 509.79
690957.971 5335990.458 530.92
690974.65 5335983.902 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly.MAAt8KO2kxmALgkWWG4K">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_.MqLcCqALtxbnDcGTBMSR">
<gml:posList srsDimension="3">
690974.527 5335983.95 540.53
690974.65 5335983.902 540.53
690974.527 5335983.95 530.92
690974.527 5335983.95 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_">
<gml:posList srsDimension="3">
690957.971 5335990.458 509.79
690957.971 5335990.458 530.92
690974.527 5335983.95 509.79
690957.971 5335990.458 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly.i4v9vtyTgsb5hd2XRmLH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_.lTlTkwc1BFTtl9Z5wR3H">
<gml:posList srsDimension="3">
690974.65 5335983.902 509.79
690957.971 5335990.458 530.92
690974.527 5335983.95 530.92
690974.65 5335983.902 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly.i4v9vtyTgsb5hd2XRmLH.20RI31OitEnsOlt3MBna">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_b5cf5691-d2f2-4eef-9019-42962b507487_poly_0_.lTlTkwc1BFTtl9Z5wR3H.ONf4DZCJ9k3Hhta7s94N">
<gml:posList srsDimension="3">
690962.066 5335988.848 530.92
690974.527 5335983.95 530.92
690957.971 5335990.458 530.92
690962.066 5335988.848 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly_0_">
<gml:posList srsDimension="3">
691045.163 5336082.106 509.79
691045.178 5336082.144 509.79
691045.163 5336082.106 536.135
691045.163 5336082.106 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly.1PAPfLBF2SijWLiTPWMW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly_0_.fDnw3YkJPNCGr8R04VPW">
<gml:posList srsDimension="3">
691047.633 5336088.327 533.788
691045.178 5336082.144 536.15
691047.633 5336088.327 509.79
691047.633 5336088.327 533.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly.xjb4r26IEMqWkEAh7NxZ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly_0_.t5x1QcRtr1GAwIiEXy8D">
<gml:posList srsDimension="3">
691045.178 5336082.144 536.15
691045.163 5336082.106 536.135
691045.178 5336082.144 509.79
691045.178 5336082.144 536.15
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly.qRgGtmQt3sugWgStUw5O">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_abe8532c-b751-4a69-bab0-25a90023178b_poly_0_.AXZkt6MegdsqVWTnKxKV">
<gml:posList srsDimension="3">
691047.633 5336088.327 509.79
691045.178 5336082.144 536.15
691045.178 5336082.144 509.79
691047.633 5336088.327 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly.FsdNOd1c6Ph0azRDZTba">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly_0_.xwptMTiwDrAzp8kWly4O">
<gml:posList srsDimension="3">
691016.45 5336090.631 540.5
691014.985 5336086.903 530.85
691016.45 5336090.631 540.53
691016.45 5336090.631 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly.6T4VS41qvhr38EDB9bmE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly_0_.zAtNRjtDHxLbvCO45GHb">
<gml:posList srsDimension="3">
691019.798 5336099.147 540.5
691019.798 5336099.147 530.85
691016.45 5336090.631 540.5
691019.798 5336099.147 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly_0_">
<gml:posList srsDimension="3">
691014.985 5336086.903 540.53
691016.45 5336090.631 540.53
691014.985 5336086.903 530.85
691014.985 5336086.903 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly.fKQsvoR3NDaWVxqlfLef">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8bc7db43-6b80-4e85-8442-7aafd9c35adb_poly_0_.QNwNpUd3nRMxTtMCRDDm">
<gml:posList srsDimension="3">
691019.798 5336099.147 530.85
691014.985 5336086.903 530.85
691016.45 5336090.631 540.5
691019.798 5336099.147 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly_0_">
<gml:posList srsDimension="3">
690969.619 5335971.456 540.61
690972.833 5335979.64 540.61
690969.619 5335971.456 530.92
690969.619 5335971.456 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly.B88dGISJMHSw5NHSavlS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly_0_.18Zu919tlRTXbewq5v1r">
<gml:posList srsDimension="3">
690974.527 5335983.95 540.53
690974.527 5335983.95 530.92
690972.833 5335979.64 540.53
690974.527 5335983.95 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly.dPPhAK550j4mieMp6WPh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly_0_.ST7AAH0RtyiNHpuLzQm1">
<gml:posList srsDimension="3">
690972.833 5335979.64 540.53
690969.619 5335971.456 530.92
690972.833 5335979.64 540.61
690972.833 5335979.64 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly.KhrrKhgZCndfNINnR8Iu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6015e32b-3a99-48c5-95c9-3a27c704ef26_poly_0_.r9zzY3hj1BxGgiKR05at">
<gml:posList srsDimension="3">
690974.527 5335983.95 530.92
690969.619 5335971.456 530.92
690972.833 5335979.64 540.53
690974.527 5335983.95 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly.uCHH81LPzfU98IVG25aG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_.fkcfr60rk6X5KbyUWZ8V">
<gml:posList srsDimension="3">
691037.55 5336077.865 509.79
691038.008 5336077.685 509.79
691042.684 5336075.844 527.279
691037.55 5336077.865 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly.qp6ZjK8R5EyEcv0ioQ0i">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_.es0Rgg3egPrGvHpxHz9W">
<gml:posList srsDimension="3">
691038.008 5336077.685 527.279
691037.55 5336077.865 509.79
691042.684 5336075.844 527.279
691038.008 5336077.685 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_">
<gml:posList srsDimension="3">
691042.684 5336075.844 509.79
691042.684 5336075.844 527.279
691038.008 5336077.685 509.79
691042.684 5336075.844 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly.aiNwdDYj0PdvYgUBkyH2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_.bG7AeyYw7n7CmlOPyv5N">
<gml:posList srsDimension="3">
691038.008 5336077.685 540.61
691037.55 5336077.865 540.61
691038.008 5336077.685 527.279
691038.008 5336077.685 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly.eeOMHpzKnzuhKce601pq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8ca959b2-4608-43b4-bcef-ba5c50a10d89_poly_0_.f2UwYRdTx5eUDL0lMtSs">
<gml:posList srsDimension="3">
691037.55 5336077.865 540.61
691037.55 5336077.865 509.79
691038.008 5336077.685 527.279
691037.55 5336077.865 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_3d28ba7f-5f4c-4f24-9dfb-cd1b8946e5ce">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_3d28ba7f-5f4c-4f24-9dfb-cd1b8946e5ce_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_3d28ba7f-5f4c-4f24-9dfb-cd1b8946e5ce_poly_0_">
<gml:posList srsDimension="3">
690990.242 5335963.358 509.79
690990.303 5335963.334 509.79
690990.303 5335963.334 540.61
690969.736 5335971.41 540.61
690969.736 5335971.41 509.79
690990.242 5335963.358 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly.SmBeyu0VjRF8P29yuObM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly_0_.ZbNmG5ABDx7VCCryiK15">
<gml:posList srsDimension="3">
691019.82 5336089.321 540.53
691039.571 5336081.644 540.5
691016.45 5336090.631 540.53
691019.82 5336089.321 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly.vO6OhmOo0KVhqLrXxZqS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly_0_.bMJFpMfy8Wq4kfFY8qdW">
<gml:posList srsDimension="3">
691039.571 5336081.644 540.61
691039.571 5336081.644 540.5
691019.82 5336089.321 540.53
691039.571 5336081.644 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly.CAfbGewusH2A3spuS7eP">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly_0_.yqrIR6ngKSwut6HoUxAS">
<gml:posList srsDimension="3">
691039.571 5336081.644 540.5
691019.82 5336089.321 540.5
691016.45 5336090.631 540.53
691039.571 5336081.644 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly.LC4gkZwChCgtJ40xqyMc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly_0_.TdvqEyY7KccaLsEKPWRI">
<gml:posList srsDimension="3">
691019.82 5336089.321 540.61
691039.571 5336081.644 540.61
691019.82 5336089.321 540.53
691019.82 5336089.321 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_25a03dc0-f803-4752-9330-c70063763232_poly_0_">
<gml:posList srsDimension="3">
691016.45 5336090.631 540.5
691016.45 5336090.631 540.53
691019.82 5336089.321 540.5
691016.45 5336090.631 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly_0_">
<gml:posList srsDimension="3">
691039.915 5336028.221 527.279
691039.915 5336028.221 534.478
691026.358 5336033.569 527.279
691039.915 5336028.221 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly.Wm7TkyWKuURGo30hl8EA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly_0_.saZbSdTTKoSUKtGBEQtm">
<gml:posList srsDimension="3">
691021.365 5336035.538 527.279
691026.358 5336033.569 527.279
691039.915 5336028.221 534.478
691021.365 5336035.538 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly.UBBPaTXLPT1z29UwdJRA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly_0_.z1iurpkeRmbKlPOQF8zn">
<gml:posList srsDimension="3">
691039.915 5336028.221 534.479
691021.365 5336035.538 534.479
691039.915 5336028.221 534.478
691039.915 5336028.221 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly.RgZFJ4lvDywvwfINqTuV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07cf3ae7-1cf2-4de4-a381-af2c8303c224_poly_0_.BQZAOHQ3C9W9D9bzsB8J">
<gml:posList srsDimension="3">
691021.365 5336035.538 534.479
691021.365 5336035.538 527.279
691039.915 5336028.221 534.478
691039.915 5336028.221 534.479
691021.365 5336035.538 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly_0_">
<gml:posList srsDimension="3">
691042.684 5336075.844 509.79
691042.711 5336075.913 509.79
691042.684 5336075.844 527.279
691042.684 5336075.844 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly.nptzVVCkPM8XhnH6sCkW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly_0_.f2xJS4UjHeA1yjVOb67M">
<gml:posList srsDimension="3">
691042.711 5336075.913 527.279
691042.684 5336075.844 527.279
691042.711 5336075.913 509.79
691042.711 5336075.913 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly.437JgFrxVoP1pk95I82u">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly_0_.GaCNnCdl3QDVPhsJte3N">
<gml:posList srsDimension="3">
691045.163 5336082.106 509.79
691042.711 5336075.913 527.279
691042.711 5336075.913 509.79
691045.163 5336082.106 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly.lYLtfKnq5Dh8ZDv8yzIa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly_0_.iEVyN5z6a2z1VJZfU85Q">
<gml:posList srsDimension="3">
691042.711 5336075.913 533.77
691042.711 5336075.913 527.279
691045.163 5336082.106 509.79
691042.711 5336075.913 533.77
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly.UR4MDpiJM9Pc7DtHwJXD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_cbc99375-6dc7-4d99-b2da-349c4c01d5f0_poly_0_.TJVJzAo8fdMlnvmcTrZP">
<gml:posList srsDimension="3">
691045.163 5336082.106 536.135
691042.711 5336075.913 533.77
691045.163 5336082.106 509.79
691045.163 5336082.106 536.135
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_5a4b0869-4aa9-4351-8bc3-b3bab6c759d9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5a4b0869-4aa9-4351-8bc3-b3bab6c759d9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5a4b0869-4aa9-4351-8bc3-b3bab6c759d9_poly_0_">
<gml:posList srsDimension="3">
691005.733 5336099.97 530.85
691005.733 5336099.97 532.02
691015.024 5336096.358 532.02
691015.024 5336096.358 530.85
691005.733 5336099.97 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly.OIKTvGpa5rJD6EE4ueKk.JmcGH9WqeIZ4PvFmV1fp">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly_0_.354CkD1i6ZJXze0Kevjq.B9vrxuMnL3OIKMZKkHiA">
<gml:posList srsDimension="3">
691023.314 5336030.639 527.279
691034.204 5336026.336 527.279
691034.403 5336026.258 527.279
691023.314 5336030.639 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly_0_">
<gml:posList srsDimension="3">
691034.403 5336026.258 509.79
691034.403 5336026.258 527.279
691034.204 5336026.336 509.79
691034.403 5336026.258 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly.Tae6HiG9KOH3TZzdhhjC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly_0_.sV664VkKuDG8XlyNhm5V">
<gml:posList srsDimension="3">
691023.314 5336030.639 509.79
691024.947 5336029.995 509.79
691034.403 5336026.258 527.279
691023.314 5336030.639 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly.RMiKdcva1YLitxGyejh4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly_0_.l0EkvMrxaqKjLSFuTkpQ">
<gml:posList srsDimension="3">
691024.947 5336029.995 509.79
691034.204 5336026.336 509.79
691034.403 5336026.258 527.279
691024.947 5336029.995 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly.OIKTvGpa5rJD6EE4ueKk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly_0_.354CkD1i6ZJXze0Kevjq">
<gml:posList srsDimension="3">
691023.314 5336030.639 509.79
691034.403 5336026.258 527.279
691023.314 5336030.639 527.279
691023.314 5336030.639 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly.OIKTvGpa5rJD6EE4ueKk.G1jBdG8vNdCXK43pnAMX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_07577e6f-c4dd-4184-87ae-d7a425099b55_poly_0_.354CkD1i6ZJXze0Kevjq.VDC3IpLrKRwZz29nciOv">
<gml:posList srsDimension="3">
691034.204 5336026.336 527.279
691023.314 5336030.639 527.279
691034.403 5336026.258 527.279
691034.204 5336026.336 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_9173e93a-34bf-488f-b49b-3b42bb8a6050">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_9173e93a-34bf-488f-b49b-3b42bb8a6050_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_9173e93a-34bf-488f-b49b-3b42bb8a6050_poly_0_">
<gml:posList srsDimension="3">
690968.595 5336017.425 530.92
690987.828 5336066.368 530.92
690987.828 5336066.368 509.79
690968.595 5336017.425 509.79
690968.595 5336017.425 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_ccc35b52-aa6a-41cc-9317-c44b76807f0b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_ccc35b52-aa6a-41cc-9317-c44b76807f0b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_ccc35b52-aa6a-41cc-9317-c44b76807f0b_poly_0_">
<gml:posList srsDimension="3">
690997.142 5335975.311 509.79
690997.142 5335975.311 540.61
690994.638 5335968.968 540.61
690994.638 5335968.968 509.79
690997.142 5335975.311 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_da187106-7015-4f58-acc5-3001211055df">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_da187106-7015-4f58-acc5-3001211055df_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_da187106-7015-4f58-acc5-3001211055df_poly_0_">
<gml:posList srsDimension="3">
690986.05 5336022.639 530.92
690986.05 5336022.639 533.82
690985.287 5336020.695 533.82
690985.287 5336020.695 530.92
690986.05 5336022.639 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly.bCUwmUDk0OovYHjfmxVg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly_0_.zk4hL17kngUzYEWhFSfU">
<gml:posList srsDimension="3">
690985.134 5336010.945 530.92
690968.595 5336017.425 530.92
690968.595 5336017.425 509.79
690985.134 5336010.945 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly.Cj6zXraLTwR7PxfKpjZk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly_0_.5Uh6eyguz0RTdli6IVdp">
<gml:posList srsDimension="3">
690985.134 5336010.945 530.92
690985.134 5336010.945 509.79
690985.323 5336010.872 540.53
690985.134 5336010.945 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly_0_">
<gml:posList srsDimension="3">
690985.323 5336010.872 509.79
690985.323 5336010.872 540.53
690985.134 5336010.945 509.79
690985.323 5336010.872 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly.kGbeRAaIPJx3SvmvfHUc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly_0_.GjKZFkiUJ3f9WlicifZG">
<gml:posList srsDimension="3">
690968.595 5336017.425 509.79
690985.134 5336010.945 509.79
690985.134 5336010.945 530.92
690968.595 5336017.425 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly.kcKr5ncV6pscybp517KB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_8b5b56c7-b263-468d-a637-68631a0b56a4_poly_0_.qahnd9qMVEe9Z4pkOF5Y">
<gml:posList srsDimension="3">
690985.134 5336010.945 540.53
690985.134 5336010.945 530.92
690985.323 5336010.872 540.53
690985.134 5336010.945 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.cQE7IuQJ8nIPqcH7JVsD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.i0UZldZhMAhzbiv0NOlB">
<gml:posList srsDimension="3">
691054.069 5336036.165 534.478
691054.069 5336036.165 509.79
691025.984 5336047.24 534.479
691054.069 5336036.165 534.479
691054.069 5336036.165 534.478
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.YzxFgIbgqBghCNQKVnbO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.9PvvvqjAiPmlTLDqvNCn">
<gml:posList srsDimension="3">
691025.984 5336047.24 534.479
691054.069 5336036.165 534.479
691054.069 5336036.165 534.478
691025.984 5336047.24 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.mOZziDyzEgxvgzyyFO6u">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.aOQfN2DChgmDDY4vU3yR">
<gml:posList srsDimension="3">
691025.984 5336047.24 534.479
691025.984 5336047.24 509.79
691025.549 5336047.411 540.61
691025.984 5336047.24 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.9bBaI8fm6EsaKrkNpJOV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.pnJDzAR2tb29Gaxq4szH">
<gml:posList srsDimension="3">
691054.069 5336036.165 509.79
691025.984 5336047.24 509.79
691025.984 5336047.24 534.479
691054.069 5336036.165 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly.v3xhsVBJSrjtEEb5jR8I">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_.SsCw4o46Tm78qPNWw3xt">
<gml:posList srsDimension="3">
691025.984 5336047.24 540.61
691025.984 5336047.24 534.479
691025.549 5336047.411 540.61
691025.984 5336047.24 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_7c02b6d4-05fc-4e6a-a3f1-44a9bb8d95a4_poly_0_">
<gml:posList srsDimension="3">
691025.549 5336047.411 509.79
691025.549 5336047.411 540.61
691025.984 5336047.24 509.79
691025.549 5336047.411 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.dAujGMYIDNYqQIulNoSK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.naPklL2S8d0yjNKQgCyu">
<gml:posList srsDimension="3">
691027.744 5336096.057 540.5
691032.41 5336094.244 540.5
691034.1 5336093.587 540.5
691034.1 5336093.588 540.5
691019.798 5336099.147 540.5
691027.744 5336096.057 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.66RtPde4CP1JsXofXtAv.UbJbMIIPyUgPzyg0HIEM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.FLq6KrYtJd640RM9Va19.PWDdoPzheiXiTunq1LxI">
<gml:posList srsDimension="3">
691017.446 5336100.061 530.85
691019.798 5336099.147 530.85
691003.217 5336105.592 530.85
691017.446 5336100.061 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.tsebirkP3xgeOLw20ECT">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.15AOb8bLaREqjjqiYCrt">
<gml:posList srsDimension="3">
691019.798 5336099.147 530.85
691019.798 5336099.147 540.5
691042.933 5336090.154 533.788
691019.798 5336099.147 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.w6bR1M1cZKHa1YPlbXv0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.jdvagXa4d2RWK4wQO7PL">
<gml:posList srsDimension="3">
691034.1 5336093.588 547.33
691034.1 5336093.588 540.5
691027.744 5336096.057 547.33
691034.1 5336093.588 547.33
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.q710Md24FpY2Sl026n5g">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.gIYDDoJPSubsYbxvXIPf">
<gml:posList srsDimension="3">
691003.217 5336105.592 509.79
691003.217 5336105.592 530.85
691047.633 5336088.327 509.79
691003.217 5336105.592 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.bUDptu4BMiV1NybQltGS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.xbGgOLHdj5T3iW39jNpM">
<gml:posList srsDimension="3">
691042.933 5336090.154 533.788
691047.633 5336088.327 533.788
691019.798 5336099.147 530.85
691042.933 5336090.154 533.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.SeJwMizABrSxnPSUCYLA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.y5kV8CgZ0mpVJStfAM0F">
<gml:posList srsDimension="3">
691047.633 5336088.327 533.788
691047.633 5336088.327 509.79
691019.798 5336099.147 530.85
691047.633 5336088.327 533.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.7vGUONbfe3UJHyZY5tNt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.y7V5CFJFC7J1wbDXL84L">
<gml:posList srsDimension="3">
691034.1 5336093.588 540.5
691019.798 5336099.147 540.5
691027.744 5336096.057 540.5
691032.41 5336094.244 540.5
691034.1 5336093.588 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_">
<gml:posList srsDimension="3">
691027.744 5336096.057 540.5
691027.744 5336096.057 547.33
691032.41 5336094.244 540.5
691027.744 5336096.057 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.GRfTE9bCQfVm3Mz1fjS5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.fz4TPGb8YgMUfVv7r1bD">
<gml:posList srsDimension="3">
691034.1 5336093.588 540.5
691032.41 5336094.244 540.5
691027.744 5336096.057 547.33
691034.1 5336093.588 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA.V3XM8ufPH6KC5ip0stXM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH.3r9zUHW5Lx7ncBISK3P3">
<gml:posList srsDimension="3">
691027.744 5336096.057 540.5
691032.41 5336094.244 540.5
691034.1 5336093.587 540.5
691042.933 5336090.154 540.5
691034.1 5336093.588 540.5
691019.798 5336099.147 540.5
691027.744 5336096.057 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA.vz03euOKW8NJPZi0EPvN">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH.ng0CBOSQQ2w3R7to11R5">
<gml:posList srsDimension="3">
691042.933 5336090.154 540.5
691034.1 5336093.587 540.5
691034.1 5336093.588 540.5
691032.41 5336094.244 540.5
691027.744 5336096.057 540.5
691042.933 5336090.154 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA.kIzHvedR8sls5N5cVGM3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH.ITqgClqz0yKiRW9xnYuU">
<gml:posList srsDimension="3">
691042.933 5336090.154 540.5
691034.1 5336093.588 540.5
691032.41 5336094.244 540.5
691034.1 5336093.587 540.5
691042.933 5336090.154 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.66RtPde4CP1JsXofXtAv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.FLq6KrYtJd640RM9Va19">
<gml:posList srsDimension="3">
691047.633 5336088.327 509.79
691003.217 5336105.592 530.85
691019.798 5336099.147 530.85
691047.633 5336088.327 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.66RtPde4CP1JsXofXtAv.hOCKGFGfVxkUMEIJLN5m">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.FLq6KrYtJd640RM9Va19.HNBaZutBT4IATBAVfmix">
<gml:posList srsDimension="3">
691019.798 5336099.147 530.85
691017.446 5336100.061 530.85
691003.217 5336105.592 530.85
691019.798 5336099.147 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH">
<gml:posList srsDimension="3">
691042.933 5336090.154 533.788
691019.798 5336099.147 540.5
691042.933 5336090.154 540.5
691042.933 5336090.154 533.788
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA.w0Fy8x1VLf1IowTQ8TVQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH.O3CtHyOFYMlb1R572oc7">
<gml:posList srsDimension="3">
691027.744 5336096.057 540.5
691042.933 5336090.154 540.5
691019.798 5336099.147 540.5
691027.744 5336096.057 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA.qV2UHz3zRIScEHs5kyS8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH.vxkvhkWKsQN3C3LYnily">
<gml:posList srsDimension="3">
691042.933 5336090.154 540.5
691032.41 5336094.244 540.5
691027.744 5336096.057 540.5
691042.933 5336090.154 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly.7DEvxeyACJkQD6JhErGc.vJ1qRoPROOtjyADU5ySA.i7186UbVaneuLqIOBkCF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_809f9c11-73d9-4671-9bf4-2030bc44e852_poly_0_.NSQAN2uHdjTCPuEusE4D.VWuE1ZhKRZGSiSNUBYnH.iNx4sEjdkjmtFPOCGqR3">
<gml:posList srsDimension="3">
691042.933 5336090.154 540.5
691034.1 5336093.588 540.5
691032.41 5336094.244 540.5
691042.933 5336090.154 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly.BLjh7DpyyBYB2iUUETGa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly_0_.Z4jJgPG4rgpKPOxde1f9">
<gml:posList srsDimension="3">
691054.069 5336036.165 509.79
691054.069 5336036.165 534.478
691054.069 5336036.165 534.479
691051.62 5336029.95 509.79
691054.069 5336036.165 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly.0aFL494z3uJnl8OQKJ28">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly_0_.qJZkJpoEF45L89pILsLZ">
<gml:posList srsDimension="3">
691054.069 5336036.165 534.479
691037.683 5335994.568 534.479
691051.62 5336029.95 509.79
691054.069 5336036.165 534.478
691054.069 5336036.165 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_e030de07-0827-4f7b-9657-9070e768b8f7_poly_0_">
<gml:posList srsDimension="3">
691037.683 5335994.568 509.79
691051.62 5336029.95 509.79
691037.683 5335994.568 534.479
691037.683 5335994.568 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.1M3paJASOIr8js7wklxx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.9povxRIATZWYQqffHUt7">
<gml:posList srsDimension="3">
691009.161 5336005.716 540.61
691009.161 5336005.716 509.79
691009.528 5336005.573 534.479
691009.161 5336005.716 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_">
<gml:posList srsDimension="3">
691037.683 5335994.568 509.79
691037.683 5335994.568 534.479
691009.528 5336005.573 509.79
691037.683 5335994.568 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.l3ue9b2w7YCmhk6MHFnF.E5yTemTlK6YJ6kctn5Bi">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.x7DWwdcv8O4t6CYSfj43.jtAOVLLBeMe4wbf1aHTy">
<gml:posList srsDimension="3">
691014.528 5336003.619 534.479
691009.528 5336005.573 534.479
691037.683 5335994.568 534.479
691014.528 5336003.619 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.9Agpc7P2tygnBZMTpzwu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.Tk9iFpO7wovFgOhYZYGu">
<gml:posList srsDimension="3">
691009.161 5336005.716 509.79
691009.528 5336005.573 509.79
691037.683 5335994.568 534.479
691009.161 5336005.716 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.qJkacLYwwWQ9tr6w595q">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.TGwCV60olKEvtuytWsVe">
<gml:posList srsDimension="3">
691009.528 5336005.573 540.61
691009.161 5336005.716 540.61
691009.528 5336005.573 534.479
691009.528 5336005.573 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.l3ue9b2w7YCmhk6MHFnF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.x7DWwdcv8O4t6CYSfj43">
<gml:posList srsDimension="3">
691009.161 5336005.716 509.79
691037.683 5335994.568 534.479
691009.528 5336005.573 534.479
691009.161 5336005.716 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly.l3ue9b2w7YCmhk6MHFnF.E7vI4T14VAjyLBUcdUOe">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_5aad7582-37d7-4f94-abf5-bd14240affff_poly_0_.x7DWwdcv8O4t6CYSfj43.i1eA04t1x6NgsQPIlYWU">
<gml:posList srsDimension="3">
691014.528 5336003.619 534.479
691009.528 5336005.573 534.479
691037.683 5335994.568 534.479
691014.528 5336003.619 534.479
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.uXpqptxWsTgTJ553XXqe">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.9fTH2Hd8IL2cXnbnHU62">
<gml:posList srsDimension="3">
691018.943 5336019.52 527.279
691020.559 5336018.886 527.279
691026.482 5336016.564 527.279
691029.837 5336015.249 527.279
691018.943 5336019.52 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_">
<gml:posList srsDimension="3">
691018.943 5336019.52 509.79
691018.943 5336019.52 527.279
691029.837 5336015.249 509.79
691018.943 5336019.52 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx.2Ka8epGccQL3XrJRFdlo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB.NbmSXttkjt66Mb5NQr41">
<gml:posList srsDimension="3">
691020.559 5336018.886 527.279
691026.482 5336016.564 527.279
691030.043 5336015.168 527.279
691029.837 5336015.249 527.279
691020.559 5336018.886 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.MboxcIUv86G78ZGAgXN6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.euhe5shlkP9ak7Y6BFjh">
<gml:posList srsDimension="3">
691030.043 5336015.168 509.79
691029.837 5336015.249 509.79
691018.943 5336019.52 527.279
691030.043 5336015.168 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD">
<gml:posList srsDimension="3">
691018.943 5336019.52 527.279
691020.559 5336018.886 527.279
691026.482 5336016.564 527.279
691018.943 5336019.52 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx.H8GlQc2wo8CJq4gDDYnH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB.oaEpCVEdQaoP7J6wJews">
<gml:posList srsDimension="3">
691029.837 5336015.249 527.279
691026.482 5336016.564 527.279
691030.043 5336015.168 527.279
691029.837 5336015.249 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx.ZErRL3yKJPZMh1evaX7W">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB.BfMWgjmpjdRkIYnk1zqi">
<gml:posList srsDimension="3">
691030.043 5336015.168 527.279
691026.482 5336016.564 527.279
691020.559 5336018.886 527.279
691018.943 5336019.52 527.279
691029.837 5336015.249 527.279
691030.043 5336015.168 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB">
<gml:posList srsDimension="3">
691030.043 5336015.168 509.79
691018.943 5336019.52 527.279
691030.043 5336015.168 527.279
691030.043 5336015.168 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx.IPXuhQIuTIM0ZM9hs8yZ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB.aVvpJpMKRqy3DTIHO59X">
<gml:posList srsDimension="3">
691030.043 5336015.168 527.279
691020.559 5336018.886 527.279
691018.943 5336019.52 527.279
691030.043 5336015.168 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx.ZbVbtMpmECXXR4eNrP9T">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB.pvdGzk8rULUxQdcMoMiv">
<gml:posList srsDimension="3">
691030.043 5336015.168 527.279
691026.482 5336016.564 527.279
691020.559 5336018.886 527.279
691030.043 5336015.168 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly.ffPVI4RhMkogYCFH3Sg6.rYDqcF4NPnBmFoSgeLXx.AD8eVx6aejWQWYDeC2GI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_4f4b19e5-54a1-4ba9-9fd0-7b2e2978a5e2_poly_0_.QzbjWgCLZM6KIJAGoHTD.4I3rQMzCSyVe0zX0BIkB.3OvfhzY08NGNTWOgDUtr">
<gml:posList srsDimension="3">
691029.837 5336015.249 527.279
691030.043 5336015.168 527.279
691026.482 5336016.564 527.279
691029.837 5336015.249 527.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly.3fSc3fPhFaCuTbylrd2F">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly_0_.StYfZryOuYpMv0xHEgpF">
<gml:posList srsDimension="3">
691014.985 5336086.903 540.53
691014.985 5336086.903 530.85
691015.17 5336086.831 540.53
691014.985 5336086.903 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly.G778rEJtuNATu5hqUKnM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly_0_.fRmWwoNuq2AnRxQeqRFY">
<gml:posList srsDimension="3">
691014.985 5336086.903 530.85
691012.608 5336087.832 530.85
691012.608 5336087.832 509.79
691014.985 5336086.903 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly.mBTsE7Al0krJdOi9N5Nq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly_0_.pMeVBB9x8FzlbCZ3UafY">
<gml:posList srsDimension="3">
691012.608 5336087.832 509.79
691014.985 5336086.903 509.79
691014.985 5336086.903 530.85
691012.608 5336087.832 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly.WNPS9jcFKrrY14xllQTy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly_0_.OepJYiWB6QtbTtP2Tybu">
<gml:posList srsDimension="3">
691014.985 5336086.903 530.85
691014.985 5336086.903 509.79
691015.17 5336086.831 540.53
691014.985 5336086.903 530.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_86706a24-48a3-44aa-846e-bc0d2687cbb5_poly_0_">
<gml:posList srsDimension="3">
691015.17 5336086.831 509.79
691015.17 5336086.831 540.53
691014.985 5336086.903 509.79
691015.17 5336086.831 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly.ULmcV2pttFRQ39Y7F39f">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly_0_.kOkgFFBSGHqJ2Sb4gf6L">
<gml:posList srsDimension="3">
690953.026 5335977.974 509.79
690969.619 5335971.456 530.92
690953.026 5335977.974 530.92
690953.026 5335977.974 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly.wVniSzVILzj3Auvxio7u">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly_0_.f7annX2odPgOrVGscKwm">
<gml:posList srsDimension="3">
690953.026 5335977.974 509.79
690957.156 5335976.351 509.79
690969.619 5335971.456 530.92
690953.026 5335977.974 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly.v9QKsqJbOVbnhP8qJzps">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly_0_.VmCxTUqomrGfC6ZaF1ee">
<gml:posList srsDimension="3">
690969.619 5335971.456 540.61
690969.619 5335971.456 530.92
690969.736 5335971.41 540.61
690969.619 5335971.456 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly_0_">
<gml:posList srsDimension="3">
690957.156 5335976.351 509.79
690969.736 5335971.41 509.79
690969.619 5335971.456 530.92
690957.156 5335976.351 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly.d8UzPxqJ33PJrYAVNO6t">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_6673f4f0-0384-4931-bf85-edc5fb6ef1e4_poly_0_.YVg9fFFPtWYPqcO0N5Mx">
<gml:posList srsDimension="3">
690969.736 5335971.41 540.61
690969.619 5335971.456 530.92
690969.736 5335971.41 509.79
690969.736 5335971.41 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly.8JkKc4ksTxQw9SPP7ugH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly_0_.ioxuS3ztD4N2Unv2zOUq">
<gml:posList srsDimension="3">
691019.82 5336089.321 540.53
690976.197 5335978.319 540.53
691018.282 5336085.409 540.61
691019.82 5336089.321 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly.D2lJ5pTmqaW0suhapATK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly_0_.OiMQusNd3iaYarChUQ6L">
<gml:posList srsDimension="3">
691019.82 5336089.321 540.61
691019.82 5336089.321 540.53
691018.282 5336085.409 540.61
691019.82 5336089.321 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly.IjQ2djBnLbWZ5koHGh9o">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly_0_.rfTEukbgytDLiaD9mBWO">
<gml:posList srsDimension="3">
691018.282 5336085.409 540.61
690976.197 5335978.319 540.53
691018.282 5336085.409 545.4
691018.282 5336085.409 540.61
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly_0_">
<gml:posList srsDimension="3">
690976.197 5335978.319 545.4
691018.282 5336085.409 545.4
690976.197 5335978.319 540.61
690976.197 5335978.319 545.4
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly.j4UzXKizuWJInCIozNFT">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_82c50f8a-7d6c-4459-9144-4c276e478584_poly_0_.hywF7lYxoVoObNeGEJHa">
<gml:posList srsDimension="3">
690976.197 5335978.319 540.53
690976.197 5335978.319 540.61
691018.282 5336085.409 545.4
690976.197 5335978.319 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly_0_">
<gml:posList srsDimension="3">
690987.828 5336066.368 509.79
690987.828 5336066.368 530.92
691004.373 5336059.891 509.79
690987.828 5336066.368 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly.a96QxjD9SzDJgvAJXeo1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly_0_.ANFdOHSv05BDmphMeM3O">
<gml:posList srsDimension="3">
691004.591 5336059.805 540.53
691004.591 5336059.805 509.79
691004.373 5336059.891 530.92
691004.591 5336059.805 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly.O79sNIuIzo3PPET91TZv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly_0_.PlA4IHEXmUgNDb3ewDZ3">
<gml:posList srsDimension="3">
691004.373 5336059.891 530.92
691004.591 5336059.805 509.79
690987.828 5336066.368 530.92
691004.373 5336059.891 530.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly.ot9y6EylPwxb5TlKzgcA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly_0_.0UDmQuB5BGaswPRO9OM1">
<gml:posList srsDimension="3">
691004.373 5336059.891 540.53
691004.591 5336059.805 540.53
691004.373 5336059.891 530.92
691004.373 5336059.891 540.53
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly.HMkse4xPS1w6j1ttf3Zv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_3d92221a-76d6-460a-8116-a810547bdcbe_poly_0_.0MiytH3ExHcK7W3ZMYLj">
<gml:posList srsDimension="3">
691004.591 5336059.805 509.79
691004.373 5336059.891 509.79
690987.828 5336066.368 530.92
691004.591 5336059.805 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847_poly_0_">
<gml:posList srsDimension="3">
691032.515 5336089.559 540.5
691034.1 5336093.587 540.5
691032.515 5336089.559 547.33
691032.515 5336089.559 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847_poly.dNgcZ2i81SSdWH6HWB1t">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847_poly_0_.t7YdXYsA0MKZwCGEeVNy">
<gml:posList srsDimension="3">
691034.1 5336093.588 547.33
691032.515 5336089.559 547.33
691034.1 5336093.587 540.5
691034.1 5336093.588 547.33
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847_poly.YPFVCTDPMFQb1dSIUp8g">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_a60919b9-0bf1-47bd-8eac-3e9d71db1847_poly_0_.YeFXsx0EgbT5Am1BR0df">
<gml:posList srsDimension="3">
691034.1 5336093.588 540.5
691034.1 5336093.588 547.33
691034.1 5336093.587 540.5
691034.1 5336093.588 540.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4959457_f08ca16a-1673-446e-84ee-846e0c68312f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4959457_f08ca16a-1673-446e-84ee-846e0c68312f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_f08ca16a-1673-446e-84ee-846e0c68312f_poly_0_">
<gml:posList srsDimension="3">
691051.62 5336029.95 509.79
691037.683 5335994.568 509.79
691009.528 5336005.573 509.79
691009.161 5336005.716 509.79
690998.599 5335978.991 509.79
690994.638 5335968.968 509.79
690992.122 5335962.625 509.79
690990.303 5335963.334 509.79
690990.242 5335963.358 509.79
690957.156 5335976.351 509.79
690953.026 5335977.974 509.79
690957.971 5335990.458 509.79
690974.527 5335983.95 509.79
690974.65 5335983.902 509.79
690985.323 5336010.872 509.79
690985.134 5336010.945 509.79
690968.595 5336017.425 509.79
690987.828 5336066.368 509.79
691004.373 5336059.891 509.79
691004.591 5336059.805 509.79
691015.17 5336086.831 509.79
691014.985 5336086.903 509.79
691012.608 5336087.832 509.79
690998.46 5336093.385 509.79
691003.217 5336105.592 509.79
691047.633 5336088.327 509.79
691045.178 5336082.144 509.79
691045.163 5336082.106 509.79
691042.711 5336075.913 509.79
691042.684 5336075.844 509.79
691038.008 5336077.685 509.79
691037.55 5336077.865 509.79
691025.549 5336047.411 509.79
691025.984 5336047.24 509.79
691054.069 5336036.165 509.79
691051.62 5336029.95 509.79
</gml:posList>
</gml:LinearRing>
</gml:exterior>
<gml:interior>
<gml:LinearRing gml:id="DEBY_LOD2_4959457_f08ca16a-1673-446e-84ee-846e0c68312f_poly_0_.SI4dEU1o6yUlE4K13PUC">
<gml:posList srsDimension="3">
691034.204 5336026.336 509.79
691023.314 5336030.639 509.79
691018.943 5336019.52 509.79
691030.043 5336015.168 509.79
691034.403 5336026.258 509.79
691034.204 5336026.336 509.79
</gml:posList>
</gml:LinearRing>
</gml:interior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>