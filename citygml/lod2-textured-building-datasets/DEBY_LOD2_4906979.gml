<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-25 22:08:47 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906979.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690997.246 5335958.473 515.54</gml:lowerCorner>
         <gml:upperCorner>691007.684 5335973.299 536.806</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906979">
<gml:name>DEBY_LOD2_4906979</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMIyx</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_496979/78und79.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906979_51e7cf17-2bcd-4d48-90bd-ff7e4af02d50_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906979_51e7cf17-2bcd-4d48-90bd-ff7e4af02d50_poly_0_">
0.9983390263273265 0.0006208291056510888 0.9984405614909931 0.9825538450621755 0.06292017248211831 0.9827048027034806 0.0018968406755157394 0.0006208291056510888 0.9983390263273265 0.0006208291056510888
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>2100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">21.266</bldg:measuredHeight>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906979_171c296b-3df4-4ef6-967a-c8ea1a4fe56a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_171c296b-3df4-4ef6-967a-c8ea1a4fe56a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_171c296b-3df4-4ef6-967a-c8ea1a4fe56a_poly_0_">
<gml:posList srsDimension="3">
691005.533 5335965.687 536.32
691007.684 5335971.159 533.864
691002.255 5335973.299 533.861
691000.097 5335967.822 536.32
691005.533 5335965.687 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc_poly_0_">
<gml:posList srsDimension="3">
690997.246 5335960.613 536.215
691002.694 5335958.473 536.806
690998.67 5335964.212 536.215
690997.246 5335960.613 536.215
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc_poly.JyZFpcVVY5JrPUDsDVle">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc_poly_0_.tsHZ4wk9637leXVc8gxx">
<gml:posList srsDimension="3">
691002.216 5335962.819 536.6
690998.67 5335964.212 536.215
691002.694 5335958.473 536.806
691002.216 5335962.819 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc_poly.gcEK9lcx7tX8hfwcDoBa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_c93a52be-fe6e-484b-b639-2411535fe6dc_poly_0_.4LKKIGHrOZ1anlrefsrB">
<gml:posList srsDimension="3">
691004.111 5335962.075 536.806
691002.216 5335962.819 536.6
691002.694 5335958.473 536.806
691004.111 5335962.075 536.806
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly_0_">
<gml:posList srsDimension="3">
690998.67 5335964.212 536.6
691002.216 5335962.819 536.6
690999.751 5335966.946 536.388
690998.67 5335964.212 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly.r1ciozrbtw7iDP5bXgFG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly_0_.bXaercJQ19Z85DCAPPr0">
<gml:posList srsDimension="3">
691000.097 5335967.822 536.32
690999.751 5335966.946 536.388
691002.216 5335962.819 536.6
691000.097 5335967.822 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly.V8sUxWgR3N7G4rvISWjz">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly_0_.I3Q1UcsBolqUUTIDOn8w">
<gml:posList srsDimension="3">
691005.533 5335965.687 536.32
691000.097 5335967.822 536.32
691002.216 5335962.819 536.6
691005.533 5335965.687 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly.GivmhEc6iIRUSfutrwuq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_f859c799-a51c-40ce-8ff7-eb239dcdac6a_poly_0_.NaJrY27RzyGoobJv2lcM">
<gml:posList srsDimension="3">
691004.111 5335962.075 536.6
691005.533 5335965.687 536.32
691002.216 5335962.819 536.6
691004.111 5335962.075 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_2d946bc1-9877-4ff4-8a73-aa92984eba93">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_2d946bc1-9877-4ff4-8a73-aa92984eba93_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_2d946bc1-9877-4ff4-8a73-aa92984eba93_poly_0_">
<gml:posList srsDimension="3">
691002.216 5335962.819 536.6
690998.67 5335964.212 536.6
690998.67 5335964.212 536.215
691002.216 5335962.819 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_7b3b3084-4e48-4252-a588-cf3e7e984fab">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_7b3b3084-4e48-4252-a588-cf3e7e984fab_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_7b3b3084-4e48-4252-a588-cf3e7e984fab_poly_0_">
<gml:posList srsDimension="3">
691002.216 5335962.819 536.6
691004.111 5335962.075 536.806
691004.111 5335962.075 536.6
691002.216 5335962.819 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_b6d8d5bb-40fd-437c-bb24-ef9b560a69ed">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_b6d8d5bb-40fd-437c-bb24-ef9b560a69ed_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_b6d8d5bb-40fd-437c-bb24-ef9b560a69ed_poly_0_">
<gml:posList srsDimension="3">
691002.694 5335958.473 515.54
691002.694 5335958.473 536.806
690997.246 5335960.613 536.215
690997.246 5335960.613 515.54
691002.694 5335958.473 515.54
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly_0_">
<gml:posList srsDimension="3">
691002.694 5335958.473 515.54
691007.684 5335971.159 515.54
691002.694 5335958.473 536.806
691002.694 5335958.473 515.54
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly.nv7lZlIynf6Zc6EmWoQM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly_0_.pbs3PErQFYOojj4LYEw7">
<gml:posList srsDimension="3">
691004.111 5335962.075 536.6
691002.694 5335958.473 536.806
691007.684 5335971.159 515.54
691004.111 5335962.075 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly.B84lQvDNczo1xzx09gBy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly_0_.W4zGgIaocE7c6RN6h43R">
<gml:posList srsDimension="3">
691005.533 5335965.687 536.32
691004.111 5335962.075 536.6
691007.684 5335971.159 515.54
691005.533 5335965.687 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly.X6K8K0rtfEMBP9q1iRUq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly_0_.aGOgMH8vjwlI3ozrmNdR">
<gml:posList srsDimension="3">
691007.684 5335971.159 533.864
691005.533 5335965.687 536.32
691007.684 5335971.159 515.54
691007.684 5335971.159 533.864
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly.WtiZc225qxldJbmUqEqw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_6012c8e4-2db2-4df3-bfa4-02b9ffa2a73b_poly_0_.W3sO0yk0Gy3BH4jCzkgd">
<gml:posList srsDimension="3">
691004.111 5335962.075 536.6
691004.111 5335962.075 536.806
691002.694 5335958.473 536.806
691004.111 5335962.075 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly_0_">
<gml:posList srsDimension="3">
690998.67 5335964.212 536.6
690999.751 5335966.946 536.388
690998.67 5335964.212 536.215
690998.67 5335964.212 536.6
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly.rHBHIfAjWsRQ34G1VpVZ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly_0_.Z7sBPf5Uc5Jcf8VQs0Jg">
<gml:posList srsDimension="3">
690997.246 5335960.613 536.215
690998.67 5335964.212 536.215
690997.246 5335960.613 515.54
690997.246 5335960.613 536.215
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly.WNJsowyYYNvQW1kER44h">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly_0_.JLZ52Pm0kN9SsFveXC96">
<gml:posList srsDimension="3">
690999.751 5335966.946 515.54
690997.246 5335960.613 515.54
690998.67 5335964.212 536.215
690999.751 5335966.946 515.54
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly.e5wjukShURGK8gnfy8r9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_559aec05-b80c-48d7-8be6-c86519e993be_poly_0_.Pm2RckS43JQklck7SPCm">
<gml:posList srsDimension="3">
690999.751 5335966.946 536.388
690999.751 5335966.946 515.54
690998.67 5335964.212 536.215
690999.751 5335966.946 536.388
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9_poly_0_">
<gml:posList srsDimension="3">
690999.751 5335966.946 536.388
691000.097 5335967.822 536.32
690999.751 5335966.946 515.54
690999.751 5335966.946 536.388
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9_poly.motS10bJgughWDwZn541">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9_poly_0_.OAGPQXxyb4b3XNIG0OAc">
<gml:posList srsDimension="3">
691002.255 5335973.299 515.54
690999.751 5335966.946 515.54
691000.097 5335967.822 536.32
691002.255 5335973.299 515.54
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9_poly.FOpQ8UAopbAjbE4wGvza">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_7adbf595-835e-47ce-aa0c-b7469116a7b9_poly_0_.F0wTxcP2CxTrVe2ypDQV">
<gml:posList srsDimension="3">
691002.255 5335973.299 533.861
691002.255 5335973.299 515.54
691000.097 5335967.822 536.32
691002.255 5335973.299 533.861
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906979_51e7cf17-2bcd-4d48-90bd-ff7e4af02d50">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_51e7cf17-2bcd-4d48-90bd-ff7e4af02d50_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_51e7cf17-2bcd-4d48-90bd-ff7e4af02d50_poly_0_">
<gml:posList srsDimension="3">
691002.255 5335973.299 515.54
691002.255 5335973.299 533.861
691007.684 5335971.159 533.864
691007.684 5335971.159 515.54
691002.255 5335973.299 515.54
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906979_d7521c4e-7778-4683-b8d2-5422f96f6992">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906979_d7521c4e-7778-4683-b8d2-5422f96f6992_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906979_d7521c4e-7778-4683-b8d2-5422f96f6992_poly_0_">
<gml:posList srsDimension="3">
691005.533 5335965.687 515.54
691002.694 5335958.473 515.54
690997.246 5335960.613 515.54
691002.255 5335973.299 515.54
691007.684 5335971.159 515.54
691005.533 5335965.687 515.54
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>