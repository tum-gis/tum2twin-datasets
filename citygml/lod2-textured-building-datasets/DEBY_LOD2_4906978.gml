<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-16 15:27:31 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906978.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690992.069 5335960.613 515.9</gml:lowerCorner>
         <gml:upperCorner>691002.202 5335975.311 537.905</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906978">
<gml:name>DEBY_LOD2_4906978</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMIxc</core:name> 
  </core:externalObject>
</core:externalReference>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>2100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">22.005</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly_0_">
<gml:posList srsDimension="3">
690993.507 5335966.251 536.7
690998.628 5335964.24 536.7
690994.585 5335968.968 536.408
690993.507 5335966.251 536.7
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly.sqewaqUn0bfyciCdu4Jw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly_0_.IVWEMTf2hyu8LVPnFvC2">
<gml:posList srsDimension="3">
690994.906 5335969.782 536.32
690994.585 5335968.968 536.408
690998.628 5335964.24 536.7
690994.906 5335969.782 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly.ZuBXjLpky0f1AgKLf1HN">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly_0_.nWiPzkZ2EITaBCJGaFU5">
<gml:posList srsDimension="3">
691000.024 5335967.772 536.32
690994.906 5335969.782 536.32
690998.628 5335964.24 536.7
691000.024 5335967.772 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly.helqUIyAQz9BRqXCdMda">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_34d4f3fc-6148-4db9-b8ba-e647cd6e414c_poly_0_.NMwsmlo7702lrOYOmD2A">
<gml:posList srsDimension="3">
690999.698 5335966.946 536.409
691000.024 5335967.772 536.32
690998.628 5335964.24 536.7
690999.698 5335966.946 536.409
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906978_a957260e-d99d-459f-9b4a-976f74a02859">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_a957260e-d99d-459f-9b4a-976f74a02859_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_a957260e-d99d-459f-9b4a-976f74a02859_poly_0_">
<gml:posList srsDimension="3">
690994.906 5335969.782 536.32
691000.024 5335967.772 536.32
690997.089 5335975.311 533.762
690994.906 5335969.782 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_a957260e-d99d-459f-9b4a-976f74a02859_poly.K1JAoEdLCG7tdRCtkHYs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_a957260e-d99d-459f-9b4a-976f74a02859_poly_0_.xp5KdHreU1Ii4UUFes9h">
<gml:posList srsDimension="3">
691002.202 5335973.299 533.763
690997.089 5335975.311 533.762
691000.024 5335967.772 536.32
691002.202 5335973.299 533.763
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906978_6a437893-2ce6-4368-be2e-0ce23071ab6d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_6a437893-2ce6-4368-be2e-0ce23071ab6d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_6a437893-2ce6-4368-be2e-0ce23071ab6d_poly_0_">
<gml:posList srsDimension="3">
690992.069 5335962.625 537.315
690997.193 5335960.613 537.904
690993.507 5335966.251 537.316
690992.069 5335962.625 537.315
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_6a437893-2ce6-4368-be2e-0ce23071ab6d_poly.HXy0ldjryurpBF396One">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_6a437893-2ce6-4368-be2e-0ce23071ab6d_poly_0_.w4h0X2gU1Dus1FlJxutG">
<gml:posList srsDimension="3">
690998.628 5335964.24 537.905
690993.507 5335966.251 537.316
690997.193 5335960.613 537.904
690998.628 5335964.24 537.905
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_703207fe-8c87-459a-adde-2936679bc47d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_703207fe-8c87-459a-adde-2936679bc47d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_703207fe-8c87-459a-adde-2936679bc47d_poly_0_">
<gml:posList srsDimension="3">
690997.193 5335960.613 515.9
690997.193 5335960.613 537.904
690992.069 5335962.625 537.315
690992.069 5335962.625 515.9
690997.193 5335960.613 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_c19dff78-452e-4ed7-8eaa-cf1f76c2afe4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_c19dff78-452e-4ed7-8eaa-cf1f76c2afe4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_c19dff78-452e-4ed7-8eaa-cf1f76c2afe4_poly_0_">
<gml:posList srsDimension="3">
690997.089 5335975.311 515.9
690997.089 5335975.311 533.762
691002.202 5335973.299 533.763
691002.202 5335973.299 515.9
690997.089 5335975.311 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_83fddd2b-c82d-4c1b-83bd-bed238c953fe">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_83fddd2b-c82d-4c1b-83bd-bed238c953fe_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_83fddd2b-c82d-4c1b-83bd-bed238c953fe_poly_0_">
<gml:posList srsDimension="3">
690993.507 5335966.251 536.7
690993.507 5335966.251 537.316
690998.628 5335964.24 537.905
690998.628 5335964.24 536.7
690993.507 5335966.251 536.7
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly_0_">
<gml:posList srsDimension="3">
690992.069 5335962.625 537.315
690993.507 5335966.251 537.316
690992.069 5335962.625 515.9
690992.069 5335962.625 537.315
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly.rbmCRbucaNCRjDEM3rYF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly_0_.fd9KYd2AM9lO2TXbEAwh">
<gml:posList srsDimension="3">
690993.507 5335966.251 536.7
690992.069 5335962.625 515.9
690993.507 5335966.251 537.316
690993.507 5335966.251 536.7
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly.bjAkPXZWlK2kFvUIhdxb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly_0_.uwB4xH1KeDyXMYJL9SUz">
<gml:posList srsDimension="3">
690994.585 5335968.968 515.9
690992.069 5335962.625 515.9
690993.507 5335966.251 536.7
690994.585 5335968.968 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly.Na02NuK0bjceiR3w9cUI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_39fbefc6-bad6-4d2e-bfae-1b8929d818ff_poly_0_.Qwf7BsSuQl6HN0Cozxjm">
<gml:posList srsDimension="3">
690994.585 5335968.968 536.408
690994.585 5335968.968 515.9
690993.507 5335966.251 536.7
690994.585 5335968.968 536.408
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0_poly_0_">
<gml:posList srsDimension="3">
690994.585 5335968.968 536.408
690994.906 5335969.782 536.32
690994.585 5335968.968 515.9
690994.585 5335968.968 536.408
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0_poly.GTflOuOeY7IHxfyQZlKM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0_poly_0_.zuuiatlWwb0wyEINjNg3">
<gml:posList srsDimension="3">
690997.089 5335975.311 515.9
690994.585 5335968.968 515.9
690994.906 5335969.782 536.32
690997.089 5335975.311 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0_poly.qe5uozeOcYAekdPiwsoh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_817caeb1-5e0c-4197-a9e8-f680b29513f0_poly_0_.Y9r1AJDzgamM4x3eLzLt">
<gml:posList srsDimension="3">
690997.089 5335975.311 533.762
690997.089 5335975.311 515.9
690994.906 5335969.782 536.32
690997.089 5335975.311 533.762
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly_0_">
<gml:posList srsDimension="3">
690999.698 5335966.946 515.9
691000.024 5335967.772 515.9
690999.698 5335966.946 536.409
690999.698 5335966.946 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly.EUa7GcItqavEWudrsSM9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly_0_.jER1JW5XG6ffVobLBTOf">
<gml:posList srsDimension="3">
691000.024 5335967.772 536.32
690999.698 5335966.946 536.409
691000.024 5335967.772 515.9
691000.024 5335967.772 536.32
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly.D3X7PjcXcLA9BqJFa5gY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly_0_.f7g7ahnec5rh8978Tuii">
<gml:posList srsDimension="3">
691002.202 5335973.299 515.9
691000.024 5335967.772 536.32
691000.024 5335967.772 515.9
691002.202 5335973.299 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly.IXtmRJxkXXBufPhbD64A">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_93b69e57-311b-49a2-b6a3-841aef988aa5_poly_0_.F0heF3GrY2hxYVpnR016">
<gml:posList srsDimension="3">
691002.202 5335973.299 533.763
691000.024 5335967.772 536.32
691002.202 5335973.299 515.9
691002.202 5335973.299 533.763
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly_0_">
<gml:posList srsDimension="3">
690997.193 5335960.613 515.9
690999.698 5335966.946 515.9
690997.193 5335960.613 537.904
690997.193 5335960.613 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly.X05LGzfskJqNYpwaGP5s">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly_0_.VQfIblH6vdckG6wCtSMO">
<gml:posList srsDimension="3">
690998.628 5335964.24 536.7
690997.193 5335960.613 537.904
690999.698 5335966.946 515.9
690998.628 5335964.24 536.7
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly.u36WQ7AP9k5nIT1yfC9Z">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly_0_.g6XNGaxeV5rbVcdNfyh9">
<gml:posList srsDimension="3">
690999.698 5335966.946 536.409
690998.628 5335964.24 536.7
690999.698 5335966.946 515.9
690999.698 5335966.946 536.409
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly.IPYp4SRQHI09ultSYt1n">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_b1efca85-de8b-41d7-b212-437565045487_poly_0_.g0HldBmTTW8vy1HZMwFx">
<gml:posList srsDimension="3">
690998.628 5335964.24 536.7
690998.628 5335964.24 537.905
690997.193 5335960.613 537.904
690998.628 5335964.24 536.7
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906978_e1e1a869-71ec-4b66-955f-effea130d5fa">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906978_e1e1a869-71ec-4b66-955f-effea130d5fa_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906978_e1e1a869-71ec-4b66-955f-effea130d5fa_poly_0_">
<gml:posList srsDimension="3">
691000.024 5335967.772 515.9
690999.698 5335966.946 515.9
690997.193 5335960.613 515.9
690992.069 5335962.625 515.9
690994.585 5335968.968 515.9
690997.089 5335975.311 515.9
691002.202 5335973.299 515.9
691000.024 5335967.772 515.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>