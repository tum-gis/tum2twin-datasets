<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-25 22:24:11 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906976.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690960.686 5335914.805 515.08</gml:lowerCorner>
         <gml:upperCorner>690989.3 5335934.247 535.11</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906976">
<gml:name>DEBY_LOD2_4906976</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJI4</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_b55e923d-dd62-4b08-80a5-e43889a12455_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_b55e923d-dd62-4b08-80a5-e43889a12455_poly_0_">
0.9924984739257388 0.0009369259798682014 0.992498473925739 0.9766178576427305 0.007259188906680648 0.9758752868265246 0.007259188906680647 0.0009369259798682016 0.9924984739257388 0.0009369259798682014
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236#3.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_878629b2-d79e-4712-86b8-a70b7825c63b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_878629b2-d79e-4712-86b8-a70b7825c63b_poly_0_">
0.9805594915777349 0.0006882848200379754 0.9805594915778056 0.9834376575130678 0.03267505887685705 0.9683520976556989 0.032675058876854735 0.0006882848200379763 0.9805594915777349 0.0006882848200379754
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_HjElfk6vCxfWyHvunBX3.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_7ffaa531-fe6b-4ab6-9865-a2dc6288a638_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_7ffaa531-fe6b-4ab6-9865-a2dc6288a638_poly_0_">
0.994225049073977 0.0009238875073070795 0.9942250490739879 0.9794576140538794 0.007052894652469354 0.9787743599680795 0.007052894652469277 0.0009238875073070796 0.994225049073977 0.0009238875073070795
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_84Obob8u88FisxPcHxFV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_73e1d888-e055-48db-8626-e13053a0a89e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_73e1d888-e055-48db-8626-e13053a0a89e_poly_0_">
0.9457999789498645 0.0009710195613210555 0.945799978949881 0.945304949575007 0.03768038457848344 0.9598849271167862 0.03768038457848277 0.0009710195613210554 0.9457999789498645 0.0009710195613210555
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_nYTbkPm1R5UdUsRVYW3n.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_497b5c50-d76a-471b-b8d4-6b513ecd591a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_497b5c50-d76a-471b-b8d4-6b513ecd591a_poly_0_">
0.9610952033511209 0.0010639384997071634 0.9610952033511138 0.9830305416226195 0.03892116481678086 0.9987039267516796 0.038921164816787965 0.0010639384997071634 0.9610952033511209 0.0010639384997071634
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_h8HiGb5e2qtaFgVHTixn.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_c5add274-59e4-431e-8dd9-fb373f14c838_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_c5add274-59e4-431e-8dd9-fb373f14c838_poly_0_">
0.9530287259246234 0.0010173570270655724 0.9530287259245968 0.9623747647074318 0.036509860769206565 0.9782494520146515 0.03650986076920759 0.0010173570270655705 0.9530287259246234 0.0010173570270655724
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_TwdHb0maaJYgT5Zsp4Dv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_a88b8d1e-7b5a-4f29-a12f-43d3c932907a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_a88b8d1e-7b5a-4f29-a12f-43d3c932907a_poly_0_">
0.9822870293186128 0.0009531284756396657 0.982287029318609 0.9990419345469316 0.016570440488059997 0.9985111930843236 0.016570440488063554 0.0009531284756396656 0.9822870293186128 0.0009531284756396657
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_mVqTA5ng4rijW75PEzI4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_9d93680f-3cde-40ea-a579-aba0b7cca362_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_9d93680f-3cde-40ea-a579-aba0b7cca362_poly_0_">
0.9688974222255275 0.0010290774601036938 0.9688974222255845 0.9791743912229586 0.04886020881088909 0.9644761894082436 0.04886020881088626 0.0010290774601036938 0.9688974222255275 0.0010290774601036938
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236#8.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_3f6833f8-cdef-43fb-9013-83e90964274f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_3f6833f8-cdef-43fb-9013-83e90964274f_poly_0_">
0.9957104124229917 0.0007642970418082793 0.9957104124229917 0.9721281597654652 0.005984474302735077 0.9713903863245089 0.005984474302735077 0.0007642970418082792 0.9957104124229917 0.0007642970418082793
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_KR1JgvZ6XHcwnzeveYmV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_a8ac1669-d19e-44d0-bfff-e4fd92baa475_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_a8ac1669-d19e-44d0-bfff-e4fd92baa475_poly_0_">
0.9903250413808706 0.000946769586807499 0.9903250413808738 0.976030854196632 0.010852049058243144 0.9753352764404007 0.010852049058243109 0.0009467695868074987 0.9903250413808706 0.000946769586807499
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_PLCiAGkutr7NofQhVTgV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_fc4fa0ae-3d07-45f8-9023-fd63c117fb51_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_fc4fa0ae-3d07-45f8-9023-fd63c117fb51_poly_0_">
0.9593947959852404 0.001039254699106339 0.9593947959852372 0.9844586585043621 0.04959251853605891 0.9699873189563956 0.04959251853605906 0.0010392546991063387 0.9593947959852404 0.001039254699106339
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_l7OuMAYBFfAlIwEfhwx8.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_c60834bb-4956-4bd9-85c1-df31944d857e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_c60834bb-4956-4bd9-85c1-df31944d857e_poly_0_">
0.9621833569225373 0.0010182932266043024 0.9621833569226173 0.9824927657004 0.047922271327327 0.9677840981730931 0.04792227132732308 0.0010182932266043026 0.9621833569225373 0.0010182932266043024
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_LDCJMLjm0ravDF4IhHZ2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_a5219231-e7f2-464f-b440-dbbd553cc43b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_a5219231-e7f2-464f-b440-dbbd553cc43b_poly_0_">
0.9598176282879046 0.0010609742271219472 0.9598176282879007 0.9986913815144952 0.04897342577878659 0.983762360047577 0.04897342577879016 0.0010609742271219472 0.9598176282879046 0.0010609742271219472
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_FfjDYf8xFMFiKdVy89s2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_c74e7ce8-2d37-48fc-8c4a-0a13a87e12d6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_c74e7ce8-2d37-48fc-8c4a-0a13a87e12d6_poly_0_">
0.9895964547147307 0.0009574618488633718 0.9895964547147311 0.9790093727076125 0.013045486673832185 0.9781946204315197 0.01304548667383218 0.0009574618488633722 0.9895964547147307 0.0009574618488633718
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236#7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_2d0bb4bd-4e8a-4ca6-98c5-c78c9e61ae9e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_2d0bb4bd-4e8a-4ca6-98c5-c78c9e61ae9e_poly_0_">
0.9943665679314563 0.0007455125458084347 0.9943665679314563 0.9835681619979043 0.008496261003210217 0.9832181123780114 0.008496261003210217 0.0007455125458084346 0.9943665679314563 0.0007455125458084347
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_tvGT84r1jwhoRM1QoDy7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_f70a0167-5b61-456e-9198-394d2b4babec_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_f70a0167-5b61-456e-9198-394d2b4babec_poly_0_">
0.9946424581991188 0.0009550831573804019 0.9946424581991188 0.9800639342755015 0.004838020141936904 0.9787811570765597 0.004838020141936904 0.0009550831573804 0.9946424581991188 0.0009550831573804019
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_TOjvZD0rnlRZGLshnm67.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_158cb9a2-a1a6-44e1-a7d8-ff2cd6ba9b14_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_158cb9a2-a1a6-44e1-a7d8-ff2cd6ba9b14_poly_0_">
0.9929511058659835 0.0009270225312414 0.9929511058660053 0.9799598190291684 0.0070764589713060875 0.9784777315351985 0.007076458971305932 0.0009270225312414006 0.9929511058659835 0.0009270225312414
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_ag32PskvahH2mkGxjoKZ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_58282cb0-2d95-4b77-98a7-512114a15c8b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_58282cb0-2d95-4b77-98a7-512114a15c8b_poly_0_">
0.9920857754580621 0.0009309540487418494 0.9920857754580646 0.980264601534399 0.007159648785508583 0.9792363097736373 0.0071596487855085655 0.0009309540487418499 0.9920857754580621 0.0009309540487418494
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_WaTnGLKlXQaSPFt51iKv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_ab42eb9e-e45d-477d-8a9f-681ededbe53a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_ab42eb9e-e45d-477d-8a9f-681ededbe53a_poly_0_">
0.9915136932478978 0.0009384840636788438 0.9915136932478978 0.9813178547825778 0.010935757010196669 0.980100856095748 0.010935757010196669 0.0009384840636788436 0.9915136932478978 0.0009384840636788438
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_QX6lWcC4mSsD1Nhm5zLR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_30f617b4-9dfa-4798-9299-a41b814a72df_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_30f617b4-9dfa-4798-9299-a41b814a72df_poly_0_">
0.9880783850092429 0.0009488466445748106 0.98807838500923 0.9852769806345117 0.01078212819585808 0.9845769309926058 0.01078212819585822 0.0009488466445748106 0.9880783850092429 0.0009488466445748106
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_S0BdlYX7LqUy8QOCbGQD.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_d2f815f6-1f1c-43cb-91a0-9f65a834d4ab_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_d2f815f6-1f1c-43cb-91a0-9f65a834d4ab_poly_0_">
0.9577867910566985 0.0009994543052163852 0.9577867910567258 0.9514241183939242 0.03736158230361151 0.9667462530737599 0.037361582303610426 0.0009994543052163848 0.9577867910566985 0.0009994543052163852
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_8tYKK6MCE4ht8nHFVdxF.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_3b83ca05-05e1-4560-b9e6-03008f537459_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_3b83ca05-05e1-4560-b9e6-03008f537459_poly_0_">
0.9931435861504688 0.0009371629335535634 0.9931435861504688 0.9801043380027077 0.0071980935188085135 0.979073635713161 0.0071980935188085135 0.0009371629335535632 0.9931435861504688 0.0009371629335535634
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236#9.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_080b147f-3948-4b74-87c2-1832dca4169c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_080b147f-3948-4b74-87c2-1832dca4169c_poly_0_">
0.9724846297985357 0.0006810686301916729 0.9724846297985718 0.9674230959514722 0.03297612887096455 0.9527086054290783 0.03297612887096335 0.0006810686301916735 0.9724846297985357 0.0006810686301916729
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_dsQRyiNie6BnH7k3Z6m5.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_d131bf6b-1dd6-40e0-a7c8-93cf1c4c7ec3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_d131bf6b-1dd6-40e0-a7c8-93cf1c4c7ec3_poly_0_">
0.9539114936993345 0.0010480373775220797 0.9539114936993228 0.9714767662481821 0.04055756233637758 0.9861531985986951 0.040557562336378084 0.0010480373775220788 0.9539114936993345 0.0010480373775220797
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_idLSD2XdDehNUdgGuXIR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_6127ceed-0101-49d2-94dc-4bbd02cfc0f6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_6127ceed-0101-49d2-94dc-4bbd02cfc0f6_poly_0_">
0.9533852114351296 0.0010800460213541943 0.9533852114351296 0.9786370894154208 0.039892367203105294 0.9939856269494554 0.039892367203105294 0.0010800460213541943 0.9533852114351296 0.0010800460213541943
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906976/IMG_6236_hhsmoyP2YUMWbCEUgjZi.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906976_270329c7-6993-4c2b-9ae8-0de82f858601_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906976_270329c7-6993-4c2b-9ae8-0de82f858601_poly_0_">
0.9867609359466967 0.0009449750453725377 0.9867609359466967 0.9845268062839928 0.010844877698215074 0.9837116188794615 0.010844877698215074 0.0009449750453725376 0.9867609359466967 0.0009449750453725377
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">20.03</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly_0_">
<gml:posList srsDimension="3">
690962.652 5335929.259 535.11
690987.371 5335919.543 535.11
690963.482 5335931.361 533.731
690962.652 5335929.259 535.11
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly.1rZbhcMdKgIzwTnrWA7S">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly_0_.zrH71stZLNd6AZNga6hU">
<gml:posList srsDimension="3">
690964.351 5335934.247 531.9
690963.482 5335931.361 533.731
690987.371 5335919.543 535.11
690964.351 5335934.247 531.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly.YXPfTWacHzcJCKf51aG9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly_0_.vVTw7VjiYDIkclU1qCMh">
<gml:posList srsDimension="3">
690967.604 5335932.953 531.909
690964.351 5335934.247 531.9
690987.371 5335919.543 535.11
690967.604 5335932.953 531.909
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly.TfOXLvWf1dDqdSShDSM5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly_0_.iLnnYAjciVeinq4kgvIv">
<gml:posList srsDimension="3">
690976.81 5335929.349 531.901
690967.604 5335932.953 531.909
690987.371 5335919.543 535.11
690976.81 5335929.349 531.901
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly.XG1qVHXxH3N0WFYbbchK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly_0_.kNk5IPb38xBZnN9m23EN">
<gml:posList srsDimension="3">
690989.3 5335924.442 531.9
690976.81 5335929.349 531.901
690987.371 5335919.543 535.11
690989.3 5335924.442 531.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly.RAZOw9vTwsLWBUuIGQly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_1474c962-2558-4be3-b67d-890e5272d4b4_poly_0_.WOBB9j7TzbrhHifspTxK">
<gml:posList srsDimension="3">
690963.482 5335931.361 533.731
690964.351 5335934.247 531.9
690963.249 5335931.452 533.732
690963.482 5335931.361 533.731
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_">
<gml:posList srsDimension="3">
690967.107 5335921.834 531.89
690968.499 5335921.308 531.902
690967.261 5335922.25 532.161
690967.107 5335921.834 531.89
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.vzr67LmDsoDUh4LCGJMQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.Tr0sf7k0wt9VS5LzjE4S">
<gml:posList srsDimension="3">
690970.611 5335920.51 531.92
690972.003 5335919.973 531.926
690970.766 5335920.906 532.18
690970.611 5335920.51 531.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.XowJqN29DID46dIxqVWo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.saBxtmnQ9bFoOIlACv9q">
<gml:posList srsDimension="3">
690974.126 5335919.165 531.941
690975.518 5335918.639 531.953
690974.261 5335919.561 532.196
690974.126 5335919.165 531.941
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.bs5XXhnFF4iWTICmNMtr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.cUWouOuahHwD6NAYg3bV">
<gml:posList srsDimension="3">
690977.621 5335917.82 531.958
690979.003 5335917.304 531.972
690977.776 5335918.227 532.223
690977.621 5335917.82 531.958
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.3zqUp54BDM090HVgB45C">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.NkBT0EJV834FB6eaZ3fr">
<gml:posList srsDimension="3">
690981.127 5335916.476 531.976
690982.507 5335915.969 531.997
690981.29 5335916.903 532.255
690981.127 5335916.476 531.976
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.0tbDtS6nBS8uWifaCWzp">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.9hQ2jrMXaLExGprl70l6">
<gml:posList srsDimension="3">
690984.612 5335915.141 531.996
690985.505 5335914.805 532.005
690984.786 5335915.548 532.266
690984.612 5335915.141 531.996
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.Ci4wA293UFDHTzDwa8j5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.gkgmkQe7WTR27afbbYRU">
<gml:posList srsDimension="3">
690960.686 5335924.286 531.85
690963.836 5335923.088 531.872
690962.652 5335929.259 535.11
690960.686 5335924.286 531.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.qoAOTpch1hTe1wNdhIgd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.lX2nOyPXHYqsLeiJv9WX">
<gml:posList srsDimension="3">
690965.168 5335923.05 532.148
690962.652 5335929.259 535.11
690963.836 5335923.088 531.872
690965.168 5335923.05 532.148
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.jj1KgrmEyG313KdBruAF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.D1DAmtRwoir3MYbSebQj">
<gml:posList srsDimension="3">
690987.371 5335919.543 535.11
690962.652 5335929.259 535.11
690965.168 5335923.05 532.148
690987.371 5335919.543 535.11
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.rxdZj7cKla1ZvwGBzvEG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.l7ETrzu2B4099PHvy8Oe">
<gml:posList srsDimension="3">
690965.004 5335922.653 531.886
690965.168 5335923.05 532.148
690963.836 5335923.088 531.872
690965.004 5335922.653 531.886
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.AENhRWPopGFcunAoQ7Do">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.nkIu03J7FTdJSAjF3ptV">
<gml:posList srsDimension="3">
690967.261 5335922.25 532.161
690987.371 5335919.543 535.11
690965.168 5335923.05 532.148
690967.261 5335922.25 532.161
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.Fxr4W0paxKm1kBNSCgYP">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.owSWr4QpHxuRr4RHII7E">
<gml:posList srsDimension="3">
690968.653 5335921.704 532.162
690987.371 5335919.543 535.11
690967.261 5335922.25 532.161
690968.653 5335921.704 532.162
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.xyWhdpYzke6sXY8vNKf0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.DbdRfnokDUNmC6aybGIj">
<gml:posList srsDimension="3">
690968.499 5335921.308 531.902
690968.653 5335921.704 532.162
690967.261 5335922.25 532.161
690968.499 5335921.308 531.902
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.xEGWJS3Cl4Tn58MxwGag">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.qpTAloI0dElIuMI5Fekk">
<gml:posList srsDimension="3">
690970.766 5335920.906 532.18
690987.371 5335919.543 535.11
690968.653 5335921.704 532.162
690970.766 5335920.906 532.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.lyPf9DopMp7x9Aem3pXE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.ZV969qealB6MVyfIE8BZ">
<gml:posList srsDimension="3">
690972.168 5335920.36 532.183
690987.371 5335919.543 535.11
690970.766 5335920.906 532.18
690972.168 5335920.36 532.183
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.ZELAhtYS3EYZZqlAHMBU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.AryOqmIj9lRveHxQplx4">
<gml:posList srsDimension="3">
690972.003 5335919.973 531.926
690972.168 5335920.36 532.183
690970.766 5335920.906 532.18
690972.003 5335919.973 531.926
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.SW0Byt1gIy2cMgjpjxVl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.RT5YOpMemsQTMbaS8H4B">
<gml:posList srsDimension="3">
690974.261 5335919.561 532.196
690987.371 5335919.543 535.11
690972.168 5335920.36 532.183
690974.261 5335919.561 532.196
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.Rm2suXh40VD2HDnkWWGX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.qq6R1zG12sghC5CFD21e">
<gml:posList srsDimension="3">
690975.663 5335919.025 532.205
690987.371 5335919.543 535.11
690974.261 5335919.561 532.196
690975.663 5335919.025 532.205
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.dy2i91rbRaOEgHt6uiA4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.Csda5zH9jAp70oytxmqJ">
<gml:posList srsDimension="3">
690975.518 5335918.639 531.953
690975.663 5335919.025 532.205
690974.261 5335919.561 532.196
690975.518 5335918.639 531.953
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.QWGa3XAPNrUma71sQGpV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.kEfUEILooeRSSMYLE4Hj">
<gml:posList srsDimension="3">
690977.776 5335918.227 532.223
690987.371 5335919.543 535.11
690975.663 5335919.025 532.205
690977.776 5335918.227 532.223
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.UXmnw07umVZ8MosDHrr6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.srzeXV9mS8Q9fp5zGNpJ">
<gml:posList srsDimension="3">
690979.147 5335917.7 532.229
690987.371 5335919.543 535.11
690977.776 5335918.227 532.223
690979.147 5335917.7 532.229
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.bV14aipA1N6MBhvTOzGh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.vrPc6L4QrPYkns1R7hmi">
<gml:posList srsDimension="3">
690979.003 5335917.304 531.972
690979.147 5335917.7 532.229
690977.776 5335918.227 532.223
690979.003 5335917.304 531.972
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.M2fM3fhCAsA8MKWF7ytb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.MJLZEdOmmGYTiTmjt17h">
<gml:posList srsDimension="3">
690981.29 5335916.903 532.255
690987.371 5335919.543 535.11
690979.147 5335917.7 532.229
690981.29 5335916.903 532.255
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.vmlVNvuIy9Ul7xzfSyek">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.93EpQrkN9TlfFooKrm77">
<gml:posList srsDimension="3">
690982.652 5335916.365 532.254
690987.371 5335919.543 535.11
690981.29 5335916.903 532.255
690982.652 5335916.365 532.254
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.zeKsP8gSxW9VcByRJQJO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.lWtkXjk1opPJ0zhzKvAc">
<gml:posList srsDimension="3">
690982.507 5335915.969 531.997
690982.652 5335916.365 532.254
690981.29 5335916.903 532.255
690982.507 5335915.969 531.997
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.QR0zmoj6Vj8oPgTG2PkV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.GoyCLiPnx5SGmpOejn4O">
<gml:posList srsDimension="3">
690984.786 5335915.548 532.266
690987.371 5335919.543 535.11
690982.652 5335916.365 532.254
690984.786 5335915.548 532.266
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly.sN3EePuXAvIMR2CNhLZl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9454cf0b-c130-45a2-8f9d-d552e4a376c4_poly_0_.2BTDBUZ5Srgx1j51bbZh">
<gml:posList srsDimension="3">
690985.505 5335914.805 532.005
690987.371 5335919.543 535.11
690984.786 5335915.548 532.266
690985.505 5335914.805 532.005
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_b55e923d-dd62-4b08-80a5-e43889a12455">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_b55e923d-dd62-4b08-80a5-e43889a12455_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_b55e923d-dd62-4b08-80a5-e43889a12455_poly_0_">
<gml:posList srsDimension="3">
690967.261 5335922.25 515.08
690967.261 5335922.25 532.161
690965.168 5335923.05 532.148
690965.168 5335923.05 515.08
690967.261 5335922.25 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_878629b2-d79e-4712-86b8-a70b7825c63b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_878629b2-d79e-4712-86b8-a70b7825c63b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_878629b2-d79e-4712-86b8-a70b7825c63b_poly_0_">
<gml:posList srsDimension="3">
690965.168 5335923.05 515.08
690965.168 5335923.05 532.148
690965.004 5335922.653 531.886
690965.004 5335922.653 515.08
690965.168 5335923.05 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_0d995529-6f48-410d-9449-4e3d36930851">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_0d995529-6f48-410d-9449-4e3d36930851_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_0d995529-6f48-410d-9449-4e3d36930851_poly_0_">
<gml:posList srsDimension="3">
690964.351 5335934.247 515.08
690964.351 5335934.247 531.9
690967.604 5335932.953 531.909
690967.604 5335932.953 515.08
690964.351 5335934.247 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_7ffaa531-fe6b-4ab6-9865-a2dc6288a638">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_7ffaa531-fe6b-4ab6-9865-a2dc6288a638_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_7ffaa531-fe6b-4ab6-9865-a2dc6288a638_poly_0_">
<gml:posList srsDimension="3">
690984.786 5335915.548 515.08
690984.786 5335915.548 532.266
690982.652 5335916.365 532.254
690982.652 5335916.365 515.08
690984.786 5335915.548 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_73e1d888-e055-48db-8626-e13053a0a89e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_73e1d888-e055-48db-8626-e13053a0a89e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_73e1d888-e055-48db-8626-e13053a0a89e_poly_0_">
<gml:posList srsDimension="3">
690970.611 5335920.51 515.08
690970.611 5335920.51 531.92
690970.766 5335920.906 532.18
690970.766 5335920.906 515.08
690970.611 5335920.51 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_497b5c50-d76a-471b-b8d4-6b513ecd591a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_497b5c50-d76a-471b-b8d4-6b513ecd591a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_497b5c50-d76a-471b-b8d4-6b513ecd591a_poly_0_">
<gml:posList srsDimension="3">
690984.612 5335915.141 515.08
690984.612 5335915.141 531.996
690984.786 5335915.548 532.266
690984.786 5335915.548 515.08
690984.612 5335915.141 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_4885cd92-b55d-410d-8154-4f97ff0e161c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_4885cd92-b55d-410d-8154-4f97ff0e161c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_4885cd92-b55d-410d-8154-4f97ff0e161c_poly_0_">
<gml:posList srsDimension="3">
690967.604 5335932.953 515.08
690967.604 5335932.953 531.909
690976.81 5335929.349 531.901
690976.81 5335929.349 515.08
690967.604 5335932.953 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_c5add274-59e4-431e-8dd9-fb373f14c838">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_c5add274-59e4-431e-8dd9-fb373f14c838_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_c5add274-59e4-431e-8dd9-fb373f14c838_poly_0_">
<gml:posList srsDimension="3">
690981.127 5335916.476 515.08
690981.127 5335916.476 531.976
690981.29 5335916.903 532.255
690981.29 5335916.903 515.08
690981.127 5335916.476 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_8921979d-cb29-46a0-9933-25c0567c4b15">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_8921979d-cb29-46a0-9933-25c0567c4b15_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_8921979d-cb29-46a0-9933-25c0567c4b15_poly_0_">
<gml:posList srsDimension="3">
690976.81 5335929.349 515.08
690976.81 5335929.349 531.901
690989.3 5335924.442 531.9
690989.3 5335924.442 515.08
690976.81 5335929.349 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_a88b8d1e-7b5a-4f29-a12f-43d3c932907a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_a88b8d1e-7b5a-4f29-a12f-43d3c932907a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_a88b8d1e-7b5a-4f29-a12f-43d3c932907a_poly_0_">
<gml:posList srsDimension="3">
690985.505 5335914.805 515.08
690985.505 5335914.805 532.005
690984.612 5335915.141 531.996
690984.612 5335915.141 515.08
690985.505 5335914.805 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_9d93680f-3cde-40ea-a579-aba0b7cca362">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_9d93680f-3cde-40ea-a579-aba0b7cca362_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_9d93680f-3cde-40ea-a579-aba0b7cca362_poly_0_">
<gml:posList srsDimension="3">
690972.168 5335920.36 515.08
690972.168 5335920.36 532.183
690972.003 5335919.973 531.926
690972.003 5335919.973 515.08
690972.168 5335920.36 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_45fca66d-8475-4638-81f7-055634091ea0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_45fca66d-8475-4638-81f7-055634091ea0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_45fca66d-8475-4638-81f7-055634091ea0_poly_0_">
<gml:posList srsDimension="3">
690963.482 5335931.361 515.08
690963.482 5335931.361 533.731
690963.249 5335931.452 533.732
690963.249 5335931.452 515.08
690963.482 5335931.361 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_3f6833f8-cdef-43fb-9013-83e90964274f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_3f6833f8-cdef-43fb-9013-83e90964274f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_3f6833f8-cdef-43fb-9013-83e90964274f_poly_0_">
<gml:posList srsDimension="3">
690974.261 5335919.561 515.08
690974.261 5335919.561 532.196
690972.168 5335920.36 532.183
690972.168 5335920.36 515.08
690974.261 5335919.561 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_a8ac1669-d19e-44d0-bfff-e4fd92baa475">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_a8ac1669-d19e-44d0-bfff-e4fd92baa475_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_a8ac1669-d19e-44d0-bfff-e4fd92baa475_poly_0_">
<gml:posList srsDimension="3">
690968.499 5335921.308 515.08
690968.499 5335921.308 531.902
690967.107 5335921.834 531.89
690967.107 5335921.834 515.08
690968.499 5335921.308 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_fc4fa0ae-3d07-45f8-9023-fd63c117fb51">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_fc4fa0ae-3d07-45f8-9023-fd63c117fb51_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_fc4fa0ae-3d07-45f8-9023-fd63c117fb51_poly_0_">
<gml:posList srsDimension="3">
690975.663 5335919.025 515.08
690975.663 5335919.025 532.205
690975.518 5335918.639 531.953
690975.518 5335918.639 515.08
690975.663 5335919.025 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_c60834bb-4956-4bd9-85c1-df31944d857e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_c60834bb-4956-4bd9-85c1-df31944d857e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_c60834bb-4956-4bd9-85c1-df31944d857e_poly_0_">
<gml:posList srsDimension="3">
690979.147 5335917.7 515.08
690979.147 5335917.7 532.229
690979.003 5335917.304 531.972
690979.003 5335917.304 515.08
690979.147 5335917.7 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_a5219231-e7f2-464f-b440-dbbd553cc43b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_a5219231-e7f2-464f-b440-dbbd553cc43b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_a5219231-e7f2-464f-b440-dbbd553cc43b_poly_0_">
<gml:posList srsDimension="3">
690982.652 5335916.365 515.08
690982.652 5335916.365 532.254
690982.507 5335915.969 531.997
690982.507 5335915.969 515.08
690982.652 5335916.365 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_33868a75-d944-40b2-be6d-8ebe0c9b2ed5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_33868a75-d944-40b2-be6d-8ebe0c9b2ed5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_33868a75-d944-40b2-be6d-8ebe0c9b2ed5_poly_0_">
<gml:posList srsDimension="3">
690963.249 5335931.452 515.08
690963.249 5335931.452 533.732
690964.351 5335934.247 531.9
690964.351 5335934.247 515.08
690963.249 5335931.452 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_c74e7ce8-2d37-48fc-8c4a-0a13a87e12d6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_c74e7ce8-2d37-48fc-8c4a-0a13a87e12d6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_c74e7ce8-2d37-48fc-8c4a-0a13a87e12d6_poly_0_">
<gml:posList srsDimension="3">
690965.004 5335922.653 515.08
690965.004 5335922.653 531.886
690963.836 5335923.088 531.872
690963.836 5335923.088 515.08
690965.004 5335922.653 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_2d0bb4bd-4e8a-4ca6-98c5-c78c9e61ae9e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_2d0bb4bd-4e8a-4ca6-98c5-c78c9e61ae9e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_2d0bb4bd-4e8a-4ca6-98c5-c78c9e61ae9e_poly_0_">
<gml:posList srsDimension="3">
690972.003 5335919.973 515.08
690972.003 5335919.973 531.926
690970.611 5335920.51 531.92
690970.611 5335920.51 515.08
690972.003 5335919.973 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_f70a0167-5b61-456e-9198-394d2b4babec">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_f70a0167-5b61-456e-9198-394d2b4babec_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_f70a0167-5b61-456e-9198-394d2b4babec_poly_0_">
<gml:posList srsDimension="3">
690963.836 5335923.088 515.08
690963.836 5335923.088 531.872
690960.686 5335924.286 531.85
690960.686 5335924.286 515.08
690963.836 5335923.088 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_158cb9a2-a1a6-44e1-a7d8-ff2cd6ba9b14">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_158cb9a2-a1a6-44e1-a7d8-ff2cd6ba9b14_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_158cb9a2-a1a6-44e1-a7d8-ff2cd6ba9b14_poly_0_">
<gml:posList srsDimension="3">
690981.29 5335916.903 515.08
690981.29 5335916.903 532.255
690979.147 5335917.7 532.229
690979.147 5335917.7 515.08
690981.29 5335916.903 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_58282cb0-2d95-4b77-98a7-512114a15c8b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_58282cb0-2d95-4b77-98a7-512114a15c8b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_58282cb0-2d95-4b77-98a7-512114a15c8b_poly_0_">
<gml:posList srsDimension="3">
690977.776 5335918.227 515.08
690977.776 5335918.227 532.223
690975.663 5335919.025 532.205
690975.663 5335919.025 515.08
690977.776 5335918.227 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_ab42eb9e-e45d-477d-8a9f-681ededbe53a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_ab42eb9e-e45d-477d-8a9f-681ededbe53a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_ab42eb9e-e45d-477d-8a9f-681ededbe53a_poly_0_">
<gml:posList srsDimension="3">
690982.507 5335915.969 515.08
690982.507 5335915.969 531.997
690981.127 5335916.476 531.976
690981.127 5335916.476 515.08
690982.507 5335915.969 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_30f617b4-9dfa-4798-9299-a41b814a72df">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_30f617b4-9dfa-4798-9299-a41b814a72df_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_30f617b4-9dfa-4798-9299-a41b814a72df_poly_0_">
<gml:posList srsDimension="3">
690975.518 5335918.639 515.08
690975.518 5335918.639 531.953
690974.126 5335919.165 531.941
690974.126 5335919.165 515.08
690975.518 5335918.639 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_d2f815f6-1f1c-43cb-91a0-9f65a834d4ab">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_d2f815f6-1f1c-43cb-91a0-9f65a834d4ab_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_d2f815f6-1f1c-43cb-91a0-9f65a834d4ab_poly_0_">
<gml:posList srsDimension="3">
690967.107 5335921.834 515.08
690967.107 5335921.834 531.89
690967.261 5335922.25 532.161
690967.261 5335922.25 515.08
690967.107 5335921.834 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_3b83ca05-05e1-4560-b9e6-03008f537459">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_3b83ca05-05e1-4560-b9e6-03008f537459_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_3b83ca05-05e1-4560-b9e6-03008f537459_poly_0_">
<gml:posList srsDimension="3">
690970.766 5335920.906 515.08
690970.766 5335920.906 532.18
690968.653 5335921.704 532.162
690968.653 5335921.704 515.08
690970.766 5335920.906 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_080b147f-3948-4b74-87c2-1832dca4169c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_080b147f-3948-4b74-87c2-1832dca4169c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_080b147f-3948-4b74-87c2-1832dca4169c_poly_0_">
<gml:posList srsDimension="3">
690968.653 5335921.704 515.08
690968.653 5335921.704 532.162
690968.499 5335921.308 531.902
690968.499 5335921.308 515.08
690968.653 5335921.704 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_d131bf6b-1dd6-40e0-a7c8-93cf1c4c7ec3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_d131bf6b-1dd6-40e0-a7c8-93cf1c4c7ec3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_d131bf6b-1dd6-40e0-a7c8-93cf1c4c7ec3_poly_0_">
<gml:posList srsDimension="3">
690974.126 5335919.165 515.08
690974.126 5335919.165 531.941
690974.261 5335919.561 532.196
690974.261 5335919.561 515.08
690974.126 5335919.165 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_6127ceed-0101-49d2-94dc-4bbd02cfc0f6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_6127ceed-0101-49d2-94dc-4bbd02cfc0f6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_6127ceed-0101-49d2-94dc-4bbd02cfc0f6_poly_0_">
<gml:posList srsDimension="3">
690977.621 5335917.82 515.08
690977.621 5335917.82 531.958
690977.776 5335918.227 532.223
690977.776 5335918.227 515.08
690977.621 5335917.82 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_270329c7-6993-4c2b-9ae8-0de82f858601">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_270329c7-6993-4c2b-9ae8-0de82f858601_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_270329c7-6993-4c2b-9ae8-0de82f858601_poly_0_">
<gml:posList srsDimension="3">
690979.003 5335917.304 515.08
690979.003 5335917.304 531.972
690977.621 5335917.82 531.958
690977.621 5335917.82 515.08
690979.003 5335917.304 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly_0_">
<gml:posList srsDimension="3">
690985.505 5335914.805 515.08
690987.371 5335919.543 515.08
690985.505 5335914.805 532.005
690985.505 5335914.805 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly.Ktr6hGk5YDvQh9ijMh4k">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly_0_.LLD3yU2kZL4paViHhQCy">
<gml:posList srsDimension="3">
690987.371 5335919.543 535.11
690985.505 5335914.805 532.005
690987.371 5335919.543 515.08
690987.371 5335919.543 535.11
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly.ViNYlW40Xd9fP0bpgKMD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly_0_.Eozf5hOKGSyV7aJIW3Js">
<gml:posList srsDimension="3">
690989.3 5335924.442 515.08
690987.371 5335919.543 535.11
690987.371 5335919.543 515.08
690989.3 5335924.442 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly.U7342317tzy4dvpnzIYg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_e727822e-36a9-4b35-a58a-3b1784cbfcda_poly_0_.cT9X0zw0EO8pp05FtAXb">
<gml:posList srsDimension="3">
690989.3 5335924.442 531.9
690987.371 5335919.543 535.11
690989.3 5335924.442 515.08
690989.3 5335924.442 531.9
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859_poly_0_">
<gml:posList srsDimension="3">
690960.686 5335924.286 531.85
690962.652 5335929.259 535.11
690960.686 5335924.286 515.08
690960.686 5335924.286 531.85
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859_poly.jJDhsDwvC4ImhuC4tlmR">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859_poly_0_.bzUUcxAh23iY3Fo2d0Pi">
<gml:posList srsDimension="3">
690963.482 5335931.361 515.08
690960.686 5335924.286 515.08
690962.652 5335929.259 535.11
690963.482 5335931.361 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859_poly.yzL8Vb3huIj14I6qgT70">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_536b7ff5-1c13-4c26-9f54-577ba3fef859_poly_0_.GcA9NcfADZY4kDCjqkry">
<gml:posList srsDimension="3">
690963.482 5335931.361 533.731
690963.482 5335931.361 515.08
690962.652 5335929.259 535.11
690963.482 5335931.361 533.731
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906976_8f0d51f4-b587-41c6-beaa-e9113189f84b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906976_8f0d51f4-b587-41c6-beaa-e9113189f84b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906976_8f0d51f4-b587-41c6-beaa-e9113189f84b_poly_0_">
<gml:posList srsDimension="3">
690987.371 5335919.543 515.08
690985.505 5335914.805 515.08
690984.612 5335915.141 515.08
690984.786 5335915.548 515.08
690982.652 5335916.365 515.08
690982.507 5335915.969 515.08
690981.127 5335916.476 515.08
690981.29 5335916.903 515.08
690979.147 5335917.7 515.08
690979.003 5335917.304 515.08
690977.621 5335917.82 515.08
690977.776 5335918.227 515.08
690975.663 5335919.025 515.08
690975.518 5335918.639 515.08
690974.126 5335919.165 515.08
690974.261 5335919.561 515.08
690972.168 5335920.36 515.08
690972.003 5335919.973 515.08
690970.611 5335920.51 515.08
690970.766 5335920.906 515.08
690968.653 5335921.704 515.08
690968.499 5335921.308 515.08
690967.107 5335921.834 515.08
690967.261 5335922.25 515.08
690965.168 5335923.05 515.08
690965.004 5335922.653 515.08
690963.836 5335923.088 515.08
690960.686 5335924.286 515.08
690963.482 5335931.361 515.08
690963.249 5335931.452 515.08
690964.351 5335934.247 515.08
690967.604 5335932.953 515.08
690976.81 5335929.349 515.08
690989.3 5335924.442 515.08
690987.371 5335919.543 515.08
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>