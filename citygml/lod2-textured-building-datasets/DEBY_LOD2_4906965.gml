<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-08-08 15:35:49 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906965.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690825.803 5335938.398 514.67</gml:lowerCorner>
         <gml:upperCorner>690928.629 5336024.425 552.18</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906965">
<gml:name>DEBY_LOD2_4906965</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJfe</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cd460378-7f74-42bc-bd23-d011aed76d28_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cd460378-7f74-42bc-bd23-d011aed76d28_poly_0_">
0.9876479800691627 0.0009418139706564414 0.9876479800691627 0.9990553993860795 0.010020409416689802 0.9985940306518494 0.010020409416689802 0.0009418139706564414 0.9876479800691627 0.0009418139706564414
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_1aa6582c-7363-408b-bf29-88af26ec14aa_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_1aa6582c-7363-408b-bf29-88af26ec14aa_poly_0_">
0.9889405815963561 0.0010124301505710614 0.9889405815963561 0.9989351096133146 0.010734067626017916 0.9989927397056837 0.010734067626017916 0.0010124301505710614 0.9889405815963561 0.0010124301505710614
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_6260069e-d326-4ecb-ad0a-05822d67710c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_6260069e-d326-4ecb-ad0a-05822d67710c_poly_0_">
0.9930740471452567 0.0007506011166277199 0.9930740471452382 0.999072065662609 0.0059387997640563085 0.9992511046627071 0.005938799764074077 0.00075060111662772 0.9930740471452567 0.0007506011166277199
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_M6ccsmLBhIFKTYnk6CLD.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2a0e6c58-3ebc-4ad7-ba90-d68c1d2ca98f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2a0e6c58-3ebc-4ad7-ba90-d68c1d2ca98f_poly_0_">
0.9912550189372951 0.0009414841862298756 0.9912550189372951 0.9990053106093382 0.010010099397774752 0.9990629688315463 0.010010099397774752 0.0009414841862298756 0.9912550189372951 0.0009414841862298756
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_a502a9aa-fe28-479c-9199-97e0eb8a419d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_a502a9aa-fe28-479c-9199-97e0eb8a419d_poly_0_">
0.9918543707292584 0.9949143620830921 0.962717570811899 0.9988291052648105 0.005488090176820582 0.9989740957530081 0.00548809017685592 0.0008595749069033988 0.99185437072926 0.0008595749068993051 0.9918543707292584 0.9949143620830921
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_5f53553c-d324-4216-9999-f5ca83f4ad2b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_5f53553c-d324-4216-9999-f5ca83f4ad2b_poly_0_">
0.9918590033190355 0.0009210875892544122 0.9918590033190355 0.9990774143715554 0.00932177288146363 0.9988495508110528 0.00932177288146363 0.0009210875892544122 0.9918590033190355 0.0009210875892544122
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234#10.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_8707ee61-6ecb-4a34-8686-73a381327520_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_8707ee61-6ecb-4a34-8686-73a381327520_poly_0_">
0.9950603684880903 0.0006916395092457377 0.9950603684880903 0.999459379074787 0.005509812424744354 0.999459379074787 0.005509812424744354 0.0006916395092457377 0.9950603684880903 0.0006916395092457377
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_D1vzwxNc4EXCNyXugNwI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_fdf9b141-8ba5-4d3e-af9f-70ec1ecd8052_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_fdf9b141-8ba5-4d3e-af9f-70ec1ecd8052_poly_0_">
0.9918386985157568 0.0009284210230431141 0.9918386985157568 0.9990711476938261 0.009433917044312068 0.9990711476938261 0.009433917044312068 0.0009284210230431141 0.9918386985157568 0.0009284210230431141
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_0bfd9816-a2e5-4601-b7b9-871b9e4516c4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_0bfd9816-a2e5-4601-b7b9-871b9e4516c4_poly_0_">
0.9883127415464188 0.0009706133686925443 0.9883127415464177 0.9990253147879267 0.010306570310319783 0.9983343405528241 0.010306570310320675 0.0009706133686925443 0.9883127415464188 0.0009706133686925443
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_x8cvOKswj97qCMLQS4sV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ad7f0377-704f-431f-bf19-9220f9acd88f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ad7f0377-704f-431f-bf19-9220f9acd88f_poly_0_">
0.9710250443242001 0.0010637300928216867 0.9710250443242077 0.9986240764560799 0.04464758678725645 0.972614773470605 0.04464758678724933 0.0010637300928216867 0.9710250443242001 0.0010637300928216867
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_QY5BbhghjpvolzchYxT2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_40056240-dbad-4a75-ba57-6a7e25fbce80_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_40056240-dbad-4a75-ba57-6a7e25fbce80_poly_0_">
0.9327011176431057 0.0010727408105498653 0.9327011176430919 0.9822478864301366 0.05378257100731789 0.9986300932060369 0.05378257100733207 0.001072740810549865 0.9327011176431057 0.0010727408105498653
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_bLLdsCwfwZkajUyL9Pwc.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_a31e0250-4a4f-4057-8bdd-65911e8d02a7_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_a31e0250-4a4f-4057-8bdd-65911e8d02a7_poly_0_">
0.9510024920368778 0.001077454204278968 0.9510024920368778 0.9986058937566816 0.06706624821786988 0.9817897996173321 0.06706624821786988 0.001077454204278968 0.9510024920368778 0.001077454204278968
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_WeWis4DW4z28tOxe5W1w.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_a1898380-9813-40b9-a5ca-986f7f9a3dae_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_a1898380-9813-40b9-a5ca-986f7f9a3dae_poly_0_">
0.9877673013773745 0.0009421398232296991 0.9877673013773887 0.9990550715598736 0.010026426344133199 0.9985935964715136 0.010026426344118988 0.000942139823229699 0.9877673013773745 0.0009421398232296991
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_WfsOd4fzEU8Fi3Yr3Jdh.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_30768b80-f3e2-4890-93df-606253a075d2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_30768b80-f3e2-4890-93df-606253a075d2_poly_0_">
0.9942609221437007 0.0008470962170259157 0.9942609221437007 0.9988687337632666 0.006333242082106949 0.9991546861927245 0.006333242082106949 0.0008470962170259157 0.9942609221437007 0.0008470962170259157
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_rZ6h0Wl3XfPlkAgAPs1K.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_1b64c064-f431-4941-9886-6ffe65c7e4bd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_1b64c064-f431-4941-9886-6ffe65c7e4bd_poly_0_">
0.9325605366362667 0.0009898807768731514 0.932560536636288 0.9835414851930118 0.05025423196065759 0.9987448359293811 0.050254231960636275 0.0009898807768731514 0.9325605366362667 0.0009898807768731514
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_eaGHTT6wXPu1ksKWnE4p.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_64aefd00-14cf-480e-afd8-726424373284_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_64aefd00-14cf-480e-afd8-726424373284_poly_0_">
0.94057293990722 0.0009728805904331802 0.9405729399072196 0.9987505771956305 0.06123039109092329 0.9833846212114903 0.061230391090923326 0.0009728805904331803 0.94057293990722 0.0009728805904331802
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_e3QFI8IzIXlGNp8tuU7U.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ab684a01-ce57-422e-8c07-841232b14daa_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ab684a01-ce57-422e-8c07-841232b14daa_poly_0_">
0.9937548607692497 0.0009405496405786505 0.9937548607692488 0.9986648932396763 0.006484820987420867 0.9990612200133226 0.006484820987421092 0.0009405496405786505 0.9937548607692497 0.0009405496405786505
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_dHqX9KSD9ASt0cb4IZ5j.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_0d8e4a0f-c2c7-4cb9-8202-13edc627a21e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_0d8e4a0f-c2c7-4cb9-8202-13edc627a21e_poly_0_">
0.9954894860815668 0.0005889045805911916 0.9954894860815668 0.9994109219384794 0.006426148145312281 0.9994109219384794 0.006426148145312281 0.0005889045805911916 0.9954894860815668 0.0005889045805911916
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_3u7WSuKMXNqDci7xYoy7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_5ba6b268-38a2-4d3d-9293-f1d95a13457c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_5ba6b268-38a2-4d3d-9293-f1d95a13457c_poly_0_">
0.9921879331369494 0.0009923637409773854 0.9921879331369485 0.9986131458941659 0.006826303338404394 0.9990096790538429 0.006826303338405282 0.0009923637409773851 0.9921879331369494 0.0009923637409773854
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_HqOFNV4guv6CzKmgCr3R.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cb4c5251-fe73-4323-ae4d-cac2e2c4a8ac_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cb4c5251-fe73-4323-ae4d-cac2e2c4a8ac_poly_0_">
0.9910884142587157 0.0010517186224560957 0.9910884142587157 0.9984906581216353 0.011150595122444429 0.998951581845988 0.011150595122444429 0.0010517186224560957 0.9910884142587157 0.0010517186224560957
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_PNd3SDIyAw3KtvwJFytA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_21a96d47-0087-4c4a-b03e-05a9705ebacf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_21a96d47-0087-4c4a-b03e-05a9705ebacf_poly_0_">
0.8988091311101272 0.0010191451062670262 0.8988091311101201 0.988123081902326 0.07004173242378897 0.9987165023908328 0.07004173242379608 0.0010191451062670262 0.8988091311101272 0.0010191451062670262
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_5bSe67t2MuVNehtTtDy4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_c8d595c2-30fe-4cf6-9037-24fa9694d179_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_c8d595c2-30fe-4cf6-9037-24fa9694d179_poly_0_">
0.950605696198636 0.001077397778672023 0.950605696198622 0.9986059667825593 0.06700299753514118 0.9817812757158232 0.06700299753515537 0.001077397778672023 0.950605696198636 0.001077397778672023
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_dIRRnPMomM7lCvigrm8J.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7ecaad76-71b4-4541-b447-318e743a2f9e_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7ecaad76-71b4-4541-b447-318e743a2f9e_poly_0_">
0.9927546912317025 0.0009164443920644433 0.9927546912316956 0.9990825815845956 0.006486743407652541 0.9989134008833768 0.006486743407659645 0.0009164443920644435 0.9927546912317025 0.0009164443920644433
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_TrVUlhJct6gUqNSXpDwT.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_4c5ddb92-4212-49fe-bc3c-73a78c9f9993_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_4c5ddb92-4212-49fe-bc3c-73a78c9f9993_poly_0_">
0.9537333834351642 0.0010350911608207635 0.9537333834351642 0.9986610821352055 0.06621759186118936 0.982278679078691 0.06621759186118936 0.0010350911608207635 0.9537333834351642 0.0010350911608207635
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_120LXa1HsaYhNlRvJpTV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cf5343da-fc71-4254-85bf-7af185678c17_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cf5343da-fc71-4254-85bf-7af185678c17_poly_0_">
0.9519744815980147 0.0009815649078712234 0.9519744815980284 0.998730583459321 0.0621575065631816 0.9821751388964713 0.06215750656316742 0.0009815649078712232 0.9519744815980147 0.0009815649078712234
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_IOxuCwLe6YkJmjROoReI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_c3562de5-1992-4d61-9fa4-d959acae6b6b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_c3562de5-1992-4d61-9fa4-d959acae6b6b_poly_0_">
0.9473859606435369 0.000985332788382786 0.9473859606435227 0.9987350861267816 0.06303480989226529 0.9835887669205875 0.0630348098922795 0.0009853327883827857 0.9473859606435369 0.000985332788382786
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234#8.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_1800c408-e7f4-4ff7-bccc-4cf78c31e705_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_1800c408-e7f4-4ff7-bccc-4cf78c31e705_poly_0_">
0.99472359693101 0.0006906378184985761 0.9947235969310098 0.999129976731966 0.007157238042871937 0.9993111899156698 0.007157238042871939 0.0006906378184985762 0.99472359693101 0.0006906378184985761
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_WytaFg9FHEhUFKT9Q6pe.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_1ae8f6bb-e569-4b99-9637-f4b0d5f6a74a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_1ae8f6bb-e569-4b99-9637-f4b0d5f6a74a_poly_0_">
0.9475101576914897 0.0009750073874656679 0.9475101576914893 0.9833391159277597 0.04968523737679218 0.9987630456935933 0.04968523737679221 0.0009750073874656679 0.9475101576914897 0.0009750073874656679
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_I0qOgJA3uSuN9hKotV22.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7f75958e-e746-414b-a505-41851cccb926_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7f75958e-e746-414b-a505-41851cccb926_poly_0_">
0.9408725037563386 0.0008808595725978874 0.9408725037563386 0.9825479391165488 0.044771508824737793 0.9988731567461051 0.044771508824737793 0.0008808595725978874 0.9408725037563386 0.0008808595725978874
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_56kmOAdBoU4VZn9kflvM.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_579a182d-c865-4f99-a562-627ea76ce06b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_579a182d-c865-4f99-a562-627ea76ce06b_poly_0_">
0.9901345352820456 0.000883417331957603 0.9901345352820456 0.9986628397006748 0.00893147065517752 0.9991184987587267 0.00893147065517752 0.0008834173319577915 0.9901345352820456 0.000883417331957603
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_KGtbXNeySI6jDfABFZsi.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d5a7d194-218f-4ce0-a6bf-5a109bf0ec12_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d5a7d194-218f-4ce0-a6bf-5a109bf0ec12_poly_0_">
0.9934161239052166 0.0009399952926984071 0.9934161239052166 0.9990157326070683 0.006504549467708287 0.999129130975223 0.006504549467708287 0.0009399952926984071 0.9934161239052166 0.0009399952926984071
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_kc7KpFLjyfetazGpB0Dk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_38baa169-970d-4ee0-8ba2-2b2bd12c130d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_38baa169-970d-4ee0-8ba2-2b2bd12c130d_poly_0_">
0.9924049079215003 0.0009019494238794046 0.9924049079214825 0.9990812310437549 0.0063631890251798495 0.9993069102844558 0.006363189025197613 0.0009019494238794046 0.9924049079215003 0.0009019494238794046
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234#9.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_fec13c34-fdf6-4c34-b915-014de1cfa28d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_fec13c34-fdf6-4c34-b915-014de1cfa28d_poly_0_">
0.9456190369040627 0.0006906868762725691 0.9456190369040194 0.9991136875095983 0.05572401390342915 0.9870742493183502 0.055724013903471814 0.0006906868762725691 0.9456190369040627 0.0006906868762725691
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_7oE4IrHEDjW6gBWahlvR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_86ca301e-b1f1-42f7-9a6b-2f9664e83fc6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_86ca301e-b1f1-42f7-9a6b-2f9664e83fc6_poly_0_">
0.9954131623327918 0.0008662493575088147 0.9954131623327915 0.998794980187381 0.006026159942944529 0.9991353378718388 0.006026159942944531 0.0008662493575088146 0.9954131623327918 0.0008662493575088147
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_NI1vVZTaOl8namS0xpOU.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_0640ce03-456d-4bf8-8d57-49d4f81c694f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_0640ce03-456d-4bf8-8d57-49d4f81c694f_poly_0_">
0.944645037034739 0.0010730909153123583 0.944645037034739 0.9986221640390524 0.07295376771288886 0.984538194112511 0.07295376771288886 0.0010730909153123583 0.944645037034739 0.0010730909153123583
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_M4GkW58IYGSra04wDMuE.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7ee76fa0-d820-4906-aac9-55c0c0947778_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7ee76fa0-d820-4906-aac9-55c0c0947778_poly_0_">
0.9936778311769634 0.0009310898113185571 0.9936778311769672 0.9990682926332483 0.006421735756827474 0.9990115577271592 0.006421735756823921 0.0009310898113185572 0.9936778311769634 0.0009310898113185571
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_hCCEGT2CnNnriTaxN2Gk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ddfaf4ef-151c-45a8-94f1-c1bc8ea1c7ed_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ddfaf4ef-151c-45a8-94f1-c1bc8ea1c7ed_poly_0_">
0.9377123660027635 0.0008393574551245869 0.9377123660027635 0.9865311895102522 0.058404359868760025 0.998923234205109 0.058404359868760025 0.0008393574551245869 0.9377123660027635 0.0008393574551245869
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_SpvBO42poKWmZWDjqCCL.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_f68f9dc0-5b8b-491e-af74-784ce901ffd0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_f68f9dc0-5b8b-491e-af74-784ce901ffd0_poly_0_">
0.9453385898373484 0.001041652149278932 0.945338589837278 0.9816078499244619 0.050632271317695696 0.9986695543889009 0.05063227131776671 0.0010416521492789318 0.9453385898373484 0.001041652149278932
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_Hd55q6zRe6p9dy6TQ1v0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e445faf1-4355-45ff-a9eb-f9eabb2fb956_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e445faf1-4355-45ff-a9eb-f9eabb2fb956_poly_0_">
0.9604531995787937 0.0009882602633687959 0.9604531995787937 0.9987216724063516 0.06382480344650786 0.9823309859517516 0.06382480344650786 0.0009882602633687959 0.9604531995787937 0.0009882602633687959
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_ol3P7PJpr1eILvYV2XhG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_331a8a95-99cf-4533-b8a9-a386fee05cc6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_331a8a95-99cf-4533-b8a9-a386fee05cc6_poly_0_">
0.9597840174490884 0.0009808005080930522 0.9597840174490884 0.9987399602387852 0.06630974881889529 0.9841511973589078 0.06630974881889529 0.0009808005080930522 0.9597840174490884 0.0009808005080930522
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_5uPgci3vrR2v1SR9Kx4a.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2b700087-f865-4aa9-849f-d586756eb1c7_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2b700087-f865-4aa9-849f-d586756eb1c7_poly_0_">
0.9797121950315244 0.0005892057553857538 0.9797121950315244 0.9994106205861593 0.017810430688285805 0.9994106205861593 0.017810430688285805 0.0005892057553857538 0.9797121950315244 0.0005892057553857538
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_BpeAmEQ0JVRgI3I8n4ff.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_99b062c0-ee64-432a-9dfa-e1fe5aca1689_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_99b062c0-ee64-432a-9dfa-e1fe5aca1689_poly_0_">
0.9909054227670212 0.001125858991523758 0.9909054227670212 0.9987647621291775 0.007769680606330987 0.9988781044267393 0.007769680606330987 0.001125858991523758 0.9909054227670212 0.001125858991523758
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_1c9tdWZWGN5x8L04X2Vr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_62b98986-0a4e-43b6-8384-a2f1b0d317f9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_62b98986-0a4e-43b6-8384-a2f1b0d317f9_poly_0_">
0.9421694264981113 0.0010503644126522473 0.9421694264980975 0.9986415872276474 0.0655822490977727 0.9820953425777196 0.06558224909778687 0.0010503644126522473 0.9421694264981113 0.0010503644126522473
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_tP3508IFg2Kq9TXaoJsc.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_6d6ee038-9cb7-4370-a5a3-08d1b19e3bf4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_6d6ee038-9cb7-4370-a5a3-08d1b19e3bf4_poly_0_">
0.9837778840875302 0.0008024314503734595 0.9837778840875302 0.999195863685527 0.01968372326955148 0.9990586777607021 0.01968372326955148 0.0008024314503734595 0.9837778840875302 0.0008024314503734595
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_PrqvIK8jk6GpckTr4GqJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_b035d83f-a0db-45f6-a09e-3edf1986bcc0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_b035d83f-a0db-45f6-a09e-3edf1986bcc0_poly_0_">
0.8910949396070437 0.0010418558575427553 0.891094939607051 0.9889961715045307 0.07710614499170279 0.9986908395950876 0.07710614499169566 0.001041855857542755 0.8910949396070437 0.0010418558575427553
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_4LMyXWlfIhv7sS41a13J.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_63aca288-6a4f-49e2-91ab-32a68d832dda_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_63aca288-6a4f-49e2-91ab-32a68d832dda_poly_0_">
0.990287372085966 0.0008015474336700668 0.990287372085966 0.9991964095169392 0.008361978251009816 0.9987889198902217 0.008361978251009816 0.0008015474336700668 0.990287372085966 0.0008015474336700668
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_FX2XWh71fI4KYqUscaOa.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cc5cf8de-0b6e-4ad8-89a5-e48b27b3dc72_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cc5cf8de-0b6e-4ad8-89a5-e48b27b3dc72_poly_0_">
0.9810900375185271 0.000877696706005986 0.981090037518527 0.998843201913663 0.018684338705057254 0.99912789567335 0.01868433870505726 0.0008776967060059861 0.9810900375185271 0.000877696706005986
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_PQ9hp6bZ14lPuISEo2f0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_c63ce54d-174e-4c5b-ad03-c58ec56601e8_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_c63ce54d-174e-4c5b-ad03-c58ec56601e8_poly_0_">
0.9386566357000419 0.001035118704479219 0.9386566357000419 0.9818411565697214 0.050848662582538395 0.9986773254083515 0.050848662582538395 0.001035118704479219 0.9386566357000419 0.001035118704479219
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_G8AjkJfaHJoIHQ5JrcBI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7da17286-c25e-467a-9861-7462b01191cf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7da17286-c25e-467a-9861-7462b01191cf_poly_0_">
0.9405081901405198 0.001021318404565027 0.940508190140534 0.9986879666176117 0.10026483066263836 0.9892164478054623 0.10026483066262415 0.0010213184045650269 0.9405081901405198 0.001021318404565027
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_izWpH0tfVeSSfuxnxogI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_9991eb22-9916-4fdc-abba-0b489052a330_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_9991eb22-9916-4fdc-abba-0b489052a330_poly_0_">
0.9939168339868534 0.0008526292718429925 0.9939168339868534 0.9991463417312695 0.005469076998760869 0.9989048229543642 0.005469076998760869 0.0008526292718429925 0.9939168339868534 0.0008526292718429925
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_HSC8OmH6d3fE0hJmTZJd.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_76f520ac-ca6a-4245-b4a1-3375559e4503_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_76f520ac-ca6a-4245-b4a1-3375559e4503_poly_0_">
0.9899679797824881 0.0009369304009543393 0.9899679797824952 0.9989615685017658 0.009946083166905595 0.9989615685017658 0.00994608316689849 0.0009369304009543393 0.9899679797824881 0.0009369304009543393
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_ObE7Stxi7B1Vdxj0QKGU.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_02744a47-1af7-4f09-92f8-4eb1a0c1b833_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_02744a47-1af7-4f09-92f8-4eb1a0c1b833_poly_0_">
0.9473125908839819 0.0008023183210408956 0.9473125908840101 0.9871263592636315 0.052421834337920345 0.9989844425069753 0.05242183433789194 0.0008023183210408956 0.9473125908839819 0.0008023183210408956
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_ZqvHeKpM8ttKkdZ6pNSr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cfc0a8e4-2ce6-4d60-bd55-da49f81d016b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cfc0a8e4-2ce6-4d60-bd55-da49f81d016b_poly_0_">
0.992114041956107 0.0009138435920379194 0.992114041956107 0.9990857385665038 0.009287524364097521 0.9990857385665038 0.009287524364097521 0.0009138435920379194 0.992114041956107 0.0009138435920379194
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_yLc9Zm6YUJAASPHcJWpI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d5b1a094-a393-43d6-8ef4-1de37ec99562_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d5b1a094-a393-43d6-8ef4-1de37ec99562_poly_0_">
0.9273596465683125 0.0009457315281702483 0.9273596465683123 0.9838268737249724 0.048445647253725334 0.9988005850662107 0.04844564725372534 0.0009457315281702483 0.9273596465683125 0.0009457315281702483
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_p9pbzHehuvKKwpFh3vEv.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_44d25014-d47c-4dd6-826f-c880d4ecd08a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_44d25014-d47c-4dd6-826f-c880d4ecd08a_poly_0_">
0.9919628863776496 0.0008984290578426894 0.9919628863776498 0.9991000571529854 0.009519363299698828 0.9988693391065054 0.009519363299698826 0.0008984290578426894 0.9919628863776496 0.0008984290578426894
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_W28ocqmFaOREleRE8xdX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_3e84316b-88b3-4e45-8728-0dd026a263d5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_3e84316b-88b3-4e45-8728-0dd026a263d5_poly_0_">
0.9920986626962112 0.000977743340200905 0.9920986626962112 0.9986273806917174 0.00672284960666758 0.9990242182562964 0.00672284960666758 0.000977743340200905 0.9920986626962112 0.000977743340200905
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_i6Hmv6uEO2PHNd9T0o7Q.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2d936ef0-1a01-4602-ad88-83deac0eb9f2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2d936ef0-1a01-4602-ad88-83deac0eb9f2_poly_0_">
0.9366479946174593 0.0010980618167849541 0.9366479946174311 0.9820020989082177 0.05426298671248732 0.998598789031668 0.054262986712515726 0.0010980618167849541 0.9366479946174593 0.0010980618167849541
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_DUOu5FQ8s97u6rEyZ458.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cf970899-7eec-4419-8ee6-a84e5e6f035d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cf970899-7eec-4419-8ee6-a84e5e6f035d_poly_0_">
0.933330888599837 0.0009222401637327539 0.9333308885998337 0.9990626024273315 0.05653690635553944 0.9985814317937683 0.05653690635554298 0.0009222401637327539 0.933330888599837 0.0009222401637327539
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_wF4CNgcEhzWvXYtlfLmw.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_63390a18-1ae8-4346-882c-d26c92717009_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_63390a18-1ae8-4346-882c-d26c92717009_poly_0_">
0.9316704581640207 0.0012135004350728393 0.9316704581640489 0.9820817636940393 0.06046715153931356 0.9984531716109807 0.060467151539285155 0.0012135004350728393 0.9316704581640207 0.0012135004350728393
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_QTI7UXhhBAfwd6UEDIzl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ba769ac7-26fd-455b-984a-c1f30383dbcd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ba769ac7-26fd-455b-984a-c1f30383dbcd_poly_0_">
0.9471081663840195 0.0008646581365005277 0.9471081663840195 0.9988766943496153 0.07016971645499126 0.9855361983291986 0.07016971645499126 0.0008646581365005277 0.9471081663840195 0.0008646581365005277
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_2RWuq80EmpQmpFfKR7t2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_60d0fd5c-173b-4217-9db0-7e9b71becbbd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_60d0fd5c-173b-4217-9db0-7e9b71becbbd_poly_0_">
0.9412540493833317 0.001047900718742206 0.941254049383317 0.9986441710601088 0.06897638355096088 0.9829522819695603 0.06897638355097513 0.0010479007187422062 0.9412540493833317 0.001047900718742206
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_GhDUo9knluKZsjLyFMXn.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_86b86c0a-083f-4315-8fcf-382da5f8a163_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_86b86c0a-083f-4315-8fcf-382da5f8a163_poly_0_">
0.9606775361548685 0.000650741738009381 0.9606775361548898 0.9993490464261924 0.04596162864278597 0.9993490464261924 0.04596162864276465 0.000650741738009381 0.9606775361548685 0.000650741738009381
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_bU2ne3klmKfKGbQQt6SV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_993c6dee-1bd0-425d-9b49-26c2fa996d8b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_993c6dee-1bd0-425d-9b49-26c2fa996d8b_poly_0_">
0.9930736086830287 0.0007584799638355023 0.9930736086830279 0.9992412322265027 0.005629219692975251 0.9992412322265027 0.0056292196929761396 0.0007584799638355023 0.9930736086830287 0.0007584799638355023
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_hKGeKJ1T1knPMaJSQauN.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_bda5b570-8c71-4106-a50b-b57b28bc097a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_bda5b570-8c71-4106-a50b-b57b28bc097a_poly_0_">
0.9919252734721127 0.0008507137091097719 0.9919252734721127 0.9991042275002116 0.006379267697134594 0.9991518654631701 0.006379267697134594 0.0008507137091099124 0.9919252734721127 0.0008507137091097719
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_gfR9lFxRR8kJzAP4jwta.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d2043bf2-4227-45b4-83cb-a7447d920bf5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d2043bf2-4227-45b4-83cb-a7447d920bf5_poly_0_">
0.9907019757065072 0.0009062155918772482 0.9907019757065072 0.9989157967174146 0.009197705314129223 0.9989157967174146 0.009197705314129223 0.0009062155918772482 0.9907019757065072 0.0009062155918772482
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_EuS879IHU7IHyGX8BkgV.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_8db669df-fb4f-41c9-a5c3-17cc41a48afd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_8db669df-fb4f-41c9-a5c3-17cc41a48afd_poly_0_">
0.9918709633869973 0.0007951789262745604 0.9918709633869973 0.9988034061859645 0.006306414264027893 0.9992060633808064 0.006306414264027893 0.0007951789262745604 0.9918709633869973 0.0007951789262745604
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_uHfq15xrp1tksiaBYqn7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_f6d289c6-a556-4cd8-b182-26d27fcf0767_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_f6d289c6-a556-4cd8-b182-26d27fcf0767_poly_0_">
0.9596260788117374 0.0008678439375885455 0.9596260788117479 0.975806884092249 0.03364341523537816 0.9988804099191598 0.033643415235367506 0.0008678439375885454 0.9596260788117374 0.0008678439375885455
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_CrPlKTnJJXuQBdNvSDS4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_6c358fc7-bd41-484f-8b9f-38062048d4c2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_6c358fc7-bd41-484f-8b9f-38062048d4c2_poly_0_">
0.9926021019373861 0.0008907071496033731 0.9926021019374001 0.9991701009441458 0.00633365328576829 0.9990017097053837 0.00633365328575408 0.0008907071496033731 0.9926021019373861 0.0008907071496033731
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_0NdKU9Bx90kdMQLjYUaN.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_3d00d7fe-50b8-4cb6-9488-3e71cc83d656_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_3d00d7fe-50b8-4cb6-9488-3e71cc83d656_poly_0_">
0.9932774999969316 0.0009307699879308558 0.9932774999969316 0.9990678819957632 0.006397454625187036 0.9987842719735979 0.006397454625187036 0.0009307699879308557 0.9932774999969316 0.0009307699879308558
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_7dcoaBoY8l6fi3e9EmGf.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_38f87776-a193-4aca-9c20-5ef51a7812ce_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_38f87776-a193-4aca-9c20-5ef51a7812ce_poly_0_">
0.9822695945967013 0.0008217497679071077 0.9822695945967158 0.9991698034234653 0.016096565388508573 0.9981735836574916 0.016096565388494355 0.0008217497679071077 0.9822695945967013 0.0008217497679071077
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_9FxcO4uUrRQfieoGT72F.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_4a5c5e24-5fa8-4154-a624-1fd8280907b4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_4a5c5e24-5fa8-4154-a624-1fd8280907b4_poly_0_">
0.9938484177613329 0.0010512804081268406 0.9938484177613329 0.9987819320396959 0.007213874581962543 0.9989519128424041 0.007213874581962543 0.0010512804081268406 0.9938484177613329 0.0010512804081268406
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_T8EfPJ3wwFs5ucdrhzea.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_0b2331c5-c5ac-4b77-ac19-df6ee41d40bc_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_0b2331c5-c5ac-4b77-ac19-df6ee41d40bc_poly_0_">
0.9954565610747063 0.0007473960054261579 0.9954565610747068 0.9992057237298488 0.0047513239809888735 0.9992542703495659 0.004751323980987987 0.0007473960054261579 0.9954565610747063 0.0007473960054261579
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_yf0XS4ZFATJcojOBbrvs.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e52f5c8f-e358-4d66-974f-76ecc978cca4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e52f5c8f-e358-4d66-974f-76ecc978cca4_poly_0_">
0.9425382541325575 0.0008936242960759802 0.9425382541325575 0.9988523792416851 0.06895768055424867 0.986393468443363 0.06895768055424867 0.0008936242960759802 0.9425382541325575 0.0008936242960759802
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_Gf53httVYNM35UOdIqSu.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_3b0cee53-9835-4f2a-a605-f5402eac3377_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_3b0cee53-9835-4f2a-a605-f5402eac3377_poly_0_">
0.9946625261542257 0.0009086410247261756 0.9946625261542225 0.9990889312376773 0.006465864435273972 0.9984688034170323 0.006465864435277522 0.0009086410247261756 0.9946625261542257 0.0009086410247261756
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_EKP9utcelhJykuCg7PQc.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_a87338ef-fbd0-4931-835b-27e56c8c949f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_a87338ef-fbd0-4931-835b-27e56c8c949f_poly_0_">
0.9945384383467015 0.0008830471563327073 0.9945384383466978 0.9991158515356832 0.006292559436779752 0.9988914034627477 0.006292559436783305 0.0008830471563327071 0.9945384383467015 0.0008830471563327073
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_xMhbXZS0UMwvBGu3yhlK.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_8a914f87-bd16-4698-a479-8e5c1ecf7998_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_8a914f87-bd16-4698-a479-8e5c1ecf7998_poly_0_">
0.919139500735767 0.0009878647192585693 0.9191395007357386 0.9987317608569463 0.0879303631964774 0.9884701543302135 0.08793036319650582 0.0009878647192585693 0.919139500735767 0.0009878647192585693
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_E4eIuvWlBskq8Rn8QzPJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_41a96f94-54a8-44bd-ad52-3e6faea1932f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_41a96f94-54a8-44bd-ad52-3e6faea1932f_poly_0_">
0.9554076447533079 0.0008805723900101449 0.9554076447533079 0.9818257663181384 0.04379211952524998 0.9988723862230148 0.04379211952524998 0.0008805723900101449 0.9554076447533079 0.0008805723900101449
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_awYgtL4NKq9lI8m3pMqX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_16c9932e-e275-4c97-86ec-af967cad08f4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_16c9932e-e275-4c97-86ec-af967cad08f4_poly_0_">
0.9349804155786785 0.0010568915335679582 0.9349804155787211 0.9823367674557745 0.05312021960146751 0.9986508357040915 0.05312021960142488 0.0010568915335679582 0.9349804155786785 0.0010568915335679582
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_B9Vq1gOlVstfcOmZJonA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_adc01ad7-a700-42af-9ecb-ecb7be7c5879_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_adc01ad7-a700-42af-9ecb-ecb7be7c5879_poly_0_">
0.9013675993642565 0.0010804830749340415 0.9013675993642425 0.9891698683575335 0.083002816096176 0.9986430759548329 0.0830028160961902 0.0010804830749340413 0.9013675993642565 0.0010804830749340415
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_FZxTYuSN5i164ByDTYE4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_5800970b-d523-4752-9c9b-b39374c9977a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_5800970b-d523-4752-9c9b-b39374c9977a_poly_0_">
0.9429375064205772 0.0010037174746938078 0.9429375064205774 0.9987116507118325 0.09468964756035805 0.988845634533017 0.09468964756035803 0.0010037174746938078 0.9429375064205772 0.0010037174746938078
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_MG3vzXOKXfnBqtHvsp3q.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_756e32cc-4d73-47c2-a279-c21a9847c48a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_756e32cc-4d73-47c2-a279-c21a9847c48a_poly_0_">
0.9356449394275328 0.0009645045474572137 0.9356449394275395 0.984023560479124 0.05053940288056676 0.9987773000197555 0.05053940288055968 0.0009645045474572137 0.9356449394275328 0.0009645045474572137
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_xueeARgt1HZqlHygwqkl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_cd5ab605-ebcb-4b85-8307-d101100634c5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_cd5ab605-ebcb-4b85-8307-d101100634c5_poly_0_">
0.9290526001455486 0.0008323853138068397 0.9954039331799367 0.0008323853136912656 0.9954039331799354 0.5819834137586868 0.9290526001455467 0.5747955632008479 0.9290526001455448 0.9994111794948378 0.004617755717227173 0.9994111794947902 0.004617755717243099 0.0008323853137559518 0.9290526001455486 0.0008323853138068397
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_bMROZK4GGjQwibb28NCr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2142cf3e-817e-4e88-871c-1b49062a31e0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2142cf3e-817e-4e88-871c-1b49062a31e0_poly_0_">
0.9600888857929917 0.0005842502994925519 0.9600888857929917 0.9994155789514776 0.0477624834391861 0.9994155789514776 0.0477624834391861 0.0005842502994925519 0.9600888857929917 0.0005842502994925519
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230#10.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_01495273-7b74-435c-8a06-6997e7ea37f9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_01495273-7b74-435c-8a06-6997e7ea37f9_poly_0_">
0.9938524924013139 0.0006912647409076562 0.9938524924013139 0.999430781163422 0.007342628636342496 0.999257799317728 0.007342628636342496 0.0006912647409076562 0.9938524924013139 0.0006912647409076562
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_HAxk85xnuiyxvdIHYj7i.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_47be2a7c-8f41-4603-8913-ed37bf46801d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_47be2a7c-8f41-4603-8913-ed37bf46801d_poly_0_">
0.9326314762401038 0.000990342323262764 0.9326314762401038 0.9987281550446024 0.09104967511444782 0.9886351870605887 0.09104967511444782 0.000990342323262764 0.9326314762401038 0.000990342323262764
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_SUmRxAMxjAUXgNnTYs5y.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_393ab129-1f7c-47fb-8103-ca7fe09b7149_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_393ab129-1f7c-47fb-8103-ca7fe09b7149_poly_0_">
0.9327660221304939 0.0008374843176617333 0.9327660221304939 0.9844921613241993 0.04984297901057744 0.9989240501065436 0.04984297901057744 0.0008374843176617333 0.9327660221304939 0.0008374843176617333
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_3DpblsBEKiEPq3ILUzBP.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_554f5612-b779-414f-8a1e-aa57053016ca_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_554f5612-b779-414f-8a1e-aa57053016ca_poly_0_">
0.921682718416605 0.0008532493136091377 0.9216827184165479 0.9862644790705515 0.05126762923096349 0.9989188503447919 0.05126762923102035 0.0008532493136091377 0.921682718416605 0.0008532493136091377
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232#17.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_132a6fdb-f9c0-4bc4-b9bf-e8539a13a014_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_132a6fdb-f9c0-4bc4-b9bf-e8539a13a014_poly_0_">
0.9587560544510012 0.0007067410896763155 0.9587560544510012 0.9991998588924829 0.046372270326102694 0.9828673764748531 0.046372270326102694 0.0007067410896763155 0.9587560544510012 0.0007067410896763155
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_JfcwB58x8teUqWvvaJbX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_b3959092-c45a-428e-88aa-3ededbb4810a_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_b3959092-c45a-428e-88aa-3ededbb4810a_poly_0_">
0.990333653475723 0.000891031439553696 0.990333653475723 0.9986552320580739 0.00901384450693854 0.9991109361917201 0.00901384450693854 0.0008910314395535117 0.990333653475723 0.000891031439553696
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_TzURaFA7bQY5FHvQNUg1.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2d11ccc8-2166-4da5-968b-82b3cce45bfd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2d11ccc8-2166-4da5-968b-82b3cce45bfd_poly_0_">
0.992807863399136 0.0009026213783667654 0.9928078633991434 0.9993448822014499 0.006217627822540097 0.9992881493040795 0.0062176278225329895 0.0009026213783667655 0.992807863399136 0.0009026213783667654
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_3qVekSoux0yAkb91zIH8.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ecb4d84e-142b-4339-bac0-9c9bb9523de6_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ecb4d84e-142b-4339-bac0-9c9bb9523de6_poly_0_">
0.9392861903218518 0.001143163604268844 0.9392861903218799 0.9821006703755296 0.057288342023710456 0.9985416157008294 0.05728834202368205 0.0011431636042688437 0.9392861903218518 0.001143163604268844
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_FqAbqrIhfXCoe1zioa0X.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_081f191c-354b-4570-8d41-340c0452945f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_081f191c-354b-4570-8d41-340c0452945f_poly_0_">
0.9183222389084662 0.0010170107791444606 0.9183222389084804 0.9887944471317095 0.07657066423086434 0.9987195915024593 0.07657066423085013 0.0010170107791444606 0.9183222389084662 0.0010170107791444606
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_fh5h8Vbr6Kb9ZHnV1e9Z.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_b9ff6968-6f2b-4fb5-a39c-6e004425dc05_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_b9ff6968-6f2b-4fb5-a39c-6e004425dc05_poly_0_">
0.9892139237039022 0.00102159848298316 0.9892139237039022 0.9989260620103724 0.010840023300593415 0.9989836710892044 0.010840023300593415 0.00102159848298316 0.9892139237039022 0.00102159848298316
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_r5LJEMIUVu1Z0Zld6eo7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d17820dd-66d7-4a00-bb18-e0a4305fbd81_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d17820dd-66d7-4a00-bb18-e0a4305fbd81_poly_0_">
0.9962741175427483 0.0008555141842012042 0.9962741175427485 0.998517465771428 0.004404967334595612 0.99914498683639 0.00440496733459561 0.0008555141842012041 0.9962741175427483 0.0008555141842012042
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_CXELLp3x4ck4D4WG5TOx.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2b5bb68b-de56-4eac-9f29-791c53c2e5f4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2b5bb68b-de56-4eac-9f29-791c53c2e5f4_poly_0_">
0.9857324606419213 0.0011257219995644148 0.9857324606419213 0.9988736438399588 0.01189671753725818 0.9988736438399588 0.01189671753725818 0.0011257219995644148 0.9857324606419213 0.0011257219995644148
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_so4G7TlWgkGv4fniOeyr.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_872f9ca8-e205-4fbb-a747-41e9a32d766f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_872f9ca8-e205-4fbb-a747-41e9a32d766f_poly_0_">
0.9232252086099635 0.0008250540603474517 0.9232252086099353 0.9866373398468529 0.051231975425253054 0.9989540028243509 0.05123197542528146 0.0008250540603474517 0.9232252086099635 0.0008250540603474517
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_JDjEV4gF38njDpx9z0TE.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_a2f9f934-0e90-499e-8bed-62246f972905_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_a2f9f934-0e90-499e-8bed-62246f972905_poly_0_">
0.986828018358205 0.001087946379954436 0.986828018358205 0.9989114613230486 0.011511720226881296 0.9989114613230486 0.011511720226881296 0.001087946379954436 0.986828018358205 0.001087946379954436
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_M2b6ZYlJPG1AzbZ9DZF5.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d2efe944-8bcd-4d69-b911-8c905b742884_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d2efe944-8bcd-4d69-b911-8c905b742884_poly_0_">
0.9362893697142454 0.001027663960933603 0.9362893697142743 0.9827520586988305 0.05303629243718436 0.998687854142268 0.05303629243715591 0.001027663960933603 0.9362893697142454 0.001027663960933603
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_4CLco1JLLv0rlqrdggxL.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2d677e2c-0f62-4f82-a296-1090ae61ef52_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2d677e2c-0f62-4f82-a296-1090ae61ef52_poly_0_">
0.9940338518412588 0.0008678900672257334 0.9940338518412588 0.9986847763521343 0.006190790410016689 0.9991334098085657 0.006190790410016689 0.0008678900672259436 0.9940338518412588 0.0008678900672257334
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_Bei2v3ylUAZgFmat9ufA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_684f95ab-716a-43b5-b6a0-540897263563_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_684f95ab-716a-43b5-b6a0-540897263563_poly_0_">
0.9365363129015237 0.0011840543499087005 0.9365363129015237 0.9821223263584867 0.059377078772598686 0.9984901762782604 0.059377078772598686 0.0011840543499087005 0.9365363129015237 0.0011840543499087005
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234_mAqHBg9CUARLe9sgk3e4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_5dccb95c-c204-4eb6-beae-5ab6a41bf398_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_5dccb95c-c204-4eb6-beae-5ab6a41bf398_poly_0_">
0.9431507284686661 0.0007754999165996948 0.943150728468595 0.9869802361511513 0.04989383591195917 0.9990174916548986 0.04989383591203023 0.0007754999165996947 0.9431507284686661 0.0007754999165996948
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_5h9fmfTUqXxSkzO1Xw0j.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_6154ed3e-650b-47ee-aad2-e804f4e8ce14_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_6154ed3e-650b-47ee-aad2-e804f4e8ce14_poly_0_">
0.9383458747576725 0.0012294974219781776 0.9383458747576725 0.9984096789644662 0.07769131652025862 0.982324300595838 0.07769131652025862 0.0012294974219781776 0.9383458747576725 0.0012294974219781776
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_bVro3WIBnpheP5PxWe3f.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_babffc92-e109-401d-bcc3-2308e74a43f5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_babffc92-e109-401d-bcc3-2308e74a43f5_poly_0_">
0.9899214720535952 0.0009741304443220477 0.9899214720535952 0.9990241916000675 0.010299356250616398 0.9987936018556778 0.010299356250616398 0.0009741304443220477 0.9899214720535952 0.0009741304443220477
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_sa7IvlmGYn6ifqsK4CuZ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_652b2825-c6d6-4a1f-a061-f14cb5d02793_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_652b2825-c6d6-4a1f-a061-f14cb5d02793_poly_0_">
0.9955869320539881 0.0007942786628207416 0.9955869320539878 0.9990157485286788 0.005926652195073379 0.9992075113698785 0.005926652195073601 0.0007942786628207416 0.9955869320539881 0.0007942786628207416
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_bBlm1eas6c4pAr3C4UIq.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_9211b454-99f4-45bf-a62d-88f09caee705_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_9211b454-99f4-45bf-a62d-88f09caee705_poly_0_">
0.954129970324644 0.0009474310475469866 0.954129970324644 0.9987843151710951 0.0755630193613115 0.9867562580896632 0.0755630193613115 0.0009474310475469865 0.954129970324644 0.0009474310475469866
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_o7xx2tEw7dmHSuT128Hi.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_163040e2-cd2d-4232-b8c3-f151668cd44c_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_163040e2-cd2d-4232-b8c3-f151668cd44c_poly_0_">
0.9933394356453817 0.0008756140415873355 0.9933394356453817 0.9988457097505059 0.006215965314197547 0.9991262433131385 0.006215965314197547 0.0008756140415873355 0.9933394356453817 0.0008756140415873355
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_pmNdmBdeI1x6Wt3Qrc3f.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_11c0424c-eadf-41e5-b5c4-782d6269e069_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_11c0424c-eadf-41e5-b5c4-782d6269e069_poly_0_">
0.9939400438092393 0.000930491792869469 0.9939400438092393 0.9990688913526402 0.006420959061081533 0.9990121789435743 0.006420959061081533 0.0009304917928695984 0.9939400438092393 0.000930491792869469
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_Yj4qAU8iOVsXc0zxZnPR.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d0ce8efd-7980-4a6a-bf14-5c89fa65bf85_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d0ce8efd-7980-4a6a-bf14-5c89fa65bf85_poly_0_">
0.9320180957727473 0.0008208403352933024 0.9320180957727763 0.9989473274926639 0.08673297358211589 0.9902462929621659 0.08673297358208742 0.0008208403352933024 0.9320180957727473 0.0008208403352933024
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_v1TjgeayrbRS3EZnIomZ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e3de98ad-f7d4-476b-ae62-90ec14224408_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e3de98ad-f7d4-476b-ae62-90ec14224408_poly_0_">
0.9490144699365716 0.0009939252466243985 0.9490144699365854 0.9987141241101276 0.06434051591034516 0.9825505124300402 0.06434051591033098 0.0009939252466243985 0.9490144699365716 0.0009939252466243985
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_cvp1L57jN1WgXEd4etzb.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ee9c2a08-f05e-490c-a03b-c9a70b820f80_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ee9c2a08-f05e-490c-a03b-c9a70b820f80_poly_0_">
0.926494140752709 0.0005796038428014346 0.926494140752709 0.9994202281138388 0.055957941309998205 0.9994202281138388 0.055957941309998205 0.0005796038428014346 0.926494140752709 0.0005796038428014346
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230#18.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_3aa30b16-38cf-401b-bee4-650c2be0a4e2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_3aa30b16-38cf-401b-bee4-650c2be0a4e2_poly_0_">
0.9913566024389342 0.0006935902299788942 0.9913566024389342 0.9990837294538986 0.006788446951830229 0.9993141449026001 0.006788446951830229 0.0006935902299788942 0.9913566024389342 0.0006935902299788942
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_fzOh4gL6yUbYqhU0EuIp.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2e36f84a-8228-400e-9ca7-fecb0dfe7417_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2e36f84a-8228-400e-9ca7-fecb0dfe7417_poly_0_">
0.9434729640508551 0.0010663809026676143 0.9434729640508269 0.9815170474470429 0.051829933861697 0.9986373770970228 0.051829933861725415 0.0010663809026676143 0.9434729640508551 0.0010663809026676143
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_Be7FoMZOcsYruGXoM5dP.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d293b224-fc90-4c8c-901c-6a8793c51028_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d293b224-fc90-4c8c-901c-6a8793c51028_poly_0_">
0.9432991477946758 0.0011290770089636556 0.9432991477946326 0.9985390828898085 0.0694757145294744 0.9817192571036055 0.06947571452951706 0.0011290770089636556 0.9432991477946758 0.0011290770089636556
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230#13.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_95c9840d-ccde-41b7-8869-e3377b9cd92b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_95c9840d-ccde-41b7-8869-e3377b9cd92b_poly_0_">
0.9943890947257669 0.0006927371629414775 0.9943890947257669 0.999141958230448 0.004759438662437 0.9993688011751413 0.004759438662437 0.0006927371629414775 0.9943890947257669 0.0006927371629414775
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_WgIkGosToKQWMogLx0Hw.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ffa63fad-6b56-4af1-a804-c6e7d0e94a36_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ffa63fad-6b56-4af1-a804-c6e7d0e94a36_poly_0_">
0.9533381596604895 0.0010492958036629645 0.9533381596604756 0.9986427620595028 0.06542877089606237 0.9818360651692659 0.06542877089607656 0.0010492958036629645 0.9533381596604895 0.0010492958036629645
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234_z8U5hpoX3bZhPxaabfPe.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_deaa4b8d-13dc-408e-8436-e5b195e32566_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_deaa4b8d-13dc-408e-8436-e5b195e32566_poly_0_">
0.9378742364085895 0.0007714360006460486 0.9378742364085895 0.9873415460646089 0.05078520897276917 0.9990230231197599 0.05078520897276917 0.0007714360006460484 0.9378742364085895 0.0007714360006460486
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234_5OWNczUoLYk2dPrtNPqg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_2c3924f9-b88f-4765-9a29-29bfe3d9ba33_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_2c3924f9-b88f-4765-9a29-29bfe3d9ba33_poly_0_">
0.9391045712476254 0.0007375298348152833 0.9391045712476115 0.9990541290390427 0.06113232472905851 0.9875529919583772 0.06113232472907271 0.0007375298348152835 0.9391045712476254 0.0007375298348152833
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228#30.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ba9a25a3-6c5f-40e2-899a-804fdc698162_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ba9a25a3-6c5f-40e2-899a-804fdc698162_poly_0_">
0.9570689285135785 0.0006909607529252962 0.9570689285135576 0.9991131036941283 0.04483772557085077 0.9837449970054093 0.04483772557087207 0.0006909607529252962 0.9570689285135785 0.0006909607529252962
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_mblpYSzzzMw97v2kjPNd.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_27832f6e-23ee-499f-99bd-1a07eca099e5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_27832f6e-23ee-499f-99bd-1a07eca099e5_poly_0_">
0.9927303475786946 0.0010135827952375638 0.9927303475786946 0.9989857031492941 0.006987282260565664 0.9989290323872071 0.006987282260565664 0.0010135827952375638 0.9927303475786946 0.0010135827952375638
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_jUcxuz0maSw3xuhHJGUy.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_39924390-7b8e-464d-a0eb-e8f25d564db9_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_39924390-7b8e-464d-a0eb-e8f25d564db9_poly_0_">
0.9614048630698588 0.0007912052063240228 0.9614048630698588 0.9989839331452537 0.0627549250846897 0.9865226996856039 0.0627549250846897 0.0007912052063240227 0.9614048630698588 0.0007912052063240228
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_n0I8lpLGdsOmHJ4MWsJt.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_21d82ce8-37a5-431e-9a89-c211b4e79c21_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_21d82ce8-37a5-431e-9a89-c211b4e79c21_poly_0_">
0.995475553992311 0.0007796608990498096 0.995475553992311 0.9990273487374443 0.0039639767125216885 0.9992215036898586 0.0039639767125216885 0.0007796608990498096 0.995475553992311 0.0007796608990498096
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_mnb56nFG8qY7TLDrWX3O.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_3b55fe3d-558e-4980-848a-ea8bb08af045_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_3b55fe3d-558e-4980-848a-ea8bb08af045_poly_0_">
0.9174190993043823 0.0012325802030073458 0.9174190993043541 0.9822853413754199 0.061283661796821334 0.998429462683471 0.06128366179684974 0.0012325802030073458 0.9174190993043823 0.0012325802030073458
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_P3FsnKVtRNgblNl6udIN.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_c1dc10bd-a2c3-4a5b-821f-51fd1290e206_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_c1dc10bd-a2c3-4a5b-821f-51fd1290e206_poly_0_">
0.9928845720308614 0.0010886647281806321 0.9928845720308617 0.9987447645302658 0.007460480198693276 0.998914780907192 0.007460480198693274 0.0010886647281806321 0.9928845720308614 0.0010886647281806321
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_hFe2I17HfVIv1NEmhzlF.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_faf10dd1-f63e-4bac-8354-61aefe528edf_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_faf10dd1-f63e-4bac-8354-61aefe528edf_poly_0_">
0.9271159701213066 0.0010222332264695124 0.927115970121264 0.9820308727833827 0.05014351598561007 0.9986936805729566 0.050143515985652705 0.0010222332264695124 0.9271159701213066 0.0010222332264695124
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_rm4iZz8GthxxA331lH44.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_36f622f5-7adf-46aa-b078-8d1a1caab1f2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_36f622f5-7adf-46aa-b078-8d1a1caab1f2_poly_0_">
0.9459949676385404 0.0010890972362696304 0.9459949676385828 0.9985912069869782 0.0689031916044058 0.9822164208626978 0.06890319160436319 0.0010890972362696304 0.9459949676385404 0.0010890972362696304
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_lDLXe5UGXqMme0bW38t0.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_049a8c69-4783-45cf-aad7-6c3086eeff7d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_049a8c69-4783-45cf-aad7-6c3086eeff7d_poly_0_">
0.9936446259821565 0.0009423864091572709 0.9936446259821565 0.9987778018416053 0.006655094390158212 0.9990598196516309 0.006655094390158212 0.0009423864091572709 0.9936446259821565 0.0009423864091572709
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6232_zt7EAh2uJKVM1KiXLlsN.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_b8d7ec1a-3837-4d5a-9298-0970e9999ce3_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_b8d7ec1a-3837-4d5a-9298-0970e9999ce3_poly_0_">
0.9563032456498775 0.0010238363461146482 0.9563032456498775 0.9986754731117437 0.06579493277251913 0.9822917179676781 0.06579493277251913 0.001023836346114648 0.9563032456498775 0.0010238363461146482
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_5TZUGVsChufbAZHU1fnm.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_659b42db-9365-4195-b9e7-259d33c17dfd_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_659b42db-9365-4195-b9e7-259d33c17dfd_poly_0_">
0.9568271794227741 0.0008397822366157081 0.9568271794227741 0.998909231439694 0.06709657435332161 0.9851926824145706 0.06709657435332161 0.0008397822366157081 0.9568271794227741 0.0008397822366157081
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_tnb6CPZC4Ei5tdzI2WKC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ac7e008d-9d27-47de-9409-98cc387c0c73_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ac7e008d-9d27-47de-9409-98cc387c0c73_poly_0_">
0.9911026483903598 0.0009505968008203411 0.9911026483903809 0.9990478433745218 0.009612829250112752 0.9988200583008471 0.009612829250091437 0.0009505968008203411 0.9911026483903598 0.0009505968008203411
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6228_ojgLBHPnnEvX6LcfvtBB.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_d0abff2a-1f0e-4b53-b2e1-fc9d945427de_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_d0abff2a-1f0e-4b53-b2e1-fc9d945427de_poly_0_">
0.9915895695833896 0.0008984031999630409 0.9915895695833827 0.9990993721841228 0.009044287745794579 0.9987005931187828 0.009044287745801682 0.0008984031999630409 0.9915895695833896 0.0008984031999630409
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234_ONlYx6ugmLYkrOuazTNg.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_">
0.9660123986021745 0.0007907884730376885 0.9660123986021745 0.9988817309000506 0.031810219422595765 0.0007907884730376885 0.9660123986021745 0.0007907884730376885
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234_OQMXSZcc1OkcdDfCda1z.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly.wOcl2uuPM5uxocKFrvWI">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_.VFQy5c7TDv5q09E02hzB">
0.008611743460740227 0.0008195910229355466 0.7562953451754877 0.0008195910229355466 0.9972406602117514 0.9988508434818071 0.008611743460740227 0.0008195910229355466
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234#4.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly.QnGLAGTr5bCBiXhnMvcl">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_.5ij5YoGmtmbKCEKasvu1">
0.00631990333882726 0.9992427682372327 0.00631990333882726 0.0009756347648553431 0.9895504521117289 0.9794486424907183 0.00631990333882726 0.9992427682372327
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6234_XFRqOsjSARCC6r82Ehrm.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly.6tZM7N92oV5dL0Ygcg4a">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_.PbJjDA0KUWATv92kdvfQ">
0.7514450589559163 0.9460136584072105 0.012587493002769182 0.9460136584072105 0.9895461335110057 0.026188208513517 0.7514450589559163 0.9460136584072105
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_gSFtz4vmyZAYWFgcq7Et.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_">
0.9727672074665179 0.0005903660737192931 0.9727672074665179 0.9991651203754534 0.035987887066951885 0.0005903660737192931 0.9727672074665179 0.0005903660737192931
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_PS8hnMZqrLmZM9joatEE.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.wfEkxutbxEJa8GftfSTk">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.1NWn8xqPxIgoIwihePCG">
0.02419565977103843 0.46118758062824244 0.02419565977103843 0.00039277897825673195 0.9957774651602449 0.9996073130221328 0.02419565977103843 0.46118758062824244
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_HjtAAFb5C6NasZ64xkEo.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.8JA8PtbhrMG5yUiF2JYn">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.Oll6AwIyN8NFmIMNN1FV">
0.015888042775124234 0.0009351130216067526 0.9804530504305333 0.0009351130216067526 0.9804530504305333 0.9986679108805258 0.015888042775124234 0.0009351130216067526
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_yWE2gs1P7AkanUGtuOfW.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.8J9rrvBBTOHH7bYApvIV">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.a1VbI8IoFBYn0eTHxEf9">
0.04812196760431675 0.9984352306020579 0.04812196760431675 0.0022127056431092695 0.945517929551059 0.9984352306020579 0.04812196760431675 0.9984352306020579
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6233_TUprWwxN9Wp91gvYF9xl.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.tRILzgKZJ151NhN9xKrZ">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.NaXZnpKkjcl5SoCwPJ7f">
0.9804215460253616 0.9987755983745795 0.015507863031096747 0.9990639107250137 0.015507863031096747 0.001330190813462195 0.9804215460253616 0.9987755983745795
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_vcr6KsmOBHnfCRZoPscy.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_">
0.9609748575007586 0.9991555846556882 0.04759100120756443 0.9990612189236658 0.9609748575007586 0.0011907856515794091 0.9609748575007586 0.9991555846556882
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_6PgWSd4YwrjOuWgNJJ8Z.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly.8aeyj15K1J7IiO4Z5r7X">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_.XZ228onOWIGb8U5eFBmb">
0.047382861721132485 0.0008733664454778263 0.9543907917657606 0.0008733664454778263 0.047382861721132485 0.9987649167265306 0.047382861721132485 0.0008733664454778263
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_j8y0J6EEfchG4cMVi01a.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly.h3xXFQyhVsPKdCPnTd3w">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_.B1VtQWrWAM7TOxsURuXV">
0.04548822756623849 0.9798144441500883 0.975618356188008 0.0011986319255229197 0.975618356188008 0.9989206630061888 0.04548822756623849 0.9798144441500883
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_iHjxNZp6RHbbv70ZHDfp.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly.5TJQBMkHfnswFuIm19iT">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_.j6rSUQnjxGW0oUpf7uci">
0.03582123116424896 0.0008893466566863806 0.9547557536354248 0.0008893466566863806 0.03582123116424896 0.9987423560971112 0.03582123116424896 0.0008893466566863806
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_bmMVR4FBsNdkHxlvY8Vk.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_">
0.978355559899228 0.9989083628773787 0.037920526591392445 0.9756186082994598 0.978355559899228 0.001202223582819568 0.978355559899228 0.9989083628773787
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/distorted_0_IMG_6229.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly.W9gbs0kwL8buYQvQ3Ry9">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_.9MIu75vCRv8vy0T1Aehu">
4.263256414560601e-14 0.8786666666666664 0.9999999999998721 -1.262177448353619e-28 4.263256414560601e-14 0.9999999999999999 4.263256414560601e-14 0.8786666666666664
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_X93Ass3E4vRZsZAGIeV2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly.2NfmfwoTW00WVJRB1bdG">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_.yNdR5Uvmyam7hHAwcr3r">
0.007092899117989049 0.0009229374701114143 0.9919304466109296 0.0009229374701114143 0.7636368385132535 0.9986699078438313 0.007092899117989049 0.0009229374701114143
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6229_YD23wDoMRxpYFYBRlBlZ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly.LBMrsoIZYYA5Sa7Js3Ap">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_.s1dZhULJvWTWpR4y824c">
0.008890132338926549 0.9982583728117017 0.008890132338926549 0.001345124485606038 0.9900396600936574 0.9990995078507887 0.008890132338926549 0.9982583728117017
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_4dSyUtEjmqodxYJXTtmB.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_">
0.9770855389080406 0.0009575364627443725 0.9770855389080406 0.9986461996854078 0.020117419184575436 0.0009575364627443725 0.9770855389080406 0.0009575364627443725
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_Y6SmZO1A7q6owYA26hXb.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.649r3MdoxZRBxmb6HvCA">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.tLQMA8eTJpZ3bDygvosu">
0.0054141589737353845 0.0009589834778722026 0.7514520890555906 0.0009589834778722026 1.0004239181093062 0.9986798876887695 0.0054141589737353845 0.0009589834778722026
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_V1hf4gnZEFeUweWaUiA5.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.r5nAW8eaXHaHPt4waHiR">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.awyQrXgJYbuvEoq4KXPK">
0.8488604311678811 0.998964384527663 0.001234201526096653 0.0013416504231979476 0.995035465076618 0.9990234748986933 0.8488604311678811 0.998964384527663
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_wDAtdtZGmmonX6eH1xHH.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.sAM2vsX5wRdM9FEYmR0C">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.2IFVUcD7gxVYgEFVxufH">
0.0027318017510752413 0.0011509568568291533 0.9922579826217293 0.8205432612905893 0.0027318017510752413 0.9989041402098614 0.0027318017510752413 0.0011509568568291533
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230_TlI3nxzs40qRGqutwung.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.q33btEEap1NRW7FpTMbI">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.PnWOH7ybBMxnIEutqwDf">
0.9926235958196732 0.9128902489593071 0.008120907748655721 0.995794081424406 0.9926235958196732 0.006600625874935593 0.9926235958196732 0.9128902489593071
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906965/IMG_6230.png</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.qpguTn5pCk6llI6hzE8J">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.bZwFVIsESCWy3XQvwxLR">
0.6090582245700539 0.7918255718662025 0.56619242288808 0.7863245674499632 0.615485563886852 0.7850938674134257 0.6090582245700539 0.7918255718662025
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">37.51</bldg:measuredHeight>
<bldg:storeysAboveGround>5</bldg:storeysAboveGround>
<bldg:lod2MultiCurve>
<gml:MultiCurve>
<gml:curveMember>
<gml:LineString>
<gml:posList srsDimension="3">
690911.805 5335960.581 536.23
690911.805 5335960.581 536.246
</gml:posList>
</gml:LineString>
</gml:curveMember>
</gml:MultiCurve>
</bldg:lod2MultiCurve>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_eaaf386d-b354-4c0e-aa92-905f179c18c1">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_eaaf386d-b354-4c0e-aa92-905f179c18c1_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_eaaf386d-b354-4c0e-aa92-905f179c18c1_poly_0_">
<gml:posList srsDimension="3">
690917.923 5335952.576 543.44
690928.159 5335957.216 536.698
690913.209 5335962.968 536.639
690917.923 5335952.576 543.44
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_55522e1b-ac74-4823-89f6-20ed877ca67f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_55522e1b-ac74-4823-89f6-20ed877ca67f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_55522e1b-ac74-4823-89f6-20ed877ca67f_poly_0_">
<gml:posList srsDimension="3">
690908.914 5335952.814 538.72
690911.805 5335960.581 538.72
690902.263 5335964.21 538.72
690899.371 5335956.441 538.72
690908.914 5335952.814 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_9973c3c6-1f98-4ebd-9919-3747c9f5af37">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9973c3c6-1f98-4ebd-9919-3747c9f5af37_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9973c3c6-1f98-4ebd-9919-3747c9f5af37_poly_0_">
<gml:posList srsDimension="3">
690898.862 5335955.073 538.52
690902.263 5335964.21 538.52
690853.078 5335982.903 538.52
690849.676 5335973.771 538.52
690898.862 5335955.073 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_ad25f133-8ae5-4ea6-8473-292f81672129">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ad25f133-8ae5-4ea6-8473-292f81672129_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ad25f133-8ae5-4ea6-8473-292f81672129_poly_0_">
<gml:posList srsDimension="3">
690896.896 5335948.097 552.18
690896.724 5335947.63 552.18
690897.852 5335947.204 552.18
690898.066 5335946.832 552.18
690901.257 5335945.616 552.18
690901.56 5335945.788 552.18
690905.712 5335944.214 552.18
690908.914 5335952.814 552.18
690899.371 5335956.441 552.18
690896.344 5335948.311 552.18
690896.896 5335948.097 552.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_">
<gml:posList srsDimension="3">
690905.712 5335944.214 536.23
690906.01 5335944.101 536.5
690911.805 5335960.581 536.23
690905.712 5335944.214 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly.HM7ZqC5bJHlkSNIBQSRL">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_.C7MMdKn6jqwTuDvcd9U9">
<gml:posList srsDimension="3">
690916.545 5335948.874 543.44
690911.805 5335960.581 536.23
690906.01 5335944.101 536.5
690916.545 5335948.874 543.44
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly.B4BBCUTGRjrxlpO0gqqj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_.bwpUrzW7gYJfupYPY3JE">
<gml:posList srsDimension="3">
690912.155 5335961.522 536.23
690911.805 5335960.581 536.23
690916.545 5335948.874 543.44
690912.155 5335961.522 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly.wNytHZ0z0stX6MnKcWEd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_.tcfngEjK55Uf1pBNQH0N">
<gml:posList srsDimension="3">
690912.694 5335962.969 536.23
690912.155 5335961.522 536.23
690916.545 5335948.874 543.44
690912.694 5335962.969 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly.Lwz1mdJmGWU0Yg7KtlNd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_.Amuil8QGz9VxsxbgvAxM">
<gml:posList srsDimension="3">
690913.146 5335962.796 536.64
690912.694 5335962.969 536.23
690916.545 5335948.874 543.44
690913.146 5335962.796 536.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly.jzYv8X6T1opoU5L9gAQY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_.m5rs1QFUuJh4z2EVIS8c">
<gml:posList srsDimension="3">
690913.209 5335962.968 536.639
690913.146 5335962.796 536.64
690916.545 5335948.874 543.44
690913.209 5335962.968 536.639
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly.N9962fwE95rcgLRpFAqW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3293101-4d63-4895-a6d3-69ecdfda7b8d_poly_0_.DYwYkJPgua3Uhh4efuU8">
<gml:posList srsDimension="3">
690917.923 5335952.576 543.44
690913.209 5335962.968 536.639
690916.545 5335948.874 543.44
690917.923 5335952.576 543.44
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly_0_">
<gml:posList srsDimension="3">
690916.545 5335948.874 543.44
690921.208 5335938.581 536.709
690917.923 5335952.576 543.44
690916.545 5335948.874 543.44
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly.sXRhiDU2sFNT1N748Cry">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly_0_.viTbdE7vIMzgw8QDYEtF">
<gml:posList srsDimension="3">
690928.159 5335957.216 536.698
690917.923 5335952.576 543.44
690921.208 5335938.581 536.709
690928.159 5335957.216 536.698
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly.9AyHoTvlnBhoi0ptMHdM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly_0_.r10qmY7YmrrufUZOOV18">
<gml:posList srsDimension="3">
690921.691 5335938.398 536.272
690928.159 5335957.216 536.698
690921.208 5335938.581 536.709
690921.691 5335938.398 536.272
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly.7lcusTTKNCYtdk4RgSR4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly_0_.7NpqK61WsT9JyaJ7Rrcd">
<gml:posList srsDimension="3">
690921.808 5335938.713 536.272
690928.159 5335957.216 536.698
690921.691 5335938.398 536.272
690921.808 5335938.713 536.272
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly.cj7JHEvuJu4zzUrcEglS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly_0_.Sn9dDD27r7yOYonfaJHB">
<gml:posList srsDimension="3">
690926.083 5335950.21 536.276
690928.159 5335957.216 536.698
690921.808 5335938.713 536.272
690926.083 5335950.21 536.276
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly.oubPuRals2AL1uDkVSzu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b951c5f-1add-479a-aa0a-fb09ca0f461b_poly_0_.4wiycYpmBr3qskSvJ70n">
<gml:posList srsDimension="3">
690928.629 5335957.035 536.272
690928.159 5335957.216 536.698
690926.083 5335950.21 536.276
690928.629 5335957.035 536.272
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_">
<gml:posList srsDimension="3">
690843.149 5335976.251 537.23
690849.676 5335973.771 537.23
690846.742 5335985.312 537.23
690843.149 5335976.251 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.YnvS1PVbSCg4GhQe3w9o">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.DO5poDuEyetT0pQGySZu">
<gml:posList srsDimension="3">
690849.347 5335991.88 537.23
690846.742 5335985.312 537.23
690849.676 5335973.771 537.23
690849.347 5335991.88 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.DarJeYe1QOYHAW3equNy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.CoKyL8pWbJ2JvdPgG6jA">
<gml:posList srsDimension="3">
690849.824 5335991.698 537.228
690849.347 5335991.88 537.23
690849.676 5335973.771 537.23
690849.824 5335991.698 537.228
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.aDF6ze917YHkf0zHZzOl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.p594HGaB08s2fKqgGa4Y">
<gml:posList srsDimension="3">
690853.293 5335983.481 537.23
690849.824 5335991.698 537.228
690849.676 5335973.771 537.23
690853.293 5335983.481 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.t08Izeskn7w83wf5Nq73">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.tkY79x5NtuO3m7GkURvs">
<gml:posList srsDimension="3">
690852.457 5335990.697 537.23
690849.824 5335991.698 537.228
690853.293 5335983.481 537.23
690852.457 5335990.697 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.qulpEuX9bYY4W6jqmZS3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.P28DzqBBLjkmGX9fzj4s">
<gml:posList srsDimension="3">
690852.967 5335990.354 537.23
690852.457 5335990.697 537.23
690853.293 5335983.481 537.23
690852.967 5335990.354 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.JqfYPl5MBFBNTDixmIka">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.yWHjBtiyPda6eNAmxT8J">
<gml:posList srsDimension="3">
690853.456 5335989.863 537.23
690852.967 5335990.354 537.23
690853.293 5335983.481 537.23
690853.456 5335989.863 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.WMIhPdhA13xU9yQ5ZqDn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.MUUn7QqkAaseVX6BR4aI">
<gml:posList srsDimension="3">
690853.565 5335985.545 537.23
690853.456 5335989.863 537.23
690853.293 5335983.481 537.23
690853.565 5335985.545 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.F9LrKh5TeReAspC2fJGh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.LKBXwnAEmedlAoBdPoT7">
<gml:posList srsDimension="3">
690854.0 5335985.379 537.23
690853.565 5335985.545 537.23
690853.293 5335983.481 537.23
690854.0 5335985.379 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.q6JNKtJRQfG7Ptfrl4j9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.P5RILt57PH9du0Y6Tusf">
<gml:posList srsDimension="3">
690853.456 5335989.863 537.23
690853.565 5335985.545 537.23
690853.695 5335989.489 537.23
690853.456 5335989.863 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.ssDWkqkjZKQ4FjlY5aQM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.13rLsP3HnCNdcYiPtKFN">
<gml:posList srsDimension="3">
690854.154 5335987.109 537.23
690853.695 5335989.489 537.23
690853.565 5335985.545 537.23
690854.154 5335987.109 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.1cnYxAA7Ckb4uYjYsbaW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.j2fR7fOI7TpdPltvI4vk">
<gml:posList srsDimension="3">
690853.83 5335989.277 537.23
690853.695 5335989.489 537.23
690854.154 5335987.109 537.23
690853.83 5335989.277 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.w6ly0IBOL33DnCgORevQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.gAR7OhJZ5MReayTrSQWJ">
<gml:posList srsDimension="3">
690854.04 5335988.765 537.23
690853.83 5335989.277 537.23
690854.154 5335987.109 537.23
690854.04 5335988.765 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.oZg3GxTgIlCZYCXh8NH5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.rom1ZcSwKslT8uUqUxxX">
<gml:posList srsDimension="3">
690854.171 5335988.22 537.23
690854.04 5335988.765 537.23
690854.154 5335987.109 537.23
690854.171 5335988.22 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly.dKpSGqTWnIwijdtzulWr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6b6ad86-ad4a-4ef0-8293-bfa86203fadc_poly_0_.PJp3xvqdX0yXbsCsv0o4">
<gml:posList srsDimension="3">
690854.203 5335987.661 537.23
690854.171 5335988.22 537.23
690854.154 5335987.109 537.23
690854.203 5335987.661 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly_0_">
<gml:posList srsDimension="3">
690832.325 5335989.668 535.819
690836.008 5335979.881 542.5
690832.497 5335990.11 535.414
690832.325 5335989.668 535.819
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly.mOg1T5xLqZkdURdZDcLd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly_0_.QpMOvvloQlJ9yPsjnbZo">
<gml:posList srsDimension="3">
690832.791 5335990.011 535.404
690832.497 5335990.11 535.414
690836.008 5335979.881 542.5
690832.791 5335990.011 535.404
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly.qsXjje44Y7DQXkY1V3c5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly_0_.JNwr1RzfWkhhwKbYFDy1">
<gml:posList srsDimension="3">
690832.995 5335990.541 534.92
690832.791 5335990.011 535.404
690836.008 5335979.881 542.5
690832.995 5335990.541 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly.YNtFLMXwdHqkoKgTTLlM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly_0_.QHS4eDtbYExJDmZIPMfV">
<gml:posList srsDimension="3">
690835.69 5335989.515 534.92
690832.995 5335990.541 534.92
690836.008 5335979.881 542.5
690835.69 5335989.515 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly.XisfGwyjp7zkNCKRJaJ3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly_0_.nNJQPdf889yUfbk4Vs9a">
<gml:posList srsDimension="3">
690841.754 5335987.208 534.92
690835.69 5335989.515 534.92
690836.008 5335979.881 542.5
690841.754 5335987.208 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly.7QntQfycHzDFxEh93ZRS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b7d8e72-7614-4476-973c-d02251cc7b50_poly_0_.m0XFi1uNg5jhbOxI4zOt">
<gml:posList srsDimension="3">
690846.742 5335985.312 534.92
690841.754 5335987.208 534.92
690836.008 5335979.881 542.5
690846.742 5335985.312 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_">
<gml:posList srsDimension="3">
690854.0 5335985.379 532.006
690853.293 5335983.481 537.23
690888.455 5335972.219 532.159
690854.0 5335985.379 532.006
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly.XnqRnJdHRcGB9bZzwDYH">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_.MX73yDjiBwS79qDWigzL">
<gml:posList srsDimension="3">
690903.861 5335966.346 532.203
690888.455 5335972.219 532.159
690853.293 5335983.481 537.23
690903.861 5335966.346 532.203
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly.HTDpvlGc06PC2pvSAqIJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_.mN74bqQtVVY0ct0Gr24b">
<gml:posList srsDimension="3">
690912.694 5335962.969 532.248
690903.861 5335966.346 532.203
690853.293 5335983.481 537.23
690912.694 5335962.969 532.248
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly.PuhAqFPPj1bBamCW5vuM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_.s9P1IocWp0L4K6PJleuU">
<gml:posList srsDimension="3">
690912.155 5335961.522 536.23
690912.694 5335962.969 532.248
690853.293 5335983.481 537.23
690912.155 5335961.522 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly.KAJYiOgGd31t858ucgSd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_.g1LQghdGBsu8peoL6Dnh">
<gml:posList srsDimension="3">
690911.805 5335960.581 538.82
690912.155 5335961.522 536.23
690853.293 5335983.481 537.23
690911.805 5335960.581 538.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly.9mmOJJymuAPbGMXu2f7O">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_.fcnASdkghGuTf8W9xkre">
<gml:posList srsDimension="3">
690853.078 5335982.903 538.82
690911.805 5335960.581 538.82
690853.293 5335983.481 537.23
690853.078 5335982.903 538.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly.MbQNQGwpWiWkgq6YLkII">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ff55559-b00d-4504-b5bd-5582b7a76c98_poly_0_.4i2U1ByOjY0c2kpy4no4">
<gml:posList srsDimension="3">
690888.455 5335972.219 532.159
690903.861 5335966.346 532.203
690890.447 5335971.466 532.15
690888.455 5335972.219 532.159
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_">
<gml:posList srsDimension="3">
690849.824 5335991.698 537.228
690852.457 5335990.697 535.678
690852.391 5335990.742 535.72
690849.824 5335991.698 537.228
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.7QZFepxQPIPsmnEeOZQd">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.MPHmaxYwLXhhknGaaMn6">
<gml:posList srsDimension="3">
690858.816 5336005.414 535.41
690860.673 5336010.141 535.418
690860.125 5336010.35 535.741
690858.816 5336005.414 535.41
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.8CefRJqdqdkw0zrnBOfl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.hPcM9liFeIaFVaRwbCMW">
<gml:posList srsDimension="3">
690841.754 5335987.208 540.45
690846.742 5335985.312 537.499
690855.111 5336020.894 540.45
690841.754 5335987.208 540.45
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.5thfEpFzTJDF011hcdW0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.gVkWbgxlnDYSopv3MmYH">
<gml:posList srsDimension="3">
690849.347 5335991.88 537.505
690855.111 5336020.894 540.45
690846.742 5335985.312 537.499
690849.347 5335991.88 537.505
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.k1uJRpUsxA9kl4jDlePV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.RFOzTdDUnft7IC7iMLlp">
<gml:posList srsDimension="3">
690849.824 5335991.698 537.228
690855.111 5336020.894 540.45
690849.347 5335991.88 537.505
690849.824 5335991.698 537.228
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.7Nbgygy0q7z1jqLKEva1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.JqsKO2QFx73G4GDdbqFQ">
<gml:posList srsDimension="3">
690852.391 5335990.742 535.72
690855.111 5336020.894 540.45
690849.824 5335991.698 537.228
690852.391 5335990.742 535.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.j6XXHYPs8VEXq2ZMuWwr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.mG6C5ccxj91hfmJ0ZPKT">
<gml:posList srsDimension="3">
690858.257 5336005.623 535.738
690855.111 5336020.894 540.45
690852.391 5335990.742 535.72
690858.257 5336005.623 535.738
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.jpaeYoh3a2cU6vi9efmU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.FoccpnS9od6PtWGxUFSX">
<gml:posList srsDimension="3">
690863.058 5336017.78 535.748
690855.111 5336020.894 540.45
690858.257 5336005.623 535.738
690863.058 5336017.78 535.748
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.6OTPSeWyvXG04YRIHfAl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.mejhfeB3hp6VEAJClefB">
<gml:posList srsDimension="3">
690860.125 5336010.35 535.741
690863.058 5336017.78 535.748
690858.257 5336005.623 535.738
690860.125 5336010.35 535.741
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly.b6dgNdtIorOMYDe8X2w1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d126460-5f38-48a1-adba-8ce4f50456bb_poly_0_.ueHYubA9fWtlxDnXPC60">
<gml:posList srsDimension="3">
690858.816 5336005.414 535.41
690860.125 5336010.35 535.741
690858.257 5336005.623 535.738
690858.816 5336005.414 535.41
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_">
<gml:posList srsDimension="3">
690910.581 5335942.628 536.712
690912.54 5335941.894 536.721
690910.689 5335942.923 536.977
690910.581 5335942.628 536.712
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.ZzPuKsAFsphTR7hWtZYn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.jai8F5743ahg8NVCmlIx">
<gml:posList srsDimension="3">
690919.71 5335939.151 536.709
690921.208 5335938.581 536.709
690919.829 5335939.446 536.978
690919.71 5335939.151 536.709
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.E9ARqo5EPUBqYfhEfOAU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.720icysBJEnmfdY7lL4a">
<gml:posList srsDimension="3">
690915.14 5335940.895 536.713
690917.111 5335940.141 536.709
690915.259 5335941.18 536.974
690915.14 5335940.895 536.713
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.F0NMjAh2h6a9ul6EnhsW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.mKsqMgEP9Q5T8asXBfxC">
<gml:posList srsDimension="3">
690916.545 5335948.874 543.44
690915.259 5335941.18 536.974
690917.111 5335940.141 536.709
690916.545 5335948.874 543.44
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.vIXZ3f21Z2LCd7yH7RZJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.d357pkTPz6tVHC0Pva9S">
<gml:posList srsDimension="3">
690917.229 5335940.436 536.978
690916.545 5335948.874 543.44
690917.111 5335940.141 536.709
690917.229 5335940.436 536.978
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.DsYGHN28hAaGPPrCJ9II">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.c7y9BfHbaMf824O9ZSTB">
<gml:posList srsDimension="3">
690919.829 5335939.446 536.978
690916.545 5335948.874 543.44
690917.229 5335940.436 536.978
690919.829 5335939.446 536.978
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.RZCHNHahTqvLu5B7hQeY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.D2oQvM0GcrqxXJTItY53">
<gml:posList srsDimension="3">
690921.208 5335938.581 536.709
690916.545 5335948.874 543.44
690919.829 5335939.446 536.978
690921.208 5335938.581 536.709
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.NyvUP6j65To8RKoJotQU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.y5GEKaq0qhWYFYVNJrGx">
<gml:posList srsDimension="3">
690916.545 5335948.874 543.44
690906.01 5335944.101 536.5
690906.925 5335943.997 536.695
690916.545 5335948.874 543.44
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.FawWJd0SWWmx4phtyz5b">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.yljJuwM27Fn22ZtZ2jJb">
<gml:posList srsDimension="3">
690906.823 5335943.793 536.503
690906.925 5335943.997 536.695
690906.01 5335944.101 536.5
690906.823 5335943.793 536.503
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.AcZJf3ldUjWg1WVZLKgr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.C2xq3lWwGvY8dS18tKFj">
<gml:posList srsDimension="3">
690908.089 5335943.922 536.986
690916.545 5335948.874 543.44
690906.925 5335943.997 536.695
690908.089 5335943.922 536.986
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.MFI46PHIsRU0F333rvd4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.ElQ9MXB4jGGng3Dis5vJ">
<gml:posList srsDimension="3">
690907.97 5335943.627 536.717
690908.089 5335943.922 536.986
690906.925 5335943.997 536.695
690907.97 5335943.627 536.717
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.38zH7yxSgCUfUeolvjyD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.K7o0FxAV5GwyVzu4SBps">
<gml:posList srsDimension="3">
690910.689 5335942.923 536.977
690916.545 5335948.874 543.44
690908.089 5335943.922 536.986
690910.689 5335942.923 536.977
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.TSeK4QWRwd38MKu3ovV5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.O2KSuIqOKeVLmHpTWZkv">
<gml:posList srsDimension="3">
690912.649 5335942.179 536.978
690916.545 5335948.874 543.44
690910.689 5335942.923 536.977
690912.649 5335942.179 536.978
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.ckha4PtL3js8A0iB2al9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.2WrP1lsSfjLqW89cwFFE">
<gml:posList srsDimension="3">
690912.54 5335941.894 536.721
690912.649 5335942.179 536.978
690910.689 5335942.923 536.977
690912.54 5335941.894 536.721
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly.PSagaQjGcdYesNitXBCA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_91f069e0-f880-4f5d-90f9-9e488c4e7676_poly_0_.mA9g8ElyVZy0srodbZIW">
<gml:posList srsDimension="3">
690915.259 5335941.18 536.974
690916.545 5335948.874 543.44
690912.649 5335942.179 536.978
690915.259 5335941.18 536.974
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_">
<gml:posList srsDimension="3">
690832.995 5335990.541 532.463
690835.69 5335989.515 534.92
690833.672 5335992.297 532.448
690832.995 5335990.541 532.463
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.mrYmAZChpEqxcmsNMALC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.zNXEDuW3aZuOh0GK1UMn">
<gml:posList srsDimension="3">
690834.036 5335994.022 532.193
690833.672 5335992.297 532.448
690835.69 5335989.515 534.92
690834.036 5335994.022 532.193
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.tR4fd1Tm6QXsr7hkswuF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.sL5afiScfoWTCOUDq5vy">
<gml:posList srsDimension="3">
690834.219 5335993.949 532.361
690834.036 5335994.022 532.193
690835.69 5335989.515 534.92
690834.219 5335993.949 532.361
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.nA4u7RzPhSwkjSU4QOdK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.3qKkJpOqecREflSkfM4E">
<gml:posList srsDimension="3">
690835.14 5335996.257 532.366
690834.219 5335993.949 532.361
690835.69 5335989.515 534.92
690835.14 5335996.257 532.366
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.9C9YeYVGNW3J1crfRnWu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.rXvWbABVL1Q0wotbo7qp">
<gml:posList srsDimension="3">
690835.585 5335997.915 532.198
690835.14 5335996.257 532.366
690835.69 5335989.515 534.92
690835.585 5335997.915 532.198
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.Q2rOVUnmMCnN0sXRpgBI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.NWocK3R3wq6HAhkV2q62">
<gml:posList srsDimension="3">
690835.778 5335997.852 532.37
690835.585 5335997.915 532.198
690835.69 5335989.515 534.92
690835.778 5335997.852 532.37
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.pGTvloDzGGhv3dqyMARK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.e4WxYEuJdWAc077h6sbR">
<gml:posList srsDimension="3">
690841.754 5335987.208 540.45
690835.778 5335997.852 532.37
690835.69 5335989.515 534.92
690841.754 5335987.208 540.45
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.uVDVk6JlqcTkRR22KptR">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.8DCMJVt4kbipxmMxENyI">
<gml:posList srsDimension="3">
690836.689 5336000.159 532.367
690835.778 5335997.852 532.37
690841.754 5335987.208 540.45
690836.689 5336000.159 532.367
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.mPIDywkHqM85uK3Qa7TS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.7hYeRk9gQRzAruDoYEMS">
<gml:posList srsDimension="3">
690837.124 5336001.827 532.188
690836.689 5336000.159 532.367
690841.754 5335987.208 540.45
690837.124 5336001.827 532.188
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.EJ2hpIP4eZfuT3iBnKAw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.5U7DiohZlhhqrhmewzeo">
<gml:posList srsDimension="3">
690837.337 5336001.765 532.376
690837.124 5336001.827 532.188
690841.754 5335987.208 540.45
690837.337 5336001.765 532.376
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.pSvnozUWayAGqljSIGh9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.vP9K8610UKe5KAY2w0yz">
<gml:posList srsDimension="3">
690838.238 5336004.072 532.365
690837.337 5336001.765 532.376
690841.754 5335987.208 540.45
690838.238 5336004.072 532.365
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.C01z4g6PqBnJjD7cX9Cs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.XCZm4PmEay7uyHiBQPqu">
<gml:posList srsDimension="3">
690838.673 5336005.74 532.186
690838.238 5336004.072 532.365
690841.754 5335987.208 540.45
690838.673 5336005.74 532.186
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.dKH8yuAfR2kwdpsJJ0O2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.tRplfl2xev3brMBOXJRz">
<gml:posList srsDimension="3">
690838.866 5336005.668 532.362
690838.673 5336005.74 532.186
690841.754 5335987.208 540.45
690838.866 5336005.668 532.362
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.OSLnoM39jjqpqp2eXhdr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.KRQbwM2md6t650QVxuye">
<gml:posList srsDimension="3">
690839.787 5336007.975 532.366
690838.866 5336005.668 532.362
690841.754 5335987.208 540.45
690839.787 5336007.975 532.366
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.zCfXk7XP1u6a9GkwtbGV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.DbaL5kvQvEvQAweqN7cu">
<gml:posList srsDimension="3">
690840.222 5336009.653 532.184
690839.787 5336007.975 532.366
690841.754 5335987.208 540.45
690840.222 5336009.653 532.184
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.03LJQSXjCRkJGScIkY8K">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.gQi6syWf4S5eFslVCFqn">
<gml:posList srsDimension="3">
690840.517 5336009.535 532.455
690840.222 5336009.653 532.184
690841.754 5335987.208 540.45
690840.517 5336009.535 532.455
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.CMo97HHfbgpMJ1uq3tbb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.vxJskHjXj9sowG3c1Ivz">
<gml:posList srsDimension="3">
690841.428 5336011.842 532.452
690840.517 5336009.535 532.455
690841.754 5335987.208 540.45
690841.428 5336011.842 532.452
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.5gFZykDTq5DFWedIWBYV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.U6ZVtahVi1sTrZcyEi1k">
<gml:posList srsDimension="3">
690841.144 5336011.951 532.192
690841.428 5336011.842 532.452
690841.772 5336013.557 532.185
690841.144 5336011.951 532.192
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.gjEV9a8CN4rSUtjHmsxK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.tepZr3EHVrpcolZMPC8r">
<gml:posList srsDimension="3">
690841.754 5335987.208 540.45
690841.772 5336013.557 532.185
690841.428 5336011.842 532.452
690841.754 5335987.208 540.45
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.HhQzb4Cr4SHKSgVcxy4Y">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.odydDw0xEbyFd50PNRjv">
<gml:posList srsDimension="3">
690842.067 5336013.428 532.46
690841.772 5336013.557 532.185
690841.754 5335987.208 540.45
690842.067 5336013.428 532.46
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.F1rWaVmXYnvHCYRslczs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.keLNU9ppMgzzzdUj1JL4">
<gml:posList srsDimension="3">
690855.111 5336020.894 540.45
690842.067 5336013.428 532.46
690841.754 5335987.208 540.45
690855.111 5336020.894 540.45
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.CoyU5oIotOjymt7i2ywc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.u53RjKGq8aLG1enYMDb8">
<gml:posList srsDimension="3">
690842.978 5336015.736 532.456
690842.067 5336013.428 532.46
690855.111 5336020.894 540.45
690842.978 5336015.736 532.456
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.SwsHFBg21Fkjl6nZtyTx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.VHY05yEeqIqg5d6Bv7SC">
<gml:posList srsDimension="3">
690843.616 5336017.342 532.457
690842.978 5336015.736 532.456
690855.111 5336020.894 540.45
690843.616 5336017.342 532.457
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.BtNANiUPlAqrj114aTWt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.dX9IOFT9qV7jy8meyWWS">
<gml:posList srsDimension="3">
690843.331 5336017.461 532.194
690842.978 5336015.736 532.456
690843.616 5336017.342 532.457
690843.331 5336017.461 532.194
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.UuEkOmnHn2SKvOl1lXsm">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.SRRpedtuVPRkA18jSaZa">
<gml:posList srsDimension="3">
690844.537 5336019.65 532.462
690843.616 5336017.342 532.457
690855.111 5336020.894 540.45
690844.537 5336019.65 532.462
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.i8q7plc9v8cS77KzsPXC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.dUZMVTCnOTZ4duJ3gEPX">
<gml:posList srsDimension="3">
690845.175 5336021.256 532.463
690844.537 5336019.65 532.462
690855.111 5336020.894 540.45
690845.175 5336021.256 532.463
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.kfI0fRGaeQXRuqDAhDWB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.Q411jw5W9IvOl7wgFUbx">
<gml:posList srsDimension="3">
690844.881 5336021.365 532.196
690844.537 5336019.65 532.462
690845.175 5336021.256 532.463
690844.881 5336021.365 532.196
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.RQuuDLWXN4laRXFwctOw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.lGbb2kGa0BzGdM6eoCld">
<gml:posList srsDimension="3">
690846.097 5336023.554 532.471
690845.175 5336021.256 532.463
690855.111 5336020.894 540.45
690846.097 5336023.554 532.471
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.uzSE2lJ7eA8sgQleRt5d">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.zaZz3MzRK9gftNhda0Vl">
<gml:posList srsDimension="3">
690846.103 5336024.425 532.202
690846.097 5336023.554 532.471
690855.111 5336020.894 540.45
690846.103 5336024.425 532.202
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.Ppf3ISmR2looCKFHY1ht">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.KhXC4rnTwIvq7QNY6Jsd">
<gml:posList srsDimension="3">
690846.097 5336023.554 532.471
690846.103 5336024.425 532.202
690845.801 5336023.683 532.197
690846.097 5336023.554 532.471
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.FciE4c3pcJNbFrvXLAav">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.AYR78NPpmlIS0LsElP1z">
<gml:posList srsDimension="3">
690844.537 5336019.65 532.462
690844.881 5336021.365 532.196
690844.242 5336019.778 532.188
690844.537 5336019.65 532.462
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.DZuBYcNBEAR988G7g78T">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.dT8sFlQaWIwamaFfnRoo">
<gml:posList srsDimension="3">
690842.978 5336015.736 532.456
690843.331 5336017.461 532.194
690842.692 5336015.875 532.186
690842.978 5336015.736 532.456
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.wibqOO0iBHADiRIq4CpE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.ERHv1cUqYPEpCwi6AysW">
<gml:posList srsDimension="3">
690839.787 5336007.975 532.366
690840.222 5336009.653 532.184
690839.594 5336008.068 532.184
690839.787 5336007.975 532.366
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.50YRCszbuuD4LluEkbGq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.3DfI1XhV5jXYMlAJgqgp">
<gml:posList srsDimension="3">
690838.238 5336004.072 532.365
690838.673 5336005.74 532.186
690838.045 5336004.155 532.186
690838.238 5336004.072 532.365
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.BOZY9Ba58yRg86vq5a6H">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.gv68pzXg4uCb9zgbruaG">
<gml:posList srsDimension="3">
690836.689 5336000.159 532.367
690837.124 5336001.827 532.188
690836.496 5336000.232 532.192
690836.689 5336000.159 532.367
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.wOjJlJLvGZfLvmZe4k9a">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.rgYgJoqoc2LCP2DftE3p">
<gml:posList srsDimension="3">
690835.14 5335996.257 532.366
690835.585 5335997.915 532.198
690834.957 5335996.329 532.198
690835.14 5335996.257 532.366
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly.KqIn5OWzPujVu1x26myS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a277e7cc-3e56-4cc5-a35e-326da5dbfb11_poly_0_.5Qx1afjYr95Um1SQaErz">
<gml:posList srsDimension="3">
690833.672 5335992.297 532.448
690834.036 5335994.022 532.193
690833.407 5335992.427 532.197
690833.672 5335992.297 532.448
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_">
<gml:posList srsDimension="3">
690831.421 5335972.512 535.24
690835.181 5335971.078 535.236
690831.52 5335972.796 535.497
690831.421 5335972.512 535.24
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.BGMaf9yWqD3oRuav3GSC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.nNeRa5av1LXgu92zs7Oy">
<gml:posList srsDimension="3">
690837.782 5335970.079 535.228
690840.014 5335969.236 535.232
690837.881 5335970.373 535.492
690837.782 5335970.079 535.228
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.9gbBxQ72c2g0lX00UZjq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.hZKFxG7rCo7DiWZCoppM">
<gml:posList srsDimension="3">
690825.803 5335974.645 535.238
690828.81 5335973.501 535.237
690825.838 5335974.733 535.319
690825.803 5335974.645 535.238
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.f06jbtpJZdjLdaF7Dmqs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.FPel8VMhnjYxreeluA3a">
<gml:posList srsDimension="3">
690828.919 5335973.786 535.497
690825.838 5335974.733 535.319
690828.81 5335973.501 535.237
690828.919 5335973.786 535.497
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.gNDG7aQSVhwxUl7YTVmC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.haNXzIzrz57fG3jl9irK">
<gml:posList srsDimension="3">
690836.008 5335979.881 542.5
690825.838 5335974.733 535.319
690828.919 5335973.786 535.497
690836.008 5335979.881 542.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.jvvChIkYBn6zKo3CVI1u">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.COXHWHElSP7kXZ9vw4If">
<gml:posList srsDimension="3">
690831.52 5335972.796 535.497
690836.008 5335979.881 542.5
690828.919 5335973.786 535.497
690831.52 5335972.796 535.497
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.EZxGV0lhg0aKtPz4ijJ0">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.fXd4bi5wsAE8uaufbNbC">
<gml:posList srsDimension="3">
690835.181 5335971.078 535.236
690836.008 5335979.881 542.5
690831.52 5335972.796 535.497
690835.181 5335971.078 535.236
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.MVV7vOBEPhmapSWbZFig">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.Qm0O7KQaLHvG5Ix7nrwR">
<gml:posList srsDimension="3">
690835.29 5335971.363 535.496
690836.008 5335979.881 542.5
690835.181 5335971.078 535.236
690835.29 5335971.363 535.496
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.DphW4ATQzgMScXKibOaa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.jYbEzyPiLaHx3WqDQTvn">
<gml:posList srsDimension="3">
690837.881 5335970.373 535.492
690836.008 5335979.881 542.5
690835.29 5335971.363 535.496
690837.881 5335970.373 535.492
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly.NSQaXQquIhBbesZmfX0p">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f69c36af-9279-4eff-afbc-3225eb52263c_poly_0_.6ClRst14Ru1BvpSyxyqa">
<gml:posList srsDimension="3">
690840.014 5335969.236 535.232
690836.008 5335979.881 542.5
690837.881 5335970.373 535.492
690840.014 5335969.236 535.232
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly_0_">
<gml:posList srsDimension="3">
690836.008 5335979.881 542.5
690840.014 5335969.236 535.232
690846.742 5335985.312 534.92
690836.008 5335979.881 542.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly.7Tdo7tf2QH9MrTa0rayl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly_0_.Cu8He8mvmqS6F0iGTnVh">
<gml:posList srsDimension="3">
690841.688 5335972.567 534.92
690846.742 5335985.312 534.92
690840.321 5335969.12 534.92
690841.688 5335972.567 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly.BSOCek9EYmNQmxzfdGAE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly_0_.fw3dfecA9JNa7Vqp2S07">
<gml:posList srsDimension="3">
690840.014 5335969.236 535.232
690840.321 5335969.12 534.92
690846.742 5335985.312 534.92
690840.014 5335969.236 535.232
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly.BSOCek9EYmNQmxzfdGAE.2yuWvmkncEXpFddpPJsq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_07d9890d-4820-4921-ad43-f0f7a9b29df0_poly_0_.fw3dfecA9JNa7Vqp2S07.ZWsNBORfFLUkoS0lZhiI">
<gml:posList srsDimension="3">
690841.688 5335972.567 534.92
690846.742 5335985.312 534.92
690840.321 5335969.12 534.92
690841.688 5335972.567 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_">
<gml:posList srsDimension="3">
690826.988 5335977.624 535.322
690825.838 5335974.733 535.319
690827.292 5335977.526 535.625
690826.988 5335977.624 535.322
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.nokeGm1kODXPbms1AtGe">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.vwoJmoXD5vU3vIzd8VhN">
<gml:posList srsDimension="3">
690836.008 5335979.881 542.5
690827.292 5335977.526 535.625
690825.838 5335974.733 535.319
690836.008 5335979.881 542.5
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.eeGLZBiL757lP7hDVUfb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.zWr5FoEq0fMyubm7Z7YG">
<gml:posList srsDimension="3">
690828.313 5335980.098 535.626
690827.292 5335977.526 535.625
690836.008 5335979.881 542.5
690828.313 5335980.098 535.626
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.3FdimV5x3Qg9X8hT3WsP">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.r2cbqCG36sXx4zNQFpN1">
<gml:posList srsDimension="3">
690829.524 5335983.939 535.351
690828.313 5335980.098 535.626
690836.008 5335979.881 542.5
690829.524 5335983.939 535.351
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.cF8UWqUlbTQZK5uS1tpw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.rKvQKAQWZ0JaaZDnZMQg">
<gml:posList srsDimension="3">
690829.778 5335983.839 535.611
690829.524 5335983.939 535.351
690836.008 5335979.881 542.5
690829.778 5335983.839 535.611
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.GTIjOJmmDe6BkB87EmSU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.DtXKjXikJBHtDJxah9i2">
<gml:posList srsDimension="3">
690830.809 5335986.421 535.617
690829.778 5335983.839 535.611
690836.008 5335979.881 542.5
690830.809 5335986.421 535.617
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.hHsWIYcL9VkFanw1k6sR">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.owV1hW3E969A1fmdi5Q4">
<gml:posList srsDimension="3">
690831.718 5335989.529 535.332
690830.809 5335986.421 535.617
690836.008 5335979.881 542.5
690831.718 5335989.529 535.332
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.OXhirNfxJaZBh4vWxLeq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.7cwObRb8hbo4J0HQ2uF5">
<gml:posList srsDimension="3">
690832.196 5335989.338 535.821
690831.718 5335989.529 535.332
690836.008 5335979.881 542.5
690832.196 5335989.338 535.821
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.i0NnIs247aYKcZpiyTgr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.76skl5K2nyBUPGWgbz4L">
<gml:posList srsDimension="3">
690832.325 5335989.668 535.819
690832.196 5335989.338 535.821
690836.008 5335979.881 542.5
690832.325 5335989.668 535.819
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.YvAEkmufL3AqZ2vQjujq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.LyjrOsSLkM7cOL989t6h">
<gml:posList srsDimension="3">
690830.809 5335986.421 535.617
690831.718 5335989.529 535.332
690830.543 5335986.551 535.337
690830.809 5335986.421 535.617
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly.3hg5tqF7JUuZBunztP76">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_42f21bb1-7fc4-412d-8fc3-17c96e3ff7a0_poly_0_.TvNGRPXRLUKppCTWbscb">
<gml:posList srsDimension="3">
690828.313 5335980.098 535.626
690829.524 5335983.939 535.351
690828.037 5335980.228 535.338
690828.313 5335980.098 535.626
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_">
<gml:posList srsDimension="3">
690844.925 5335967.888 531.991
690846.438 5335967.327 532.003
690845.053 5335968.193 532.292
690844.925 5335967.888 531.991
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.6ZKnugtfzfCnu3n8UjTj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.bMBdCSwNxchXw8OUla9u">
<gml:posList srsDimension="3">
690848.806 5335966.409 531.988
690850.32 5335965.838 531.991
690848.925 5335966.704 532.276
690848.806 5335966.409 531.988
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.VRpz3g7hvEOaAAXZeIzU">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.1hQNHuxPaAJFVtAtBrwy">
<gml:posList srsDimension="3">
690852.687 5335964.939 531.993
690854.191 5335964.368 531.992
690852.806 5335965.234 532.281
690852.687 5335964.939 531.993
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.R3hudiu5yNQCcKguxcGX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.qQgT8l2ufSiJjkYDyixh">
<gml:posList srsDimension="3">
690856.568 5335963.46 531.989
690858.072 5335962.888 531.989
690856.677 5335963.754 532.274
690856.568 5335963.46 531.989
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.N9IsYDQxvfyI8fVDsBTx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.KOY38v9xOkxpVrj6dV0N">
<gml:posList srsDimension="3">
690860.45 5335961.98 531.985
690861.954 5335961.409 531.985
690860.569 5335962.275 532.274
690860.45 5335961.98 531.985
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.Mj0hOYhhzqNqJ7nwcevX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.CpFbGAds726RzFw875HO">
<gml:posList srsDimension="3">
690864.331 5335960.51 531.99
690865.835 5335959.929 531.982
690864.45 5335960.805 532.279
690864.331 5335960.51 531.99
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.8XND6SRNcDcXSAUPc0DY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.5Df3F1NGfWJwGunJlh6R">
<gml:posList srsDimension="3">
690868.212 5335959.03 531.987
690869.716 5335958.458 531.986
690868.321 5335959.335 532.28
690868.212 5335959.03 531.987
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.lgChe8j6uZ05YkfH1vNV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.9RaY1Fu78it66wTkN1mn">
<gml:posList srsDimension="3">
690872.084 5335957.55 531.979
690873.597 5335956.978 531.983
690872.202 5335957.845 532.268
690872.084 5335957.55 531.979
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.ifz3u7HKFAJVz5TS0Ahh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.KunD7DUOlOIYtd7b6lQX">
<gml:posList srsDimension="3">
690875.975 5335956.069 531.979
690877.479 5335955.497 531.979
690876.083 5335956.374 532.273
690875.975 5335956.069 531.979
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.SLyQ18zFJptoCLriZy1c">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.DXdbw4Qgp60ONitcEAQm">
<gml:posList srsDimension="3">
690879.847 5335954.588 531.972
690881.36 5335954.017 531.976
690879.965 5335954.893 532.269
690879.847 5335954.588 531.972
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.VcL4FsX0iAEoEWbtoGTu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.qAojoEycVeby3YoTOrO3">
<gml:posList srsDimension="3">
690883.738 5335953.118 531.981
690885.241 5335952.546 531.98
690883.836 5335953.412 532.262
690883.738 5335953.118 531.981
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.TzPSfBTc7fHYTfZ7StDo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.uWT8F7S4ol7HtdUbPBKq">
<gml:posList srsDimension="3">
690887.619 5335951.627 531.969
690889.123 5335951.066 531.977
690887.727 5335951.942 532.27
690887.619 5335951.627 531.969
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.Yhv8tkF2zCqW6HQdWlkO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.kFpl5QA6C5pXGTFZ8jVe">
<gml:posList srsDimension="3">
690891.501 5335950.148 531.965
690893.004 5335949.586 531.973
690891.609 5335950.462 532.267
690891.501 5335950.148 531.965
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.i8I7T9esJgnqKXpqO0iX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.x7xG8o8x4Q14crFLUg8K">
<gml:posList srsDimension="3">
690895.392 5335948.679 531.973
690896.344 5335948.311 531.968
690895.49 5335948.983 532.263
690895.392 5335948.679 531.973
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.6wJ2YeDLz54ax5sMDUed">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.D63oT11w339CDvlK86DX">
<gml:posList srsDimension="3">
690840.321 5335969.12 531.553
690840.759 5335968.955 531.554
690841.688 5335972.567 534.92
690840.321 5335969.12 531.553
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.otAMGINfhSn9tiEBXgrf">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.y1asm2Op6CKwchLDmZua">
<gml:posList srsDimension="3">
690840.931 5335969.422 532.006
690841.688 5335972.567 534.92
690840.759 5335968.955 531.554
690840.931 5335969.422 532.006
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.wNXYC8nPlhn3HUeuC3nJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.f2rE8o0nKfKoatmG2hUr">
<gml:posList srsDimension="3">
690842.567 5335968.796 532.002
690841.688 5335972.567 534.92
690840.931 5335969.422 532.006
690842.567 5335968.796 532.002
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.cte3PBFZuu0ezLii9bVD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.wvijWcfGwVX3XuMUX2fi">
<gml:posList srsDimension="3">
690843.149 5335976.251 538.52
690841.688 5335972.567 534.92
690842.567 5335968.796 532.002
690843.149 5335976.251 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.C8jBedL1Oe1dzt7yJlFJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.BxNnn2h37r1roexEwWh7">
<gml:posList srsDimension="3">
690842.685 5335969.101 532.299
690843.149 5335976.251 538.52
690842.567 5335968.796 532.002
690842.685 5335969.101 532.299
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.evRwoLo1mPWXOTaq89yk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.8mcShGRs7q3zDN2o4dj3">
<gml:posList srsDimension="3">
690845.053 5335968.193 532.292
690843.149 5335976.251 538.52
690842.685 5335969.101 532.299
690845.053 5335968.193 532.292
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.k5WEXmJrMdoDdR2G3bB3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.pWmhHbqjD0Ia1F9IvmkM">
<gml:posList srsDimension="3">
690898.862 5335955.073 538.52
690843.149 5335976.251 538.52
690845.053 5335968.193 532.292
690898.862 5335955.073 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.sh30jLsmfQYpZMp0rash">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.azQTsVpMr0YmRlAQPsrZ">
<gml:posList srsDimension="3">
690846.547 5335967.611 532.28
690898.862 5335955.073 538.52
690845.053 5335968.193 532.292
690846.547 5335967.611 532.28
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.DNCkIgOhqiNFiwDkfUW9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.RPDVrouQRVI96dDskGV9">
<gml:posList srsDimension="3">
690846.438 5335967.327 532.003
690846.547 5335967.611 532.28
690845.053 5335968.193 532.292
690846.438 5335967.327 532.003
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.JDGoGlpkWELDvmIcDWf3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.5XWUrMpUltT4W39QbQKP">
<gml:posList srsDimension="3">
690848.925 5335966.704 532.276
690898.862 5335955.073 538.52
690846.547 5335967.611 532.28
690848.925 5335966.704 532.276
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.h52P67GifzoRBThrvB7v">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.yAeqM4iwBplVBOVCoEpv">
<gml:posList srsDimension="3">
690850.438 5335966.142 532.288
690898.862 5335955.073 538.52
690848.925 5335966.704 532.276
690850.438 5335966.142 532.288
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.0FLdzbQGWAgpmisloRwy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.TMteK6qmgPktsTddt0Rr">
<gml:posList srsDimension="3">
690850.32 5335965.838 531.991
690850.438 5335966.142 532.288
690848.925 5335966.704 532.276
690850.32 5335965.838 531.991
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.znZBPhjZz3XmqhWFKFCx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.FrXW8BAoMoILz2uJjS6s">
<gml:posList srsDimension="3">
690852.806 5335965.234 532.281
690898.862 5335955.073 538.52
690850.438 5335966.142 532.288
690852.806 5335965.234 532.281
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.9fDfjzdQh0yFV142jCrm">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.zDUDSH9oMwEgjcIZIAd7">
<gml:posList srsDimension="3">
690854.32 5335964.653 532.276
690898.862 5335955.073 538.52
690852.806 5335965.234 532.281
690854.32 5335964.653 532.276
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.ui73Ww8BfoQbsItKd3dA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.83eOQta1uPs7kmZ13gLN">
<gml:posList srsDimension="3">
690854.191 5335964.368 531.992
690854.32 5335964.653 532.276
690852.806 5335965.234 532.281
690854.191 5335964.368 531.992
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.p8GMuNYUOlDfd0rwbO8f">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.b33DtrJJgXIyYgNTctXf">
<gml:posList srsDimension="3">
690856.677 5335963.754 532.274
690898.862 5335955.073 538.52
690854.32 5335964.653 532.276
690856.677 5335963.754 532.274
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.sIxc9EWT6RYn9DixedFg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.LrAF4YRWpMtdV6XJV3wW">
<gml:posList srsDimension="3">
690858.191 5335963.183 532.277
690898.862 5335955.073 538.52
690856.677 5335963.754 532.274
690858.191 5335963.183 532.277
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.hK5OPfQUZiQh7pD0yVPa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.WxETMFKmI3PLPI1OT4Gj">
<gml:posList srsDimension="3">
690858.072 5335962.888 531.989
690858.191 5335963.183 532.277
690856.677 5335963.754 532.274
690858.072 5335962.888 531.989
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.3a2H5VeJyCDYSzqLApEM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.FTZR6YbsAslWhOUjaNZ5">
<gml:posList srsDimension="3">
690860.569 5335962.275 532.274
690898.862 5335955.073 538.52
690858.191 5335963.183 532.277
690860.569 5335962.275 532.274
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.lS1DCqxgZkmOmPMfi0mf">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.Qze2dQDS2xelfAIEezld">
<gml:posList srsDimension="3">
690862.072 5335961.713 532.282
690898.862 5335955.073 538.52
690860.569 5335962.275 532.274
690862.072 5335961.713 532.282
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.wU5ToMjIQ470D9LUFkCt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.dYX4EjSAYVC9h7BHWibm">
<gml:posList srsDimension="3">
690861.954 5335961.409 531.985
690862.072 5335961.713 532.282
690860.569 5335962.275 532.274
690861.954 5335961.409 531.985
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.SDcuQfrmyJNteBvWzGlY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.XEeUd55ViVkQ8XwlmcQf">
<gml:posList srsDimension="3">
690864.45 5335960.805 532.279
690898.862 5335955.073 538.52
690862.072 5335961.713 532.282
690864.45 5335960.805 532.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.oJ0gADax80HG6XwB9Wbl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.PLMS8sk0mkC9dis6sxNI">
<gml:posList srsDimension="3">
690865.953 5335960.233 532.279
690898.862 5335955.073 538.52
690864.45 5335960.805 532.279
690865.953 5335960.233 532.279
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.dHrAFHzQ2HEJeA4shQZ3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.KxMp4WsQODclJLno6Utm">
<gml:posList srsDimension="3">
690865.835 5335959.929 531.982
690865.953 5335960.233 532.279
690864.45 5335960.805 532.279
690865.835 5335959.929 531.982
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.1In2g0XLRkTg9hSeTr8O">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.042jtdXGdu2OoUN9IR8s">
<gml:posList srsDimension="3">
690868.321 5335959.335 532.28
690898.862 5335955.073 538.52
690865.953 5335960.233 532.279
690868.321 5335959.335 532.28
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.j1Fpz4PwJlvW8h0OcOjg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.QX4y0NNGE9IggVxRtVN8">
<gml:posList srsDimension="3">
690869.835 5335958.753 532.275
690898.862 5335955.073 538.52
690868.321 5335959.335 532.28
690869.835 5335958.753 532.275
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.zcAfTrgUd9iPULzH3w1j">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.lViyEPTFr2dZF3vjv81Z">
<gml:posList srsDimension="3">
690869.716 5335958.458 531.986
690869.835 5335958.753 532.275
690868.321 5335959.335 532.28
690869.716 5335958.458 531.986
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.QxE5qCwSJ3v4QkA4g7ER">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.F3oZqgsErOTjHo6icjb2">
<gml:posList srsDimension="3">
690872.202 5335957.845 532.268
690898.862 5335955.073 538.52
690869.835 5335958.753 532.275
690872.202 5335957.845 532.268
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.PGYj8IjTDgDhZroipAIc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.9tKhlnQFxuD21zVd8eXY">
<gml:posList srsDimension="3">
690873.726 5335957.273 532.275
690898.862 5335955.073 538.52
690872.202 5335957.845 532.268
690873.726 5335957.273 532.275
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.G6X7Gtg8MVnvlBjOS2dv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.pZqaSDJeROLsC3BKXcXk">
<gml:posList srsDimension="3">
690873.597 5335956.978 531.983
690873.726 5335957.273 532.275
690872.202 5335957.845 532.268
690873.597 5335956.978 531.983
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.4iRSjJfMWPu1122BFIIC">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.rvDNOMJudDdVdBLwGqvD">
<gml:posList srsDimension="3">
690876.083 5335956.374 532.273
690898.862 5335955.073 538.52
690873.726 5335957.273 532.275
690876.083 5335956.374 532.273
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.ilJM0NXIV0JKhXX9lqhV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.KURrS3zAItbfkjORDjRr">
<gml:posList srsDimension="3">
690877.597 5335955.792 532.268
690898.862 5335955.073 538.52
690876.083 5335956.374 532.273
690877.597 5335955.792 532.268
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.MPsG4Xow3l3aM9Tj4GRa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.1xIHc4S5bD8Z6cY9JfYf">
<gml:posList srsDimension="3">
690877.479 5335955.497 531.979
690877.597 5335955.792 532.268
690876.083 5335956.374 532.273
690877.479 5335955.497 531.979
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.3AtybtowQ7uRSm9XZYW4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.smws0vZHVNBCovn61wrL">
<gml:posList srsDimension="3">
690879.965 5335954.893 532.269
690898.862 5335955.073 538.52
690877.597 5335955.792 532.268
690879.965 5335954.893 532.269
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.srdajDAoxf4raPnhO8IY">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.OADj8d6LnM5ae58rUZsH">
<gml:posList srsDimension="3">
690881.489 5335954.312 532.268
690898.862 5335955.073 538.52
690879.965 5335954.893 532.269
690881.489 5335954.312 532.268
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.85V3rAk5CiyWhOdZcqr2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.AM04PT6mawT45L41lXGx">
<gml:posList srsDimension="3">
690881.36 5335954.017 531.976
690881.489 5335954.312 532.268
690879.965 5335954.893 532.269
690881.36 5335954.017 531.976
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.YuOKxZEIGnX7W4EWtjoR">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.tsaKHh9tHWqclE1A5h13">
<gml:posList srsDimension="3">
690883.836 5335953.412 532.262
690898.862 5335955.073 538.52
690881.489 5335954.312 532.268
690883.836 5335953.412 532.262
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.Ijc7ZTL5lKOnqxEJUpSw">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.Iyz5EAz3lHwDYEYJz2cS">
<gml:posList srsDimension="3">
690885.36 5335952.841 532.269
690898.862 5335955.073 538.52
690883.836 5335953.412 532.262
690885.36 5335952.841 532.269
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.q3j8Z94qpQaInE2Pmav5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.DhVkfgD007zirRjo8QjR">
<gml:posList srsDimension="3">
690885.241 5335952.546 531.98
690885.36 5335952.841 532.269
690883.836 5335953.412 532.262
690885.241 5335952.546 531.98
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.m50U66xSxurajrNBKQtB">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.EfB5gSftsNjLvWVzYxkz">
<gml:posList srsDimension="3">
690887.727 5335951.942 532.27
690898.862 5335955.073 538.52
690885.36 5335952.841 532.269
690887.727 5335951.942 532.27
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.WbK4VJ2yVh7zpOXJIINM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.acRoN4xkrptzTQ8bi7VK">
<gml:posList srsDimension="3">
690889.231 5335951.36 532.262
690898.862 5335955.073 538.52
690887.727 5335951.942 532.27
690889.231 5335951.36 532.262
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.9OAf4BEVJv6cavK9h5Xu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.wyBq6tOmESqxlzY5ehQg">
<gml:posList srsDimension="3">
690889.123 5335951.066 531.977
690889.231 5335951.36 532.262
690887.727 5335951.942 532.27
690889.123 5335951.066 531.977
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.7JApSoAYuCAPMxFQxp2Q">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.WsnronkeMC4b8FekIgFK">
<gml:posList srsDimension="3">
690891.609 5335950.462 532.267
690898.862 5335955.073 538.52
690889.231 5335951.36 532.262
690891.609 5335950.462 532.267
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.JCWylEcJhyOtI5htY84B">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.1w1BtQeAXOsalNDLvSl3">
<gml:posList srsDimension="3">
690893.123 5335949.881 532.262
690898.862 5335955.073 538.52
690891.609 5335950.462 532.267
690893.123 5335949.881 532.262
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.NMLeRKqmpFJTBBXAwWMK">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.fS79bL2I6IomUH15F1FW">
<gml:posList srsDimension="3">
690893.004 5335949.586 531.973
690893.123 5335949.881 532.262
690891.609 5335950.462 532.267
690893.004 5335949.586 531.973
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.2L6tFtk0s0VvZwrFYWb1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.v23sYkXOrgWDJ8jDVgsP">
<gml:posList srsDimension="3">
690895.49 5335948.983 532.263
690898.862 5335955.073 538.52
690893.123 5335949.881 532.262
690895.49 5335948.983 532.263
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly.YHExHWcAJPE8Nj1g9tLF">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_208f1f51-5734-4c30-99bb-2371dd9539ce_poly_0_.YNKWv4QwJ0tS3vqpdtVk">
<gml:posList srsDimension="3">
690896.344 5335948.311 531.968
690898.862 5335955.073 538.52
690895.49 5335948.983 532.263
690896.344 5335948.311 531.968
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cd460378-7f74-42bc-bd23-d011aed76d28">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cd460378-7f74-42bc-bd23-d011aed76d28_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cd460378-7f74-42bc-bd23-d011aed76d28_poly_0_">
<gml:posList srsDimension="3">
690889.123 5335951.066 514.67
690889.123 5335951.066 531.977
690887.619 5335951.627 531.969
690887.619 5335951.627 514.67
690889.123 5335951.066 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_1aa6582c-7363-408b-bf29-88af26ec14aa">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_1aa6582c-7363-408b-bf29-88af26ec14aa_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_1aa6582c-7363-408b-bf29-88af26ec14aa_poly_0_">
<gml:posList srsDimension="3">
690869.716 5335958.458 514.67
690869.716 5335958.458 531.986
690868.212 5335959.03 531.987
690868.212 5335959.03 514.67
690869.716 5335958.458 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_6260069e-d326-4ecb-ad0a-05822d67710c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6260069e-d326-4ecb-ad0a-05822d67710c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6260069e-d326-4ecb-ad0a-05822d67710c_poly_0_">
<gml:posList srsDimension="3">
690915.259 5335941.18 514.67
690915.259 5335941.18 536.974
690912.649 5335942.179 536.978
690912.649 5335942.179 514.67
690915.259 5335941.18 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_0395ada5-1c4f-479a-a3ab-ba846587fc8c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_0395ada5-1c4f-479a-a3ab-ba846587fc8c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_0395ada5-1c4f-479a-a3ab-ba846587fc8c_poly_0_">
<gml:posList srsDimension="3">
690899.371 5335956.441 538.72
690899.371 5335956.441 552.18
690908.914 5335952.814 552.18
690908.914 5335952.814 538.72
690899.371 5335956.441 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a4be6ea5-49c7-4121-b96d-a9c83e631793">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a4be6ea5-49c7-4121-b96d-a9c83e631793_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a4be6ea5-49c7-4121-b96d-a9c83e631793_poly_0_">
<gml:posList srsDimension="3">
690860.125 5336010.35 514.67
690860.125 5336010.35 535.741
690860.673 5336010.141 535.418
690860.673 5336010.141 514.67
690860.125 5336010.35 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_980c2eb6-36ba-4ce9-8baf-66bff3a68372">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_980c2eb6-36ba-4ce9-8baf-66bff3a68372_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_980c2eb6-36ba-4ce9-8baf-66bff3a68372_poly_0_">
<gml:posList srsDimension="3">
690841.688 5335972.567 534.92
690840.321 5335969.12 534.92
690840.321 5335969.12 531.553
690841.688 5335972.567 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2a0e6c58-3ebc-4ad7-ba90-d68c1d2ca98f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2a0e6c58-3ebc-4ad7-ba90-d68c1d2ca98f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2a0e6c58-3ebc-4ad7-ba90-d68c1d2ca98f_poly_0_">
<gml:posList srsDimension="3">
690885.241 5335952.546 514.67
690885.241 5335952.546 531.98
690883.738 5335953.118 531.981
690883.738 5335953.118 514.67
690885.241 5335952.546 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a502a9aa-fe28-479c-9199-97e0eb8a419d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a502a9aa-fe28-479c-9199-97e0eb8a419d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a502a9aa-fe28-479c-9199-97e0eb8a419d_poly_0_">
<gml:posList srsDimension="3">
690825.803 5335974.645 535.238
690825.838 5335974.733 535.319
690826.988 5335977.624 535.322
690826.988 5335977.624 514.67
690825.803 5335974.645 514.67
690825.803 5335974.645 535.238
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_5f53553c-d324-4216-9999-f5ca83f4ad2b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_5f53553c-d324-4216-9999-f5ca83f4ad2b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_5f53553c-d324-4216-9999-f5ca83f4ad2b_poly_0_">
<gml:posList srsDimension="3">
690836.496 5336000.232 514.67
690836.496 5336000.232 532.192
690837.124 5336001.827 532.188
690837.124 5336001.827 514.67
690836.496 5336000.232 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_8707ee61-6ecb-4a34-8686-73a381327520">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8707ee61-6ecb-4a34-8686-73a381327520_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8707ee61-6ecb-4a34-8686-73a381327520_poly_0_">
<gml:posList srsDimension="3">
690919.829 5335939.446 514.67
690919.829 5335939.446 536.978
690917.229 5335940.436 536.978
690917.229 5335940.436 514.67
690919.829 5335939.446 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_fdf9b141-8ba5-4d3e-af9f-70ec1ecd8052">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_fdf9b141-8ba5-4d3e-af9f-70ec1ecd8052_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_fdf9b141-8ba5-4d3e-af9f-70ec1ecd8052_poly_0_">
<gml:posList srsDimension="3">
690834.957 5335996.329 514.67
690834.957 5335996.329 532.198
690835.585 5335997.915 532.198
690835.585 5335997.915 514.67
690834.957 5335996.329 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_0bfd9816-a2e5-4601-b7b9-871b9e4516c4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_0bfd9816-a2e5-4601-b7b9-871b9e4516c4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_0bfd9816-a2e5-4601-b7b9-871b9e4516c4_poly_0_">
<gml:posList srsDimension="3">
690846.438 5335967.327 514.67
690846.438 5335967.327 532.003
690844.925 5335967.888 531.991
690844.925 5335967.888 514.67
690846.438 5335967.327 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ad7f0377-704f-431f-bf19-9220f9acd88f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ad7f0377-704f-431f-bf19-9220f9acd88f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ad7f0377-704f-431f-bf19-9220f9acd88f_poly_0_">
<gml:posList srsDimension="3">
690840.931 5335969.422 514.67
690840.931 5335969.422 532.006
690840.759 5335968.955 531.554
690840.759 5335968.955 514.67
690840.931 5335969.422 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_40056240-dbad-4a75-ba57-6a7e25fbce80">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_40056240-dbad-4a75-ba57-6a7e25fbce80_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_40056240-dbad-4a75-ba57-6a7e25fbce80_poly_0_">
<gml:posList srsDimension="3">
690872.084 5335957.55 514.67
690872.084 5335957.55 531.979
690872.202 5335957.845 532.268
690872.202 5335957.845 514.67
690872.084 5335957.55 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a31e0250-4a4f-4057-8bdd-65911e8d02a7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a31e0250-4a4f-4057-8bdd-65911e8d02a7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a31e0250-4a4f-4057-8bdd-65911e8d02a7_poly_0_">
<gml:posList srsDimension="3">
690850.438 5335966.142 514.67
690850.438 5335966.142 532.288
690850.32 5335965.838 531.991
690850.32 5335965.838 514.67
690850.438 5335966.142 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_8bbc7dff-e051-4fb1-827d-379bb0d7d54e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8bbc7dff-e051-4fb1-827d-379bb0d7d54e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8bbc7dff-e051-4fb1-827d-379bb0d7d54e_poly_0_">
<gml:posList srsDimension="3">
690854.0 5335985.379 532.006
690854.0 5335985.379 537.23
690853.293 5335983.481 537.23
690854.0 5335985.379 532.006
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a1898380-9813-40b9-a5ca-986f7f9a3dae">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a1898380-9813-40b9-a5ca-986f7f9a3dae_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a1898380-9813-40b9-a5ca-986f7f9a3dae_poly_0_">
<gml:posList srsDimension="3">
690893.004 5335949.586 514.67
690893.004 5335949.586 531.973
690891.501 5335950.148 531.965
690891.501 5335950.148 514.67
690893.004 5335949.586 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_30768b80-f3e2-4890-93df-606253a075d2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_30768b80-f3e2-4890-93df-606253a075d2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_30768b80-f3e2-4890-93df-606253a075d2_poly_0_">
<gml:posList srsDimension="3">
690829.778 5335983.839 514.67
690829.778 5335983.839 535.611
690830.809 5335986.421 535.617
690830.809 5335986.421 514.67
690829.778 5335983.839 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_1b64c064-f431-4941-9886-6ffe65c7e4bd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_1b64c064-f431-4941-9886-6ffe65c7e4bd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_1b64c064-f431-4941-9886-6ffe65c7e4bd_poly_0_">
<gml:posList srsDimension="3">
690840.222 5336009.653 514.67
690840.222 5336009.653 532.184
690840.517 5336009.535 532.455
690840.517 5336009.535 514.67
690840.222 5336009.653 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_06037798-c501-4b2d-8977-81f96fc4c096">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_06037798-c501-4b2d-8977-81f96fc4c096_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_06037798-c501-4b2d-8977-81f96fc4c096_poly_0_">
<gml:posList srsDimension="3">
690849.824 5335991.698 537.228
690852.457 5335990.697 537.23
690852.457 5335990.697 535.678
690849.824 5335991.698 537.228
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_64aefd00-14cf-480e-afd8-726424373284">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_64aefd00-14cf-480e-afd8-726424373284_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_64aefd00-14cf-480e-afd8-726424373284_poly_0_">
<gml:posList srsDimension="3">
690844.537 5336019.65 514.67
690844.537 5336019.65 532.462
690844.242 5336019.778 532.188
690844.242 5336019.778 514.67
690844.537 5336019.65 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_01a02043-37fb-48a0-b425-163f3d6088e5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_01a02043-37fb-48a0-b425-163f3d6088e5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_01a02043-37fb-48a0-b425-163f3d6088e5_poly_0_">
<gml:posList srsDimension="3">
690921.808 5335938.713 514.67
690921.808 5335938.713 536.272
690921.691 5335938.398 536.272
690921.691 5335938.398 514.67
690921.808 5335938.713 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ab684a01-ce57-422e-8c07-841232b14daa">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ab684a01-ce57-422e-8c07-841232b14daa_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ab684a01-ce57-422e-8c07-841232b14daa_poly_0_">
<gml:posList srsDimension="3">
690845.053 5335968.193 514.67
690845.053 5335968.193 532.292
690842.685 5335969.101 532.299
690842.685 5335969.101 514.67
690845.053 5335968.193 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_0d8e4a0f-c2c7-4cb9-8202-13edc627a21e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_0d8e4a0f-c2c7-4cb9-8202-13edc627a21e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_0d8e4a0f-c2c7-4cb9-8202-13edc627a21e_poly_0_">
<gml:posList srsDimension="3">
690901.257 5335945.616 514.67
690901.257 5335945.616 552.18
690898.066 5335946.832 552.18
690898.066 5335946.832 514.67
690901.257 5335945.616 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_5ba6b268-38a2-4d3d-9293-f1d95a13457c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_5ba6b268-38a2-4d3d-9293-f1d95a13457c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_5ba6b268-38a2-4d3d-9293-f1d95a13457c_poly_0_">
<gml:posList srsDimension="3">
690852.806 5335965.234 514.67
690852.806 5335965.234 532.281
690850.438 5335966.142 532.288
690850.438 5335966.142 514.67
690852.806 5335965.234 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cb4c5251-fe73-4323-ae4d-cac2e2c4a8ac">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cb4c5251-fe73-4323-ae4d-cac2e2c4a8ac_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cb4c5251-fe73-4323-ae4d-cac2e2c4a8ac_poly_0_">
<gml:posList srsDimension="3">
690865.835 5335959.929 514.67
690865.835 5335959.929 531.982
690864.331 5335960.51 531.99
690864.331 5335960.51 514.67
690865.835 5335959.929 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_21a96d47-0087-4c4a-b03e-05a9705ebacf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_21a96d47-0087-4c4a-b03e-05a9705ebacf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_21a96d47-0087-4c4a-b03e-05a9705ebacf_poly_0_">
<gml:posList srsDimension="3">
690837.124 5336001.827 514.67
690837.124 5336001.827 532.188
690837.337 5336001.765 532.376
690837.337 5336001.765 514.67
690837.124 5336001.827 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_c8d595c2-30fe-4cf6-9037-24fa9694d179">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c8d595c2-30fe-4cf6-9037-24fa9694d179_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c8d595c2-30fe-4cf6-9037-24fa9694d179_poly_0_">
<gml:posList srsDimension="3">
690865.953 5335960.233 514.67
690865.953 5335960.233 532.279
690865.835 5335959.929 531.982
690865.835 5335959.929 514.67
690865.953 5335960.233 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_7ecaad76-71b4-4541-b447-318e743a2f9e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7ecaad76-71b4-4541-b447-318e743a2f9e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7ecaad76-71b4-4541-b447-318e743a2f9e_poly_0_">
<gml:posList srsDimension="3">
690835.778 5335997.852 514.67
690835.778 5335997.852 532.37
690836.689 5336000.159 532.367
690836.689 5336000.159 514.67
690835.778 5335997.852 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_229204e1-4bcb-4d10-9715-f80ee63027fd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_229204e1-4bcb-4d10-9715-f80ee63027fd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_229204e1-4bcb-4d10-9715-f80ee63027fd_poly_0_">
<gml:posList srsDimension="3">
690863.058 5336017.78 514.67
690863.058 5336017.78 535.748
690860.125 5336010.35 535.741
690860.125 5336010.35 514.67
690863.058 5336017.78 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_4c5ddb92-4212-49fe-bc3c-73a78c9f9993">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_4c5ddb92-4212-49fe-bc3c-73a78c9f9993_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_4c5ddb92-4212-49fe-bc3c-73a78c9f9993_poly_0_">
<gml:posList srsDimension="3">
690885.36 5335952.841 514.67
690885.36 5335952.841 532.269
690885.241 5335952.546 531.98
690885.241 5335952.546 514.67
690885.36 5335952.841 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cf5343da-fc71-4254-85bf-7af185678c17">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cf5343da-fc71-4254-85bf-7af185678c17_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cf5343da-fc71-4254-85bf-7af185678c17_poly_0_">
<gml:posList srsDimension="3">
690881.489 5335954.312 514.67
690881.489 5335954.312 532.268
690881.36 5335954.017 531.976
690881.36 5335954.017 514.67
690881.489 5335954.312 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_c3562de5-1992-4d61-9fa4-d959acae6b6b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c3562de5-1992-4d61-9fa4-d959acae6b6b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c3562de5-1992-4d61-9fa4-d959acae6b6b_poly_0_">
<gml:posList srsDimension="3">
690842.978 5336015.736 514.67
690842.978 5336015.736 532.456
690842.692 5336015.875 532.186
690842.692 5336015.875 514.67
690842.978 5336015.736 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_1800c408-e7f4-4ff7-bccc-4cf78c31e705">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_1800c408-e7f4-4ff7-bccc-4cf78c31e705_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_1800c408-e7f4-4ff7-bccc-4cf78c31e705_poly_0_">
<gml:posList srsDimension="3">
690917.111 5335940.141 514.67
690917.111 5335940.141 536.709
690915.14 5335940.895 536.713
690915.14 5335940.895 514.67
690917.111 5335940.141 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_1ae8f6bb-e569-4b99-9637-f4b0d5f6a74a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_1ae8f6bb-e569-4b99-9637-f4b0d5f6a74a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_1ae8f6bb-e569-4b99-9637-f4b0d5f6a74a_poly_0_">
<gml:posList srsDimension="3">
690841.772 5336013.557 514.67
690841.772 5336013.557 532.185
690842.067 5336013.428 532.46
690842.067 5336013.428 514.67
690841.772 5336013.557 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_7f75958e-e746-414b-a505-41851cccb926">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7f75958e-e746-414b-a505-41851cccb926_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7f75958e-e746-414b-a505-41851cccb926_poly_0_">
<gml:posList srsDimension="3">
690848.806 5335966.409 514.67
690848.806 5335966.409 531.988
690848.925 5335966.704 532.276
690848.925 5335966.704 514.67
690848.806 5335966.409 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_579a182d-c865-4f99-a562-627ea76ce06b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_579a182d-c865-4f99-a562-627ea76ce06b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_579a182d-c865-4f99-a562-627ea76ce06b_poly_0_">
<gml:posList srsDimension="3">
690844.242 5336019.778 514.67
690844.242 5336019.778 532.188
690844.881 5336021.365 532.196
690844.881 5336021.365 514.67
690844.242 5336019.778 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d5a7d194-218f-4ce0-a6bf-5a109bf0ec12">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d5a7d194-218f-4ce0-a6bf-5a109bf0ec12_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d5a7d194-218f-4ce0-a6bf-5a109bf0ec12_poly_0_">
<gml:posList srsDimension="3">
690876.083 5335956.374 514.67
690876.083 5335956.374 532.273
690873.726 5335957.273 532.275
690873.726 5335957.273 514.67
690876.083 5335956.374 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_38baa169-970d-4ee0-8ba2-2b2bd12c130d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_38baa169-970d-4ee0-8ba2-2b2bd12c130d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_38baa169-970d-4ee0-8ba2-2b2bd12c130d_poly_0_">
<gml:posList srsDimension="3">
690838.866 5336005.668 514.67
690838.866 5336005.668 532.362
690839.787 5336007.975 532.366
690839.787 5336007.975 514.67
690838.866 5336005.668 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_887c8882-7060-4ecf-aa96-8052dcc2b622">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_887c8882-7060-4ecf-aa96-8052dcc2b622_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_887c8882-7060-4ecf-aa96-8052dcc2b622_poly_0_">
<gml:posList srsDimension="3">
690854.04 5335988.765 514.67
690854.04 5335988.765 537.23
690854.171 5335988.22 537.23
690854.171 5335988.22 514.67
690854.04 5335988.765 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_fec13c34-fdf6-4c34-b915-014de1cfa28d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_fec13c34-fdf6-4c34-b915-014de1cfa28d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_fec13c34-fdf6-4c34-b915-014de1cfa28d_poly_0_">
<gml:posList srsDimension="3">
690917.229 5335940.436 514.67
690917.229 5335940.436 536.978
690917.111 5335940.141 536.709
690917.111 5335940.141 514.67
690917.229 5335940.436 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_86ca301e-b1f1-42f7-9a6b-2f9664e83fc6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_86ca301e-b1f1-42f7-9a6b-2f9664e83fc6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_86ca301e-b1f1-42f7-9a6b-2f9664e83fc6_poly_0_">
<gml:posList srsDimension="3">
690883.836 5335953.412 514.67
690883.836 5335953.412 532.262
690881.489 5335954.312 532.268
690881.489 5335954.312 514.67
690883.836 5335953.412 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_0640ce03-456d-4bf8-8d57-49d4f81c694f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_0640ce03-456d-4bf8-8d57-49d4f81c694f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_0640ce03-456d-4bf8-8d57-49d4f81c694f_poly_0_">
<gml:posList srsDimension="3">
690833.672 5335992.297 514.67
690833.672 5335992.297 532.448
690833.407 5335992.427 532.197
690833.407 5335992.427 514.67
690833.672 5335992.297 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_7ee76fa0-d820-4906-aac9-55c0c0947778">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7ee76fa0-d820-4906-aac9-55c0c0947778_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7ee76fa0-d820-4906-aac9-55c0c0947778_poly_0_">
<gml:posList srsDimension="3">
690895.49 5335948.983 514.67
690895.49 5335948.983 532.263
690893.123 5335949.881 532.262
690893.123 5335949.881 514.67
690895.49 5335948.983 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ddfaf4ef-151c-45a8-94f1-c1bc8ea1c7ed">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ddfaf4ef-151c-45a8-94f1-c1bc8ea1c7ed_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ddfaf4ef-151c-45a8-94f1-c1bc8ea1c7ed_poly_0_">
<gml:posList srsDimension="3">
690829.524 5335983.939 514.67
690829.524 5335983.939 535.351
690829.778 5335983.839 535.611
690829.778 5335983.839 514.67
690829.524 5335983.939 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f68f9dc0-5b8b-491e-af74-784ce901ffd0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f68f9dc0-5b8b-491e-af74-784ce901ffd0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f68f9dc0-5b8b-491e-af74-784ce901ffd0_poly_0_">
<gml:posList srsDimension="3">
690887.619 5335951.627 514.67
690887.619 5335951.627 531.969
690887.727 5335951.942 532.27
690887.727 5335951.942 514.67
690887.619 5335951.627 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_65f39d56-16e3-4de1-b4fd-1452237245ea">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_65f39d56-16e3-4de1-b4fd-1452237245ea_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_65f39d56-16e3-4de1-b4fd-1452237245ea_poly_0_">
<gml:posList srsDimension="3">
690928.629 5335957.035 514.67
690928.629 5335957.035 536.272
690926.083 5335950.21 536.276
690926.083 5335950.21 514.67
690928.629 5335957.035 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_897d3846-9f87-42e2-be3a-535163f0ac37">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_897d3846-9f87-42e2-be3a-535163f0ac37_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_897d3846-9f87-42e2-be3a-535163f0ac37_poly_0_">
<gml:posList srsDimension="3">
690913.146 5335962.796 514.67
690913.146 5335962.796 536.64
690913.209 5335962.968 536.639
690913.209 5335962.968 514.67
690913.146 5335962.796 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_e445faf1-4355-45ff-a9eb-f9eabb2fb956">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e445faf1-4355-45ff-a9eb-f9eabb2fb956_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e445faf1-4355-45ff-a9eb-f9eabb2fb956_poly_0_">
<gml:posList srsDimension="3">
690893.123 5335949.881 514.67
690893.123 5335949.881 532.262
690893.004 5335949.586 531.973
690893.004 5335949.586 514.67
690893.123 5335949.881 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_331a8a95-99cf-4533-b8a9-a386fee05cc6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_331a8a95-99cf-4533-b8a9-a386fee05cc6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_331a8a95-99cf-4533-b8a9-a386fee05cc6_poly_0_">
<gml:posList srsDimension="3">
690841.428 5336011.842 514.67
690841.428 5336011.842 532.452
690841.144 5336011.951 532.192
690841.144 5336011.951 514.67
690841.428 5336011.842 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2b700087-f865-4aa9-849f-d586756eb1c7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2b700087-f865-4aa9-849f-d586756eb1c7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2b700087-f865-4aa9-849f-d586756eb1c7_poly_0_">
<gml:posList srsDimension="3">
690897.852 5335947.204 514.67
690897.852 5335947.204 552.18
690896.724 5335947.63 552.18
690896.724 5335947.63 514.67
690897.852 5335947.204 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_99b062c0-ee64-432a-9dfa-e1fe5aca1689">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_99b062c0-ee64-432a-9dfa-e1fe5aca1689_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_99b062c0-ee64-432a-9dfa-e1fe5aca1689_poly_0_">
<gml:posList srsDimension="3">
690856.677 5335963.754 514.67
690856.677 5335963.754 532.274
690854.32 5335964.653 532.276
690854.32 5335964.653 514.67
690856.677 5335963.754 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_62b98986-0a4e-43b6-8384-a2f1b0d317f9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_62b98986-0a4e-43b6-8384-a2f1b0d317f9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_62b98986-0a4e-43b6-8384-a2f1b0d317f9_poly_0_">
<gml:posList srsDimension="3">
690873.726 5335957.273 514.67
690873.726 5335957.273 532.275
690873.597 5335956.978 531.983
690873.597 5335956.978 514.67
690873.726 5335957.273 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_58681b9d-5ccf-427f-8b88-6e323850252a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_58681b9d-5ccf-427f-8b88-6e323850252a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_58681b9d-5ccf-427f-8b88-6e323850252a_poly_0_">
<gml:posList srsDimension="3">
690853.83 5335989.277 514.67
690853.83 5335989.277 537.23
690854.04 5335988.765 537.23
690854.04 5335988.765 514.67
690853.83 5335989.277 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_1f9890d3-54e1-4dc2-834d-5bcb3228fe9c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_1f9890d3-54e1-4dc2-834d-5bcb3228fe9c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_1f9890d3-54e1-4dc2-834d-5bcb3228fe9c_poly_0_">
<gml:posList srsDimension="3">
690860.673 5336010.141 514.67
690860.673 5336010.141 535.418
690858.816 5336005.414 535.41
690858.816 5336005.414 514.67
690860.673 5336010.141 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_6d6ee038-9cb7-4370-a5a3-08d1b19e3bf4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6d6ee038-9cb7-4370-a5a3-08d1b19e3bf4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6d6ee038-9cb7-4370-a5a3-08d1b19e3bf4_poly_0_">
<gml:posList srsDimension="3">
690906.823 5335943.793 514.67
690906.823 5335943.793 536.503
690906.01 5335944.101 536.5
690906.01 5335944.101 514.67
690906.823 5335943.793 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2be52e5f-d8d1-4aef-a8f4-30ef42fdd8d2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2be52e5f-d8d1-4aef-a8f4-30ef42fdd8d2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2be52e5f-d8d1-4aef-a8f4-30ef42fdd8d2_poly_0_">
<gml:posList srsDimension="3">
690858.257 5336005.623 514.67
690858.257 5336005.623 535.738
690852.391 5335990.742 535.72
690852.391 5335990.742 514.67
690858.257 5336005.623 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_b035d83f-a0db-45f6-a09e-3edf1986bcc0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_b035d83f-a0db-45f6-a09e-3edf1986bcc0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_b035d83f-a0db-45f6-a09e-3edf1986bcc0_poly_0_">
<gml:posList srsDimension="3">
690835.585 5335997.915 514.67
690835.585 5335997.915 532.198
690835.778 5335997.852 532.37
690835.778 5335997.852 514.67
690835.585 5335997.915 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_63aca288-6a4f-49e2-91ab-32a68d832dda">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_63aca288-6a4f-49e2-91ab-32a68d832dda_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_63aca288-6a4f-49e2-91ab-32a68d832dda_poly_0_">
<gml:posList srsDimension="3">
690912.54 5335941.894 514.67
690912.54 5335941.894 536.721
690910.581 5335942.628 536.712
690910.581 5335942.628 514.67
690912.54 5335941.894 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cc5cf8de-0b6e-4ad8-89a5-e48b27b3dc72">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cc5cf8de-0b6e-4ad8-89a5-e48b27b3dc72_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cc5cf8de-0b6e-4ad8-89a5-e48b27b3dc72_poly_0_">
<gml:posList srsDimension="3">
690845.801 5336023.683 514.67
690845.801 5336023.683 532.197
690846.103 5336024.425 532.202
690846.103 5336024.425 514.67
690845.801 5336023.683 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_c63ce54d-174e-4c5b-ad03-c58ec56601e8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c63ce54d-174e-4c5b-ad03-c58ec56601e8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c63ce54d-174e-4c5b-ad03-c58ec56601e8_poly_0_">
<gml:posList srsDimension="3">
690879.847 5335954.588 514.67
690879.847 5335954.588 531.972
690879.965 5335954.893 532.269
690879.965 5335954.893 514.67
690879.847 5335954.588 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_7da17286-c25e-467a-9861-7462b01191cf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7da17286-c25e-467a-9861-7462b01191cf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7da17286-c25e-467a-9861-7462b01191cf_poly_0_">
<gml:posList srsDimension="3">
690835.14 5335996.257 514.67
690835.14 5335996.257 532.366
690834.957 5335996.329 532.198
690834.957 5335996.329 514.67
690835.14 5335996.257 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_9991eb22-9916-4fdc-abba-0b489052a330">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9991eb22-9916-4fdc-abba-0b489052a330_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9991eb22-9916-4fdc-abba-0b489052a330_poly_0_">
<gml:posList srsDimension="3">
690830.543 5335986.551 514.67
690830.543 5335986.551 535.337
690831.718 5335989.529 535.332
690831.718 5335989.529 514.67
690830.543 5335986.551 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_76f520ac-ca6a-4245-b4a1-3375559e4503">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_76f520ac-ca6a-4245-b4a1-3375559e4503_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_76f520ac-ca6a-4245-b4a1-3375559e4503_poly_0_">
<gml:posList srsDimension="3">
690877.479 5335955.497 514.67
690877.479 5335955.497 531.979
690875.975 5335956.069 531.979
690875.975 5335956.069 514.67
690877.479 5335955.497 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f8c7cb09-64b1-4b27-bd65-5b82a17a146d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f8c7cb09-64b1-4b27-bd65-5b82a17a146d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f8c7cb09-64b1-4b27-bd65-5b82a17a146d_poly_0_">
<gml:posList srsDimension="3">
690890.447 5335971.466 514.67
690890.447 5335971.466 532.15
690903.861 5335966.346 532.203
690903.861 5335966.346 514.67
690890.447 5335971.466 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_02744a47-1af7-4f09-92f8-4eb1a0c1b833">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_02744a47-1af7-4f09-92f8-4eb1a0c1b833_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_02744a47-1af7-4f09-92f8-4eb1a0c1b833_poly_0_">
<gml:posList srsDimension="3">
690910.581 5335942.628 514.67
690910.581 5335942.628 536.712
690910.689 5335942.923 536.977
690910.689 5335942.923 514.67
690910.581 5335942.628 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cfc0a8e4-2ce6-4d60-bd55-da49f81d016b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cfc0a8e4-2ce6-4d60-bd55-da49f81d016b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cfc0a8e4-2ce6-4d60-bd55-da49f81d016b_poly_0_">
<gml:posList srsDimension="3">
690838.045 5336004.155 514.67
690838.045 5336004.155 532.186
690838.673 5336005.74 532.186
690838.673 5336005.74 514.67
690838.045 5336004.155 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f61b3034-b333-40b1-8675-c1a5379a9d69">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f61b3034-b333-40b1-8675-c1a5379a9d69_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f61b3034-b333-40b1-8675-c1a5379a9d69_poly_0_">
<gml:posList srsDimension="3">
690853.565 5335985.545 514.67
690854.154 5335987.109 514.67
690854.154 5335987.109 537.23
690853.565 5335985.545 537.23
690853.565 5335985.545 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d5b1a094-a393-43d6-8ef4-1de37ec99562">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d5b1a094-a393-43d6-8ef4-1de37ec99562_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d5b1a094-a393-43d6-8ef4-1de37ec99562_poly_0_">
<gml:posList srsDimension="3">
690844.881 5336021.365 514.67
690844.881 5336021.365 532.196
690845.175 5336021.256 532.463
690845.175 5336021.256 514.67
690844.881 5336021.365 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_44d25014-d47c-4dd6-826f-c880d4ecd08a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_44d25014-d47c-4dd6-826f-c880d4ecd08a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_44d25014-d47c-4dd6-826f-c880d4ecd08a_poly_0_">
<gml:posList srsDimension="3">
690881.36 5335954.017 514.67
690881.36 5335954.017 531.976
690879.847 5335954.588 531.972
690879.847 5335954.588 514.67
690881.36 5335954.017 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d22257c7-af8e-412a-8171-3cfe22448a9f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d22257c7-af8e-412a-8171-3cfe22448a9f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d22257c7-af8e-412a-8171-3cfe22448a9f_poly_0_">
<gml:posList srsDimension="3">
690849.824 5335991.698 537.228
690849.347 5335991.88 537.505
690849.347 5335991.88 537.23
690849.824 5335991.698 537.228
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_3e84316b-88b3-4e45-8728-0dd026a263d5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3e84316b-88b3-4e45-8728-0dd026a263d5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3e84316b-88b3-4e45-8728-0dd026a263d5_poly_0_">
<gml:posList srsDimension="3">
690872.202 5335957.845 514.67
690872.202 5335957.845 532.268
690869.835 5335958.753 532.275
690869.835 5335958.753 514.67
690872.202 5335957.845 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2d936ef0-1a01-4602-ad88-83deac0eb9f2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2d936ef0-1a01-4602-ad88-83deac0eb9f2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2d936ef0-1a01-4602-ad88-83deac0eb9f2_poly_0_">
<gml:posList srsDimension="3">
690868.212 5335959.03 514.67
690868.212 5335959.03 531.987
690868.321 5335959.335 532.28
690868.321 5335959.335 514.67
690868.212 5335959.03 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cf970899-7eec-4419-8ee6-a84e5e6f035d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cf970899-7eec-4419-8ee6-a84e5e6f035d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cf970899-7eec-4419-8ee6-a84e5e6f035d_poly_0_">
<gml:posList srsDimension="3">
690832.497 5335990.11 514.67
690832.497 5335990.11 535.414
690832.791 5335990.011 535.404
690832.791 5335990.011 514.67
690832.497 5335990.11 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_63390a18-1ae8-4346-882c-d26c92717009">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_63390a18-1ae8-4346-882c-d26c92717009_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_63390a18-1ae8-4346-882c-d26c92717009_poly_0_">
<gml:posList srsDimension="3">
690860.45 5335961.98 514.67
690860.45 5335961.98 531.985
690860.569 5335962.275 532.274
690860.569 5335962.275 514.67
690860.45 5335961.98 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ba769ac7-26fd-455b-984a-c1f30383dbcd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ba769ac7-26fd-455b-984a-c1f30383dbcd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ba769ac7-26fd-455b-984a-c1f30383dbcd_poly_0_">
<gml:posList srsDimension="3">
690830.809 5335986.421 514.67
690830.809 5335986.421 535.617
690830.543 5335986.551 535.337
690830.543 5335986.551 514.67
690830.809 5335986.421 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_dab3439b-678f-4311-8302-7b63c7505e69">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_dab3439b-678f-4311-8302-7b63c7505e69_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_dab3439b-678f-4311-8302-7b63c7505e69_poly_0_">
<gml:posList srsDimension="3">
690832.995 5335990.541 532.463
690832.995 5335990.541 534.92
690835.69 5335989.515 534.92
690832.995 5335990.541 532.463
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_60d0fd5c-173b-4217-9db0-7e9b71becbbd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_60d0fd5c-173b-4217-9db0-7e9b71becbbd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_60d0fd5c-173b-4217-9db0-7e9b71becbbd_poly_0_">
<gml:posList srsDimension="3">
690846.547 5335967.611 514.67
690846.547 5335967.611 532.28
690846.438 5335967.327 532.003
690846.438 5335967.327 514.67
690846.547 5335967.611 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_86b86c0a-083f-4315-8fcf-382da5f8a163">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_86b86c0a-083f-4315-8fcf-382da5f8a163_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_86b86c0a-083f-4315-8fcf-382da5f8a163_poly_0_">
<gml:posList srsDimension="3">
690896.724 5335947.63 514.67
690896.724 5335947.63 552.18
690896.896 5335948.097 552.18
690896.896 5335948.097 514.67
690896.724 5335947.63 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_993c6dee-1bd0-425d-9b49-26c2fa996d8b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_993c6dee-1bd0-425d-9b49-26c2fa996d8b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_993c6dee-1bd0-425d-9b49-26c2fa996d8b_poly_0_">
<gml:posList srsDimension="3">
690831.52 5335972.796 514.67
690831.52 5335972.796 535.497
690828.919 5335973.786 535.497
690828.919 5335973.786 514.67
690831.52 5335972.796 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_bda5b570-8c71-4106-a50b-b57b28bc097a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_bda5b570-8c71-4106-a50b-b57b28bc097a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_bda5b570-8c71-4106-a50b-b57b28bc097a_poly_0_">
<gml:posList srsDimension="3">
690827.292 5335977.526 514.67
690827.292 5335977.526 535.625
690828.313 5335980.098 535.626
690828.313 5335980.098 514.67
690827.292 5335977.526 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f44f5bfd-997a-4e47-88b2-fedb219666de">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f44f5bfd-997a-4e47-88b2-fedb219666de_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f44f5bfd-997a-4e47-88b2-fedb219666de_poly_0_">
<gml:posList srsDimension="3">
690852.967 5335990.354 514.67
690852.967 5335990.354 537.23
690853.456 5335989.863 537.23
690853.456 5335989.863 514.67
690852.967 5335990.354 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d89f2971-4ecd-4ac9-a12a-bf519b5fe3d7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d89f2971-4ecd-4ac9-a12a-bf519b5fe3d7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d89f2971-4ecd-4ac9-a12a-bf519b5fe3d7_poly_0_">
<gml:posList srsDimension="3">
690858.816 5336005.414 514.67
690858.816 5336005.414 535.41
690858.257 5336005.623 535.738
690858.257 5336005.623 514.67
690858.816 5336005.414 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d2043bf2-4227-45b4-83cb-a7447d920bf5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d2043bf2-4227-45b4-83cb-a7447d920bf5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d2043bf2-4227-45b4-83cb-a7447d920bf5_poly_0_">
<gml:posList srsDimension="3">
690839.594 5336008.068 514.67
690839.594 5336008.068 532.184
690840.222 5336009.653 532.184
690840.222 5336009.653 514.67
690839.594 5336008.068 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_8db669df-fb4f-41c9-a5c3-17cc41a48afd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8db669df-fb4f-41c9-a5c3-17cc41a48afd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8db669df-fb4f-41c9-a5c3-17cc41a48afd_poly_0_">
<gml:posList srsDimension="3">
690910.689 5335942.923 514.67
690910.689 5335942.923 536.977
690908.089 5335943.922 536.986
690908.089 5335943.922 514.67
690910.689 5335942.923 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f6d289c6-a556-4cd8-b182-26d27fcf0767">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f6d289c6-a556-4cd8-b182-26d27fcf0767_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f6d289c6-a556-4cd8-b182-26d27fcf0767_poly_0_">
<gml:posList srsDimension="3">
690831.718 5335989.529 514.67
690831.718 5335989.529 535.332
690832.196 5335989.338 535.821
690832.196 5335989.338 514.67
690831.718 5335989.529 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_6c358fc7-bd41-484f-8b9f-38062048d4c2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6c358fc7-bd41-484f-8b9f-38062048d4c2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6c358fc7-bd41-484f-8b9f-38062048d4c2_poly_0_">
<gml:posList srsDimension="3">
690840.517 5336009.535 514.67
690840.517 5336009.535 532.455
690841.428 5336011.842 532.452
690841.428 5336011.842 514.67
690840.517 5336009.535 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_3d00d7fe-50b8-4cb6-9488-3e71cc83d656">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3d00d7fe-50b8-4cb6-9488-3e71cc83d656_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3d00d7fe-50b8-4cb6-9488-3e71cc83d656_poly_0_">
<gml:posList srsDimension="3">
690891.609 5335950.462 514.67
690891.609 5335950.462 532.267
690889.231 5335951.36 532.262
690889.231 5335951.36 514.67
690891.609 5335950.462 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_38f87776-a193-4aca-9c20-5ef51a7812ce">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_38f87776-a193-4aca-9c20-5ef51a7812ce_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_38f87776-a193-4aca-9c20-5ef51a7812ce_poly_0_">
<gml:posList srsDimension="3">
690907.97 5335943.627 514.67
690907.97 5335943.627 536.717
690906.925 5335943.997 536.695
690906.925 5335943.997 514.67
690907.97 5335943.627 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_4a5c5e24-5fa8-4154-a624-1fd8280907b4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_4a5c5e24-5fa8-4154-a624-1fd8280907b4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_4a5c5e24-5fa8-4154-a624-1fd8280907b4_poly_0_">
<gml:posList srsDimension="3">
690864.45 5335960.805 514.67
690864.45 5335960.805 532.279
690862.072 5335961.713 532.282
690862.072 5335961.713 514.67
690864.45 5335960.805 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_0b2331c5-c5ac-4b77-ac19-df6ee41d40bc">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_0b2331c5-c5ac-4b77-ac19-df6ee41d40bc_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_0b2331c5-c5ac-4b77-ac19-df6ee41d40bc_poly_0_">
<gml:posList srsDimension="3">
690828.81 5335973.501 514.67
690828.81 5335973.501 535.237
690825.803 5335974.645 535.238
690825.803 5335974.645 514.67
690828.81 5335973.501 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_e52f5c8f-e358-4d66-974f-76ecc978cca4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e52f5c8f-e358-4d66-974f-76ecc978cca4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e52f5c8f-e358-4d66-974f-76ecc978cca4_poly_0_">
<gml:posList srsDimension="3">
690835.29 5335971.363 514.67
690835.29 5335971.363 535.496
690835.181 5335971.078 535.236
690835.181 5335971.078 514.67
690835.29 5335971.363 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_c9a9076e-6ba2-4435-83c7-5ff7f58faa18">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c9a9076e-6ba2-4435-83c7-5ff7f58faa18_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c9a9076e-6ba2-4435-83c7-5ff7f58faa18_poly_0_">
<gml:posList srsDimension="3">
690849.347 5335991.88 537.23
690849.347 5335991.88 537.505
690846.742 5335985.312 537.499
690846.742 5335985.312 537.23
690849.347 5335991.88 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_3b0cee53-9835-4f2a-a605-f5402eac3377">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3b0cee53-9835-4f2a-a605-f5402eac3377_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3b0cee53-9835-4f2a-a605-f5402eac3377_poly_0_">
<gml:posList srsDimension="3">
690837.337 5336001.765 514.67
690837.337 5336001.765 532.376
690838.238 5336004.072 532.365
690838.238 5336004.072 514.67
690837.337 5336001.765 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a87338ef-fbd0-4931-835b-27e56c8c949f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a87338ef-fbd0-4931-835b-27e56c8c949f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a87338ef-fbd0-4931-835b-27e56c8c949f_poly_0_">
<gml:posList srsDimension="3">
690842.067 5336013.428 514.67
690842.067 5336013.428 532.46
690842.978 5336015.736 532.456
690842.978 5336015.736 514.67
690842.067 5336013.428 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_8a914f87-bd16-4698-a479-8e5c1ecf7998">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8a914f87-bd16-4698-a479-8e5c1ecf7998_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8a914f87-bd16-4698-a479-8e5c1ecf7998_poly_0_">
<gml:posList srsDimension="3">
690839.787 5336007.975 514.67
690839.787 5336007.975 532.366
690839.594 5336008.068 532.184
690839.594 5336008.068 514.67
690839.787 5336007.975 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_41a96f94-54a8-44bd-ad52-3e6faea1932f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_41a96f94-54a8-44bd-ad52-3e6faea1932f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_41a96f94-54a8-44bd-ad52-3e6faea1932f_poly_0_">
<gml:posList srsDimension="3">
690844.925 5335967.888 514.67
690844.925 5335967.888 531.991
690845.053 5335968.193 532.292
690845.053 5335968.193 514.67
690844.925 5335967.888 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_16c9932e-e275-4c97-86ec-af967cad08f4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_16c9932e-e275-4c97-86ec-af967cad08f4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_16c9932e-e275-4c97-86ec-af967cad08f4_poly_0_">
<gml:posList srsDimension="3">
690852.687 5335964.939 514.67
690852.687 5335964.939 531.993
690852.806 5335965.234 532.281
690852.806 5335965.234 514.67
690852.687 5335964.939 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_adc01ad7-a700-42af-9ecb-ecb7be7c5879">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_adc01ad7-a700-42af-9ecb-ecb7be7c5879_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_adc01ad7-a700-42af-9ecb-ecb7be7c5879_poly_0_">
<gml:posList srsDimension="3">
690834.036 5335994.022 514.67
690834.036 5335994.022 532.193
690834.219 5335993.949 532.361
690834.219 5335993.949 514.67
690834.036 5335994.022 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_5800970b-d523-4752-9c9b-b39374c9977a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_5800970b-d523-4752-9c9b-b39374c9977a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_5800970b-d523-4752-9c9b-b39374c9977a_poly_0_">
<gml:posList srsDimension="3">
690836.689 5336000.159 514.67
690836.689 5336000.159 532.367
690836.496 5336000.232 532.192
690836.496 5336000.232 514.67
690836.689 5336000.159 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_756e32cc-4d73-47c2-a279-c21a9847c48a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_756e32cc-4d73-47c2-a279-c21a9847c48a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_756e32cc-4d73-47c2-a279-c21a9847c48a_poly_0_">
<gml:posList srsDimension="3">
690843.331 5336017.461 514.67
690843.331 5336017.461 532.194
690843.616 5336017.342 532.457
690843.616 5336017.342 514.67
690843.331 5336017.461 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cd5ab605-ebcb-4b85-8307-d101100634c5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cd5ab605-ebcb-4b85-8307-d101100634c5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cd5ab605-ebcb-4b85-8307-d101100634c5_poly_0_">
<gml:posList srsDimension="3">
690905.712 5335944.214 514.67
690906.01 5335944.101 514.67
690906.01 5335944.101 536.5
690905.712 5335944.214 536.23
690905.712 5335944.214 552.18
690901.56 5335945.788 552.18
690901.56 5335945.788 514.67
690905.712 5335944.214 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2142cf3e-817e-4e88-871c-1b49062a31e0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2142cf3e-817e-4e88-871c-1b49062a31e0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2142cf3e-817e-4e88-871c-1b49062a31e0_poly_0_">
<gml:posList srsDimension="3">
690898.066 5335946.832 514.67
690898.066 5335946.832 552.18
690897.852 5335947.204 552.18
690897.852 5335947.204 514.67
690898.066 5335946.832 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_01495273-7b74-435c-8a06-6997e7ea37f9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_01495273-7b74-435c-8a06-6997e7ea37f9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_01495273-7b74-435c-8a06-6997e7ea37f9_poly_0_">
<gml:posList srsDimension="3">
690850.32 5335965.838 514.67
690850.32 5335965.838 531.991
690848.806 5335966.409 531.988
690848.806 5335966.409 514.67
690850.32 5335965.838 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_47be2a7c-8f41-4603-8913-ed37bf46801d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_47be2a7c-8f41-4603-8913-ed37bf46801d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_47be2a7c-8f41-4603-8913-ed37bf46801d_poly_0_">
<gml:posList srsDimension="3">
690838.238 5336004.072 514.67
690838.238 5336004.072 532.365
690838.045 5336004.155 532.186
690838.045 5336004.155 514.67
690838.238 5336004.072 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_c6079cc0-2b6f-48b7-9f7e-185fc353741d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c6079cc0-2b6f-48b7-9f7e-185fc353741d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c6079cc0-2b6f-48b7-9f7e-185fc353741d_poly_0_">
<gml:posList srsDimension="3">
690912.155 5335961.522 536.23
690912.694 5335962.969 536.23
690912.694 5335962.969 532.248
690912.155 5335961.522 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_393ab129-1f7c-47fb-8103-ca7fe09b7149">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_393ab129-1f7c-47fb-8103-ca7fe09b7149_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_393ab129-1f7c-47fb-8103-ca7fe09b7149_poly_0_">
<gml:posList srsDimension="3">
690826.988 5335977.624 514.67
690826.988 5335977.624 535.322
690827.292 5335977.526 535.625
690827.292 5335977.526 514.67
690826.988 5335977.624 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_554f5612-b779-414f-8a1e-aa57053016ca">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_554f5612-b779-414f-8a1e-aa57053016ca_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_554f5612-b779-414f-8a1e-aa57053016ca_poly_0_">
<gml:posList srsDimension="3">
690837.782 5335970.079 514.67
690837.782 5335970.079 535.228
690837.881 5335970.373 535.492
690837.881 5335970.373 514.67
690837.782 5335970.079 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_132a6fdb-f9c0-4bc4-b9bf-e8539a13a014">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_132a6fdb-f9c0-4bc4-b9bf-e8539a13a014_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_132a6fdb-f9c0-4bc4-b9bf-e8539a13a014_poly_0_">
<gml:posList srsDimension="3">
690858.191 5335963.183 514.67
690858.191 5335963.183 532.277
690858.072 5335962.888 531.989
690858.072 5335962.888 514.67
690858.191 5335963.183 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_b3959092-c45a-428e-88aa-3ededbb4810a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_b3959092-c45a-428e-88aa-3ededbb4810a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_b3959092-c45a-428e-88aa-3ededbb4810a_poly_0_">
<gml:posList srsDimension="3">
690842.692 5336015.875 514.67
690842.692 5336015.875 532.186
690843.331 5336017.461 532.194
690843.331 5336017.461 514.67
690842.692 5336015.875 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2d11ccc8-2166-4da5-968b-82b3cce45bfd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2d11ccc8-2166-4da5-968b-82b3cce45bfd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2d11ccc8-2166-4da5-968b-82b3cce45bfd_poly_0_">
<gml:posList srsDimension="3">
690879.965 5335954.893 514.67
690879.965 5335954.893 532.269
690877.597 5335955.792 532.268
690877.597 5335955.792 514.67
690879.965 5335954.893 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ecb4d84e-142b-4339-bac0-9c9bb9523de6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ecb4d84e-142b-4339-bac0-9c9bb9523de6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ecb4d84e-142b-4339-bac0-9c9bb9523de6_poly_0_">
<gml:posList srsDimension="3">
690895.392 5335948.679 514.67
690895.392 5335948.679 531.973
690895.49 5335948.983 532.263
690895.49 5335948.983 514.67
690895.392 5335948.679 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_081f191c-354b-4570-8d41-340c0452945f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_081f191c-354b-4570-8d41-340c0452945f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_081f191c-354b-4570-8d41-340c0452945f_poly_0_">
<gml:posList srsDimension="3">
690838.673 5336005.74 514.67
690838.673 5336005.74 532.186
690838.866 5336005.668 532.362
690838.866 5336005.668 514.67
690838.673 5336005.74 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2e9abf8c-ca9e-41bf-9146-9e22d24bc6d4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2e9abf8c-ca9e-41bf-9146-9e22d24bc6d4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2e9abf8c-ca9e-41bf-9146-9e22d24bc6d4_poly_0_">
<gml:posList srsDimension="3">
690926.083 5335950.21 514.67
690926.083 5335950.21 536.276
690921.808 5335938.713 536.272
690921.808 5335938.713 514.67
690926.083 5335950.21 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_b9ff6968-6f2b-4fb5-a39c-6e004425dc05">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_b9ff6968-6f2b-4fb5-a39c-6e004425dc05_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_b9ff6968-6f2b-4fb5-a39c-6e004425dc05_poly_0_">
<gml:posList srsDimension="3">
690854.191 5335964.368 514.67
690854.191 5335964.368 531.992
690852.687 5335964.939 531.993
690852.687 5335964.939 514.67
690854.191 5335964.368 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d17820dd-66d7-4a00-bb18-e0a4305fbd81">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d17820dd-66d7-4a00-bb18-e0a4305fbd81_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d17820dd-66d7-4a00-bb18-e0a4305fbd81_poly_0_">
<gml:posList srsDimension="3">
690828.037 5335980.228 514.67
690828.037 5335980.228 535.338
690829.524 5335983.939 535.351
690829.524 5335983.939 514.67
690828.037 5335980.228 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2b5bb68b-de56-4eac-9f29-791c53c2e5f4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2b5bb68b-de56-4eac-9f29-791c53c2e5f4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2b5bb68b-de56-4eac-9f29-791c53c2e5f4_poly_0_">
<gml:posList srsDimension="3">
690858.072 5335962.888 514.67
690858.072 5335962.888 531.989
690856.568 5335963.46 531.989
690856.568 5335963.46 514.67
690858.072 5335962.888 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_872f9ca8-e205-4fbb-a747-41e9a32d766f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_872f9ca8-e205-4fbb-a747-41e9a32d766f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_872f9ca8-e205-4fbb-a747-41e9a32d766f_poly_0_">
<gml:posList srsDimension="3">
690831.421 5335972.512 514.67
690831.421 5335972.512 535.24
690831.52 5335972.796 535.497
690831.52 5335972.796 514.67
690831.421 5335972.512 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a2f9f934-0e90-499e-8bed-62246f972905">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a2f9f934-0e90-499e-8bed-62246f972905_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a2f9f934-0e90-499e-8bed-62246f972905_poly_0_">
<gml:posList srsDimension="3">
690861.954 5335961.409 514.67
690861.954 5335961.409 531.985
690860.45 5335961.98 531.985
690860.45 5335961.98 514.67
690861.954 5335961.409 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d2efe944-8bcd-4d69-b911-8c905b742884">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d2efe944-8bcd-4d69-b911-8c905b742884_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d2efe944-8bcd-4d69-b911-8c905b742884_poly_0_">
<gml:posList srsDimension="3">
690883.738 5335953.118 514.67
690883.738 5335953.118 531.981
690883.836 5335953.412 532.262
690883.836 5335953.412 514.67
690883.738 5335953.118 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2d677e2c-0f62-4f82-a296-1090ae61ef52">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2d677e2c-0f62-4f82-a296-1090ae61ef52_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2d677e2c-0f62-4f82-a296-1090ae61ef52_poly_0_">
<gml:posList srsDimension="3">
690845.175 5336021.256 514.67
690845.175 5336021.256 532.463
690846.097 5336023.554 532.471
690846.097 5336023.554 514.67
690845.175 5336021.256 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_684f95ab-716a-43b5-b6a0-540897263563">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_684f95ab-716a-43b5-b6a0-540897263563_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_684f95ab-716a-43b5-b6a0-540897263563_poly_0_">
<gml:posList srsDimension="3">
690864.331 5335960.51 514.67
690864.331 5335960.51 531.99
690864.45 5335960.805 532.279
690864.45 5335960.805 514.67
690864.331 5335960.51 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_5dccb95c-c204-4eb6-beae-5ab6a41bf398">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_5dccb95c-c204-4eb6-beae-5ab6a41bf398_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_5dccb95c-c204-4eb6-beae-5ab6a41bf398_poly_0_">
<gml:posList srsDimension="3">
690919.71 5335939.151 514.67
690919.71 5335939.151 536.709
690919.829 5335939.446 536.978
690919.829 5335939.446 514.67
690919.71 5335939.151 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_b4684999-858d-446e-9a1f-1bccdbd889ac">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_b4684999-858d-446e-9a1f-1bccdbd889ac_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_b4684999-858d-446e-9a1f-1bccdbd889ac_poly_0_">
<gml:posList srsDimension="3">
690854.203 5335987.661 514.67
690854.203 5335987.661 537.23
690854.154 5335987.109 537.23
690854.154 5335987.109 514.67
690854.203 5335987.661 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_6154ed3e-650b-47ee-aad2-e804f4e8ce14">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6154ed3e-650b-47ee-aad2-e804f4e8ce14_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6154ed3e-650b-47ee-aad2-e804f4e8ce14_poly_0_">
<gml:posList srsDimension="3">
690854.32 5335964.653 514.67
690854.32 5335964.653 532.276
690854.191 5335964.368 531.992
690854.191 5335964.368 514.67
690854.32 5335964.653 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_babffc92-e109-401d-bcc3-2308e74a43f5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_babffc92-e109-401d-bcc3-2308e74a43f5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_babffc92-e109-401d-bcc3-2308e74a43f5_poly_0_">
<gml:posList srsDimension="3">
690873.597 5335956.978 514.67
690873.597 5335956.978 531.983
690872.084 5335957.55 531.979
690872.084 5335957.55 514.67
690873.597 5335956.978 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_652b2825-c6d6-4a1f-a061-f14cb5d02793">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_652b2825-c6d6-4a1f-a061-f14cb5d02793_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_652b2825-c6d6-4a1f-a061-f14cb5d02793_poly_0_">
<gml:posList srsDimension="3">
690837.881 5335970.373 514.67
690837.881 5335970.373 535.492
690835.29 5335971.363 535.496
690835.29 5335971.363 514.67
690837.881 5335970.373 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_9211b454-99f4-45bf-a62d-88f09caee705">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9211b454-99f4-45bf-a62d-88f09caee705_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9211b454-99f4-45bf-a62d-88f09caee705_poly_0_">
<gml:posList srsDimension="3">
690908.089 5335943.922 514.67
690908.089 5335943.922 536.986
690907.97 5335943.627 536.717
690907.97 5335943.627 514.67
690908.089 5335943.922 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_163040e2-cd2d-4232-b8c3-f151668cd44c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_163040e2-cd2d-4232-b8c3-f151668cd44c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_163040e2-cd2d-4232-b8c3-f151668cd44c_poly_0_">
<gml:posList srsDimension="3">
690843.616 5336017.342 514.67
690843.616 5336017.342 532.457
690844.537 5336019.65 532.462
690844.537 5336019.65 514.67
690843.616 5336017.342 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_11c0424c-eadf-41e5-b5c4-782d6269e069">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_11c0424c-eadf-41e5-b5c4-782d6269e069_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_11c0424c-eadf-41e5-b5c4-782d6269e069_poly_0_">
<gml:posList srsDimension="3">
690887.727 5335951.942 514.67
690887.727 5335951.942 532.27
690885.36 5335952.841 532.269
690885.36 5335952.841 514.67
690887.727 5335951.942 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d0ce8efd-7980-4a6a-bf14-5c89fa65bf85">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d0ce8efd-7980-4a6a-bf14-5c89fa65bf85_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d0ce8efd-7980-4a6a-bf14-5c89fa65bf85_poly_0_">
<gml:posList srsDimension="3">
690906.925 5335943.997 514.67
690906.925 5335943.997 536.695
690906.823 5335943.793 536.503
690906.823 5335943.793 514.67
690906.925 5335943.997 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_e3de98ad-f7d4-476b-ae62-90ec14224408">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e3de98ad-f7d4-476b-ae62-90ec14224408_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e3de98ad-f7d4-476b-ae62-90ec14224408_poly_0_">
<gml:posList srsDimension="3">
690889.231 5335951.36 514.67
690889.231 5335951.36 532.262
690889.123 5335951.066 531.977
690889.123 5335951.066 514.67
690889.231 5335951.36 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ee9c2a08-f05e-490c-a03b-c9a70b820f80">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ee9c2a08-f05e-490c-a03b-c9a70b820f80_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ee9c2a08-f05e-490c-a03b-c9a70b820f80_poly_0_">
<gml:posList srsDimension="3">
690901.56 5335945.788 514.67
690901.56 5335945.788 552.18
690901.257 5335945.616 552.18
690901.257 5335945.616 514.67
690901.56 5335945.788 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_3aa30b16-38cf-401b-bee4-650c2be0a4e2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3aa30b16-38cf-401b-bee4-650c2be0a4e2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3aa30b16-38cf-401b-bee4-650c2be0a4e2_poly_0_">
<gml:posList srsDimension="3">
690842.567 5335968.796 514.67
690842.567 5335968.796 532.002
690840.931 5335969.422 532.006
690840.931 5335969.422 514.67
690842.567 5335968.796 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2e36f84a-8228-400e-9ca7-fecb0dfe7417">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2e36f84a-8228-400e-9ca7-fecb0dfe7417_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2e36f84a-8228-400e-9ca7-fecb0dfe7417_poly_0_">
<gml:posList srsDimension="3">
690891.501 5335950.148 514.67
690891.501 5335950.148 531.965
690891.609 5335950.462 532.267
690891.609 5335950.462 514.67
690891.501 5335950.148 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d293b224-fc90-4c8c-901c-6a8793c51028">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d293b224-fc90-4c8c-901c-6a8793c51028_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d293b224-fc90-4c8c-901c-6a8793c51028_poly_0_">
<gml:posList srsDimension="3">
690862.072 5335961.713 514.67
690862.072 5335961.713 532.282
690861.954 5335961.409 531.985
690861.954 5335961.409 514.67
690862.072 5335961.713 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_95c9840d-ccde-41b7-8869-e3377b9cd92b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_95c9840d-ccde-41b7-8869-e3377b9cd92b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_95c9840d-ccde-41b7-8869-e3377b9cd92b_poly_0_">
<gml:posList srsDimension="3">
690848.925 5335966.704 514.67
690848.925 5335966.704 532.276
690846.547 5335967.611 532.28
690846.547 5335967.611 514.67
690848.925 5335966.704 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_b8784bf9-b389-49dd-a266-573d06029ba6">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_b8784bf9-b389-49dd-a266-573d06029ba6_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_b8784bf9-b389-49dd-a266-573d06029ba6_poly_0_">
<gml:posList srsDimension="3">
690843.149 5335976.251 537.23
690843.149 5335976.251 538.52
690849.676 5335973.771 538.52
690849.676 5335973.771 537.23
690843.149 5335976.251 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ffa63fad-6b56-4af1-a804-c6e7d0e94a36">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ffa63fad-6b56-4af1-a804-c6e7d0e94a36_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ffa63fad-6b56-4af1-a804-c6e7d0e94a36_poly_0_">
<gml:posList srsDimension="3">
690842.685 5335969.101 514.67
690842.685 5335969.101 532.299
690842.567 5335968.796 532.002
690842.567 5335968.796 514.67
690842.685 5335969.101 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_deaa4b8d-13dc-408e-8436-e5b195e32566">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_deaa4b8d-13dc-408e-8436-e5b195e32566_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_deaa4b8d-13dc-408e-8436-e5b195e32566_poly_0_">
<gml:posList srsDimension="3">
690915.14 5335940.895 514.67
690915.14 5335940.895 536.713
690915.259 5335941.18 536.974
690915.259 5335941.18 514.67
690915.14 5335940.895 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_2c3924f9-b88f-4765-9a29-29bfe3d9ba33">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_2c3924f9-b88f-4765-9a29-29bfe3d9ba33_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_2c3924f9-b88f-4765-9a29-29bfe3d9ba33_poly_0_">
<gml:posList srsDimension="3">
690912.649 5335942.179 514.67
690912.649 5335942.179 536.978
690912.54 5335941.894 536.721
690912.54 5335941.894 514.67
690912.649 5335942.179 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ba9a25a3-6c5f-40e2-899a-804fdc698162">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ba9a25a3-6c5f-40e2-899a-804fdc698162_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ba9a25a3-6c5f-40e2-899a-804fdc698162_poly_0_">
<gml:posList srsDimension="3">
690846.097 5336023.554 514.67
690846.097 5336023.554 532.471
690845.801 5336023.683 532.197
690845.801 5336023.683 514.67
690846.097 5336023.554 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_4d02f2ed-fc6d-4b5d-8a61-b5ca0722bc4c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_4d02f2ed-fc6d-4b5d-8a61-b5ca0722bc4c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_4d02f2ed-fc6d-4b5d-8a61-b5ca0722bc4c_poly_0_">
<gml:posList srsDimension="3">
690888.455 5335972.219 514.67
690888.455 5335972.219 532.159
690890.447 5335971.466 532.15
690890.447 5335971.466 514.67
690888.455 5335972.219 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_27832f6e-23ee-499f-99bd-1a07eca099e5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_27832f6e-23ee-499f-99bd-1a07eca099e5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_27832f6e-23ee-499f-99bd-1a07eca099e5_poly_0_">
<gml:posList srsDimension="3">
690868.321 5335959.335 514.67
690868.321 5335959.335 532.28
690865.953 5335960.233 532.279
690865.953 5335960.233 514.67
690868.321 5335959.335 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_39924390-7b8e-464d-a0eb-e8f25d564db9">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_39924390-7b8e-464d-a0eb-e8f25d564db9_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_39924390-7b8e-464d-a0eb-e8f25d564db9_poly_0_">
<gml:posList srsDimension="3">
690828.919 5335973.786 514.67
690828.919 5335973.786 535.497
690828.81 5335973.501 535.237
690828.81 5335973.501 514.67
690828.919 5335973.786 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_21d82ce8-37a5-431e-9a89-c211b4e79c21">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_21d82ce8-37a5-431e-9a89-c211b4e79c21_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_21d82ce8-37a5-431e-9a89-c211b4e79c21_poly_0_">
<gml:posList srsDimension="3">
690835.181 5335971.078 514.67
690835.181 5335971.078 535.236
690831.421 5335972.512 535.24
690831.421 5335972.512 514.67
690835.181 5335971.078 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_3b55fe3d-558e-4980-848a-ea8bb08af045">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_3b55fe3d-558e-4980-848a-ea8bb08af045_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_3b55fe3d-558e-4980-848a-ea8bb08af045_poly_0_">
<gml:posList srsDimension="3">
690856.568 5335963.46 514.67
690856.568 5335963.46 531.989
690856.677 5335963.754 532.274
690856.677 5335963.754 514.67
690856.568 5335963.46 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_c1dc10bd-a2c3-4a5b-821f-51fd1290e206">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_c1dc10bd-a2c3-4a5b-821f-51fd1290e206_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_c1dc10bd-a2c3-4a5b-821f-51fd1290e206_poly_0_">
<gml:posList srsDimension="3">
690860.569 5335962.275 514.67
690860.569 5335962.275 532.274
690858.191 5335963.183 532.277
690858.191 5335963.183 514.67
690860.569 5335962.275 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_faf10dd1-f63e-4bac-8354-61aefe528edf">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_faf10dd1-f63e-4bac-8354-61aefe528edf_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_faf10dd1-f63e-4bac-8354-61aefe528edf_poly_0_">
<gml:posList srsDimension="3">
690875.975 5335956.069 514.67
690875.975 5335956.069 531.979
690876.083 5335956.374 532.273
690876.083 5335956.374 514.67
690875.975 5335956.069 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_36f622f5-7adf-46aa-b078-8d1a1caab1f2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_36f622f5-7adf-46aa-b078-8d1a1caab1f2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_36f622f5-7adf-46aa-b078-8d1a1caab1f2_poly_0_">
<gml:posList srsDimension="3">
690869.835 5335958.753 514.67
690869.835 5335958.753 532.275
690869.716 5335958.458 531.986
690869.716 5335958.458 514.67
690869.835 5335958.753 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_049a8c69-4783-45cf-aad7-6c3086eeff7d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_049a8c69-4783-45cf-aad7-6c3086eeff7d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_049a8c69-4783-45cf-aad7-6c3086eeff7d_poly_0_">
<gml:posList srsDimension="3">
690834.219 5335993.949 514.67
690834.219 5335993.949 532.361
690835.14 5335996.257 532.366
690835.14 5335996.257 514.67
690834.219 5335993.949 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_9ebd86f9-9b52-49ca-abf7-076eb6ac014d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_9ebd86f9-9b52-49ca-abf7-076eb6ac014d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_9ebd86f9-9b52-49ca-abf7-076eb6ac014d_poly_0_">
<gml:posList srsDimension="3">
690854.171 5335988.22 514.67
690854.171 5335988.22 537.23
690854.203 5335987.661 537.23
690854.203 5335987.661 514.67
690854.171 5335988.22 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_b8d7ec1a-3837-4d5a-9298-0970e9999ce3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_b8d7ec1a-3837-4d5a-9298-0970e9999ce3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_b8d7ec1a-3837-4d5a-9298-0970e9999ce3_poly_0_">
<gml:posList srsDimension="3">
690877.597 5335955.792 514.67
690877.597 5335955.792 532.268
690877.479 5335955.497 531.979
690877.479 5335955.497 514.67
690877.597 5335955.792 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_659b42db-9365-4195-b9e7-259d33c17dfd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_659b42db-9365-4195-b9e7-259d33c17dfd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_659b42db-9365-4195-b9e7-259d33c17dfd_poly_0_">
<gml:posList srsDimension="3">
690828.313 5335980.098 514.67
690828.313 5335980.098 535.626
690828.037 5335980.228 535.338
690828.037 5335980.228 514.67
690828.313 5335980.098 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ac7e008d-9d27-47de-9409-98cc387c0c73">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ac7e008d-9d27-47de-9409-98cc387c0c73_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ac7e008d-9d27-47de-9409-98cc387c0c73_poly_0_">
<gml:posList srsDimension="3">
690833.407 5335992.427 514.67
690833.407 5335992.427 532.197
690834.036 5335994.022 532.193
690834.036 5335994.022 514.67
690833.407 5335992.427 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_d0abff2a-1f0e-4b53-b2e1-fc9d945427de">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_d0abff2a-1f0e-4b53-b2e1-fc9d945427de_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_d0abff2a-1f0e-4b53-b2e1-fc9d945427de_poly_0_">
<gml:posList srsDimension="3">
690841.144 5336011.951 514.67
690841.144 5336011.951 532.192
690841.772 5336013.557 532.185
690841.772 5336013.557 514.67
690841.144 5336011.951 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly_0_">
<gml:posList srsDimension="3">
690905.712 5335944.214 552.18
690905.712 5335944.214 536.23
690908.914 5335952.814 538.72
690905.712 5335944.214 552.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly.UgAlA5hv6TwQFk3NGfUb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly_0_.ajHngEi4u1WhwCNv6M7o">
<gml:posList srsDimension="3">
690911.805 5335960.581 536.23
690908.914 5335952.814 538.72
690905.712 5335944.214 536.23
690911.805 5335960.581 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly.dzNXwiLj8liIm89fUTpt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly_0_.ke5qNOQrndZ3So5myWmT">
<gml:posList srsDimension="3">
690911.805 5335960.581 538.72
690908.914 5335952.814 538.72
690911.805 5335960.581 536.23
690911.805 5335960.581 536.246
690911.805 5335960.581 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly.hNDetFVsfyoN5KK2CrdX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly_0_.SQANN5m1YzURXZLLGkm0">
<gml:posList srsDimension="3">
690912.155 5335961.522 536.23
690911.805 5335960.581 538.72
690911.805 5335960.581 536.246
690912.155 5335961.522 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly.aKApJ7SVioBVwHUHboOZ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly_0_.WenUQik41ChdIlA8ef1m">
<gml:posList srsDimension="3">
690911.805 5335960.581 538.82
690911.805 5335960.581 538.72
690912.155 5335961.522 536.23
690911.805 5335960.581 538.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly.fJzRz9JKoX7RC0KNlZ3M">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f03a02f1-1aee-4f95-93c5-97994195ec10_poly_0_.k7K2sn4HoqRPHpy0ELeG">
<gml:posList srsDimension="3">
690908.914 5335952.814 538.72
690908.914 5335952.814 552.18
690905.712 5335944.214 552.18
690908.914 5335952.814 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_">
<gml:posList srsDimension="3">
690921.691 5335938.398 514.67
690921.691 5335938.398 536.272
690921.208 5335938.581 514.67
690921.691 5335938.398 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly.wOcl2uuPM5uxocKFrvWI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_.VFQy5c7TDv5q09E02hzB">
<gml:posList srsDimension="3">
690919.71 5335939.151 514.67
690921.208 5335938.581 514.67
690921.691 5335938.398 536.272
690919.71 5335939.151 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly.QnGLAGTr5bCBiXhnMvcl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_.5ij5YoGmtmbKCEKasvu1">
<gml:posList srsDimension="3">
690919.71 5335939.151 536.709
690919.71 5335939.151 514.67
690921.691 5335938.398 536.272
690919.71 5335939.151 536.709
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly.6tZM7N92oV5dL0Ygcg4a">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f491bf6a-6183-444e-ad7f-dc8f3e3c5b1d_poly_0_.PbJjDA0KUWATv92kdvfQ">
<gml:posList srsDimension="3">
690921.208 5335938.581 536.709
690919.71 5335939.151 536.709
690921.691 5335938.398 536.272
690921.208 5335938.581 536.709
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_">
<gml:posList srsDimension="3">
690896.896 5335948.097 514.67
690896.896 5335948.097 552.18
690896.344 5335948.311 514.67
690896.896 5335948.097 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.wfEkxutbxEJa8GftfSTk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.1NWn8xqPxIgoIwihePCG">
<gml:posList srsDimension="3">
690896.344 5335948.311 531.968
690896.344 5335948.311 514.67
690896.896 5335948.097 552.18
690896.344 5335948.311 531.968
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.8JA8PtbhrMG5yUiF2JYn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.Oll6AwIyN8NFmIMNN1FV">
<gml:posList srsDimension="3">
690895.392 5335948.679 514.67
690896.344 5335948.311 514.67
690896.344 5335948.311 531.968
690895.392 5335948.679 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.8J9rrvBBTOHH7bYApvIV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.a1VbI8IoFBYn0eTHxEf9">
<gml:posList srsDimension="3">
690896.344 5335948.311 552.18
690896.344 5335948.311 531.968
690896.896 5335948.097 552.18
690896.344 5335948.311 552.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly.tRILzgKZJ151NhN9xKrZ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e74d58df-98c7-47a8-83b0-302d3182459f_poly_0_.NaXZnpKkjcl5SoCwPJ7f">
<gml:posList srsDimension="3">
690896.344 5335948.311 531.968
690895.392 5335948.679 531.973
690895.392 5335948.679 514.67
690896.344 5335948.311 531.968
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670_poly_0_">
<gml:posList srsDimension="3">
690846.742 5335985.312 534.92
690841.688 5335972.567 534.92
690843.149 5335976.251 537.23
690846.742 5335985.312 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670_poly.nC3FPQNc0zgt2JA9opHW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670_poly_0_.9qvP4emweSKA4k13z5iq">
<gml:posList srsDimension="3">
690843.149 5335976.251 538.52
690843.149 5335976.251 537.23
690841.688 5335972.567 534.92
690843.149 5335976.251 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670_poly.fyq6nLq0Ek6PcdSP63jS">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ada9970e-897d-4001-8ed7-9f09e2b1d670_poly_0_.9yRRePKuys232d1nr6Zs">
<gml:posList srsDimension="3">
690846.742 5335985.312 537.23
690846.742 5335985.312 534.92
690843.149 5335976.251 537.23
690846.742 5335985.312 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly_0_">
<gml:posList srsDimension="3">
690846.742 5335985.312 534.92
690846.742 5335985.312 537.23
690841.754 5335987.208 534.92
690846.742 5335985.312 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly.8W6I0LSWOcSEeb1szIJa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly_0_.E0tBiXhBXOlABuJ5dqUR">
<gml:posList srsDimension="3">
690835.69 5335989.515 534.92
690841.754 5335987.208 534.92
690846.742 5335985.312 537.23
690835.69 5335989.515 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly.QmXF3r9gjg9cKGPkAe5L">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly_0_.63tnffbhwd2qBqcKFyoV">
<gml:posList srsDimension="3">
690841.754 5335987.208 540.45
690835.69 5335989.515 534.92
690846.742 5335985.312 537.23
690841.754 5335987.208 540.45
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly.bOghJbKtS7UlsN3G8NJ9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_cfedfe2d-dfc7-4d57-b9dc-7e6234bafc00_poly_0_.Z0QDj6kRGwaMLtb2VPnp">
<gml:posList srsDimension="3">
690846.742 5335985.312 537.499
690841.754 5335987.208 540.45
690846.742 5335985.312 537.23
690846.742 5335985.312 537.499
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly_0_">
<gml:posList srsDimension="3">
690903.861 5335966.346 514.67
690903.861 5335966.346 532.203
690913.146 5335962.796 514.67
690903.861 5335966.346 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly.qN5as2Rwghg5jZXzUV9S">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly_0_.CC8r9f0L20spzAL4qcno">
<gml:posList srsDimension="3">
690912.694 5335962.969 532.248
690913.146 5335962.796 514.67
690903.861 5335966.346 532.203
690912.694 5335962.969 532.248
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly.hf5las1bFqmkt9vNfa6t">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly_0_.Y3LPFOOrMA2bGKOd3Bi9">
<gml:posList srsDimension="3">
690913.146 5335962.796 536.64
690913.146 5335962.796 514.67
690912.694 5335962.969 532.248
690913.146 5335962.796 536.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly.Iok5aurrO0ZUetVlmD6h">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a663ff29-9866-40d2-8672-d302c12b8242_poly_0_.sVDJYJNHkvSGiEaPxXxT">
<gml:posList srsDimension="3">
690912.694 5335962.969 536.23
690913.146 5335962.796 536.64
690912.694 5335962.969 532.248
690912.694 5335962.969 536.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862_poly_0_">
<gml:posList srsDimension="3">
690913.209 5335962.968 514.67
690913.209 5335962.968 536.639
690928.629 5335957.035 514.67
690913.209 5335962.968 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862_poly.UmurXFigctajGVlNpi3O">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862_poly_0_.C4GXNIsplkfZmPTjQiz2">
<gml:posList srsDimension="3">
690928.629 5335957.035 536.272
690928.629 5335957.035 514.67
690913.209 5335962.968 536.639
690928.629 5335957.035 536.272
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862_poly.tgIsYlUuOHFS12G6UNAl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_f8b14167-598c-46c9-837e-20ebcebf4862_poly_0_.hc5WdZKY1xwzb1pO9yEM">
<gml:posList srsDimension="3">
690928.159 5335957.216 536.698
690928.629 5335957.035 536.272
690913.209 5335962.968 536.639
690928.159 5335957.216 536.698
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_">
<gml:posList srsDimension="3">
690832.196 5335989.338 535.821
690832.325 5335989.668 535.819
690832.196 5335989.338 514.67
690832.196 5335989.338 535.821
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly.8aeyj15K1J7IiO4Z5r7X">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_.XZ228onOWIGb8U5eFBmb">
<gml:posList srsDimension="3">
690832.325 5335989.668 514.67
690832.196 5335989.338 514.67
690832.325 5335989.668 535.819
690832.325 5335989.668 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly.h3xXFQyhVsPKdCPnTd3w">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_.B1VtQWrWAM7TOxsURuXV">
<gml:posList srsDimension="3">
690832.497 5335990.11 535.414
690832.325 5335989.668 514.67
690832.325 5335989.668 535.819
690832.497 5335990.11 535.414
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly.5TJQBMkHfnswFuIm19iT">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_965365ea-265a-4b8a-ae23-11058773db21_poly_0_.j6rSUQnjxGW0oUpf7uci">
<gml:posList srsDimension="3">
690832.497 5335990.11 514.67
690832.325 5335989.668 514.67
690832.497 5335990.11 535.414
690832.497 5335990.11 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly_0_">
<gml:posList srsDimension="3">
690911.805 5335960.581 538.72
690911.805 5335960.581 538.82
690902.263 5335964.21 538.72
690911.805 5335960.581 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly.iAYO0ZElmrUHgFWL3VEL">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly_0_.QdtKdvL5PdSheBNxlgnC">
<gml:posList srsDimension="3">
690902.263 5335964.21 538.52
690902.263 5335964.21 538.72
690853.078 5335982.903 538.52
690902.263 5335964.21 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly.Mcr7uiqZSVcZ8JrOeLbV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly_0_.bL6Odtew91Lc8zHCHUiJ">
<gml:posList srsDimension="3">
690853.078 5335982.903 538.82
690853.078 5335982.903 538.52
690902.263 5335964.21 538.72
690853.078 5335982.903 538.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly.hMHhOkcasafrtZ8pQELE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_66733546-4a35-44bc-afdb-9777976170f0_poly_0_.FaIloFIB3Aa8clOWf9xs">
<gml:posList srsDimension="3">
690911.805 5335960.581 538.82
690853.078 5335982.903 538.82
690902.263 5335964.21 538.72
690911.805 5335960.581 538.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly_0_">
<gml:posList srsDimension="3">
690853.83 5335989.277 514.67
690853.695 5335989.489 514.67
690853.83 5335989.277 537.23
690853.83 5335989.277 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly.9UssJOaYkFugZIsesjSA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly_0_.DJCWMGRXKIbYHsBUx2fM">
<gml:posList srsDimension="3">
690853.695 5335989.489 537.23
690853.83 5335989.277 537.23
690853.695 5335989.489 514.67
690853.695 5335989.489 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly.n8NRsaY0C8m7AH6cv8Gp">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly_0_.SniMznlbpaM586rzZ3uI">
<gml:posList srsDimension="3">
690853.456 5335989.863 514.67
690853.695 5335989.489 537.23
690853.695 5335989.489 514.67
690853.456 5335989.863 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly.UyoV529LB2GXdjbGroxl">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_bb6a288a-2dd2-4710-9b8a-855ba10d980b_poly_0_.UlcPjXHx4nOTjzZSnDJa">
<gml:posList srsDimension="3">
690853.456 5335989.863 537.23
690853.695 5335989.489 537.23
690853.456 5335989.863 514.67
690853.456 5335989.863 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12_poly_0_">
<gml:posList srsDimension="3">
690846.103 5336024.425 514.67
690846.103 5336024.425 532.202
690863.058 5336017.78 514.67
690846.103 5336024.425 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12_poly.qt1LamUkhVzQvPC5q3oM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12_poly_0_.b0QhRpnL320YUWTGIaLz">
<gml:posList srsDimension="3">
690863.058 5336017.78 535.748
690863.058 5336017.78 514.67
690846.103 5336024.425 532.202
690863.058 5336017.78 535.748
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12_poly.n8xP0dIJ2ekcFJJLXoJa">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_6b13086e-1f15-4e41-893d-53bf703fce12_poly_0_.uTWAXBcyj7xzq2Aws6pN">
<gml:posList srsDimension="3">
690855.111 5336020.894 540.45
690863.058 5336017.78 535.748
690846.103 5336024.425 532.202
690855.111 5336020.894 540.45
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3_poly_0_">
<gml:posList srsDimension="3">
690853.078 5335982.903 538.82
690853.293 5335983.481 537.23
690853.078 5335982.903 538.52
690853.078 5335982.903 538.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3_poly.shllM0NZItBwmyRz1HPi">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3_poly_0_.prA2bA3gLixMXemGNqde">
<gml:posList srsDimension="3">
690849.676 5335973.771 538.52
690853.078 5335982.903 538.52
690849.676 5335973.771 537.23
690849.676 5335973.771 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3_poly.SsWkYHvCKFfaKOC3x1JA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_65af2d65-f853-4a91-878f-4b906b4048f3_poly_0_.n09AwmTZf7OfE00WRkMT">
<gml:posList srsDimension="3">
690853.293 5335983.481 537.23
690849.676 5335973.771 537.23
690853.078 5335982.903 538.52
690853.293 5335983.481 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_">
<gml:posList srsDimension="3">
690832.791 5335990.011 535.404
690832.995 5335990.541 534.92
690832.791 5335990.011 514.67
690832.791 5335990.011 535.404
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly.W9gbs0kwL8buYQvQ3Ry9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_.9MIu75vCRv8vy0T1Aehu">
<gml:posList srsDimension="3">
690832.995 5335990.541 532.463
690832.791 5335990.011 514.67
690832.995 5335990.541 534.92
690832.995 5335990.541 532.463
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly.2NfmfwoTW00WVJRB1bdG">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_.yNdR5Uvmyam7hHAwcr3r">
<gml:posList srsDimension="3">
690833.672 5335992.297 514.67
690832.791 5335990.011 514.67
690832.995 5335990.541 532.463
690833.672 5335992.297 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly.LBMrsoIZYYA5Sa7Js3Ap">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_ca79422d-5719-4ba6-9c3d-e50933c3bc00_poly_0_.s1dZhULJvWTWpR4y824c">
<gml:posList srsDimension="3">
690833.672 5335992.297 532.448
690833.672 5335992.297 514.67
690832.995 5335990.541 532.463
690833.672 5335992.297 532.448
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly_0_">
<gml:posList srsDimension="3">
690888.455 5335972.219 514.67
690853.565 5335985.545 514.67
690854.0 5335985.379 532.006
690888.455 5335972.219 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly.qXBcMQzxvfzzU9l1t5BD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly_0_.CgcdHzB14BuW2JmhBOqM">
<gml:posList srsDimension="3">
690853.565 5335985.545 537.23
690854.0 5335985.379 532.006
690853.565 5335985.545 514.67
690853.565 5335985.545 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly.iS9iReCQpbsc1yG9lSSf">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly_0_.ImJNUb1Zla7H15AfFZnB">
<gml:posList srsDimension="3">
690854.0 5335985.379 537.23
690854.0 5335985.379 532.006
690853.565 5335985.545 537.23
690854.0 5335985.379 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly.Iyjq4tVbHTNY2aTH1zjo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_8bfaf748-92e8-471f-afe0-e973f39a405a_poly_0_.lHy8BnJuZCcIGDNkIFOc">
<gml:posList srsDimension="3">
690854.0 5335985.379 532.006
690888.455 5335972.219 532.159
690888.455 5335972.219 514.67
690854.0 5335985.379 532.006
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly_0_">
<gml:posList srsDimension="3">
690852.391 5335990.742 514.67
690852.391 5335990.742 535.72
690852.457 5335990.697 514.67
690852.391 5335990.742 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly.CARyYvtTFqGq5rqSNHnO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly_0_.cRmjtKCJ02Q66yKjIBvV">
<gml:posList srsDimension="3">
690852.967 5335990.354 514.67
690852.457 5335990.697 514.67
690852.391 5335990.742 535.72
690852.967 5335990.354 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly.izS3JRjI7uLEt8yBfsD6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly_0_.YGYqloP7c8pc1dvK4Slw">
<gml:posList srsDimension="3">
690852.457 5335990.697 535.678
690852.967 5335990.354 514.67
690852.391 5335990.742 535.72
690852.457 5335990.697 535.678
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly.DWYSFceopiuBCBs0x7tq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly_0_.0ZMXAiNagHje47vvILnq">
<gml:posList srsDimension="3">
690852.967 5335990.354 514.67
690852.457 5335990.697 535.678
690852.967 5335990.354 537.23
690852.967 5335990.354 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly.Zroowux1UuXAIXW4uW8W">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_e2d273f7-f61f-4ae2-8998-14da42a192e7_poly_0_.4JtWf4QKUBKtzXNTUrOX">
<gml:posList srsDimension="3">
690852.457 5335990.697 537.23
690852.967 5335990.354 537.23
690852.457 5335990.697 535.678
690852.457 5335990.697 537.23
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_">
<gml:posList srsDimension="3">
690840.759 5335968.955 514.67
690840.759 5335968.955 531.554
690840.014 5335969.236 514.67
690840.759 5335968.955 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.649r3MdoxZRBxmb6HvCA">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.tLQMA8eTJpZ3bDygvosu">
<gml:posList srsDimension="3">
690837.782 5335970.079 514.67
690840.014 5335969.236 514.67
690840.759 5335968.955 531.554
690837.782 5335970.079 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.r5nAW8eaXHaHPt4waHiR">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.awyQrXgJYbuvEoq4KXPK">
<gml:posList srsDimension="3">
690840.321 5335969.12 531.553
690837.782 5335970.079 514.67
690840.759 5335968.955 531.554
690840.321 5335969.12 531.553
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.sAM2vsX5wRdM9FEYmR0C">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.2IFVUcD7gxVYgEFVxufH">
<gml:posList srsDimension="3">
690837.782 5335970.079 514.67
690840.321 5335969.12 531.553
690837.782 5335970.079 535.228
690837.782 5335970.079 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.q33btEEap1NRW7FpTMbI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.PnWOH7ybBMxnIEutqwDf">
<gml:posList srsDimension="3">
690840.321 5335969.12 534.92
690837.782 5335970.079 535.228
690840.321 5335969.12 531.553
690840.321 5335969.12 534.92
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly.qpguTn5pCk6llI6hzE8J">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_7b11e516-a57e-4edf-9cfd-d0bb0de1fa2b_poly_0_.bZwFVIsESCWy3XQvwxLR">
<gml:posList srsDimension="3">
690840.014 5335969.236 535.232
690837.782 5335970.079 535.228
690840.321 5335969.12 534.92
690840.014 5335969.236 535.232
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly_0_">
<gml:posList srsDimension="3">
690896.344 5335948.311 552.18
690899.371 5335956.441 552.18
690896.344 5335948.311 531.968
690896.344 5335948.311 552.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly.hwnyjbBJGELisJN42J2p">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly_0_.SbIny9VyR4he9Y2L6Bm0">
<gml:posList srsDimension="3">
690898.862 5335955.073 538.52
690896.344 5335948.311 531.968
690899.371 5335956.441 552.18
690898.862 5335955.073 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly.fKI7WGhtNCTLUXMyaR9X">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly_0_.AVyI3eXtzvAeh9ERGRJF">
<gml:posList srsDimension="3">
690899.371 5335956.441 538.72
690898.862 5335955.073 538.52
690899.371 5335956.441 552.18
690899.371 5335956.441 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly.3TLWoZg0hD3jaLh6DfPr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly_0_.JRlXMWxkx3QhELczwryP">
<gml:posList srsDimension="3">
690902.263 5335964.21 538.52
690898.862 5335955.073 538.52
690899.371 5335956.441 538.72
690902.263 5335964.21 538.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly.K8yDhkbzjRbs1OG81xx6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_a4a5cea9-b35f-44da-977d-dc78d5b5493c_poly_0_.BTObX8KbJJGuzCFb5P5f">
<gml:posList srsDimension="3">
690902.263 5335964.21 538.72
690902.263 5335964.21 538.52
690899.371 5335956.441 538.72
690902.263 5335964.21 538.72
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906965_96ab9232-ae84-457f-8bd8-bc8e7a252d10">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906965_96ab9232-ae84-457f-8bd8-bc8e7a252d10_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906965_96ab9232-ae84-457f-8bd8-bc8e7a252d10_poly_0_">
<gml:posList srsDimension="3">
690926.083 5335950.21 514.67
690921.808 5335938.713 514.67
690921.691 5335938.398 514.67
690921.208 5335938.581 514.67
690919.71 5335939.151 514.67
690919.829 5335939.446 514.67
690917.229 5335940.436 514.67
690917.111 5335940.141 514.67
690915.14 5335940.895 514.67
690915.259 5335941.18 514.67
690912.649 5335942.179 514.67
690912.54 5335941.894 514.67
690910.581 5335942.628 514.67
690910.689 5335942.923 514.67
690908.089 5335943.922 514.67
690907.97 5335943.627 514.67
690906.925 5335943.997 514.67
690906.823 5335943.793 514.67
690906.01 5335944.101 514.67
690905.712 5335944.214 514.67
690901.56 5335945.788 514.67
690901.257 5335945.616 514.67
690898.066 5335946.832 514.67
690897.852 5335947.204 514.67
690896.724 5335947.63 514.67
690896.896 5335948.097 514.67
690896.344 5335948.311 514.67
690895.392 5335948.679 514.67
690895.49 5335948.983 514.67
690893.123 5335949.881 514.67
690893.004 5335949.586 514.67
690891.501 5335950.148 514.67
690891.609 5335950.462 514.67
690889.231 5335951.36 514.67
690889.123 5335951.066 514.67
690887.619 5335951.627 514.67
690887.727 5335951.942 514.67
690885.36 5335952.841 514.67
690885.241 5335952.546 514.67
690883.738 5335953.118 514.67
690883.836 5335953.412 514.67
690881.489 5335954.312 514.67
690881.36 5335954.017 514.67
690879.847 5335954.588 514.67
690879.965 5335954.893 514.67
690877.597 5335955.792 514.67
690877.479 5335955.497 514.67
690875.975 5335956.069 514.67
690876.083 5335956.374 514.67
690873.726 5335957.273 514.67
690873.597 5335956.978 514.67
690872.084 5335957.55 514.67
690872.202 5335957.845 514.67
690869.835 5335958.753 514.67
690869.716 5335958.458 514.67
690868.212 5335959.03 514.67
690868.321 5335959.335 514.67
690865.953 5335960.233 514.67
690865.835 5335959.929 514.67
690864.331 5335960.51 514.67
690864.45 5335960.805 514.67
690862.072 5335961.713 514.67
690861.954 5335961.409 514.67
690860.45 5335961.98 514.67
690860.569 5335962.275 514.67
690858.191 5335963.183 514.67
690858.072 5335962.888 514.67
690856.568 5335963.46 514.67
690856.677 5335963.754 514.67
690854.32 5335964.653 514.67
690854.191 5335964.368 514.67
690852.687 5335964.939 514.67
690852.806 5335965.234 514.67
690850.438 5335966.142 514.67
690850.32 5335965.838 514.67
690848.806 5335966.409 514.67
690848.925 5335966.704 514.67
690846.547 5335967.611 514.67
690846.438 5335967.327 514.67
690844.925 5335967.888 514.67
690845.053 5335968.193 514.67
690842.685 5335969.101 514.67
690842.567 5335968.796 514.67
690840.931 5335969.422 514.67
690840.759 5335968.955 514.67
690840.014 5335969.236 514.67
690837.782 5335970.079 514.67
690837.881 5335970.373 514.67
690835.29 5335971.363 514.67
690835.181 5335971.078 514.67
690831.421 5335972.512 514.67
690831.52 5335972.796 514.67
690828.919 5335973.786 514.67
690828.81 5335973.501 514.67
690825.803 5335974.645 514.67
690826.988 5335977.624 514.67
690827.292 5335977.526 514.67
690828.313 5335980.098 514.67
690828.037 5335980.228 514.67
690829.524 5335983.939 514.67
690829.778 5335983.839 514.67
690830.809 5335986.421 514.67
690830.543 5335986.551 514.67
690831.718 5335989.529 514.67
690832.196 5335989.338 514.67
690832.325 5335989.668 514.67
690832.497 5335990.11 514.67
690832.791 5335990.011 514.67
690833.672 5335992.297 514.67
690833.407 5335992.427 514.67
690834.036 5335994.022 514.67
690834.219 5335993.949 514.67
690835.14 5335996.257 514.67
690834.957 5335996.329 514.67
690835.585 5335997.915 514.67
690835.778 5335997.852 514.67
690836.689 5336000.159 514.67
690836.496 5336000.232 514.67
690837.124 5336001.827 514.67
690837.337 5336001.765 514.67
690838.238 5336004.072 514.67
690838.045 5336004.155 514.67
690838.673 5336005.74 514.67
690838.866 5336005.668 514.67
690839.787 5336007.975 514.67
690839.594 5336008.068 514.67
690840.222 5336009.653 514.67
690840.517 5336009.535 514.67
690841.428 5336011.842 514.67
690841.144 5336011.951 514.67
690841.772 5336013.557 514.67
690842.067 5336013.428 514.67
690842.978 5336015.736 514.67
690842.692 5336015.875 514.67
690843.331 5336017.461 514.67
690843.616 5336017.342 514.67
690844.537 5336019.65 514.67
690844.242 5336019.778 514.67
690844.881 5336021.365 514.67
690845.175 5336021.256 514.67
690846.097 5336023.554 514.67
690845.801 5336023.683 514.67
690846.103 5336024.425 514.67
690863.058 5336017.78 514.67
690860.125 5336010.35 514.67
690860.673 5336010.141 514.67
690858.816 5336005.414 514.67
690858.257 5336005.623 514.67
690852.391 5335990.742 514.67
690852.457 5335990.697 514.67
690852.967 5335990.354 514.67
690853.456 5335989.863 514.67
690853.695 5335989.489 514.67
690853.83 5335989.277 514.67
690854.04 5335988.765 514.67
690854.171 5335988.22 514.67
690854.203 5335987.661 514.67
690854.154 5335987.109 514.67
690853.565 5335985.545 514.67
690888.455 5335972.219 514.67
690890.447 5335971.466 514.67
690913.146 5335962.796 514.67
690913.209 5335962.968 514.67
690928.629 5335957.035 514.67
690926.083 5335950.21 514.67
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>