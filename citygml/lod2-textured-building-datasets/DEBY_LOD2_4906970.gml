<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-02 14:06:03 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Downloads\tum2twin-datasets-main\citygml\lod2-building-datasets\DEBY_LOD2_4906970.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690921.647 5335934.028 514.82</gml:lowerCorner>
         <gml:upperCorner>690938.275 5335950.21 535.81</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906970">
<gml:name>DEBY_LOD2_4906970</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJG3</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906970/IMG_6234.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906970_c368b4d3-0164-4919-b93a-65c11c4df672_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906970_c368b4d3-0164-4919-b93a-65c11c4df672_poly_0_">
0.9989362553727545 0.0009850879889716326 0.9989362553727545 0.9993037903690126 0.0013179488645944926 0.999243425227981 0.0013179488645944926 0.0009850879889716326 0.9989362553727545 0.0009850879889716326
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">20.99</bldg:measuredHeight>
<bldg:storeysAboveGround>3</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906970_412ffb9a-a2c3-4547-b862-de9a88133182">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_412ffb9a-a2c3-4547-b862-de9a88133182_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_412ffb9a-a2c3-4547-b862-de9a88133182_poly_0_">
<gml:posList srsDimension="3">
690935.569 5335939.796 535.81
690937.019 5335943.397 533.2
690925.222 5335947.895 533.2
690923.869 5335944.257 535.81
690935.569 5335939.796 535.81
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906970_2d1b6565-05c4-4ec7-8bd7-ee7ee648661f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_2d1b6565-05c4-4ec7-8bd7-ee7ee648661f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_2d1b6565-05c4-4ec7-8bd7-ee7ee648661f_poly_0_">
<gml:posList srsDimension="3">
690922.518 5335940.62 533.2
690934.118 5335936.196 533.2
690923.869 5335944.257 535.81
690922.518 5335940.62 533.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_2d1b6565-05c4-4ec7-8bd7-ee7ee648661f_poly.1sBYmajzZp7l78jlQqc8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_2d1b6565-05c4-4ec7-8bd7-ee7ee648661f_poly_0_.FIh9TTjHK1yAIirTuFvl">
<gml:posList srsDimension="3">
690935.569 5335939.796 535.81
690923.869 5335944.257 535.81
690934.118 5335936.196 533.2
690935.569 5335939.796 535.81
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly_0_">
<gml:posList srsDimension="3">
690925.222 5335947.895 532.52
690937.019 5335943.397 532.52
690926.083 5335950.21 531.452
690925.222 5335947.895 532.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly.nrEvWVuG6tTcOqx4p0PP">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly_0_.CV64Qa8zniFfVN4m2YZD">
<gml:posList srsDimension="3">
690937.904 5335945.702 531.452
690926.083 5335950.21 531.452
690937.019 5335943.397 532.52
690937.904 5335945.702 531.452
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly.493MuQiyGYKNvD914XyV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly_0_.hJwk8Dp4AlHc5hGQ5DUO">
<gml:posList srsDimension="3">
690937.082 5335943.553 532.447
690937.904 5335945.702 531.452
690937.019 5335943.397 532.52
690937.082 5335943.553 532.447
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly.sXHEaoIh7nf2uMrCqYPj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly_0_.m9GsshzXfzsVsNLJlTte">
<gml:posList srsDimension="3">
690937.102 5335943.545 532.447
690937.904 5335945.702 531.452
690937.082 5335943.553 532.447
690937.102 5335943.545 532.447
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly.GH16bLbE4jTcEohUamsE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly_0_.E1Q0yZAdGPjEMqhCszNk">
<gml:posList srsDimension="3">
690937.457 5335943.417 532.444
690937.904 5335945.702 531.452
690937.102 5335943.545 532.447
690937.457 5335943.417 532.444
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly.GlDn8FSiFtR1T9W3vHDm">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_322159a7-b41e-4cf1-94f3-fc4a22bda1fa_poly_0_.7spZrCf5K6mDzrWFlMKO">
<gml:posList srsDimension="3">
690938.275 5335945.561 531.452
690937.904 5335945.702 531.452
690937.457 5335943.417 532.444
690938.275 5335945.561 531.452
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906970_a63d16cc-092a-4470-aaba-19fe006af35e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_a63d16cc-092a-4470-aaba-19fe006af35e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_a63d16cc-092a-4470-aaba-19fe006af35e_poly_0_">
<gml:posList srsDimension="3">
690921.808 5335938.713 531.357
690933.358 5335934.309 531.358
690922.518 5335940.62 532.74
690921.808 5335938.713 531.357
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_a63d16cc-092a-4470-aaba-19fe006af35e_poly.AncxUxVIHSXQXX2jKlm2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_a63d16cc-092a-4470-aaba-19fe006af35e_poly_0_.jVCxT0vL4pf7VZa2puFf">
<gml:posList srsDimension="3">
690934.118 5335936.196 532.74
690922.518 5335940.62 532.74
690933.358 5335934.309 531.358
690934.118 5335936.196 532.74
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_acf142e0-f6f6-4b95-9368-09a5e5906df4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_acf142e0-f6f6-4b95-9368-09a5e5906df4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_acf142e0-f6f6-4b95-9368-09a5e5906df4_poly_0_">
<gml:posList srsDimension="3">
690938.275 5335945.561 514.82
690938.275 5335945.561 531.452
690937.457 5335943.417 532.444
690937.457 5335943.417 514.82
690938.275 5335945.561 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_82f43746-eb7f-4892-a87f-e25a86c52344">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_82f43746-eb7f-4892-a87f-e25a86c52344_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_82f43746-eb7f-4892-a87f-e25a86c52344_poly_0_">
<gml:posList srsDimension="3">
690925.222 5335947.895 532.52
690925.222 5335947.895 533.2
690937.019 5335943.397 533.2
690937.019 5335943.397 532.52
690925.222 5335947.895 532.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_c368b4d3-0164-4919-b93a-65c11c4df672">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_c368b4d3-0164-4919-b93a-65c11c4df672_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_c368b4d3-0164-4919-b93a-65c11c4df672_poly_0_">
<gml:posList srsDimension="3">
690933.358 5335934.309 514.82
690933.358 5335934.309 531.358
690921.808 5335938.713 531.357
690921.808 5335938.713 514.82
690933.358 5335934.309 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_c38c5955-de48-491e-83a8-9812f970fab5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_c38c5955-de48-491e-83a8-9812f970fab5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_c38c5955-de48-491e-83a8-9812f970fab5_poly_0_">
<gml:posList srsDimension="3">
690934.118 5335936.196 532.74
690934.118 5335936.196 533.2
690922.518 5335940.62 533.2
690922.518 5335940.62 532.74
690934.118 5335936.196 532.74
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_">
<gml:posList srsDimension="3">
690922.518 5335940.62 533.2
690923.869 5335944.257 535.81
690922.518 5335940.62 532.74
690922.518 5335940.62 533.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly.GqEj6CevHTBlCncJrLRV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_.vogv14LkoIHZeiAmQSkn">
<gml:posList srsDimension="3">
690921.808 5335938.713 531.357
690922.518 5335940.62 532.74
690921.808 5335938.713 514.82
690921.808 5335938.713 531.357
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly.5XrN0mPKhbF9ApLQ9Y0X">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_.zmVKbhKZJK3WarfLAu3d">
<gml:posList srsDimension="3">
690926.083 5335950.21 514.82
690921.808 5335938.713 514.82
690922.518 5335940.62 532.74
690926.083 5335950.21 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly.hVVw6Rr1Xti60Vzgcwv3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_.ERY826lvsx29YIfuV8Wg">
<gml:posList srsDimension="3">
690923.869 5335944.257 535.81
690926.083 5335950.21 514.82
690922.518 5335940.62 532.74
690923.869 5335944.257 535.81
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly.RFDsq6LBLm37nlt5DbSn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_.4ShiFI1rkKKh769K2Rq5">
<gml:posList srsDimension="3">
690925.222 5335947.895 532.52
690926.083 5335950.21 514.82
690923.869 5335944.257 535.81
690925.222 5335947.895 532.52
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly.whOCQkSAhSO6aCIiZBV4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_.EyKFb8yJXpHwxxMj7J3s">
<gml:posList srsDimension="3">
690925.222 5335947.895 533.2
690925.222 5335947.895 532.52
690923.869 5335944.257 535.81
690925.222 5335947.895 533.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly.EcSZpVQ1rryOcBFMcgT6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_918bbac4-f5bb-4498-96e3-599bb652c4e8_poly_0_.LPOlJgYDHxYdIL3LWrRG">
<gml:posList srsDimension="3">
690926.083 5335950.21 531.452
690926.083 5335950.21 514.82
690925.222 5335947.895 532.52
690926.083 5335950.21 531.452
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly_0_">
<gml:posList srsDimension="3">
690937.457 5335943.417 514.82
690937.457 5335943.417 532.444
690937.102 5335943.545 514.82
690937.457 5335943.417 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly.dRPAFQQIVDPbTIL3RqFD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly_0_.VHX8ty1IgKNvq6aEgtvA">
<gml:posList srsDimension="3">
690937.082 5335943.553 514.82
690937.102 5335943.545 514.82
690937.457 5335943.417 532.444
690937.082 5335943.553 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly.ebJFTPxd9WegGbadXh8K">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly_0_.TjMvr71A39qWyVFn0g6M">
<gml:posList srsDimension="3">
690937.082 5335943.553 532.447
690937.082 5335943.553 514.82
690937.457 5335943.417 532.444
690937.082 5335943.553 532.447
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly.gYQEq2QprA2NviycAgYX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_a7f99420-0663-4794-a778-cfb68d46e1ca_poly_0_.2KlnPKqrfPUu47jhlRKf">
<gml:posList srsDimension="3">
690937.102 5335943.545 532.447
690937.082 5335943.553 532.447
690937.457 5335943.417 532.444
690937.102 5335943.545 532.447
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly_0_">
<gml:posList srsDimension="3">
690926.083 5335950.21 514.82
690926.083 5335950.21 531.452
690937.904 5335945.702 514.82
690926.083 5335950.21 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly.HK5vykgVk75YACS94O40">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly_0_.HO0XOD1xYm1XnLsvomfM">
<gml:posList srsDimension="3">
690938.275 5335945.561 514.82
690937.904 5335945.702 514.82
690926.083 5335950.21 531.452
690938.275 5335945.561 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly.YIem3Sq47fT25FtlHYY3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly_0_.Lnl1BOfI1H62Sx67fk7q">
<gml:posList srsDimension="3">
690938.275 5335945.561 531.452
690938.275 5335945.561 514.82
690926.083 5335950.21 531.452
690938.275 5335945.561 531.452
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly.5BpC5rf92K0UpcLwTOog">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_7cf6dec6-dbef-4ae5-827b-7da7e68d7381_poly_0_.VmCgdHzWNnv03ptETR5t">
<gml:posList srsDimension="3">
690937.904 5335945.702 531.452
690938.275 5335945.561 531.452
690926.083 5335950.21 531.452
690937.904 5335945.702 531.452
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_">
<gml:posList srsDimension="3">
690937.019 5335943.397 514.82
690937.082 5335943.553 514.82
690937.019 5335943.397 532.52
690937.019 5335943.397 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly.KEQbgptOQzECVd6kROV7">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_.iLpwtCPPBOqqWPdnjcwe">
<gml:posList srsDimension="3">
690937.082 5335943.553 532.447
690937.019 5335943.397 532.52
690937.082 5335943.553 514.82
690937.082 5335943.553 532.447
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly.YhzZxju201KeSeS6ai5N">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_.O5sww26yFEiOhDrGDyqF">
<gml:posList srsDimension="3">
690933.358 5335934.309 514.82
690937.019 5335943.397 514.82
690933.358 5335934.309 531.358
690933.358 5335934.309 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly.IyJaNg53HZRJGoiOeK2E">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_.Xu4V7Bb6Xrgzor9EZiaf">
<gml:posList srsDimension="3">
690934.118 5335936.196 532.74
690933.358 5335934.309 531.358
690937.019 5335943.397 514.82
690934.118 5335936.196 532.74
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly.3oN6Oa390RpiLEgmxS8r">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_.xc2LwTGW1XtJuOcNYNyo">
<gml:posList srsDimension="3">
690934.118 5335936.196 533.2
690934.118 5335936.196 532.74
690937.019 5335943.397 514.82
690934.118 5335936.196 533.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly.nD7hRnju3HvyAMB7nuPq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_.BB4gAhsGQCaigLHOcLQh">
<gml:posList srsDimension="3">
690935.569 5335939.796 535.81
690934.118 5335936.196 533.2
690937.019 5335943.397 514.82
690935.569 5335939.796 535.81
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly.RKBGM66J12A9NQAIiK8v">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_5a809dcc-9c96-431c-a0f7-eaca0234fb6a_poly_0_.sWcitXSfY6nflpfkF4n9">
<gml:posList srsDimension="3">
690937.019 5335943.397 533.2
690935.569 5335939.796 535.81
690937.019 5335943.397 514.82
690937.019 5335943.397 533.2
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906970_aeca15e3-9fba-4432-b682-1db09f1e674f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906970_aeca15e3-9fba-4432-b682-1db09f1e674f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906970_aeca15e3-9fba-4432-b682-1db09f1e674f_poly_0_">
<gml:posList srsDimension="3">
690937.457 5335943.417 514.82
690937.102 5335943.545 514.82
690937.082 5335943.553 514.82
690937.019 5335943.397 514.82
690933.358 5335934.309 514.82
690921.808 5335938.713 514.82
690926.083 5335950.21 514.82
690937.904 5335945.702 514.82
690938.275 5335945.561 514.82
690937.457 5335943.417 514.82
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>