<?xml version="1.0" encoding="utf-8"?>
<!-- Exported with 3DIS GmbH (https://www.3dis.de) CityEditor 3.1.0.0 -->
<!-- Date: 2024-07-25 17:48:01 +0200 ModelName: unnamed ModelPath: C:\Users\ge96wis\Documents\ProjezierteGML\DEBY_LOD2_4906966_projStep.skp -->
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:noise="http://www.citygml.org/ade/noise_de/2.0" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.citygml.org/ade/noise_de/2.0 http://schemas.opengis.net/citygml/examples/2.0/ade/noise-ade/CityGML-NoiseADE.xsd ">
<gml:boundedBy>
     <gml:Envelope srsName="EPSG:25832" srsDimension="3">
         <gml:lowerCorner>690845.615 5336017.78 512.64</gml:lowerCorner>
         <gml:upperCorner>690892.666 5336102.173 542.17</gml:upperCorner>
    </gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DEBY_LOD2_4906966">
<gml:name>DEBY_LOD2_4906966</gml:name>
<core:creationDate>2022-09-01</core:creationDate>
<core:externalReference>
  <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
  <core:externalObject>
    <core:name>DEBYvAAAAABTMJRG</core:name> 
  </core:externalObject>
</core:externalReference>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_63197fed-2bd7-4254-9523-93862f8e2a77_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_63197fed-2bd7-4254-9523-93862f8e2a77_poly_0_">
0.9004230520262304 0.0007203683683929535 0.9004230520262304 0.9986260395908803 0.12777185081707287 0.9992805572482762 0.12777185081707287 0.0007203683683929535 0.9004230520262304 0.0007203683683929535
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227#2.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_46370a0a-c954-4c79-8aa4-9b3a529bb848_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_46370a0a-c954-4c79-8aa4-9b3a529bb848_poly_0_">
0.9283068120974258 0.0006901265856336787 0.9283068120974258 0.9873871620683 0.045277778640120836 0.9991239367005982 0.045277778640120836 0.0006901265856336787 0.9283068120974258 0.0006901265856336787
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227#2_EYmKS6fYvPhFev1FlKgT.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_ee899ad8-ebcb-4b8a-a21a-872c4269e8d0_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_ee899ad8-ebcb-4b8a-a21a-872c4269e8d0_poly_0_">
0.931639349280831 0.0006898917232128786 0.931639349280831 0.9878163178867934 0.04738377673023564 0.9991239975457017 0.04738377673023564 0.0006898917232128786 0.931639349280831 0.0006898917232128786
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_23MpeZDNdXNtoqGsP8d6.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_baf71027-627b-4a91-acc7-9e040731db1f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_baf71027-627b-4a91-acc7-9e040731db1f_poly_0_">
0.995249223652876 0.000680377838742694 0.995249223652876 0.9989376678092813 0.0066808014215373746 0.9993206146406071 0.0066808014215373746 0.000680377838742694 0.995249223652876 0.000680377838742694
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_fXeyJhWAYCncILxBP8E5.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_d35c49e1-73b4-49ab-8f2a-fbf8f95f41df_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_d35c49e1-73b4-49ab-8f2a-fbf8f95f41df_poly_0_">
0.8936440817003586 0.0007134817978093067 0.8936440817003586 0.9992333960839841 0.12154421269400473 0.999326964927566 0.12154421269400473 0.0007134817978093067 0.8936440817003586 0.0007134817978093067
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_jAY6Y53US5G5NFzVlQ6W.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_6f7bc8ce-1f40-41d7-92de-0c70fb20d93d_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_6f7bc8ce-1f40-41d7-92de-0c70fb20d93d_poly_0_">
0.996486760976353 0.0007165564974686724 0.996486760976353 0.99928133943882 0.004929971549898049 0.9985333158175332 0.004929971549898049 0.0007165564974686724 0.996486760976353 0.0007165564974686724
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_DnFBGju3OwQbJ64Odx5f.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_86b2e4d8-a117-47d5-914c-4bfc8c39eade_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_86b2e4d8-a117-47d5-914c-4bfc8c39eade_poly_0_">
0.9910249552536614 0.0006720210187175045 0.9910249552536614 0.9987759227943005 0.006605082181188493 0.9993283709879006 0.006605082181188493 0.0006720210187175045 0.9910249552536614 0.0006720210187175045
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_LoxRnNtiKl4nNp5SZrcC.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_2082c916-2dc0-4435-b5c2-9c77e4aa4ddb_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_2082c916-2dc0-4435-b5c2-9c77e4aa4ddb_poly_0_">
0.9609442151590404 0.000712606837210822 0.9609442151590404 0.9969611920136902 0.041765967400441806 0.9992536772524263 0.041765967400441806 0.000712606837210822 0.9609442151590404 0.000712606837210822
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_TpOaXdYFLlqhEipesgXq.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_bafd6f8a-c368-42ec-8c91-f1568d180802_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_bafd6f8a-c368-42ec-8c91-f1568d180802_poly_0_">
0.9500340716269751 0.0006653409083199393 0.9500340716269751 0.9992885836478438 0.04126547712488282 0.996995909755034 0.04126547712488282 0.0006653409083199393 0.9500340716269751 0.0006653409083199393
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6224.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_e46a61e6-9b73-4c39-8740-7c475e4556d4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_e46a61e6-9b73-4c39-8740-7c475e4556d4_poly_0_">
0.9353145333147026 0.0007611447628162781 0.9353145333147026 0.999185426677771 0.07509929016910633 0.9977840638791631 0.07509929016910633 0.0007611447628162781 0.9353145333147026 0.0007611447628162781
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_Ze1g6Twnb54C9iX0Iz0K.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_9e9ccafb-7add-4ebb-92d0-b3d31ad57af5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_9e9ccafb-7add-4ebb-92d0-b3d31ad57af5_poly_0_">
0.9889860272526247 0.0006839293918507245 0.9889860272526247 0.9993142815376208 0.011885298132559896 0.9990561152179658 0.011885298132559896 0.0006839293918507245 0.9889860272526247 0.0006839293918507245
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6226.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_1beaa57a-859c-49c4-844a-b701522a8e5b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_1beaa57a-859c-49c4-844a-b701522a8e5b_poly_0_">
0.9639416265896372 0.0007579815341786938 0.9639416265896372 0.9991890658873767 0.05440359838896125 0.9971791156707481 0.05440359838896125 0.0007579815341786938 0.9639416265896372 0.0007579815341786938
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6224_upCcJ0GhGChBUQ17yzB1.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_92be1c0d-23d2-4120-998e-2450872cdd1f_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_92be1c0d-23d2-4120-998e-2450872cdd1f_poly_0_">
0.9529979480240662 0.0007961777411661868 0.9529979480240662 0.9971603073709271 0.0531851817308544 0.9991701425998091 0.0531851817308544 0.0007961777411661868 0.9529979480240662 0.0007961777411661868
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_R7FH4N2v4VZn3Y9gkb3v.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_639a6734-84e4-4c2d-bb99-c805bca475de_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_639a6734-84e4-4c2d-bb99-c805bca475de_poly_0_">
0.9852042970990968 0.0006797885135185129 0.9852042970990968 0.9991512110925256 0.011791971712654004 0.9993232352855875 0.011791971712654004 0.0006797885135185129 0.9852042970990968 0.0006797885135185129
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227#2_e2tE0k9BDI47FKnDqdmh.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_67986505-da98-46dd-b2ec-d55cf4d17767_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_67986505-da98-46dd-b2ec-d55cf4d17767_poly_0_">
0.9610274242681269 0.000690148429701764 0.9610274242681269 0.9991130353422907 0.06195243742386225 0.9880137775910262 0.06195243742386225 0.000690148429701764 0.9610274242681269 0.000690148429701764
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_NFChE7mx6SUZF4QdMTPS.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_77bd083d-b6e9-41d2-97b6-32c150d48b08_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_77bd083d-b6e9-41d2-97b6-32c150d48b08_poly_0_">
0.9917826521872857 0.0006883787460619518 0.9917826521872857 0.9992714795879418 0.009031708533540517 0.9993145535257775 0.009031708533540517 0.0006883787460619518 0.9917826521872857 0.0006883787460619518
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6224_LlQVBc0GTM3yZUyVvLK6.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_3003fd83-2442-4357-b372-a1281f4f2d28_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_3003fd83-2442-4357-b372-a1281f4f2d28_poly_0_">
0.9973758519494549 0.0007497320621974557 0.9973758519494549 0.9992042454544199 0.0021475918263966776 0.9992510246201521 0.0021475918263966776 0.0007497320621974557 0.9973758519494549 0.0007497320621974557
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6225.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_6d8cddf2-58c2-49e9-9f14-271599709294_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_6d8cddf2-58c2-49e9-9f14-271599709294_poly_0_">
0.8556628920063503 0.0008246827248860947 0.8556628920063503 0.9986839718259174 0.1777400395485884 0.9991991530978649 0.1777400395485884 0.0008246827248860947 0.8556628920063503 0.0008246827248860947
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6226_iMBLxicK4mQvdnmxbpPW.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_40bf49be-966c-454a-b999-7ecda556b459_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_40bf49be-966c-454a-b999-7ecda556b459_poly_0_">
0.9993887245616544 0.000694697174845503 0.9993887245616544 0.9988846390284235 0.0007232017677427649 0.9993054019658355 0.0007232017677427649 0.000694697174845503 0.9993887245616544 0.000694697174845503
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6224_CWwD4qXRPTFE0cUbSKDi.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_74be7243-8047-4183-985c-1020e5f2760b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_74be7243-8047-4183-985c-1020e5f2760b_poly_0_">
0.9991090121897761 0.0007617559544478785 0.9991090121897761 0.9985842926622086 0.0009674365069596291 0.9992382961975038 0.0009674365069596291 0.0007617559544478785 0.9991090121897761 0.0007617559544478785
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_5j7wJRedxxfKAs0Jpbt7.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_17e0365e-19a0-4722-861b-3a252addea82_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_17e0365e-19a0-4722-861b-3a252addea82_poly_0_">
0.9821550036546292 0.0007167046848622448 0.9821550036546292 0.9992826814072266 0.014851658647966559 0.9992357871658102 0.014851658647966559 0.0007167046848622448 0.9821550036546292 0.0007167046848622448
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227#2_sGijoksTMpxJBLfn1atJ.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_fcf94bd6-2533-4f35-8c4e-c6492de31b35_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_fcf94bd6-2533-4f35-8c4e-c6492de31b35_poly_0_">
0.9569813808264618 0.0006895033323076177 0.9569813808264618 0.9991138368427267 0.05915267771521471 0.9875151201172652 0.05915267771521471 0.0006895033323076177 0.9569813808264618 0.0006895033323076177
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6226_3MKPQF1V7wiFvRJVE5se.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_1dcca785-f643-4258-a8ce-d74562a551c7_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_1dcca785-f643-4258-a8ce-d74562a551c7_poly_0_">
0.9655440127276655 0.0007202028307809442 0.9655440127276655 0.9965301031758119 0.03695403717903645 0.9992425896370908 0.03695403717903645 0.0007202028307809442 0.9655440127276655 0.0007202028307809442
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6225_J7H4cP8d69eCRdbfGJpA.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_5bf14b7e-a063-40f4-b2c3-5b34113b7d34_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_5bf14b7e-a063-40f4-b2c3-5b34113b7d34_poly_0_">
0.9992014235971802 0.0007513520441313373 0.9992014235971802 0.9992481142348694 0.0009750472949532352 0.9987328698578886 0.0009750472949532352 0.0007513520441313373 0.9992014235971802 0.0007513520441313373
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227#2_8CxP9Ca684oSHl9TkoRu.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_23c8b75d-b5de-469b-9bb5-ad7666b6989b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_23c8b75d-b5de-469b-9bb5-ad7666b6989b_poly_0_">
0.9607004758549271 0.0006903833029936504 0.9607004758549271 0.9991127334400012 0.06193136083681061 0.9880096983575231 0.06193136083681061 0.0006903833029936504 0.9607004758549271 0.0006903833029936504
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227#2_m50YCPaSYU5E9LHQl5ey.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_c1000721-d8a8-44fd-aa95-833e5355f3e2_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_c1000721-d8a8-44fd-aa95-833e5355f3e2_poly_0_">
0.930590074500417 0.0006906546992056267 0.930590074500417 0.9873341671400064 0.04556383175620127 0.9991224803278127 0.04556383175620127 0.0006906546992056267 0.930590074500417 0.0006906546992056267
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_Js27mA2RygjcDuDUPzHX.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_4b733b0a-f1a4-4aaf-9841-319b8de5c25b_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_4b733b0a-f1a4-4aaf-9841-319b8de5c25b_poly_0_">
0.9935861142041471 0.0006762861316647916 0.9935861142041471 0.9993233429659478 0.006621081522374617 0.999280809337734 0.006621081522374617 0.0006762861316647916 0.9935861142041471 0.0006762861316647916
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6225_nNym3Zy5k3ricClE5XDI.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_863a97e9-e985-468d-bfd0-b8dc637a30a4_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_863a97e9-e985-468d-bfd0-b8dc637a30a4_poly_0_">
0.9975715768639546 0.0007487491952216338 0.9975715768639546 0.9992509703345885 0.003522848982186133 0.9992509703345885 0.003522848982186133 0.0007487491952216338 0.9975715768639546 0.0007487491952216338
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_SXGafSDjawnlnoS6xPov.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_">
0.9070256747799021 0.9991226996711597 0.23039979025142543 0.9966270640736118 0.9070256747799021 0.0009545170518971786 0.9070256747799021 0.9991226996711597
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_Iu0SI5sEoSZezZvX5KuL.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly.Z6nI0PLOSHMv05PSg9eo">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_.65v4cpVCtEJGMunac3Jx">
0.1613482550093579 0.0006767133068415773 0.7880921114522721 0.0006767133068415773 0.1613482550093579 0.9990429837337788 0.1613482550093579 0.0006767133068415773
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_oiKej1xMf0y0FnhDEJLu.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly.f9pXAVKIhuKGhCrRXmHx">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_.LLBHTPtbVjnfSqhFXUCq">
0.09511884615241684 0.9992845911669559 0.8872907324955008 0.0010107620779559615 0.8872907324955008 0.9992377106452555 0.09511884615241684 0.9992845911669559
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_kZPXKcKGBzGstxmQSR3z.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly.HInUn0aAMWwwvc0sG5Ai">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_.0EZwXuv4T7nJICSm2KYF">
0.0952132249960016 0.0007148569944366654 0.8872561491450881 0.0007148569944366654 0.0952132249960016 0.9989890440805681 0.0952132249960016 0.0007148569944366654
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_GHzorqzzIpwPfJZ3CS1o.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly_0_">
0.9894303003626064 0.9993226803294933 0.009106426984025973 0.9988068829915273 0.9894303003626064 0.000953949584878392 0.9894303003626064 0.9993226803294933
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<app:appearance>
<app:Appearance>
<app:theme>relod</app:theme>
<app:surfaceDataMember>
<app:ParameterizedTexture>
<app:imageURI>DEBY_LOD2_4906966/IMG_6227_xVIPdxh8sTYTa6JNi2rG.jpg</app:imageURI>
<app:wrapMode>wrap</app:wrapMode>
<app:target uri="#DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly.MbE7N7VE0SahHlEIqdz1">
<app:TexCoordList>
<app:textureCoordinates ring="#DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly_0_.KhyRNoV5auDMDntaQofm">
0.008676158454802518 0.0006724842951300248 0.9886326932463554 0.0006724842951300248 0.008676158454802518 0.9990496484979432 0.008676158454802518 0.0006724842951300248
</app:textureCoordinates>
</app:TexCoordList>
</app:target>
</app:ParameterizedTexture>
</app:surfaceDataMember>
</app:Appearance>
</app:appearance>
<bldg:function>31001_9998</bldg:function>
<bldg:roofType>3100</bldg:roofType>
<bldg:measuredHeight uom="urn:adv:uom:m">29.53</bldg:measuredHeight>
<bldg:storeysAboveGround>4</bldg:storeysAboveGround>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906966_014c5ce3-c454-48f9-bb12-5646103e7e62">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_014c5ce3-c454-48f9-bb12-5646103e7e62_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_014c5ce3-c454-48f9-bb12-5646103e7e62_poly_0_">
<gml:posList srsDimension="3">
690863.14 5336017.989 536.041
690867.876 5336029.997 536.007
690858.823 5336026.594 542.17
690863.14 5336017.989 536.041
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_">
<gml:posList srsDimension="3">
690846.252 5336026.242 535.824
690845.615 5336024.616 535.823
690846.556 5336026.134 536.101
690846.252 5336026.242 535.824
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.gx4qTCCUA7jmY6Wsut3Q">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.r7NPWjMbFHcgRWgivC88">
<gml:posList srsDimension="3">
690854.94 5336028.115 542.17
690846.556 5336026.134 536.101
690845.615 5336024.616 535.823
690854.94 5336028.115 542.17
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.YVidyRFfP9b4FXbTHrRJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.VFDsdG2MNX0TnA94l5Sp">
<gml:posList srsDimension="3">
690847.431 5336028.34 536.11
690846.556 5336026.134 536.101
690854.94 5336028.115 542.17
690847.431 5336028.34 536.11
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.5kOZpZPhJdcY7EhecHRL">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.oTFATNdWs0aHDMVx7bvs">
<gml:posList srsDimension="3">
690847.924 5336029.57 536.119
690847.431 5336028.34 536.11
690854.94 5336028.115 542.17
690847.924 5336029.57 536.119
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.vkpr7N2wptZipGRlHJN9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.nPTZ3OPZyzx1sFhYOMT8">
<gml:posList srsDimension="3">
690847.619 5336029.679 535.843
690847.431 5336028.34 536.11
690847.924 5336029.57 536.119
690847.619 5336029.679 535.843
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.leTO4rH5qqwblXcgO8f1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.DUB0nBvxLZb2mtPFWCDI">
<gml:posList srsDimension="3">
690848.788 5336031.786 536.118
690847.924 5336029.57 536.119
690854.94 5336028.115 542.17
690848.788 5336031.786 536.118
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.nxgM8qOxY2TCRV4HS1Tu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.u5FmjvF4g5kd8vcWSdpf">
<gml:posList srsDimension="3">
690848.987 5336033.115 535.861
690848.788 5336031.786 536.118
690854.94 5336028.115 542.17
690848.987 5336033.115 535.861
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.5VL2JtinQwds7uW6EIRs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.Ukx679TMot4TVuEJXK9g">
<gml:posList srsDimension="3">
690849.281 5336033.016 536.127
690848.987 5336033.115 535.861
690854.94 5336028.115 542.17
690849.281 5336033.016 536.127
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.ptnEMivJTWjzAl5k0mXt">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.4QJGvqzN2o37SpVA9lAv">
<gml:posList srsDimension="3">
690850.156 5336035.212 536.14
690849.281 5336033.016 536.127
690854.94 5336028.115 542.17
690850.156 5336035.212 536.14
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.ghuN6zftLsriYGcIoaVn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.82ZXsq8FNS3Yo7ibApLl">
<gml:posList srsDimension="3">
690850.491 5336036.981 535.855
690850.156 5336035.212 536.14
690854.94 5336028.115 542.17
690850.491 5336036.981 535.855
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.qfqQLcJmDct782W4uQe5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.wbwBJ6NKrbzSY0QowKAC">
<gml:posList srsDimension="3">
690850.156 5336035.212 536.14
690850.491 5336036.981 535.855
690849.861 5336035.331 535.867
690850.156 5336035.212 536.14
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.gQK0wygIpo03MD8t0B8C">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.X5KGasXzcIUwhJbSdIwC">
<gml:posList srsDimension="3">
690848.788 5336031.786 536.118
690848.987 5336033.115 535.861
690848.504 5336031.895 535.857
690848.788 5336031.786 536.118
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly.2o21NA5CmUCnjZY2fqlD">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff0710e0-19f5-4cc2-98cf-36ba73e1bbfb_poly_0_.f36g2QW2NilAqOzdx7rr">
<gml:posList srsDimension="3">
690847.431 5336028.34 536.11
690847.619 5336029.679 535.843
690847.147 5336028.449 535.849
690847.431 5336028.34 536.11
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_">
<gml:posList srsDimension="3">
690850.521 5336037.053 533.933
690858.167 5336034.058 535.13
690850.57 5336037.17 533.934
690850.521 5336037.053 533.933
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.bObcrScWUj5bCcQQ7kRh">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.OuZQxNzp7KR2i1ooYu31">
<gml:posList srsDimension="3">
690850.895 5336037.063 533.983
690850.57 5336037.17 533.934
690858.167 5336034.058 535.13
690850.895 5336037.063 533.983
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.HiICQIvJGqtieoFmGPTq">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.8c3sxFewmxSK8A1PGnoJ">
<gml:posList srsDimension="3">
690850.941 5336037.155 533.985
690850.895 5336037.063 533.983
690858.167 5336034.058 535.13
690850.941 5336037.155 533.985
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.bdazj88kdloDNkMtm6Tn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.rbxMq5giwhUahzlfjHjz">
<gml:posList srsDimension="3">
690851.033 5336037.118 533.999
690850.941 5336037.155 533.985
690858.167 5336034.058 535.13
690851.033 5336037.118 533.999
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.EQ4gH8lOFL8V8M6kRwI3">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.o1TyaVvjq8JXAr4vHrJR">
<gml:posList srsDimension="3">
690852.06 5336040.041 533.983
690851.033 5336037.118 533.999
690858.167 5336034.058 535.13
690852.06 5336040.041 533.983
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.w18OQsFU39Z6b6Ti8M8E">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.xu4TpjD7yQR7ufcTpvlL">
<gml:posList srsDimension="3">
690852.109 5336041.104 533.933
690852.06 5336040.041 533.983
690858.167 5336034.058 535.13
690852.109 5336041.104 533.933
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.rD2MVdczOAgpSjVcgGk2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.YkuV7Rqas4FzNZOq8tDU">
<gml:posList srsDimension="3">
690852.475 5336040.958 533.991
690852.109 5336041.104 533.933
690858.167 5336034.058 535.13
690852.475 5336040.958 533.991
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.zgf74LPzPSIix5Vosa6e">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.R1fjhzKMWTWtnZnjhV9B">
<gml:posList srsDimension="3">
690860.007 5336060.048 534.0
690852.475 5336040.958 533.991
690858.167 5336034.058 535.13
690860.007 5336060.048 534.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.HulGCxMaaMc8HcgkldCc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.ZJBLfTsyTEX3dsstUdFR">
<gml:posList srsDimension="3">
690883.711 5336099.393 535.13
690860.007 5336060.048 534.0
690858.167 5336034.058 535.13
690883.711 5336099.393 535.13
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.p2lgqRI5m2tj0R8xoEuk">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.NrRMhpbtUEQC7PfRq8SV">
<gml:posList srsDimension="3">
690865.716 5336075.474 533.957
690860.007 5336060.048 534.0
690883.711 5336099.393 535.13
690865.716 5336075.474 533.957
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.O6wOeyXHnm4oIsGTKDLv">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.wcBir0DbSwcrM1fsRy5g">
<gml:posList srsDimension="3">
690865.645 5336075.492 533.946
690860.007 5336060.048 534.0
690865.716 5336075.474 533.957
690865.645 5336075.492 533.946
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.UWrEQIUHpnZRIOnDzeA8">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.aW1hv8HbiHThrzmXgDAT">
<gml:posList srsDimension="3">
690867.637 5336079.573 534.0
690865.716 5336075.474 533.957
690883.711 5336099.393 535.13
690867.637 5336079.573 534.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.Yf2cGZabFguPkHo6rxVI">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.eTHktm3iUEPtRAvpr1KR">
<gml:posList srsDimension="3">
690867.362 5336079.682 533.957
690865.716 5336075.474 533.957
690867.637 5336079.573 534.0
690867.362 5336079.682 533.957
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.bCPP98CDosJwFxiTzhhQ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.zFetKagjbE3K77UGzAbR">
<gml:posList srsDimension="3">
690873.852 5336095.209 534.014
690867.637 5336079.573 534.0
690883.711 5336099.393 535.13
690873.852 5336095.209 534.014
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.t7KihbjJrHEP0pPAq6x7">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.STei1ILJ2n1yYRup46vd">
<gml:posList srsDimension="3">
690876.841 5336101.991 534.059
690873.852 5336095.209 534.014
690883.711 5336099.393 535.13
690876.841 5336101.991 534.059
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.6rRWAj8TTLSTxrIijM2N">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.cLexGF5UtOWieXbcNsnr">
<gml:posList srsDimension="3">
690876.364 5336102.173 533.985
690873.852 5336095.209 534.014
690876.841 5336101.991 534.059
690876.364 5336102.173 533.985
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.JFCGe1DdjhuPR38xWnC2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.OWf27tVc3OiG66Q4lZ24">
<gml:posList srsDimension="3">
690876.877 5336102.093 534.059
690876.841 5336101.991 534.059
690883.711 5336099.393 535.13
690876.877 5336102.093 534.059
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.v9HIA9QIeeSgZGbgyPUV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.LCsk9vccZ7Mn1I5pN1Z2">
<gml:posList srsDimension="3">
690873.852 5336095.209 534.014
690876.364 5336102.173 533.985
690873.651 5336095.251 533.984
690873.852 5336095.209 534.014
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.0EotBVtNMC61MeaeWzU9">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.tdSZa6Zl3v7BTxHlrDeu">
<gml:posList srsDimension="3">
690860.007 5336060.048 534.0
690865.645 5336075.492 533.946
690859.732 5336060.157 533.957
690860.007 5336060.048 534.0
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly.ifki5jcUEaw8MwSZwnN2">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f660705-d8d2-4847-8dee-57070e5bbf7b_poly_0_.imFWH1IWEAWQc8b4vXqy">
<gml:posList srsDimension="3">
690852.06 5336040.041 533.983
690852.109 5336041.104 533.933
690851.745 5336040.169 533.934
690852.06 5336040.041 533.983
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly_0_">
<gml:posList srsDimension="3">
690845.615 5336024.616 535.823
690846.103 5336024.425 535.828
690854.94 5336028.115 542.17
690845.615 5336024.616 535.823
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly.etlhfUrfwZZRCifWfIq4">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly_0_.L7I7inwDC9YaqxW8ivbc">
<gml:posList srsDimension="3">
690863.058 5336017.78 535.828
690854.94 5336028.115 542.17
690846.103 5336024.425 535.828
690863.058 5336017.78 535.828
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly.G0RzDzQNZVcg8TIwWWUy">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly_0_.myxQqAifxnKsIdFEov8d">
<gml:posList srsDimension="3">
690858.823 5336026.594 542.17
690854.94 5336028.115 542.17
690863.058 5336017.78 535.828
690858.823 5336026.594 542.17
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly.kTd8QdurMpvQraCe9QeJ">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_705e0e00-cf28-4157-a8de-8d883d22d7d8_poly_0_.7G6fcyQvIq0GFsvmAYHQ">
<gml:posList srsDimension="3">
690863.14 5336017.989 536.041
690858.823 5336026.594 542.17
690863.058 5336017.78 535.828
690863.14 5336017.989 536.041
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_">
<gml:posList srsDimension="3">
690858.823 5336026.594 542.17
690867.876 5336029.997 536.007
690866.202 5336030.816 535.863
690858.823 5336026.594 542.17
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.NK56znMKCOY1kQREdk5T">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.XXqWOTbYhR7xwGWOahl4">
<gml:posList srsDimension="3">
690867.929 5336030.133 535.868
690866.202 5336030.816 535.863
690867.876 5336029.997 536.007
690867.929 5336030.133 535.868
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.c46eIoZA7DT5XXtzpYjX">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.JTwbE24jP5ArAltLUPbg">
<gml:posList srsDimension="3">
690850.491 5336036.981 535.855
690854.94 5336028.115 542.17
690850.497 5336036.997 535.838
690850.491 5336036.981 535.855
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.1JkcIxddv9NzSdGlMZcV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.7QdWb0yIXIgNRN01M6X1">
<gml:posList srsDimension="3">
690850.521 5336037.053 535.78
690850.497 5336036.997 535.838
690854.94 5336028.115 542.17
690850.521 5336037.053 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.lPxEk5WOlQHzdXEnUkV5">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.R9QzAmioy8MKdio9zvRl">
<gml:posList srsDimension="3">
690858.167 5336034.058 535.78
690850.521 5336037.053 535.78
690854.94 5336028.115 542.17
690858.167 5336034.058 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.WlySyzxoULmtQwyiMFHW">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.o7dzN9rcDqw105yvULwr">
<gml:posList srsDimension="3">
690858.823 5336026.594 542.17
690858.167 5336034.058 535.78
690854.94 5336028.115 542.17
690858.823 5336026.594 542.17
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.Gwxg742xQoE4ATG7raFn">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.wlY46Vh6ah43rCesKJ6V">
<gml:posList srsDimension="3">
690866.234 5336030.897 535.78
690858.167 5336034.058 535.78
690858.823 5336026.594 542.17
690866.234 5336030.897 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly.ULrX4UKExjymZ1CpvscM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_a86c21c1-21b7-45c6-9d81-7d2ce8413a32_poly_0_.mOkxySPaFkoKqMypmZOL">
<gml:posList srsDimension="3">
690866.202 5336030.816 535.863
690866.234 5336030.897 535.78
690858.823 5336026.594 542.17
690866.202 5336030.816 535.863
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:RoofSurface gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_">
<gml:posList srsDimension="3">
690871.183 5336038.037 532.745
690876.083 5336050.418 532.733
690874.232 5336051.176 533.18
690871.183 5336038.037 532.745
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.6u0MNJkAtTzUbU7058df">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.9me23fA48CMQYwLBQI9q">
<gml:posList srsDimension="3">
690878.674 5336062.468 533.174
690881.369 5336069.309 533.17
690880.444 5336069.674 533.392
690878.674 5336062.468 533.174
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.VhaZnPXiWsYagP2dUBHM">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.NWoteTuSnjlarAhqGJkN">
<gml:posList srsDimension="3">
690887.832 5336083.444 532.975
690892.403 5336095.022 532.965
690892.239 5336095.876 533.069
690887.832 5336083.444 532.975
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.n7enoM2rc9uEwVPCqBCj">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.cbOxa2iGrjFveyMDkumm">
<gml:posList srsDimension="3">
690892.666 5336095.703 532.966
690892.239 5336095.876 533.069
690892.403 5336095.022 532.965
690892.666 5336095.703 532.966
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.e41KTyANEyHzXnkimZ2k">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.JiNJg7cKSCO6ZEWKtCS7">
<gml:posList srsDimension="3">
690858.167 5336034.058 535.13
690866.234 5336030.897 533.194
690883.711 5336099.393 535.13
690858.167 5336034.058 535.13
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.9OFyS7dCEdGTPGD5vd7T">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.otLFNObKMbwce1zHwpN9">
<gml:posList srsDimension="3">
690869.343 5336038.766 533.187
690883.711 5336099.393 535.13
690866.234 5336030.897 533.194
690869.343 5336038.766 533.187
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.rDSaeZDFg2KGQ2mgWL7t">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.fENpI3GdXotDSfTgtvJg">
<gml:posList srsDimension="3">
690871.183 5336038.037 532.745
690883.711 5336099.393 535.13
690869.343 5336038.766 533.187
690871.183 5336038.037 532.745
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.3BJ4njlDsVecrenM2Apu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.7gEhCbmfRXki8CGr8dlJ">
<gml:posList srsDimension="3">
690874.232 5336051.176 533.18
690883.711 5336099.393 535.13
690871.183 5336038.037 532.745
690874.232 5336051.176 533.18
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.T1UTefHJpD2Lm00hG87H">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.SO2HC59s2tqywO06cooe">
<gml:posList srsDimension="3">
690878.019 5336060.791 533.174
690883.711 5336099.393 535.13
690874.232 5336051.176 533.18
690878.019 5336060.791 533.174
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.QLJh18Nz7pTE6S1U97qL">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.wEP2u6tM4zaU1UC3weoN">
<gml:posList srsDimension="3">
690878.674 5336062.468 533.174
690883.711 5336099.393 535.13
690878.019 5336060.791 533.174
690878.674 5336062.468 533.174
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.jiMegoFT5Q4QyRzTFb8X">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.k0Tsl12TDoHF3KeVP3AY">
<gml:posList srsDimension="3">
690880.444 5336069.674 533.392
690883.711 5336099.393 535.13
690878.674 5336062.468 533.174
690880.444 5336069.674 533.392
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.e99u8uvVjaZRtGcisbys">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.hMRHpokpjt6c7gnwFZHa">
<gml:posList srsDimension="3">
690886.177 5336084.07 533.37
690883.711 5336099.393 535.13
690880.444 5336069.674 533.392
690886.177 5336084.07 533.37
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.AsaFOe8efuLkbJO03LYu">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.1Vt243IhSzWOaoYN3Ov8">
<gml:posList srsDimension="3">
690892.274 5336096.008 533.072
690883.711 5336099.393 535.13
690886.177 5336084.07 533.37
690892.274 5336096.008 533.072
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.ERNr7nvPZLYmltvU2ZJi">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.MtaIEzBY8fqMlR08EThu">
<gml:posList srsDimension="3">
690887.832 5336083.444 532.975
690892.274 5336096.008 533.072
690886.177 5336084.07 533.37
690887.832 5336083.444 532.975
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly.sL0XuAW0p5bwQqRwD8Et">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4d8b26ef-d7b2-4c45-8280-59d44ece9fa2_poly_0_.8jI95f9klN3zH6oxfZdX">
<gml:posList srsDimension="3">
690892.239 5336095.876 533.069
690892.274 5336096.008 533.072
690887.832 5336083.444 532.975
690892.239 5336095.876 533.069
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:RoofSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_63197fed-2bd7-4254-9523-93862f8e2a77">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_63197fed-2bd7-4254-9523-93862f8e2a77_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_63197fed-2bd7-4254-9523-93862f8e2a77_poly_0_">
<gml:posList srsDimension="3">
690850.941 5336037.155 512.64
690850.941 5336037.155 533.985
690851.033 5336037.118 533.999
690851.033 5336037.118 512.64
690850.941 5336037.155 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_ac5db7c0-a7e3-46f7-88a0-79aeb18cd694">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ac5db7c0-a7e3-46f7-88a0-79aeb18cd694_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ac5db7c0-a7e3-46f7-88a0-79aeb18cd694_poly_0_">
<gml:posList srsDimension="3">
690892.274 5336096.008 512.64
690892.274 5336096.008 533.072
690892.239 5336095.876 533.069
690892.239 5336095.876 512.64
690892.274 5336096.008 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_cefb01f1-2117-4fb5-ab8f-5cd7ff7f27eb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_cefb01f1-2117-4fb5-ab8f-5cd7ff7f27eb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_cefb01f1-2117-4fb5-ab8f-5cd7ff7f27eb_poly_0_">
<gml:posList srsDimension="3">
690876.083 5336050.418 512.64
690876.083 5336050.418 532.733
690871.183 5336038.037 532.745
690871.183 5336038.037 512.64
690876.083 5336050.418 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_0f0c7a54-c171-47b2-9505-e7a2122b0f2e">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_0f0c7a54-c171-47b2-9505-e7a2122b0f2e_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_0f0c7a54-c171-47b2-9505-e7a2122b0f2e_poly_0_">
<gml:posList srsDimension="3">
690880.444 5336069.674 512.64
690880.444 5336069.674 533.392
690881.369 5336069.309 533.17
690881.369 5336069.309 512.64
690880.444 5336069.674 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_92715023-4189-4e27-af71-462e37e683ed">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_92715023-4189-4e27-af71-462e37e683ed_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_92715023-4189-4e27-af71-462e37e683ed_poly_0_">
<gml:posList srsDimension="3">
690878.674 5336062.468 512.64
690878.674 5336062.468 533.174
690878.019 5336060.791 533.174
690878.019 5336060.791 512.64
690878.674 5336062.468 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_46370a0a-c954-4c79-8aa4-9b3a529bb848">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_46370a0a-c954-4c79-8aa4-9b3a529bb848_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_46370a0a-c954-4c79-8aa4-9b3a529bb848_poly_0_">
<gml:posList srsDimension="3">
690847.619 5336029.679 512.64
690847.619 5336029.679 535.843
690847.924 5336029.57 536.119
690847.924 5336029.57 512.64
690847.619 5336029.679 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_ee899ad8-ebcb-4b8a-a21a-872c4269e8d0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ee899ad8-ebcb-4b8a-a21a-872c4269e8d0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ee899ad8-ebcb-4b8a-a21a-872c4269e8d0_poly_0_">
<gml:posList srsDimension="3">
690848.987 5336033.115 512.64
690848.987 5336033.115 535.861
690849.281 5336033.016 536.127
690849.281 5336033.016 512.64
690848.987 5336033.115 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_baf71027-627b-4a91-acc7-9e040731db1f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_baf71027-627b-4a91-acc7-9e040731db1f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_baf71027-627b-4a91-acc7-9e040731db1f_poly_0_">
<gml:posList srsDimension="3">
690846.556 5336026.134 512.64
690846.556 5336026.134 536.101
690847.431 5336028.34 536.11
690847.431 5336028.34 512.64
690846.556 5336026.134 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_d35c49e1-73b4-49ab-8f2a-fbf8f95f41df">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_d35c49e1-73b4-49ab-8f2a-fbf8f95f41df_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_d35c49e1-73b4-49ab-8f2a-fbf8f95f41df_poly_0_">
<gml:posList srsDimension="3">
690850.895 5336037.063 512.64
690850.895 5336037.063 533.983
690850.941 5336037.155 533.985
690850.941 5336037.155 512.64
690850.895 5336037.063 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_84b5ca47-8928-49e7-a0dc-d639dfe550d0">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_84b5ca47-8928-49e7-a0dc-d639dfe550d0_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_84b5ca47-8928-49e7-a0dc-d639dfe550d0_poly_0_">
<gml:posList srsDimension="3">
690891.702 5336093.243 512.64
690891.702 5336093.243 532.967
690887.832 5336083.444 532.975
690887.832 5336083.444 512.64
690891.702 5336093.243 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_6f7bc8ce-1f40-41d7-92de-0c70fb20d93d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6f7bc8ce-1f40-41d7-92de-0c70fb20d93d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6f7bc8ce-1f40-41d7-92de-0c70fb20d93d_poly_0_">
<gml:posList srsDimension="3">
690851.033 5336037.118 512.64
690851.033 5336037.118 533.999
690852.06 5336040.041 533.983
690852.06 5336040.041 512.64
690851.033 5336037.118 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_235045d8-e1cf-47fd-b07b-a71e409f3f10">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_235045d8-e1cf-47fd-b07b-a71e409f3f10_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_235045d8-e1cf-47fd-b07b-a71e409f3f10_poly_0_">
<gml:posList srsDimension="3">
690874.232 5336051.176 512.64
690874.232 5336051.176 533.18
690876.083 5336050.418 532.733
690876.083 5336050.418 512.64
690874.232 5336051.176 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_86b2e4d8-a117-47d5-914c-4bfc8c39eade">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_86b2e4d8-a117-47d5-914c-4bfc8c39eade_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_86b2e4d8-a117-47d5-914c-4bfc8c39eade_poly_0_">
<gml:posList srsDimension="3">
690849.281 5336033.016 512.64
690849.281 5336033.016 536.127
690850.156 5336035.212 536.14
690850.156 5336035.212 512.64
690849.281 5336033.016 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_ad8f8bdd-2faf-4d1f-9707-d7761719a53c">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ad8f8bdd-2faf-4d1f-9707-d7761719a53c_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ad8f8bdd-2faf-4d1f-9707-d7761719a53c_poly_0_">
<gml:posList srsDimension="3">
690892.403 5336095.022 512.64
690892.403 5336095.022 532.965
690891.702 5336093.243 532.967
690891.702 5336093.243 512.64
690892.403 5336095.022 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_2313fbed-4ea6-4699-82ae-282200e836a2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_2313fbed-4ea6-4699-82ae-282200e836a2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_2313fbed-4ea6-4699-82ae-282200e836a2_poly_0_">
<gml:posList srsDimension="3">
690892.239 5336095.876 512.64
690892.239 5336095.876 533.069
690892.666 5336095.703 532.966
690892.666 5336095.703 512.64
690892.239 5336095.876 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_2082c916-2dc0-4435-b5c2-9c77e4aa4ddb">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_2082c916-2dc0-4435-b5c2-9c77e4aa4ddb_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_2082c916-2dc0-4435-b5c2-9c77e4aa4ddb_poly_0_">
<gml:posList srsDimension="3">
690850.57 5336037.17 512.64
690850.57 5336037.17 533.934
690850.895 5336037.063 533.983
690850.895 5336037.063 512.64
690850.57 5336037.17 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_bafd6f8a-c368-42ec-8c91-f1568d180802">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_bafd6f8a-c368-42ec-8c91-f1568d180802_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_bafd6f8a-c368-42ec-8c91-f1568d180802_poly_0_">
<gml:posList srsDimension="3">
690852.06 5336040.041 512.64
690852.06 5336040.041 533.983
690851.745 5336040.169 533.934
690851.745 5336040.169 512.64
690852.06 5336040.041 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_e46a61e6-9b73-4c39-8740-7c475e4556d4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_e46a61e6-9b73-4c39-8740-7c475e4556d4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_e46a61e6-9b73-4c39-8740-7c475e4556d4_poly_0_">
<gml:posList srsDimension="3">
690873.852 5336095.209 512.64
690873.852 5336095.209 534.014
690873.651 5336095.251 533.984
690873.651 5336095.251 512.64
690873.852 5336095.209 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_64da5727-f738-4997-a675-90d0327d12b3">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_64da5727-f738-4997-a675-90d0327d12b3_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_64da5727-f738-4997-a675-90d0327d12b3_poly_0_">
<gml:posList srsDimension="3">
690892.666 5336095.703 512.64
690892.666 5336095.703 532.966
690892.403 5336095.022 532.965
690892.403 5336095.022 512.64
690892.666 5336095.703 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_9e9ccafb-7add-4ebb-92d0-b3d31ad57af5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_9e9ccafb-7add-4ebb-92d0-b3d31ad57af5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_9e9ccafb-7add-4ebb-92d0-b3d31ad57af5_poly_0_">
<gml:posList srsDimension="3">
690847.147 5336028.449 512.64
690847.147 5336028.449 535.849
690847.619 5336029.679 535.843
690847.619 5336029.679 512.64
690847.147 5336028.449 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_1beaa57a-859c-49c4-844a-b701522a8e5b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_1beaa57a-859c-49c4-844a-b701522a8e5b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_1beaa57a-859c-49c4-844a-b701522a8e5b_poly_0_">
<gml:posList srsDimension="3">
690860.007 5336060.048 512.64
690860.007 5336060.048 534.0
690859.732 5336060.157 533.957
690859.732 5336060.157 512.64
690860.007 5336060.048 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_56d10644-8ccf-4fb2-9905-b7336ba10555">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_56d10644-8ccf-4fb2-9905-b7336ba10555_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_56d10644-8ccf-4fb2-9905-b7336ba10555_poly_0_">
<gml:posList srsDimension="3">
690887.832 5336083.444 512.64
690887.832 5336083.444 532.975
690886.177 5336084.07 533.37
690886.177 5336084.07 512.64
690887.832 5336083.444 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_92be1c0d-23d2-4120-998e-2450872cdd1f">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_92be1c0d-23d2-4120-998e-2450872cdd1f_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_92be1c0d-23d2-4120-998e-2450872cdd1f_poly_0_">
<gml:posList srsDimension="3">
690867.362 5336079.682 512.64
690867.362 5336079.682 533.957
690867.637 5336079.573 534.0
690867.637 5336079.573 512.64
690867.362 5336079.682 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_639a6734-84e4-4c2d-bb99-c805bca475de">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_639a6734-84e4-4c2d-bb99-c805bca475de_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_639a6734-84e4-4c2d-bb99-c805bca475de_poly_0_">
<gml:posList srsDimension="3">
690848.504 5336031.895 512.64
690848.504 5336031.895 535.857
690848.987 5336033.115 535.861
690848.987 5336033.115 512.64
690848.504 5336031.895 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_23bab892-afdd-4dce-ae1b-f2498fc69021">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_23bab892-afdd-4dce-ae1b-f2498fc69021_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_23bab892-afdd-4dce-ae1b-f2498fc69021_poly_0_">
<gml:posList srsDimension="3">
690878.019 5336060.791 512.64
690878.019 5336060.791 533.174
690874.232 5336051.176 533.18
690874.232 5336051.176 512.64
690878.019 5336060.791 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_67986505-da98-46dd-b2ec-d55cf4d17767">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_67986505-da98-46dd-b2ec-d55cf4d17767_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_67986505-da98-46dd-b2ec-d55cf4d17767_poly_0_">
<gml:posList srsDimension="3">
690848.788 5336031.786 512.64
690848.788 5336031.786 536.118
690848.504 5336031.895 535.857
690848.504 5336031.895 512.64
690848.788 5336031.786 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_383c8156-ff1c-473a-82be-82816d27ffdd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_383c8156-ff1c-473a-82be-82816d27ffdd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_383c8156-ff1c-473a-82be-82816d27ffdd_poly_0_">
<gml:posList srsDimension="3">
690881.369 5336069.309 512.64
690881.369 5336069.309 533.17
690878.674 5336062.468 533.174
690878.674 5336062.468 512.64
690881.369 5336069.309 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_77bd083d-b6e9-41d2-97b6-32c150d48b08">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_77bd083d-b6e9-41d2-97b6-32c150d48b08_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_77bd083d-b6e9-41d2-97b6-32c150d48b08_poly_0_">
<gml:posList srsDimension="3">
690845.615 5336024.616 512.64
690845.615 5336024.616 535.823
690846.252 5336026.242 535.824
690846.252 5336026.242 512.64
690845.615 5336024.616 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_3003fd83-2442-4357-b372-a1281f4f2d28">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_3003fd83-2442-4357-b372-a1281f4f2d28_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_3003fd83-2442-4357-b372-a1281f4f2d28_poly_0_">
<gml:posList srsDimension="3">
690873.651 5336095.251 512.64
690873.651 5336095.251 533.984
690876.364 5336102.173 533.985
690876.364 5336102.173 512.64
690873.651 5336095.251 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_90a44bef-2641-4d5a-ab42-5aa1287b2c6d">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_90a44bef-2641-4d5a-ab42-5aa1287b2c6d_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_90a44bef-2641-4d5a-ab42-5aa1287b2c6d_poly_0_">
<gml:posList srsDimension="3">
690866.202 5336030.816 512.64
690869.343 5336038.766 512.64
690869.343 5336038.766 533.187
690866.234 5336030.897 533.194
690866.234 5336030.897 535.78
690866.202 5336030.816 535.863
690866.202 5336030.816 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_ff50ef19-3f24-457d-9179-47cf5716d24a">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_ff50ef19-3f24-457d-9179-47cf5716d24a_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_ff50ef19-3f24-457d-9179-47cf5716d24a_poly_0_">
<gml:posList srsDimension="3">
690876.841 5336101.991 512.64
690876.841 5336101.991 534.059
690876.877 5336102.093 534.059
690876.877 5336102.093 512.64
690876.841 5336101.991 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_6d8cddf2-58c2-49e9-9f14-271599709294">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_6d8cddf2-58c2-49e9-9f14-271599709294_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_6d8cddf2-58c2-49e9-9f14-271599709294_poly_0_">
<gml:posList srsDimension="3">
690865.645 5336075.492 512.64
690865.645 5336075.492 533.946
690865.716 5336075.474 533.957
690865.716 5336075.474 512.64
690865.645 5336075.492 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_45c7be24-ffb0-4e66-ba3f-ece98c2566fd">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_45c7be24-ffb0-4e66-ba3f-ece98c2566fd_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_45c7be24-ffb0-4e66-ba3f-ece98c2566fd_poly_0_">
<gml:posList srsDimension="3">
690876.364 5336102.173 512.64
690876.364 5336102.173 533.985
690876.841 5336101.991 534.059
690876.841 5336101.991 512.64
690876.364 5336102.173 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_40bf49be-966c-454a-b999-7ecda556b459">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_40bf49be-966c-454a-b999-7ecda556b459_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_40bf49be-966c-454a-b999-7ecda556b459_poly_0_">
<gml:posList srsDimension="3">
690852.475 5336040.958 512.64
690852.475 5336040.958 533.991
690860.007 5336060.048 534.0
690860.007 5336060.048 512.64
690852.475 5336040.958 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_74be7243-8047-4183-985c-1020e5f2760b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_74be7243-8047-4183-985c-1020e5f2760b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_74be7243-8047-4183-985c-1020e5f2760b_poly_0_">
<gml:posList srsDimension="3">
690867.637 5336079.573 512.64
690867.637 5336079.573 534.0
690873.852 5336095.209 534.014
690873.852 5336095.209 512.64
690867.637 5336079.573 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_b8510aed-3792-409f-9354-0625cfe47b62">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_b8510aed-3792-409f-9354-0625cfe47b62_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_b8510aed-3792-409f-9354-0625cfe47b62_poly_0_">
<gml:posList srsDimension="3">
690871.183 5336038.037 512.64
690871.183 5336038.037 532.745
690869.343 5336038.766 533.187
690869.343 5336038.766 512.64
690871.183 5336038.037 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_17e0365e-19a0-4722-861b-3a252addea82">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_17e0365e-19a0-4722-861b-3a252addea82_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_17e0365e-19a0-4722-861b-3a252addea82_poly_0_">
<gml:posList srsDimension="3">
690851.745 5336040.169 512.64
690851.745 5336040.169 533.934
690852.109 5336041.104 533.933
690852.109 5336041.104 512.64
690851.745 5336040.169 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_fcf94bd6-2533-4f35-8c4e-c6492de31b35">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_fcf94bd6-2533-4f35-8c4e-c6492de31b35_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_fcf94bd6-2533-4f35-8c4e-c6492de31b35_poly_0_">
<gml:posList srsDimension="3">
690850.156 5336035.212 512.64
690850.156 5336035.212 536.14
690849.861 5336035.331 535.867
690849.861 5336035.331 512.64
690850.156 5336035.212 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_1dcca785-f643-4258-a8ce-d74562a551c7">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_1dcca785-f643-4258-a8ce-d74562a551c7_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_1dcca785-f643-4258-a8ce-d74562a551c7_poly_0_">
<gml:posList srsDimension="3">
690852.109 5336041.104 512.64
690852.109 5336041.104 533.933
690852.475 5336040.958 533.991
690852.475 5336040.958 512.64
690852.109 5336041.104 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_44758d4c-bc24-4aba-856c-1dd79c72e658">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_44758d4c-bc24-4aba-856c-1dd79c72e658_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_44758d4c-bc24-4aba-856c-1dd79c72e658_poly_0_">
<gml:posList srsDimension="3">
690886.177 5336084.07 512.64
690886.177 5336084.07 533.37
690880.444 5336069.674 533.392
690880.444 5336069.674 512.64
690886.177 5336084.07 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_5bf14b7e-a063-40f4-b2c3-5b34113b7d34">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_5bf14b7e-a063-40f4-b2c3-5b34113b7d34_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_5bf14b7e-a063-40f4-b2c3-5b34113b7d34_poly_0_">
<gml:posList srsDimension="3">
690859.732 5336060.157 512.64
690859.732 5336060.157 533.957
690865.645 5336075.492 533.946
690865.645 5336075.492 512.64
690859.732 5336060.157 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_23c8b75d-b5de-469b-9bb5-ad7666b6989b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_23c8b75d-b5de-469b-9bb5-ad7666b6989b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_23c8b75d-b5de-469b-9bb5-ad7666b6989b_poly_0_">
<gml:posList srsDimension="3">
690847.431 5336028.34 512.64
690847.431 5336028.34 536.11
690847.147 5336028.449 535.849
690847.147 5336028.449 512.64
690847.431 5336028.34 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_c1000721-d8a8-44fd-aa95-833e5355f3e2">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_c1000721-d8a8-44fd-aa95-833e5355f3e2_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_c1000721-d8a8-44fd-aa95-833e5355f3e2_poly_0_">
<gml:posList srsDimension="3">
690846.252 5336026.242 512.64
690846.252 5336026.242 535.824
690846.556 5336026.134 536.101
690846.556 5336026.134 512.64
690846.252 5336026.242 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_4b733b0a-f1a4-4aaf-9841-319b8de5c25b">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_4b733b0a-f1a4-4aaf-9841-319b8de5c25b_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_4b733b0a-f1a4-4aaf-9841-319b8de5c25b_poly_0_">
<gml:posList srsDimension="3">
690847.924 5336029.57 512.64
690847.924 5336029.57 536.119
690848.788 5336031.786 536.118
690848.788 5336031.786 512.64
690847.924 5336029.57 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_0d260c93-3ee0-4a59-96b0-d6d3fbe1b790">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_0d260c93-3ee0-4a59-96b0-d6d3fbe1b790_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_0d260c93-3ee0-4a59-96b0-d6d3fbe1b790_poly_0_">
<gml:posList srsDimension="3">
690866.202 5336030.816 512.64
690866.202 5336030.816 535.863
690867.929 5336030.133 535.868
690867.929 5336030.133 512.64
690866.202 5336030.816 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_863a97e9-e985-468d-bfd0-b8dc637a30a4">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_863a97e9-e985-468d-bfd0-b8dc637a30a4_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_863a97e9-e985-468d-bfd0-b8dc637a30a4_poly_0_">
<gml:posList srsDimension="3">
690865.716 5336075.474 512.64
690865.716 5336075.474 533.957
690867.362 5336079.682 533.957
690867.362 5336079.682 512.64
690865.716 5336075.474 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly_0_">
<gml:posList srsDimension="3">
690863.058 5336017.78 512.64
690863.14 5336017.989 512.64
690863.058 5336017.78 535.828
690863.058 5336017.78 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly.p4s4IFWddogZdnUrXa0F">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly_0_.jmclotdZfBT1HSORsAXn">
<gml:posList srsDimension="3">
690863.14 5336017.989 536.041
690863.058 5336017.78 535.828
690863.14 5336017.989 512.64
690863.14 5336017.989 536.041
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly.Kb24maE20w2bwCbOnuIr">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly_0_.X2ZNBMeJrEXWgettiSli">
<gml:posList srsDimension="3">
690867.876 5336029.997 512.64
690863.14 5336017.989 536.041
690863.14 5336017.989 512.64
690867.876 5336029.997 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly.n36zusIRawdVJEWiwuAm">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly_0_.GObd9MPfCSOoY0fb0Pj3">
<gml:posList srsDimension="3">
690867.876 5336029.997 536.007
690863.14 5336017.989 536.041
690867.876 5336029.997 512.64
690867.876 5336029.997 536.007
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly.Q8WB4JQ2kIZj9gyh1hBc">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly_0_.rSoRbnPNrxyPrLca4rxk">
<gml:posList srsDimension="3">
690867.929 5336030.133 512.64
690867.876 5336029.997 536.007
690867.876 5336029.997 512.64
690867.929 5336030.133 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly.7nZwJblDEoSU4MrNVCln">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_974deb39-b493-4fcd-bc99-e65f528b25c8_poly_0_.fqGfhEKwAyq12d2oJiVp">
<gml:posList srsDimension="3">
690867.929 5336030.133 535.868
690867.876 5336029.997 536.007
690867.929 5336030.133 512.64
690867.929 5336030.133 535.868
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400_poly_0_">
<gml:posList srsDimension="3">
690876.877 5336102.093 512.64
690876.877 5336102.093 534.059
690892.274 5336096.008 512.64
690876.877 5336102.093 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400_poly.92aSz5fJ0WFYQFItXczx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400_poly_0_.7zhaXEXGua3NLtgDQXzf">
<gml:posList srsDimension="3">
690892.274 5336096.008 533.072
690892.274 5336096.008 512.64
690876.877 5336102.093 534.059
690892.274 5336096.008 533.072
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400_poly.tg55sDf81qXHs1BcqynV">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_82fea215-3898-4c63-b58d-0303b0295400_poly_0_.MBu12AkdubXeKHhPXNK2">
<gml:posList srsDimension="3">
690883.711 5336099.393 535.13
690892.274 5336096.008 533.072
690876.877 5336102.093 534.059
690883.711 5336099.393 535.13
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly_0_">
<gml:posList srsDimension="3">
690850.521 5336037.053 533.933
690850.521 5336037.053 535.78
690858.167 5336034.058 535.13
690850.521 5336037.053 533.933
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly.K5HfXZ80WF49eEhs4TJs">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly_0_.GLuyEYhx7eutZkBRQfxP">
<gml:posList srsDimension="3">
690866.234 5336030.897 533.194
690858.167 5336034.058 535.13
690866.234 5336030.897 535.78
690866.234 5336030.897 533.194
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly.3P8Kl3MHTucUaho0D0t6">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly_0_.WOxAfTnQiDdJm2YmWbHp">
<gml:posList srsDimension="3">
690850.521 5336037.053 535.78
690866.234 5336030.897 535.78
690858.167 5336034.058 535.13
690850.521 5336037.053 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly.rlVvs5AMj8yxHOyGREZE">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_34fb9c88-449e-4deb-8c4f-e317defca449_poly_0_.H2OzhGuK7FKiYQc5UtkN">
<gml:posList srsDimension="3">
690858.167 5336034.058 535.78
690866.234 5336030.897 535.78
690850.521 5336037.053 535.78
690858.167 5336034.058 535.78
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_">
<gml:posList srsDimension="3">
690850.497 5336036.997 535.838
690850.521 5336037.053 535.78
690850.497 5336036.997 512.64
690850.497 5336036.997 535.838
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly.Z6nI0PLOSHMv05PSg9eo">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_.65v4cpVCtEJGMunac3Jx">
<gml:posList srsDimension="3">
690850.521 5336037.053 512.64
690850.497 5336036.997 512.64
690850.521 5336037.053 535.78
690850.521 5336037.053 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly.f9pXAVKIhuKGhCrRXmHx">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_.LLBHTPtbVjnfSqhFXUCq">
<gml:posList srsDimension="3">
690850.57 5336037.17 533.934
690850.521 5336037.053 512.64
690850.521 5336037.053 533.933
690850.57 5336037.17 533.934
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly.HInUn0aAMWwwvc0sG5Ai">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_943d1f74-b711-4689-b391-508b40ce31b5_poly_0_.0EZwXuv4T7nJICSm2KYF">
<gml:posList srsDimension="3">
690850.57 5336037.17 512.64
690850.521 5336037.053 512.64
690850.57 5336037.17 533.934
690850.57 5336037.17 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly_0_">
<gml:posList srsDimension="3">
690863.058 5336017.78 512.64
690863.058 5336017.78 535.828
690846.103 5336024.425 512.64
690863.058 5336017.78 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly.4YBE9aWScguCXyzglWLb">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly_0_.G42jNATKUXuvgL04PJNm">
<gml:posList srsDimension="3">
690845.615 5336024.616 512.64
690846.103 5336024.425 512.64
690863.058 5336017.78 535.828
690845.615 5336024.616 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly.BDPeLzYGpm14Xl5GBewg">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly_0_.NIL6I40BXVZIFk4KFJcz">
<gml:posList srsDimension="3">
690845.615 5336024.616 535.823
690845.615 5336024.616 512.64
690863.058 5336017.78 535.828
690845.615 5336024.616 535.823
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly.U7ewvzsdUQACFPsP7vJO">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_52f3af59-276c-4ef7-9d91-f5e31311be12_poly_0_.O2hn33qNJ8imbBvfvxud">
<gml:posList srsDimension="3">
690846.103 5336024.425 535.828
690845.615 5336024.616 535.823
690863.058 5336017.78 535.828
690846.103 5336024.425 535.828
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:WallSurface gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly_0_">
<gml:posList srsDimension="3">
690849.861 5336035.331 535.867
690850.491 5336036.981 535.855
690849.861 5336035.331 512.64
690849.861 5336035.331 535.867
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly.MbE7N7VE0SahHlEIqdz1">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly_0_.KhyRNoV5auDMDntaQofm">
<gml:posList srsDimension="3">
690850.491 5336036.981 512.64
690849.861 5336035.331 512.64
690850.491 5336036.981 535.855
690850.491 5336036.981 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly.qZklhJJNpsoh8n9Fz16h">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly_0_.OhCkB9wXQrDExYTM3kdK">
<gml:posList srsDimension="3">
690850.497 5336036.997 535.838
690850.491 5336036.981 512.64
690850.491 5336036.981 535.855
690850.497 5336036.997 535.838
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly.caAqthHZODBcyJQg5Que">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_227b6dda-2b3d-4439-9ed8-dcb04ae76165_poly_0_.4591owx1ZKaDM116sRZf">
<gml:posList srsDimension="3">
690850.497 5336036.997 512.64
690850.491 5336036.981 512.64
690850.497 5336036.997 535.838
690850.497 5336036.997 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:WallSurface>
</bldg:boundedBy>
<bldg:boundedBy>
<bldg:GroundSurface gml:id="DEBY_LOD2_4906966_5fdf6058-2dab-4889-ad60-c41beaf279a5">
<bldg:lod2MultiSurface>
<gml:MultiSurface>
<gml:surfaceMember>
<gml:Polygon gml:id="DEBY_LOD2_4906966_5fdf6058-2dab-4889-ad60-c41beaf279a5_poly">
<gml:exterior>
<gml:LinearRing gml:id="DEBY_LOD2_4906966_5fdf6058-2dab-4889-ad60-c41beaf279a5_poly_0_">
<gml:posList srsDimension="3">
690892.403 5336095.022 512.64
690887.832 5336083.444 512.64
690886.177 5336084.07 512.64
690880.444 5336069.674 512.64
690881.369 5336069.309 512.64
690878.674 5336062.468 512.64
690878.019 5336060.791 512.64
690874.232 5336051.176 512.64
690876.083 5336050.418 512.64
690871.183 5336038.037 512.64
690869.343 5336038.766 512.64
690866.202 5336030.816 512.64
690867.929 5336030.133 512.64
690867.876 5336029.997 512.64
690863.14 5336017.989 512.64
690863.058 5336017.78 512.64
690846.103 5336024.425 512.64
690845.615 5336024.616 512.64
690846.252 5336026.242 512.64
690846.556 5336026.134 512.64
690847.431 5336028.34 512.64
690847.147 5336028.449 512.64
690847.619 5336029.679 512.64
690847.924 5336029.57 512.64
690848.788 5336031.786 512.64
690848.504 5336031.895 512.64
690848.987 5336033.115 512.64
690849.281 5336033.016 512.64
690850.156 5336035.212 512.64
690849.861 5336035.331 512.64
690850.491 5336036.981 512.64
690850.497 5336036.997 512.64
690850.521 5336037.053 512.64
690850.57 5336037.17 512.64
690850.895 5336037.063 512.64
690850.941 5336037.155 512.64
690851.033 5336037.118 512.64
690852.06 5336040.041 512.64
690851.745 5336040.169 512.64
690852.109 5336041.104 512.64
690852.475 5336040.958 512.64
690860.007 5336060.048 512.64
690859.732 5336060.157 512.64
690865.645 5336075.492 512.64
690865.716 5336075.474 512.64
690867.362 5336079.682 512.64
690867.637 5336079.573 512.64
690873.852 5336095.209 512.64
690873.651 5336095.251 512.64
690876.364 5336102.173 512.64
690876.841 5336101.991 512.64
690876.877 5336102.093 512.64
690892.274 5336096.008 512.64
690892.239 5336095.876 512.64
690892.666 5336095.703 512.64
690892.403 5336095.022 512.64
</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:Polygon>
</gml:surfaceMember>
</gml:MultiSurface>
</bldg:lod2MultiSurface>
</bldg:GroundSurface>
</bldg:boundedBy>
<bldg:address>
<core:Address>
<core:xalAddress>
<xal:AddressDetails>
<xal:Country>
<xal:CountryName>Germany</xal:CountryName>
<xal:Locality Type="Town">
<xal:LocalityName>München</xal:LocalityName>
<xal:Thoroughfare Type="Street">
<xal:ThoroughfareName>Arcisstraße 21</xal:ThoroughfareName>
</xal:Thoroughfare>
</xal:Locality>
</xal:Country>
</xal:AddressDetails>
</core:xalAddress>
</core:Address>
</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>